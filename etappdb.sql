-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 29, 2019 at 11:25 AM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `etappdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `detailsid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `content` mediumtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `event_id`, `detailsid`, `userid`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'MMC 2019', 'This is an official mobile app for Marrakech Mining Convention 2019. It will provide you with the latest updates about our event, keeping you informed and connected. Use this app to access resources, network with other attendees, and share your experience at our event.', '2019-10-30 08:57:26', '2019-10-30 08:57:26'),
(2, 1, 6, 1, 'MMC 2019', 'Content', '2019-10-31 07:52:14', '2019-10-31 07:52:14'),
(3, 2, 12, 7, '11 Teams, 60 Matches, Per day 5 matches per 2 hours', 'Full AC stadium with Food and Services', '2019-11-04 13:40:56', '2019-11-04 13:40:56'),
(6, 3, 21, 6, 'Title Demo', 'Title Demo Title Demo Title Demo Title Demo Title Demo', '2019-11-05 05:57:52', '2019-11-05 05:57:52'),
(7, 3, 22, 6, 'demodemo', 'demo demo demo demo demo demo', '2019-11-05 06:24:56', '2019-11-05 06:24:56'),
(8, 4, 24, 1, 'valiant test media', 'jdbc.jkdbv.kzjbvkzjdfv.kjdfvb.kadjfv.kjfd', '2019-11-06 05:39:39', '2019-11-06 05:39:39'),
(9, 5, 29, 1, 'aBOUT UD', 'VVJKHJVYJHJ', '2019-11-06 05:47:05', '2019-11-06 05:47:05'),
(10, 6, 33, 1, 'Story Writer and Screenplay Writers', 'They Usually most utenf the basix stop writting and cut and paste it multiple times They Usually most utenf the basix stop writting and cut and paste it multiple times They Usually most utenf the basix stop writting and cut and paste it multiple times They Usually most utenf the basix stop writting and cut and paste it multiple times They Usually most utenf the basix stop writting and cut and paste it multiple times They Usually most utenf the basix stop writting and cut and paste it multiple times They Usually most utenf the basix stop writting and cut and paste it multiple times .', '2019-11-08 06:19:41', '2019-11-08 06:19:41'),
(11, 7, 46, 1, 'Event Description', 'Matches, Quiz, Football, Symonds, Coolers, Devops and too firm functionality.', '2019-11-08 09:19:06', '2019-11-08 09:19:06'),
(12, 8, 51, 1, 'This Event is the Main Worth', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', '2019-11-08 10:02:49', '2019-11-08 10:02:49'),
(13, 10, 67, 1, 'vbm', 'dadbcbducdbucbdsbucsd', '2019-11-16 17:31:42', '2019-11-16 17:32:21'),
(14, 10, 103, 1, 'Gipex 2019', 'The Department of Energy, Go-Invest and Valiant Business Media are most excited to host the 2nd edition of GIPEX, an exclusive convention in the capital city of Guyana. We are pleased to announce that GIPEX 2018 has exceeded expectations in the level of participation, interest and support for the summit in its inaugural year with global participation of 600 delegates and 200 international and national oil & gas companies, cementing GIPEX as South America’s largest petroleum summit up to date.\r\n\r\nGuyana is a bustling center for petroleum activity and as predicted, has become the Gateway to a Golden Future. Guyana is a major frontier market, showcasing large volume and diversity in the Oil and Gas industry and offering limitless opportunities for participation.\r\n\r\nGIPEX 2019 will provide many opportunities for companies across North and South America, Europe, Africa and Asia. Many well-established companies in the oil and gas sector have shown their interest and made their commitment to be a part of this event which will be held in Guyana- the most prominent Oil and Gas destination in South America and the Caribbean.', '2019-11-17 10:29:25', '2019-11-17 10:29:25'),
(15, 12, 104, 1, 'ff', 'fff', '2019-11-25 05:27:18', '2019-11-25 05:27:18'),
(16, 11, 115, 1, 'European Mining Convention ( EMC 2019 )', 'European Mining Convention EMC - 2019 aims at connecting the European Mining Industry with the rest of the world to reframe sustainability with innovation and experience. This event will demonstrate the latest technology and innovative solutions to various stakeholders in the industry to take this opportunity to expand connections, launch new products and forge new business partnerships.', '2019-11-29 10:15:14', '2019-11-29 10:29:04');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `type`, `email`, `password`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(1, 'etadmin', 0, 'etadmin@gmail.com', '$2y$10$oIS1wzirLdplId6xIAPH0.uRHOda5YdNCJov56uaibvMbwBXQeLku', '5LWVD1cxQga0BeXZbLNxCSkQEWIYZ4YZURIvOAP0GA5CHrZzCnqSNLz5107B', 1, '2019-10-03 02:11:40', '2019-11-29 10:20:35'),
(2, 'ankit', 1, 'ankit@mobulous.com', '$2y$10$yuzO91Q6sSJ56ULvQDG4.uure8gUoX57RQGZnGgRn1h5nK7BnhSoq', NULL, 1, '2019-10-24 07:40:58', '2019-10-24 07:40:58'),
(3, 'amam05', 1, 'ankit.mittal@gmail.com', '$2y$10$23Y5r2RKHN7ehswjjWgcKuVSLsEtyv7WikifssPuaqfPcrVj0BZ8.', NULL, 1, '2019-10-31 07:38:35', '2019-10-31 07:38:35'),
(5, 'gaurav', 1, 'grv@yopmail.com', '$2y$10$fP5941yqqBvNC.EXqepnBu6idohTl7J5tFVrSz1pv0SaGd6kyRQ8m', NULL, 1, '2019-11-04 07:38:05', '2019-11-13 10:25:59'),
(6, 'gaurav raj', 1, 'grvraj@yopmail.com', '$2y$10$sIsOH5LKm7UtipbuRzvyY.7h3AYHxshASZQSqaPHkKEfbTrUIdy4G', NULL, 0, '2019-11-04 12:22:13', '2019-11-13 06:08:30'),
(7, 'aakriti', 1, 'aak@yopmail.com', '$2y$10$LOBtc4Q3TnJDNl1/X3xM4.Bp1mlRN3xi23pRfnTooNIY8YXOvuFpm', NULL, 1, '2019-11-04 13:30:59', '2019-11-04 13:30:59');

-- --------------------------------------------------------

--
-- Table structure for table `bookmarks`
--

CREATE TABLE `bookmarks` (
  `id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `bookmark_id` int(11) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookmarks`
--

INSERT INTO `bookmarks` (`id`, `event_id`, `user_id`, `bookmark_id`, `type`, `created_at`, `updated_at`) VALUES
(1, 1, 25, 1, 'speakers', '2019-11-12 12:38:00', '2019-11-12 12:38:00'),
(2, 8, 25, 19, 'exhibitors', '2019-11-12 12:38:18', '2019-11-12 12:38:18'),
(4, 8, 25, 20, 'exhibitors', '2019-11-12 12:49:38', '2019-11-12 12:49:38'),
(5, 8, 25, 36, 'sponsers', '2019-11-12 12:49:47', '2019-11-12 12:49:47'),
(6, 8, 25, 9, 'speakers', '2019-11-12 12:50:00', '2019-11-12 12:50:00'),
(7, 1, 12, 1, 'exhibitors', '2019-11-12 18:37:48', '2019-11-12 18:37:48'),
(8, 8, 12, 19, 'exhibitors', '2019-11-13 09:37:26', '2019-11-13 09:37:26'),
(9, 8, 12, 7, 'sponsers', '2019-11-13 09:37:31', '2019-11-13 09:37:31'),
(10, 8, 12, 8, 'sponsers', '2019-11-13 09:37:31', '2019-11-13 09:37:31'),
(11, 8, 12, 9, 'speakers', '2019-11-13 09:37:35', '2019-11-13 09:37:35'),
(12, 8, 12, 38, 'People', '2019-11-13 09:37:39', '2019-11-13 09:37:39'),
(13, 8, 12, 39, 'People', '2019-11-13 09:37:40', '2019-11-13 09:37:40'),
(14, 8, 12, 40, 'People', '2019-11-13 09:37:42', '2019-11-13 09:37:42'),
(15, 8, 12, 36, 'People', '2019-11-13 09:37:45', '2019-11-13 09:37:45'),
(16, 10, 20, 19, 'People', '2019-11-17 22:21:51', '2019-11-17 22:21:51'),
(17, 10, 61, 123, 'exhibitors', '2019-11-20 11:34:43', '2019-11-20 11:34:43'),
(18, 10, 96, 96, 'People', '2019-11-20 11:51:59', '2019-11-20 11:51:59'),
(19, 10, 96, 77, 'People', '2019-11-20 11:57:57', '2019-11-20 11:57:57'),
(20, 10, 96, 78, 'People', '2019-11-20 11:57:59', '2019-11-20 11:57:59'),
(21, 10, 51, 95, 'exhibitors', '2019-11-20 13:29:53', '2019-11-20 13:29:53'),
(22, 10, 51, 98, 'exhibitors', '2019-11-20 13:31:27', '2019-11-20 13:31:27'),
(23, 10, 51, 99, 'exhibitors', '2019-11-20 13:31:38', '2019-11-20 13:31:38'),
(24, 10, 51, 102, 'exhibitors', '2019-11-20 13:31:42', '2019-11-20 13:31:42'),
(25, 10, 51, 109, 'exhibitors', '2019-11-20 13:31:44', '2019-11-20 13:31:44'),
(26, 10, 51, 71, 'exhibitors', '2019-11-20 13:33:13', '2019-11-20 13:33:13'),
(27, 10, 64, 89, 'exhibitors', '2019-11-20 13:47:08', '2019-11-20 13:47:08'),
(28, 10, 75, 44, 'speakers', '2019-11-20 18:13:00', '2019-11-20 18:13:00'),
(29, 12, 21, 12, 'People', '2019-11-25 09:47:35', '2019-11-25 09:47:35');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `reciver_id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  `read_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT ' 0 for unread 1 for read',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `room_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `sender_id`, `reciver_id`, `message`, `read_status`, `created_at`, `room_id`) VALUES
(1, 1, 3, 'Hi', '1', '2019-11-08 10:50:53', 'room13'),
(2, 3, 1, 'Hello', '1', '2019-11-08 10:51:27', 'room13'),
(3, 3, 1, 'Hi', '1', '2019-11-08 11:06:37', 'room13'),
(4, 3, 1, 'Hi', '1', '2019-11-08 11:06:55', 'room13'),
(5, 3, 1, 'HHHHHHHHHHHHHHHH', '1', '2019-11-08 11:07:08', 'room13'),
(6, 9, 12, 'Hii', '1', '2019-11-09 10:14:01', 'room912'),
(7, 9, 25, 'Ggggg', '1', '2019-11-09 10:17:57', 'room925'),
(8, 9, 12, 'Ccfgg', '1', '2019-11-09 10:46:41', 'room912'),
(9, 9, 12, 'Hello', '1', '2019-11-11 05:05:22', 'room912'),
(10, 9, 12, 'Hii', '1', '2019-11-11 05:38:10', 'room912'),
(11, 9, 12, 'Hii', '1', '2019-11-11 05:43:32', 'room912'),
(12, 9, 12, 'Hii', '1', '2019-11-11 05:50:16', 'room912'),
(13, 9, 12, 'New message', '1', '2019-11-11 06:16:32', 'room912'),
(14, 9, 12, 'Hii', '1', '2019-11-11 06:21:12', 'room912'),
(15, 9, 12, 'Hello', '1', '2019-11-11 06:23:00', 'room912'),
(16, 9, 28, 'Hii', '0', '2019-11-11 06:35:21', 'room928'),
(17, 9, 12, 'Hiii', '1', '2019-11-11 06:45:08', 'room912'),
(18, 9, 12, 'Noo', '1', '2019-11-11 06:48:27', 'room912'),
(19, 9, 12, 'Jjjjj', '1', '2019-11-11 06:51:04', 'room912'),
(20, 9, 12, 'Gg', '1', '2019-11-11 06:51:07', 'room912'),
(21, 9, 12, 'Hh', '1', '2019-11-11 06:51:09', 'room912'),
(22, 12, 9, 'Hii', '1', '2019-11-11 07:03:18', 'room912'),
(23, 12, 9, 'Same', '1', '2019-11-11 07:03:26', 'room912'),
(24, 9, 12, 'Noo', '1', '2019-11-11 07:03:32', 'room912'),
(25, 9, 12, 'Hi gaurav', '1', '2019-11-11 07:12:18', 'room912'),
(26, 12, 9, 'Hello tarun', '1', '2019-11-11 07:12:26', 'room912'),
(27, 21, 12, 'hii', '1', '2019-11-11 07:12:35', 'room2112'),
(28, 21, 12, 'hii', '1', '2019-11-11 07:12:46', 'room2112'),
(29, 21, 12, 'hii', '1', '2019-11-11 07:13:09', 'room2112'),
(30, 21, 12, 'ggg', '1', '2019-11-11 07:13:22', 'room2112'),
(31, 21, 12, 'gggd', '1', '2019-11-11 07:14:03', 'room2112'),
(32, 21, 12, 'gii', '1', '2019-11-11 07:14:39', 'room2112'),
(33, 21, 12, 'hii', '1', '2019-11-11 07:16:37', 'room2112'),
(34, 21, 12, 'tedy', '1', '2019-11-11 07:16:44', 'room2112'),
(35, 21, 12, 'ravi', '1', '2019-11-11 07:17:30', 'room2112'),
(36, 21, 12, 'testoo', '1', '2019-11-11 07:19:58', 'room2112'),
(37, 21, 36, 'hii', '1', '2019-11-11 07:25:37', 'room2136'),
(38, 21, 36, 'test', '1', '2019-11-11 07:26:01', 'room2136'),
(39, 21, 12, 'hii', '1', '2019-11-11 07:33:33', 'room2112'),
(40, 21, 12, 'hii', '1', '2019-11-11 07:35:12', 'room2112'),
(41, 21, 12, 'hii', '1', '2019-11-11 07:43:17', 'room2112'),
(42, 21, 12, 'testgg', '1', '2019-11-11 07:55:50', 'room2112'),
(43, 21, 12, 'hiì', '1', '2019-11-11 07:59:01', 'room2112'),
(44, 21, 12, 'hiiff', '1', '2019-11-11 08:00:21', 'room2112'),
(45, 21, 12, 'kkk', '1', '2019-11-11 08:00:29', 'room2112'),
(46, 21, 12, 'hello', '1', '2019-11-11 08:00:34', 'room2112'),
(47, 21, 12, 'ankit', '1', '2019-11-11 08:00:41', 'room2112'),
(48, 21, 12, 'prem', '1', '2019-11-11 08:00:45', 'room2112'),
(49, 21, 12, 'Jay ho', '1', '2019-11-11 08:00:49', 'room2112'),
(50, 21, 12, 'test1', '1', '2019-11-11 08:00:54', 'room2112'),
(51, 21, 12, 'test', '1', '2019-11-11 09:19:31', 'room2112'),
(52, 21, 12, 'Jay ho', '1', '2019-11-11 09:20:08', 'room2112'),
(53, 21, 12, 'Test noti', '1', '2019-11-11 09:20:17', 'room2112'),
(54, 21, 12, 'right', '1', '2019-11-11 09:20:25', 'room2112'),
(55, 21, 12, 'yes it no', '1', '2019-11-11 09:21:44', 'room2112'),
(56, 21, 12, 'test', '1', '2019-11-11 09:21:48', 'room2112'),
(57, 21, 12, 'testy', '1', '2019-11-11 09:21:56', 'room2112'),
(58, 12, 21, 'jay', '1', '2019-11-11 09:22:13', 'room2112'),
(59, 9, 12, 'Hiiii', '1', '2019-11-11 09:22:25', 'room912'),
(60, 12, 9, 'Noo', '1', '2019-11-11 09:23:10', 'room912'),
(61, 12, 21, 'gjrfkxjcmcscwgldjvdfhvjdmvgchsmxncgdjdnchcnxmxhd', '1', '2019-11-11 09:23:31', 'room2112'),
(62, 12, 21, 'hii', '1', '2019-11-11 09:23:39', 'room2112'),
(63, 21, 12, 'hii', '1', '2019-11-11 09:25:01', 'room2112'),
(64, 12, 9, 'Hii', '1', '2019-11-11 09:26:56', 'room912'),
(65, 12, 9, 'Kkk', '1', '2019-11-11 09:27:14', 'room912'),
(66, 12, 12, 'hi', '1', '2019-11-11 09:28:15', 'room1212'),
(67, 21, 12, 'connet', '1', '2019-11-11 09:28:53', 'room2112'),
(68, 12, 21, 'hii', '1', '2019-11-11 09:28:59', 'room2112'),
(69, 21, 12, 'hello', '1', '2019-11-11 09:29:05', 'room2112'),
(70, 12, 21, 'abhi', '1', '2019-11-11 09:29:05', 'room2112'),
(71, 12, 9, 'Klllk', '1', '2019-11-11 09:29:05', 'room912'),
(72, 12, 9, 'Klllk', '1', '2019-11-11 09:29:09', 'room912'),
(73, 12, 21, 'kaise ho', '1', '2019-11-11 09:29:11', 'room2112'),
(74, 12, 21, '?', '1', '2019-11-11 09:29:15', 'room2112'),
(75, 21, 12, 'f9', '1', '2019-11-11 09:29:16', 'room2112'),
(76, 12, 9, 'Kkl', '1', '2019-11-11 09:29:19', 'room912'),
(77, 12, 21, 'gaurav', '1', '2019-11-11 09:29:26', 'room2112'),
(78, 12, 9, 'Jjh', '1', '2019-11-11 09:30:22', 'room912'),
(79, 12, 9, 'Hhh', '1', '2019-11-11 09:32:25', 'room912'),
(80, 12, 9, 'Kkkk', '1', '2019-11-11 09:35:04', 'room912'),
(81, 12, 9, 'Lkjiiu', '1', '2019-11-11 09:35:46', 'room912'),
(82, 12, 9, 'Jjhgg', '1', '2019-11-11 09:38:42', 'room912'),
(83, 12, 9, 'Kkjhg', '1', '2019-11-11 09:40:25', 'room912'),
(84, 21, 12, 'hii', '1', '2019-11-11 09:41:41', 'room2112'),
(85, 21, 12, 'test', '1', '2019-11-11 09:41:49', 'room2112'),
(86, 9, 21, 'Hello', '1', '2019-11-11 09:43:17', 'room219'),
(87, 12, 9, 'Singh', '1', '2019-11-11 09:48:30', 'room912'),
(88, 12, 9, 'Gcfcff', '1', '2019-11-11 09:49:13', 'room912'),
(89, 12, 9, 'Llllllll', '1', '2019-11-11 09:49:36', 'room912'),
(90, 12, 9, 'Hjhjhj', '1', '2019-11-11 09:49:47', 'room912'),
(91, 12, 9, 'Jjjjj', '1', '2019-11-11 09:51:45', 'room912'),
(92, 12, 9, 'Hii', '1', '2019-11-11 09:55:45', 'room912'),
(93, 12, 9, 'Hiinoo', '1', '2019-11-11 09:56:32', 'room912'),
(94, 12, 9, 'Kklljjfjf', '1', '2019-11-11 10:00:27', 'room912'),
(95, 12, 9, 'Grfgrgrgr', '1', '2019-11-11 10:01:11', 'room912'),
(96, 12, 9, 'Hooog', '1', '2019-11-11 10:04:19', 'room912'),
(97, 12, 9, 'Grgrgr', '1', '2019-11-11 10:06:12', 'room912'),
(98, 12, 9, 'Frfrfrf', '1', '2019-11-11 10:07:28', 'room912'),
(99, 21, 21, 'hii', '1', '2019-11-11 10:08:31', 'room2121'),
(100, 12, 12, 'hii', '1', '2019-11-11 10:08:41', 'room1212'),
(101, 21, 12, 'hii', '1', '2019-11-11 10:09:15', 'room2112'),
(102, 12, 21, 'hii', '1', '2019-11-11 10:09:19', 'room2112'),
(103, 12, 21, 'hii', '1', '2019-11-11 10:09:30', 'room2112'),
(104, 12, 9, 'Kiiuyyt', '1', '2019-11-11 10:16:39', 'room912'),
(105, 12, 9, 'Hello', '1', '2019-11-11 10:21:44', 'room912'),
(106, 12, 9, 'Hii', '1', '2019-11-11 10:26:56', 'room912'),
(107, 9, 12, 'Noo', '1', '2019-11-11 10:27:05', 'room912'),
(108, 9, 12, 'Hsjsj', '1', '2019-11-11 10:27:06', 'room912'),
(109, 9, 12, 'Jsjjs', '1', '2019-11-11 10:27:08', 'room912'),
(110, 9, 12, 'Jsjs', '1', '2019-11-11 10:27:09', 'room912'),
(111, 12, 9, 'Hhkhk', '1', '2019-11-11 10:27:12', 'room912'),
(112, 12, 9, 'Jkjk', '1', '2019-11-11 10:27:15', 'room912'),
(113, 12, 9, 'Kk', '1', '2019-11-11 10:27:18', 'room912'),
(114, 9, 12, 'Yyys', '1', '2019-11-11 10:27:19', 'room912'),
(115, 9, 12, 'Hshhsjsjjs', '1', '2019-11-11 10:27:21', 'room912'),
(116, 9, 12, 'Hshhs', '1', '2019-11-11 10:27:27', 'room912'),
(117, 9, 12, 'Jsjjs', '1', '2019-11-11 10:27:29', 'room912'),
(118, 9, 12, 'Sjjs', '1', '2019-11-11 10:27:30', 'room912'),
(119, 12, 9, 'Jjkjh', '1', '2019-11-11 10:37:54', 'room912'),
(120, 12, 9, 'Jhjhjhj', '1', '2019-11-11 10:39:26', 'room912'),
(121, 12, 9, 'Jjjh', '1', '2019-11-11 10:41:40', 'room912'),
(122, 12, 9, 'Jjhjhjhj', '1', '2019-11-11 10:43:41', 'room912'),
(123, 21, 12, 'hii', '1', '2019-11-11 10:45:35', 'room2112'),
(124, 12, 21, 'hell', '1', '2019-11-11 10:46:05', 'room2112'),
(125, 12, 21, 'hiiii', '1', '2019-11-11 10:48:19', 'room2112'),
(126, 12, 21, 'hello', '1', '2019-11-11 10:48:26', 'room2112'),
(127, 12, 9, 'Jjiijhuhuhuhuhu', '1', '2019-11-11 10:51:20', 'room912'),
(128, 12, 9, 'Hjhjhjhj', '1', '2019-11-11 10:52:42', 'room912'),
(129, 12, 9, 'Hfjrhfjrhfjrhfjr', '1', '2019-11-11 10:54:50', 'room912'),
(130, 12, 9, 'Hfjrhfjrhfjrhfjr', '1', '2019-11-11 10:54:51', 'room912'),
(131, 21, 12, 'hello', '1', '2019-11-11 11:07:07', 'room2112'),
(132, 21, 12, 'hee', '1', '2019-11-11 11:07:18', 'room2112'),
(133, 21, 12, 'jay', '1', '2019-11-11 11:07:21', 'room2112'),
(134, 12, 21, 'hhii', '1', '2019-11-11 11:09:03', 'room2112'),
(135, 12, 21, 'huuu', '1', '2019-11-11 11:11:12', 'room2112'),
(136, 12, 21, 'uuhh', '1', '2019-11-11 11:11:19', 'room2112'),
(137, 12, 21, 'gggg', '1', '2019-11-11 11:15:17', 'room2112'),
(138, 12, 21, 'ggg', '1', '2019-11-11 11:15:23', 'room2112'),
(139, 12, 21, 'hggg', '1', '2019-11-11 11:15:29', 'room2112'),
(140, 12, 21, 'hiii', '1', '2019-11-11 11:16:56', 'room2112'),
(141, 12, 21, 'ggg', '1', '2019-11-11 11:17:14', 'room2112'),
(142, 21, 12, 'jgkgkgjffjjfjf', '1', '2019-11-11 11:20:07', 'room2112'),
(143, 21, 12, 'hhhh', '1', '2019-11-11 11:23:30', 'room2112'),
(144, 12, 21, 'gggg', '1', '2019-11-11 11:27:01', 'room2112'),
(145, 12, 21, 'hii', '1', '2019-11-11 11:33:23', 'room2112'),
(146, 21, 12, 'hhh', '1', '2019-11-11 11:33:56', 'room2112'),
(147, 12, 21, 'gfhjj', '1', '2019-11-11 11:34:04', 'room2112'),
(148, 12, 21, 'fghj', '1', '2019-11-11 11:34:15', 'room2112'),
(149, 21, 12, 'hi gaurav', '1', '2019-11-11 11:47:20', 'room2112'),
(150, 12, 21, 'hh', '1', '2019-11-11 11:47:54', 'room2112'),
(151, 12, 21, 'hh', '1', '2019-11-11 11:47:59', 'room2112'),
(152, 12, 21, 'hhhh', '1', '2019-11-11 11:48:07', 'room2112'),
(153, 12, 21, 'hhhhi', '1', '2019-11-11 11:48:17', 'room2112'),
(154, 12, 21, 'hhahs', '1', '2019-11-11 11:48:31', 'room2112'),
(155, 12, 21, 'testnig', '1', '2019-11-11 11:48:50', 'room2112'),
(156, 12, 21, 'on e', '1', '2019-11-11 11:48:58', 'room2112'),
(157, 12, 21, 'teo', '1', '2019-11-11 11:49:02', 'room2112'),
(158, 12, 21, 'kkkk', '1', '2019-11-11 11:49:05', 'room2112'),
(159, 12, 21, 'hshhsh', '1', '2019-11-11 11:49:59', 'room2112'),
(160, 41, 36, 'hi prashant singh', '1', '2019-11-11 11:51:50', 'room4136'),
(161, 12, 21, 'hhi', '1', '2019-11-11 11:57:07', 'room2112'),
(162, 12, 21, 'jay', '1', '2019-11-11 11:57:09', 'room2112'),
(163, 12, 21, 'prrm', '1', '2019-11-11 11:57:13', 'room2112'),
(164, 36, 21, 'hello there', '1', '2019-11-11 11:57:21', 'room2136'),
(165, 21, 12, 'hhh', '1', '2019-11-11 11:57:42', 'room2112'),
(166, 36, 41, 'hello there', '1', '2019-11-11 11:57:45', 'room4136'),
(167, 21, 12, 'jay', '1', '2019-11-11 11:57:46', 'room2112'),
(168, 36, 41, 'abhishek bhai', '1', '2019-11-11 11:57:49', 'room4136'),
(169, 36, 41, 'kaise ho???', '1', '2019-11-11 11:57:54', 'room4136'),
(170, 36, 41, 'Type a message...', '1', '2019-11-11 11:58:01', 'room4136'),
(171, 12, 21, 'hhi', '1', '2019-11-11 11:59:11', 'room2112'),
(172, 12, 21, 'hhhhi', '1', '2019-11-11 11:59:20', 'room2112'),
(173, 12, 21, 'yhh', '1', '2019-11-11 11:59:38', 'room2112'),
(174, 12, 21, 'yggg', '1', '2019-11-11 12:00:40', 'room2112'),
(175, 12, 21, 'hhhh', '1', '2019-11-11 12:02:56', 'room2112'),
(176, 12, 21, 'hdh', '1', '2019-11-11 12:02:59', 'room2112'),
(177, 12, 9, 'Hii', '1', '2019-11-11 12:06:07', 'room912'),
(178, 36, 9, 'hi', '1', '2019-11-11 12:06:41', 'room369'),
(179, 12, 21, 'gegsg', '1', '2019-11-11 12:09:13', 'room2112'),
(180, 12, 21, 'hdjdj', '1', '2019-11-11 12:09:16', 'room2112'),
(181, 12, 21, 'hhshd', '1', '2019-11-11 12:09:30', 'room2112'),
(182, 12, 21, 'dhdhdh', '1', '2019-11-11 12:09:50', 'room2112'),
(183, 12, 21, 'shjej', '1', '2019-11-11 12:10:03', 'room2112'),
(184, 12, 21, 'hiii', '1', '2019-11-11 12:11:30', 'room2112'),
(185, 12, 21, 'ghhhu', '1', '2019-11-11 12:11:44', 'room2112'),
(186, 12, 21, 'hiii', '1', '2019-11-11 12:11:54', 'room2112'),
(187, 12, 21, 'ghhjjj', '1', '2019-11-11 12:11:59', 'room2112'),
(188, 9, 12, 'Noo', '1', '2019-11-11 12:36:52', 'room912'),
(189, 9, 12, 'Jii', '1', '2019-11-11 12:37:11', 'room912'),
(190, 9, 12, 'Hii', '1', '2019-11-11 12:44:33', 'room912'),
(191, 12, 9, 'Hshhhs', '1', '2019-11-11 12:44:49', 'room912'),
(192, 12, 9, 'Bshhs', '1', '2019-11-11 12:44:50', 'room912'),
(193, 36, 12, 'hello... Prashant here', '1', '2019-11-11 12:45:26', 'room3612'),
(194, 12, 36, 'Shhshhs', '1', '2019-11-11 12:45:35', 'room3612'),
(195, 12, 36, 'Hshss', '1', '2019-11-11 12:45:37', 'room3612'),
(196, 12, 36, 'Shhss', '1', '2019-11-11 12:45:38', 'room3612'),
(197, 12, 36, 'Hshshs', '1', '2019-11-11 12:45:43', 'room3612'),
(198, 9, 12, 'Bjjgjg', '1', '2019-11-11 12:47:16', 'room912'),
(199, 12, 9, 'Hehhe', '1', '2019-11-11 12:47:32', 'room912'),
(200, 9, 12, 'Hkhk', '1', '2019-11-11 12:47:37', 'room912'),
(201, 36, 12, 'jiop', '1', '2019-11-11 12:47:53', 'room3612'),
(202, 36, 12, 'hello', '1', '2019-11-11 12:48:15', 'room3612'),
(203, 36, 12, 'dquestion to be saked that needs to be desine here is the paragraph', '1', '2019-11-11 12:48:38', 'room3612'),
(204, 12, 21, 'hello', '1', '2019-11-11 12:52:21', 'room2112'),
(205, 21, 12, 'hii', '1', '2019-11-11 12:54:40', 'room2112'),
(206, 21, 12, 'jjtt', '1', '2019-11-11 12:54:45', 'room2112'),
(207, 36, 12, 'Type a message...', '1', '2019-11-11 12:55:51', 'room3612'),
(208, 36, 12, 'gaurav kumar', '1', '2019-11-11 12:56:00', 'room3612'),
(209, 12, 36, 'H', '1', '2019-11-11 12:56:30', 'room3612'),
(210, 36, 12, 'shjsjsbdbjd', '1', '2019-11-11 12:58:36', 'room3612'),
(211, 9, 12, 'Type a message...', '1', '2019-11-11 13:06:42', 'room912'),
(212, 12, 9, 'Hsjsjsjs', '1', '2019-11-11 13:09:28', 'room912'),
(213, 21, 12, 'hiii', '1', '2019-11-11 13:38:30', 'room2112'),
(214, 9, 12, 'Fkewflkelwkf ehfenwfr lefnlewjf lew fhehfek I fjefj Ed friend Effie fkefheknfeigg', '1', '2019-11-11 13:46:23', 'room912'),
(215, 21, 9, 'nikita is nice and I have to go to the store and I have to go to the store and I have to go to the store and I have to go to', '1', '2019-11-11 13:56:12', 'room219'),
(216, 21, 9, 'nikita I have to go to the store for a good day at work and I have to go to the store and I have to go', '1', '2019-11-11 13:57:36', 'room219'),
(217, 21, 9, 'fgbbbkfjdjdndndjdjdj', '1', '2019-11-11 14:04:50', 'room219'),
(218, 12, 21, 'hii', '1', '2019-11-11 14:09:13', 'room2112'),
(219, 12, 21, 'hhiaia', '1', '2019-11-11 14:09:30', 'room2112'),
(220, 12, 21, 'dbdbbdbd', '1', '2019-11-11 14:09:33', 'room2112'),
(221, 12, 21, 'hhhgh', '1', '2019-11-11 14:09:46', 'room2112'),
(222, 12, 21, 'gjkkgh', '1', '2019-11-11 14:09:48', 'room2112'),
(223, 12, 21, 'disjske', '1', '2019-11-11 14:09:51', 'room2112'),
(224, 12, 21, 'yyyyy', '1', '2019-11-11 14:10:27', 'room2112'),
(225, 12, 21, 'test q', '1', '2019-11-11 14:11:02', 'room2112'),
(226, 21, 21, 'hh', '1', '2019-11-11 14:17:43', 'room2121'),
(227, 12, 21, 'hhh', '1', '2019-11-11 14:35:09', 'room2112'),
(228, 12, 21, 'hello', '1', '2019-11-11 14:44:55', 'room2112'),
(229, 12, 21, 'hello hello', '1', '2019-11-11 14:45:20', 'room2112'),
(230, 12, 21, 'hello', '1', '2019-11-11 14:45:57', 'room2112'),
(231, 9, 12, 'Hi there I’m going back in a bit I to do a the same thing I don’t am I can do I just do a the time I can see it then I have can want you can I do it them and get it them and you can get them do something else like for', '1', '2019-11-11 14:46:34', 'room912'),
(232, 9, 12, 'Hi am I can see do you have a know where you can get them and for me and I have to get go out to and get go to get Right I it is was Software was I you have know can I we can see get you and then you I don’t am have to a lot of good things and I you can know I know want a to do it and you have know that I’m you know that you’re I just got want to you and I then I don’t have want to go get back to in the time I you know I you have know that you’re I am don’t good have know want to you and I have you love you and I have want can know that you’re I you’re just love don’t know want me to do it and then you have want a good day and then go back get your the kids time and you can see do it I them and you I have can get go and get do go to get send the a day and then I go', '1', '2019-11-11 14:48:45', 'room912'),
(233, 12, 21, 'hello hii', '1', '2019-11-11 14:49:58', 'room2112'),
(234, 9, 12, '\n\n\nDjjd', '1', '2019-11-11 14:50:47', 'room912'),
(235, 21, 12, 'jay', '1', '2019-11-11 14:59:22', 'room2112'),
(236, 12, 21, 'hello', '1', '2019-11-11 14:59:25', 'room2112'),
(237, 21, 12, 'ho', '1', '2019-11-11 14:59:26', 'room2112'),
(238, 21, 12, 'hello', '1', '2019-11-11 14:59:30', 'room2112'),
(239, 12, 21, 'hiii', '1', '2019-11-11 15:00:00', 'room2112'),
(240, 12, 21, 'helllo', '1', '2019-11-11 15:00:45', 'room2112'),
(241, 12, 21, 'hello', '1', '2019-11-11 15:01:28', 'room2112'),
(242, 12, 21, 'hello', '1', '2019-11-11 15:01:46', 'room2112'),
(243, 12, 21, 'hell', '1', '2019-11-11 15:03:49', 'room2112'),
(244, 12, 21, 'hell', '1', '2019-11-11 15:03:56', 'room2112'),
(245, 12, 21, 'hiii', '1', '2019-11-11 15:04:00', 'room2112'),
(246, 21, 12, 'hhh', '1', '2019-11-11 15:05:39', 'room2112'),
(247, 12, 21, 'hello', '1', '2019-11-11 15:05:54', 'room2112'),
(248, 9, 37, 'Hey', '0', '2019-11-11 15:34:23', 'room937'),
(249, 12, 21, 'hello........', '1', '2019-11-11 15:39:57', 'room2112'),
(250, 41, 9, 'hi tarun', '1', '2019-11-11 15:41:10', 'room419'),
(251, 41, 37, 'hi nikhil', '0', '2019-11-11 15:42:26', 'room4137'),
(252, 41, 37, 'are you all right', '0', '2019-11-11 15:44:16', 'room4137'),
(253, 37, 41, 'no', '0', '2019-11-11 15:44:20', 'room4137'),
(254, 41, 37, 'are you alright', '0', '2019-11-11 15:44:27', 'room4137'),
(255, 41, 37, 'yffufuigih', '0', '2019-11-11 15:44:53', 'room4137'),
(256, 41, 37, 'vhhj vjv\n\n\n\n\n\n\n\n\nhcvohojoj', '0', '2019-11-11 15:45:00', 'room4137'),
(257, 37, 41, 'hi', '0', '2019-11-11 15:46:04', 'room4137'),
(258, 37, 21, 'hi', '1', '2019-11-11 15:47:25', 'room3721'),
(259, 37, 21, 'hello', '1', '2019-11-11 15:49:31', 'room3721'),
(260, 37, 21, 'oye', '1', '2019-11-11 15:51:47', 'room3721'),
(261, 37, 21, 'thanks for the heads and the same thing but to get to the Chat I have to get a room at the office and will send them to you when you get back from your family are well and that your mom I have to go get a new phone and it to the gym today and I will be at work tomorrow morning so I can make the payment and will not be able to attend this interview I have to get my flat is a good idea but to me it to me when we were there last year we were going out of town on Friday and the people I work with your organization and the same to you so late I just got out the shower is not a big boy bc he is a very very cc or do you have another idea for me to you in person to see if I am going to have the opportunity I', '1', '2019-11-11 15:52:05', 'room3721'),
(262, 37, 21, 'hii', '1', '2019-11-11 15:52:21', 'room3721'),
(263, 37, 21, 'tets', '1', '2019-11-11 15:52:31', 'room3721'),
(264, 37, 21, 'oyy', '1', '2019-11-11 15:54:08', 'room3721'),
(265, 37, 21, 'hi', '1', '2019-11-11 15:54:28', 'room3721'),
(266, 37, 21, 'hi', '1', '2019-11-11 15:55:08', 'room3721'),
(267, 37, 21, 'ggh', '1', '2019-11-11 15:55:48', 'room3721'),
(268, 37, 21, 'ggg', '1', '2019-11-11 15:55:55', 'room3721'),
(269, 9, 41, 'Bsid', '1', '2019-11-11 15:57:54', 'room419'),
(270, 12, 21, 'cjcgktigncjf', '1', '2019-11-11 15:58:27', 'room2112'),
(271, 9, 41, 'K', '1', '2019-11-11 15:58:30', 'room419'),
(272, 9, 41, 'Hd', '1', '2019-11-11 15:59:17', 'room419'),
(273, 37, 21, 'twsjsj', '1', '2019-11-11 15:59:27', 'room3721'),
(274, 9, 41, 'Hisebei', '1', '2019-11-11 16:00:18', 'room419'),
(275, 41, 9, 'ho', '1', '2019-11-11 16:00:28', 'room419'),
(276, 41, 9, 'yuiihn', '1', '2019-11-11 16:00:36', 'room419'),
(277, 37, 21, 'twgssh', '1', '2019-11-11 16:01:16', 'room3721'),
(278, 37, 21, 'tt', '1', '2019-11-11 16:01:26', 'room3721'),
(279, 12, 21, 'hiii', '1', '2019-11-11 16:02:07', 'room2112'),
(280, 37, 21, 'yyshheh', '1', '2019-11-11 16:03:01', 'room3721'),
(281, 21, 12, 'hhi', '1', '2019-11-11 16:04:10', 'room2112'),
(282, 21, 12, 'ushhs', '1', '2019-11-11 16:04:17', 'room2112'),
(283, 21, 12, 'gdhshs', '1', '2019-11-11 16:04:21', 'room2112'),
(284, 12, 21, 'I am so i msg u y to be there for a good time for us ufhf to get it to you by blbhkob KBB value is test mail for your time and I will be in a meeting with you and your mom', '1', '2019-11-11 16:04:39', 'room2112'),
(285, 12, 21, 'I am so i have to go', '1', '2019-11-11 16:04:52', 'room2112'),
(286, 12, 21, 'ehjd email to the group', '1', '2019-11-11 16:04:58', 'room2112'),
(287, 12, 21, 'ehjrrhrh', '1', '2019-11-11 16:05:05', 'room2112'),
(288, 12, 21, 'rhjhrgrh', '1', '2019-11-11 16:05:14', 'room2112'),
(289, 12, 21, 'yio', '1', '2019-11-11 16:05:53', 'room2112'),
(290, 36, 9, 'hi', '1', '2019-11-11 16:07:13', 'room369'),
(291, 36, 9, 'hello', '1', '2019-11-11 16:07:19', 'room369'),
(292, 12, 21, 'hdhdh', '1', '2019-11-11 16:07:25', 'room2112'),
(293, 12, 21, 'hehdhhe', '1', '2019-11-11 16:07:37', 'room2112'),
(294, 36, 9, 'hi', '1', '2019-11-11 16:07:46', 'room369'),
(295, 36, 9, 'hi', '1', '2019-11-11 16:07:55', 'room369'),
(296, 36, 9, 'hiii', '1', '2019-11-11 16:07:59', 'room369'),
(297, 9, 36, 'Ok', '1', '2019-11-11 16:08:28', 'room369'),
(298, 12, 21, 'hdjdj', '1', '2019-11-11 16:08:59', 'room2112'),
(299, 12, 21, 'hdhdh', '1', '2019-11-11 16:09:13', 'room2112'),
(300, 12, 21, 'kya', '1', '2019-11-11 16:11:03', 'room2112'),
(301, 36, 9, 'hi', '1', '2019-11-11 16:11:03', 'room369'),
(302, 12, 21, 'hhhh', '1', '2019-11-11 16:14:05', 'room2112'),
(303, 12, 21, 'hh', '1', '2019-11-11 16:14:54', 'room2112'),
(304, 36, 9, 'hello', '1', '2019-11-11 16:17:00', 'room369'),
(305, 36, 9, 'hi', '1', '2019-11-11 16:17:03', 'room369'),
(306, 36, 9, 'hihhoi', '1', '2019-11-11 16:17:06', 'room369'),
(307, 36, 9, 'gvr', '1', '2019-11-11 16:17:10', 'room369'),
(308, 12, 21, 'hhshs', '1', '2019-11-11 16:19:20', 'room2112'),
(309, 12, 21, 'hdhhdhd', '1', '2019-11-11 16:21:14', 'room2112'),
(310, 21, 12, 'ffddd', '1', '2019-11-11 16:21:23', 'room2112'),
(311, 21, 36, 'hey', '1', '2019-11-11 16:28:08', 'room2136'),
(312, 36, 21, 'het', '1', '2019-11-11 16:28:37', 'room2136'),
(313, 21, 36, 'yu', '1', '2019-11-11 16:28:48', 'room2136'),
(314, 36, 21, 'isheusbs', '1', '2019-11-11 16:28:53', 'room2136'),
(315, 36, 21, 'hi nadeem', '1', '2019-11-11 16:30:31', 'room2136'),
(316, 21, 36, 'yghu', '1', '2019-11-11 16:30:39', 'room2136'),
(317, 21, 36, 'ugug9j', '1', '2019-11-11 16:30:45', 'room2136'),
(318, 21, 36, 'hufugugiguv', '1', '2019-11-11 16:30:50', 'room2136'),
(319, 21, 36, 'ug8hihihij', '1', '2019-11-11 16:30:53', 'room2136'),
(320, 21, 36, 'ucchgchc', '1', '2019-11-11 16:30:58', 'room2136'),
(321, 21, 36, 'ravi', '1', '2019-11-11 16:31:06', 'room2136'),
(322, 36, 21, 'hi', '1', '2019-11-11 16:31:18', 'room2136'),
(323, 37, 36, 'hi', '0', '2019-11-11 16:32:02', 'room3736'),
(324, 37, 21, 'bsjsjs', '1', '2019-11-11 16:32:28', 'room3721'),
(325, 37, 21, 'hello', '1', '2019-11-11 16:33:04', 'room3721'),
(326, 21, 37, 'ugugig', '1', '2019-11-11 16:33:12', 'room3721'),
(327, 36, 21, 'hello', '1', '2019-11-11 16:33:42', 'room2136'),
(328, 36, 21, 'dop', '1', '2019-11-11 16:34:29', 'room2136'),
(329, 36, 21, 'fhhggghh', '1', '2019-11-11 16:34:41', 'room2136'),
(330, 36, 21, 'ggggg', '1', '2019-11-11 16:35:43', 'room2136'),
(331, 36, 21, 'fffgh', '1', '2019-11-11 16:37:18', 'room2136'),
(332, 21, 37, 'fggggh', '1', '2019-11-11 16:39:10', 'room3721'),
(333, 21, 12, 'hh', '1', '2019-11-11 16:44:41', 'room2112'),
(334, 36, 21, 'ggsgd', '1', '2019-11-11 16:44:50', 'room2136'),
(335, 36, 21, 'gdhbdhd', '1', '2019-11-11 16:45:00', 'room2136'),
(336, 36, 21, 'cvggg', '1', '2019-11-11 16:45:54', 'room2136'),
(337, 21, 12, 'hhhh', '1', '2019-11-11 16:49:35', 'room2112'),
(338, 21, 12, 'hjjgg', '1', '2019-11-11 16:49:38', 'room2112'),
(339, 36, 21, 'hhd', '1', '2019-11-11 16:49:50', 'room2136'),
(340, 36, 21, 'hii', '1', '2019-11-11 17:05:45', 'room2136'),
(341, 21, 36, 'hhi', '1', '2019-11-11 17:05:58', 'room2136'),
(342, 36, 21, 'hhhh', '1', '2019-11-11 17:06:13', 'room2136'),
(343, 36, 21, 'hhhhh', '1', '2019-11-11 17:06:23', 'room2136'),
(344, 36, 21, 'hdhjd', '1', '2019-11-11 17:07:52', 'room2136'),
(345, 36, 21, 'hhhfg', '1', '2019-11-11 17:08:33', 'room2136'),
(346, 21, 12, 'hii', '1', '2019-11-11 17:32:22', 'room2112'),
(347, 21, 12, 'test', '1', '2019-11-11 17:32:28', 'room2112'),
(348, 36, 21, 'hiii', '1', '2019-11-11 17:32:42', 'room2136'),
(349, 36, 21, 'hg', '1', '2019-11-11 17:34:46', 'room2136'),
(350, 21, 12, 'hello', '1', '2019-11-12 05:31:03', 'room2112'),
(351, 21, 12, 'hiii', '1', '2019-11-12 05:31:55', 'room2112'),
(352, 21, 9, 'hii', '0', '2019-11-12 05:34:48', 'room219'),
(353, 12, 21, 'hello', '1', '2019-11-12 05:35:16', 'room2112'),
(354, 21, 12, 'hiii', '1', '2019-11-12 05:35:34', 'room2112'),
(355, 12, 21, 'hello', '1', '2019-11-12 05:35:44', 'room2112'),
(356, 12, 21, 'hi', '1', '2019-11-12 05:45:56', 'room2112'),
(357, 12, 21, 'hello', '1', '2019-11-12 05:48:03', 'room2112'),
(358, 12, 21, 'hhhh', '1', '2019-11-12 05:49:47', 'room2112'),
(359, 12, 21, 'vcdf', '1', '2019-11-12 05:50:11', 'room2112'),
(360, 21, 9, 'hii', '0', '2019-11-12 05:53:13', 'room219'),
(361, 21, 9, 'to hjj', '0', '2019-11-12 05:53:18', 'room219'),
(362, 21, 37, 'ghh', '1', '2019-11-12 05:54:03', 'room3721'),
(363, 12, 21, 'hello', '1', '2019-11-12 05:54:13', 'room2112'),
(364, 21, 12, 'hjj', '1', '2019-11-12 05:54:34', 'room2112'),
(365, 12, 21, 'heeloo', '1', '2019-11-12 05:54:50', 'room2112'),
(366, 21, 12, 'gjj', '1', '2019-11-12 05:54:52', 'room2112'),
(367, 12, 21, 'hello', '1', '2019-11-12 05:59:05', 'room2112'),
(368, 21, 12, 'hh', '1', '2019-11-12 05:59:18', 'room2112'),
(369, 12, 21, 'hiii', '1', '2019-11-12 05:59:24', 'room2112'),
(370, 21, 12, 'jjj', '1', '2019-11-12 05:59:25', 'room2112'),
(371, 12, 21, 'gigkgly', '1', '2019-11-12 05:59:29', 'room2112'),
(372, 12, 21, 'hello', '1', '2019-11-12 06:00:02', 'room2112'),
(373, 12, 21, 'hello', '1', '2019-11-12 06:18:35', 'room2112'),
(374, 21, 12, 'hhhh', '1', '2019-11-12 06:18:54', 'room2112'),
(375, 21, 12, 'prem', '1', '2019-11-12 06:19:01', 'room2112'),
(376, 12, 21, 'hello', '1', '2019-11-12 06:19:16', 'room2112'),
(377, 21, 12, 'hh', '1', '2019-11-12 06:19:27', 'room2112'),
(378, 37, 21, 'hello', '1', '2019-11-12 06:20:16', 'room3721'),
(379, 37, 21, 'gg', '1', '2019-11-12 06:20:28', 'room3721'),
(380, 21, 37, 'uguguguvuuvuvuvuvuvuvuguujvvuvi', '1', '2019-11-12 06:20:35', 'room3721'),
(381, 37, 21, 'dfyy', '1', '2019-11-12 06:20:38', 'room3721'),
(382, 21, 37, 'tcycycyyvyvyvyvyyvyvyvvy', '1', '2019-11-12 06:20:41', 'room3721'),
(383, 21, 37, 'vygygyuggyg', '1', '2019-11-12 06:20:49', 'room3721'),
(384, 37, 21, 'dfghhhhhhhuuiiiiiijjjjjjjju', '1', '2019-11-12 06:20:55', 'room3721'),
(385, 21, 37, 'nadim is', '1', '2019-11-12 06:21:16', 'room3721'),
(386, 37, 21, 'hii', '1', '2019-11-12 06:25:32', 'room3721'),
(387, 37, 21, 'hello', '1', '2019-11-12 06:25:45', 'room3721'),
(388, 21, 37, 'tyuu', '1', '2019-11-12 06:25:50', 'room3721'),
(389, 37, 21, 'hello', '1', '2019-11-12 06:26:08', 'room3721'),
(390, 21, 37, 'hii', '1', '2019-11-12 06:36:06', 'room3721'),
(391, 37, 21, 'hhe', '1', '2019-11-12 06:36:46', 'room3721'),
(392, 21, 37, 'hi', '1', '2019-11-12 06:36:56', 'room3721'),
(393, 37, 21, 'hw', '1', '2019-11-12 06:36:59', 'room3721'),
(394, 21, 41, 'hello', '1', '2019-11-12 06:54:13', 'room2141'),
(395, 21, 41, 'hi there', '1', '2019-11-12 06:54:33', 'room2141'),
(396, 21, 41, 'mm is that a good day at work and I have to to to the store and I have to go to the store and I have to go to the store for you and I have to go to the store and I have to go to the store and and I have to go to the store and I have to go to 5 million in its the store', '1', '2019-11-12 06:54:46', 'room2141'),
(397, 21, 41, 'hello', '1', '2019-11-12 06:57:34', 'room2141'),
(398, 41, 21, 'hi', '1', '2019-11-12 06:57:40', 'room2141'),
(399, 41, 21, 'bhh', '1', '2019-11-12 06:57:52', 'room2141'),
(400, 41, 21, 'uokka', '1', '2019-11-12 06:58:02', 'room2141'),
(401, 41, 21, 'all right', '1', '2019-11-12 06:58:13', 'room2141'),
(402, 21, 25, 'hii', '1', '2019-11-12 06:58:42', 'room2125'),
(403, 21, 25, 'test', '1', '2019-11-12 06:58:51', 'room2125'),
(404, 21, 25, 'hello', '1', '2019-11-12 07:06:10', 'room2125'),
(405, 21, 25, 'test', '1', '2019-11-12 07:06:18', 'room2125'),
(406, 21, 25, 'hell', '1', '2019-11-12 07:07:09', 'room2125'),
(407, 25, 21, 'ghhj', '1', '2019-11-12 07:07:18', 'room2125'),
(408, 25, 21, 'ggffgjj', '1', '2019-11-12 07:07:32', 'room2125'),
(409, 21, 25, 'hi', '1', '2019-11-12 07:07:54', 'room2125'),
(410, 25, 21, 'fghh', '1', '2019-11-12 07:07:54', 'room2125'),
(411, 21, 25, 'hello', '1', '2019-11-12 07:08:08', 'room2125'),
(412, 41, 25, 'hi mittal sab', '0', '2019-11-12 07:08:19', 'room4125'),
(413, 25, 21, 'ghhj', '1', '2019-11-12 07:21:50', 'room2125'),
(414, 25, 21, 'fggh', '1', '2019-11-12 07:31:07', 'room2125'),
(415, 25, 36, 'gghhh', '1', '2019-11-12 07:31:45', 'room2536'),
(416, 25, 37, 'fvgv', '1', '2019-11-12 07:32:50', 'room2537'),
(417, 25, 37, 'fgghh', '1', '2019-11-12 07:34:32', 'room2537'),
(418, 37, 25, 'hi', '1', '2019-11-12 07:35:00', 'room2537'),
(419, 37, 25, 'hello', '1', '2019-11-12 07:35:06', 'room2537'),
(420, 37, 25, 'why?', '1', '2019-11-12 07:35:13', 'room2537'),
(421, 37, 25, 'o', '1', '2019-11-12 07:35:17', 'room2537'),
(422, 21, 41, 'hii', '1', '2019-11-12 07:50:48', 'room2141'),
(423, 21, 41, 'test', '1', '2019-11-12 07:50:54', 'room2141'),
(424, 41, 21, 'hii', '1', '2019-11-12 07:55:12', 'room2141'),
(425, 41, 21, 'test', '1', '2019-11-12 07:55:17', 'room2141'),
(426, 21, 41, 'plz find', '1', '2019-11-12 07:55:29', 'room2141'),
(427, 21, 41, 'tesy', '1', '2019-11-12 07:55:37', 'room2141'),
(428, 21, 41, 'one', '1', '2019-11-12 07:55:42', 'room2141'),
(429, 41, 21, 'hii', '1', '2019-11-12 08:40:39', 'room2141'),
(430, 41, 21, 'test', '1', '2019-11-12 08:40:43', 'room2141'),
(431, 41, 21, 'hhh', '1', '2019-11-12 08:43:13', 'room2141'),
(432, 41, 21, 'tesy', '1', '2019-11-12 08:43:18', 'room2141'),
(433, 21, 41, 'test', '1', '2019-11-12 08:43:45', 'room2141'),
(434, 21, 41, 'test', '1', '2019-11-12 08:43:53', 'room2141'),
(435, 21, 41, 'one', '1', '2019-11-12 08:43:57', 'room2141'),
(436, 21, 41, 'test', '1', '2019-11-12 08:44:07', 'room2141'),
(437, 41, 21, 'test', '1', '2019-11-12 08:44:25', 'room2141'),
(438, 25, 36, 'hi abhishek', '1', '2019-11-12 08:45:25', 'room2536'),
(439, 41, 21, 'Test', '1', '2019-11-12 08:46:35', 'room2141'),
(440, 41, 21, 'hiii', '1', '2019-11-12 08:49:12', 'room2141'),
(441, 41, 21, 'hhgf', '1', '2019-11-12 08:49:24', 'room2141'),
(442, 41, 21, 'one', '1', '2019-11-12 08:49:32', 'room2141'),
(443, 19, 28, 'Hy...', '1', '2019-11-12 08:56:59', 'room1928'),
(444, 19, 20, 'Jameel Bhai...', '1', '2019-11-12 08:57:58', 'room1920'),
(445, 41, 21, 'one', '1', '2019-11-12 09:00:44', 'room2141'),
(446, 41, 21, 'test', '1', '2019-11-12 09:00:54', 'room2141'),
(447, 41, 21, 'hii', '1', '2019-11-12 09:08:16', 'room2141'),
(448, 41, 21, 'test', '1', '2019-11-12 09:09:05', 'room2141'),
(449, 41, 21, 'tess', '1', '2019-11-12 09:11:46', 'room2141'),
(450, 41, 21, 'test', '1', '2019-11-12 09:12:09', 'room2141'),
(451, 41, 21, 'one two', '1', '2019-11-12 09:12:17', 'room2141'),
(452, 41, 21, 'hello', '1', '2019-11-12 09:26:18', 'room2141'),
(453, 41, 21, 'hiii', '1', '2019-11-12 09:31:34', 'room2141'),
(454, 41, 21, 'heeo', '1', '2019-11-12 09:34:08', 'room2141'),
(455, 41, 21, 'hshshs', '1', '2019-11-12 09:34:26', 'room2141'),
(456, 41, 21, 'hello', '1', '2019-11-12 09:34:42', 'room2141'),
(457, 41, 21, 'test', '1', '2019-11-12 09:35:03', 'room2141'),
(458, 41, 21, 'one', '1', '2019-11-12 09:35:07', 'room2141'),
(459, 41, 21, 'test', '1', '2019-11-12 09:35:11', 'room2141'),
(460, 41, 21, 'hhh', '1', '2019-11-12 09:35:30', 'room2141'),
(461, 41, 21, 'kkk', '1', '2019-11-12 09:35:39', 'room2141'),
(462, 41, 21, 'one', '1', '2019-11-12 09:35:42', 'room2141'),
(463, 41, 21, 'test', '1', '2019-11-12 09:35:48', 'room2141'),
(464, 41, 21, 'one', '1', '2019-11-12 09:35:53', 'room2141'),
(465, 41, 21, 'prem', '1', '2019-11-12 09:35:59', 'room2141'),
(466, 41, 25, 'fghhh', '0', '2019-11-12 09:37:14', 'room4125'),
(467, 41, 25, 'hfhfjcjfjf', '0', '2019-11-12 09:37:28', 'room4125'),
(468, 41, 21, 'hi', '1', '2019-11-12 09:39:25', 'room2141'),
(469, 41, 21, 'hii', '1', '2019-11-12 09:39:42', 'room2141'),
(470, 25, 21, 'hiii', '1', '2019-11-12 09:43:01', 'room2125'),
(471, 21, 25, 'hii', '1', '2019-11-12 09:43:20', 'room2125'),
(472, 21, 25, 'test', '1', '2019-11-12 09:43:25', 'room2125'),
(473, 21, 25, 'test', '1', '2019-11-12 09:43:38', 'room2125'),
(474, 21, 25, 'one', '1', '2019-11-12 09:43:44', 'room2125'),
(475, 21, 25, 'oo', '1', '2019-11-12 09:43:49', 'room2125'),
(476, 21, 25, 'test', '1', '2019-11-12 09:43:53', 'room2125'),
(477, 41, 21, 'hii', '1', '2019-11-12 09:58:10', 'room2141'),
(478, 41, 21, 'hii', '1', '2019-11-12 09:58:20', 'room2141'),
(479, 41, 21, 'test', '1', '2019-11-12 09:58:26', 'room2141'),
(480, 41, 21, 'one', '1', '2019-11-12 09:58:30', 'room2141'),
(481, 41, 21, 'hii', '1', '2019-11-12 10:20:55', 'room2141'),
(482, 41, 21, 'one', '1', '2019-11-12 10:22:40', 'room2141'),
(483, 41, 21, 'hii', '1', '2019-11-12 10:23:27', 'room2141'),
(484, 41, 21, 'one', '1', '2019-11-12 10:23:33', 'room2141'),
(485, 41, 21, 'test', '1', '2019-11-12 10:23:38', 'room2141'),
(486, 41, 21, 'iii', '1', '2019-11-12 10:24:03', 'room2141'),
(487, 41, 21, 'ooo', '1', '2019-11-12 10:29:13', 'room2141'),
(488, 41, 21, 'one', '1', '2019-11-12 10:29:17', 'room2141'),
(489, 41, 21, 'one', '1', '2019-11-12 10:29:23', 'room2141'),
(490, 41, 21, 'testmm', '1', '2019-11-12 10:29:28', 'room2141'),
(491, 12, 36, 'No', '1', '2019-11-12 11:00:18', 'room3612'),
(492, 21, 12, 'hii', '1', '2019-11-12 11:03:20', 'room2112'),
(493, 21, 12, 'test', '1', '2019-11-12 11:03:37', 'room2112'),
(494, 37, 12, 'hey Gaurav', '1', '2019-11-12 11:05:10', 'room3712'),
(495, 12, 21, 'hi', '1', '2019-11-12 11:06:00', 'room2112'),
(496, 12, 21, 'hi', '1', '2019-11-12 11:06:14', 'room2112'),
(497, 12, 21, 'hi', '1', '2019-11-12 11:06:24', 'room2112'),
(498, 37, 12, 'hi', '1', '2019-11-12 11:06:59', 'room3712'),
(499, 12, 37, 'hello', '1', '2019-11-12 11:07:33', 'room3712'),
(500, 37, 12, 'hi', '1', '2019-11-12 11:08:02', 'room3712'),
(501, 41, 12, 'hi.....', '1', '2019-11-12 11:09:34', 'room4112'),
(502, 37, 12, 'hey gaurav', '1', '2019-11-12 11:18:19', 'room3712'),
(503, 12, 37, 'hello', '1', '2019-11-12 11:18:40', 'room3712'),
(504, 12, 37, 'fine bro', '1', '2019-11-12 11:19:12', 'room3712'),
(505, 37, 12, 'all right', '1', '2019-11-12 11:19:17', 'room3712'),
(506, 37, 12, 'take care bro', '1', '2019-11-12 11:19:23', 'room3712'),
(507, 37, 12, 'What is your plan today', '1', '2019-11-12 11:19:34', 'room3712'),
(508, 37, 12, 'ok', '1', '2019-11-12 11:19:49', 'room3712'),
(509, 12, 37, 'ummm', '1', '2019-11-12 11:20:34', 'room3712'),
(510, 12, 37, 'hey', '1', '2019-11-12 11:21:01', 'room3712'),
(511, 37, 12, 'what', '1', '2019-11-12 11:21:11', 'room3712'),
(512, 37, 12, 'nothing', '1', '2019-11-12 11:21:17', 'room3712'),
(513, 37, 12, 'hi', '1', '2019-11-12 11:22:03', 'room3712'),
(514, 41, 12, 'hey', '1', '2019-11-12 11:22:45', 'room4112'),
(515, 41, 21, 'hi', '1', '2019-11-12 11:26:02', 'room2141'),
(516, 41, 21, 'nadim bhai', '1', '2019-11-12 11:26:16', 'room2141'),
(517, 12, 21, 'hello', '1', '2019-11-12 11:26:43', 'room2112'),
(518, 12, 21, 'hi', '1', '2019-11-12 11:26:54', 'room2112'),
(519, 41, 21, 'helllo', '1', '2019-11-12 11:27:12', 'room2141'),
(520, 41, 21, 'geop', '1', '2019-11-12 11:27:22', 'room2141'),
(521, 41, 21, 'okay', '1', '2019-11-12 11:27:28', 'room2141'),
(522, 41, 21, 'okay', '1', '2019-11-12 11:27:36', 'room2141'),
(523, 12, 21, 'hello', '1', '2019-11-12 11:27:45', 'room2112'),
(524, 41, 12, 'welcome', '1', '2019-11-12 11:28:14', 'room4112'),
(525, 41, 12, 'how u doing??', '1', '2019-11-12 11:28:36', 'room4112'),
(526, 12, 41, 'so', '1', '2019-11-12 11:28:43', 'room4112'),
(527, 12, 41, 'I thing', '1', '2019-11-12 11:29:00', 'room4112'),
(528, 41, 12, 'hi', '1', '2019-11-12 11:29:29', 'room4112'),
(529, 12, 41, 'so??', '1', '2019-11-12 11:29:35', 'room4112'),
(530, 12, 41, 'how are you??', '1', '2019-11-12 11:29:47', 'room4112'),
(531, 12, 41, 'hi', '1', '2019-11-12 11:30:00', 'room4112'),
(532, 41, 12, 'hello', '1', '2019-11-12 11:30:28', 'room4112'),
(533, 41, 12, 'hi', '1', '2019-11-12 11:30:36', 'room4112'),
(534, 12, 41, 'hi', '1', '2019-11-12 11:30:50', 'room4112'),
(535, 12, 41, 'hop', '1', '2019-11-12 11:30:57', 'room4112'),
(536, 12, 41, 'hello', '1', '2019-11-12 11:31:13', 'room4112'),
(537, 12, 41, 'how', '1', '2019-11-12 11:31:23', 'room4112'),
(538, 12, 41, 'h', '1', '2019-11-12 11:31:25', 'room4112'),
(539, 12, 41, 'jjk', '1', '2019-11-12 11:31:27', 'room4112'),
(540, 12, 41, 'how', '1', '2019-11-12 11:31:32', 'room4112'),
(541, 12, 41, 'loiup', '1', '2019-11-12 11:31:39', 'room4112'),
(542, 12, 41, 'hi', '1', '2019-11-12 11:32:30', 'room4112'),
(543, 12, 41, 'hello', '1', '2019-11-12 11:33:02', 'room4112'),
(544, 12, 41, 'hey', '1', '2019-11-12 11:33:16', 'room4112'),
(545, 41, 12, 'hahah hahaha and wht', '1', '2019-11-12 11:35:49', 'room4112'),
(546, 41, 12, 'ur', '1', '2019-11-12 11:35:58', 'room4112'),
(547, 12, 41, 'are u', '1', '2019-11-12 11:36:10', 'room4112'),
(548, 12, 41, 'hey', '1', '2019-11-12 11:36:54', 'room4112'),
(549, 12, 41, 'why notification is not comming', '1', '2019-11-12 11:37:05', 'room4112'),
(550, 41, 12, 'o', '1', '2019-11-12 11:37:29', 'room4112'),
(551, 41, 12, 'i D', '1', '2019-11-12 11:37:36', 'room4112'),
(552, 41, 12, 'are', '1', '2019-11-12 11:38:21', 'room4112'),
(553, 12, 36, 'fried', '0', '2019-11-12 11:40:58', 'room3612'),
(554, 12, 9, 'Hey', '1', '2019-11-12 11:42:34', 'room912'),
(555, 12, 21, 'hey', '1', '2019-11-12 11:53:28', 'room2112'),
(556, 21, 12, 'hey', '1', '2019-11-12 11:53:59', 'room2112'),
(557, 12, 21, 'how', '1', '2019-11-12 11:55:08', 'room2112'),
(558, 21, 12, 'hey', '1', '2019-11-12 11:55:24', 'room2112'),
(559, 12, 21, 'ffg', '1', '2019-11-12 11:59:46', 'room2112'),
(560, 12, 21, 'hello', '1', '2019-11-12 12:00:31', 'room2112'),
(561, 12, 21, 'hey', '1', '2019-11-12 12:00:55', 'room2112'),
(562, 12, 21, 'jsjsju', '1', '2019-11-12 12:00:59', 'room2112'),
(563, 12, 21, 'he', '1', '2019-11-12 12:01:08', 'room2112'),
(564, 12, 21, 'heoo', '1', '2019-11-12 12:02:15', 'room2112'),
(565, 41, 21, 'nadima', '1', '2019-11-12 12:02:50', 'room2141'),
(566, 21, 25, 'hii', '1', '2019-11-12 12:05:15', 'room2125'),
(567, 21, 12, 'hello', '1', '2019-11-12 12:05:54', 'room2112'),
(568, 21, 12, 'iii', '1', '2019-11-12 12:06:13', 'room2112'),
(569, 21, 12, 'jdjd', '1', '2019-11-12 12:07:04', 'room2112'),
(570, 21, 12, 'fujvkv', '1', '2019-11-12 12:08:39', 'room2112'),
(571, 21, 25, 'hello', '1', '2019-11-12 12:09:05', 'room2125'),
(572, 21, 25, 'ghhh', '1', '2019-11-12 12:11:25', 'room2125'),
(573, 21, 12, 'hi', '1', '2019-11-12 12:12:45', 'room2112'),
(574, 21, 37, 'fine', '1', '2019-11-12 12:12:54', 'room3721'),
(575, 41, 21, 'hey', '1', '2019-11-12 12:13:23', 'room2141'),
(576, 21, 41, 'bol', '1', '2019-11-12 12:13:44', 'room2141'),
(577, 21, 42, 'hey', '1', '2019-11-12 12:14:47', 'room2142'),
(578, 21, 36, 'hhh', '1', '2019-11-12 12:15:52', 'room2136'),
(579, 41, 38, 'hey', '0', '2019-11-12 12:22:44', 'room4138'),
(580, 21, 36, 'hii', '1', '2019-11-12 12:29:00', 'room2136'),
(581, 12, 9, 'Noooo', '1', '2019-11-12 12:32:28', 'room912'),
(582, 12, 9, 'Yyyaaa', '1', '2019-11-12 12:33:07', 'room912'),
(583, 12, 9, 'Weee', '1', '2019-11-12 12:34:12', 'room912'),
(584, 41, 21, 'hey', '0', '2019-11-12 13:01:48', 'room2141'),
(585, 41, 21, 'hii', '0', '2019-11-12 13:09:31', 'room2141'),
(586, 41, 21, 'hi', '0', '2019-11-12 13:09:46', 'room2141'),
(587, 41, 21, 'test', '0', '2019-11-12 13:09:50', 'room2141'),
(588, 41, 21, 'hii', '0', '2019-11-12 13:14:16', 'room2141'),
(589, 37, 21, 'hey dude', '1', '2019-11-12 13:14:36', 'room3721'),
(590, 41, 21, 'hii', '0', '2019-11-12 13:15:24', 'room2141'),
(591, 37, 21, 'oka', '1', '2019-11-12 13:15:32', 'room3721'),
(592, 41, 21, 'hii', '0', '2019-11-12 13:16:01', 'room2141'),
(593, 37, 21, 'hay dude I am so I can tomorrow and will get it to you in the morning to get a hold of a few of the other guys that are in a position to help you with the new one is not working properly to get the kids together for a drink and a bite of my life with you and yours have ordered a pair in my room for a bit but to me', '1', '2019-11-12 13:16:05', 'room3721'),
(594, 41, 21, 'hey', '0', '2019-11-12 13:26:19', 'room2141'),
(595, 21, 41, 'hi', '0', '2019-11-12 13:27:02', 'room2141'),
(596, 12, 9, 'Hello', '1', '2019-11-12 18:30:39', 'room912'),
(597, 9, 12, 'hi', '1', '2019-11-12 18:30:46', 'room912'),
(598, 12, 9, 'Naveen', '1', '2019-11-12 18:30:59', 'room912'),
(599, 12, 9, 'He r u', '1', '2019-11-12 18:31:05', 'room912'),
(600, 12, 9, 'Gdhwgdhwg', '1', '2019-11-12 18:31:21', 'room912'),
(601, 12, 9, 'Nnn', '1', '2019-11-12 18:31:32', 'room912'),
(602, 9, 12, 'hi', '1', '2019-11-12 18:31:51', 'room912'),
(603, 9, 12, 'tarun', '1', '2019-11-12 18:31:54', 'room912'),
(604, 9, 12, 'hello tarun', '1', '2019-11-12 18:32:23', 'room912'),
(605, 9, 12, 'hahag', '1', '2019-11-12 18:32:28', 'room912'),
(606, 9, 12, 'gwgwg', '1', '2019-11-12 18:32:31', 'room912'),
(607, 9, 12, 'vwgwg', '1', '2019-11-12 18:32:33', 'room912'),
(608, 12, 9, 'Hjhj', '1', '2019-11-12 18:39:13', 'room912'),
(609, 19, 28, 'Hy', '1', '2019-11-13 05:39:11', 'room1928'),
(610, 19, 20, 'Hy', '1', '2019-11-13 05:39:41', 'room1920'),
(611, 28, 19, 'wansa', '1', '2019-11-13 05:52:13', 'room1928'),
(612, 19, 28, 'Hy', '1', '2019-11-13 07:18:26', 'room1928'),
(613, 19, 28, 'Hy', '1', '2019-11-13 07:18:49', 'room1928'),
(614, 28, 19, 'hello', '1', '2019-11-13 07:19:21', 'room1928'),
(615, 37, 25, 'hello ankit', '1', '2019-11-13 07:25:14', 'room2537'),
(616, 37, 21, 'hello', '1', '2019-11-13 07:26:40', 'room3721'),
(617, 37, 21, 'hello', '1', '2019-11-13 07:27:12', 'room3721'),
(618, 21, 37, 'hello', '1', '2019-11-13 07:28:19', 'room3721'),
(619, 37, 21, 'hello', '1', '2019-11-13 07:28:32', 'room3721'),
(620, 21, 37, 'hi there', '1', '2019-11-13 07:29:04', 'room3721'),
(621, 19, 21, 'Hi...', '1', '2019-11-13 07:34:20', 'room1921'),
(622, 37, 21, 'hu', '1', '2019-11-13 07:34:32', 'room3721'),
(623, 21, 37, 'hdhfhfiifigiiiggiigjccjcj', '1', '2019-11-13 07:34:33', 'room3721'),
(624, 21, 37, 'g', '1', '2019-11-13 07:34:36', 'room3721'),
(625, 21, 37, 'haha', '1', '2019-11-13 07:34:39', 'room3721'),
(626, 37, 21, 'hg', '1', '2019-11-13 07:34:42', 'room3721'),
(627, 37, 21, 'hey', '1', '2019-11-13 07:35:56', 'room3721'),
(628, 21, 37, 'gyi', '1', '2019-11-13 07:36:08', 'room3721'),
(629, 37, 21, 'hey', '1', '2019-11-13 07:37:32', 'room3721'),
(630, 37, 21, 'hello', '1', '2019-11-13 07:38:55', 'room3721'),
(631, 37, 21, 'hello there', '1', '2019-11-13 07:39:04', 'room3721'),
(632, 21, 37, 'hey', '0', '2019-11-13 07:43:06', 'room3721'),
(633, 21, 37, 'hello', '0', '2019-11-13 07:44:55', 'room3721'),
(634, 21, 37, 'iOS....', '0', '2019-11-13 07:45:14', 'room3721'),
(635, 12, 37, 'Hii', '1', '2019-11-13 07:46:19', 'room3712'),
(636, 12, 9, 'Hii', '1', '2019-11-13 07:47:22', 'room912'),
(637, 12, 37, 'Hii', '1', '2019-11-13 07:48:51', 'room3712'),
(638, 21, 37, 'hello', '0', '2019-11-13 07:49:11', 'room3721'),
(639, 37, 12, 'hey', '1', '2019-11-13 07:54:14', 'room3712'),
(640, 37, 12, 'hi', '1', '2019-11-13 07:56:14', 'room3712'),
(641, 12, 21, 'hello', '1', '2019-11-13 07:56:36', 'room2112'),
(642, 37, 12, 'hello', '1', '2019-11-13 08:02:20', 'room3712'),
(643, 12, 37, 'hi', '1', '2019-11-13 08:02:27', 'room3712'),
(644, 37, 12, 'ger', '1', '2019-11-13 08:03:41', 'room3712'),
(645, 12, 37, 'hio', '1', '2019-11-13 08:04:00', 'room3712'),
(646, 37, 12, 'gi', '0', '2019-11-13 08:04:14', 'room3712'),
(647, 12, 20, 'hello Jamil', '0', '2019-11-13 08:37:47', 'room1220'),
(648, 12, 20, 'how you doing?', '0', '2019-11-13 08:37:58', 'room1220'),
(649, 12, 28, 'hi jahan', '0', '2019-11-13 08:39:19', 'room1228'),
(650, 12, 28, 'how are you?', '0', '2019-11-13 08:39:24', 'room1228'),
(651, 12, 19, 'hi there', '0', '2019-11-13 08:39:35', 'room1912'),
(652, 12, 19, 'hope you are doing good', '0', '2019-11-13 08:39:41', 'room1912'),
(653, 28, 12, 'all good', '0', '2019-11-13 08:49:40', 'room1228'),
(654, 28, 12, 'OK I received the notification now', '0', '2019-11-13 08:50:06', 'room1228'),
(655, 12, 28, 'cool', '0', '2019-11-13 08:50:11', 'room1228'),
(656, 12, 21, 'hello', '1', '2019-11-13 09:26:25', 'room2112'),
(657, 12, 21, 'qwerty', '1', '2019-11-13 09:27:08', 'room2112'),
(658, 12, 21, 'hi', '1', '2019-11-13 09:28:13', 'room2112'),
(659, 12, 21, 'hello', '1', '2019-11-13 09:29:46', 'room2112'),
(660, 12, 21, 'gone', '1', '2019-11-13 09:29:55', 'room2112'),
(661, 12, 21, 'hi', '1', '2019-11-13 09:30:09', 'room2112'),
(662, 21, 12, 'hello', '1', '2019-11-13 09:30:15', 'room2112'),
(663, 21, 12, 'test', '1', '2019-11-13 09:30:17', 'room2112'),
(664, 21, 37, 'test', '0', '2019-11-13 09:31:53', 'room3721'),
(665, 21, 12, 'gaurav', '1', '2019-11-13 09:32:06', 'room2112'),
(666, 21, 19, 'hii', '1', '2019-11-13 09:32:19', 'room1921'),
(667, 12, 21, 'hello', '1', '2019-11-13 09:32:41', 'room2112'),
(668, 12, 21, 'hello', '1', '2019-11-13 09:33:01', 'room2112'),
(669, 12, 21, 'hello', '1', '2019-11-13 09:33:20', 'room2112'),
(670, 21, 12, 'Did you guys are going', '1', '2019-11-13 09:34:37', 'room2112'),
(671, 12, 21, 'No', '1', '2019-11-13 09:34:43', 'room2112'),
(672, 12, 21, 'But I will go tomorrow', '1', '2019-11-13 09:34:58', 'room2112'),
(673, 21, 12, 'ok', '1', '2019-11-13 09:35:11', 'room2112'),
(674, 21, 12, 'I will also come with you', '1', '2019-11-13 09:35:29', 'room2112'),
(675, 12, 21, 'Cool......You will enjoy it lot', '1', '2019-11-13 09:35:46', 'room2112'),
(676, 25, 37, ':)', '0', '2019-11-14 08:51:04', 'room2537'),
(677, 25, 37, ';)', '0', '2019-11-14 08:51:33', 'room2537'),
(678, 25, 37, '\n', '0', '2019-11-14 08:51:41', 'room2537'),
(679, 12, 9, 'hi tarun', '1', '2019-11-14 14:08:51', 'room912'),
(680, 12, 9, 'hi', '1', '2019-11-14 14:09:11', 'room912'),
(681, 12, 9, 'heu', '1', '2019-11-14 14:09:54', 'room912'),
(682, 12, 9, 'yi', '1', '2019-11-14 14:10:01', 'room912'),
(683, 9, 12, 'Hello', '1', '2019-11-14 14:10:16', 'room912'),
(684, 12, 9, 'hello', '1', '2019-11-14 14:10:25', 'room912'),
(685, 41, 9, 'Hii', '1', '2019-11-14 14:14:00', 'room419'),
(686, 9, 41, 'Huuio', '1', '2019-11-14 14:15:43', 'room419'),
(687, 41, 9, 'Iijiji', '1', '2019-11-14 14:15:48', 'room419'),
(688, 9, 41, 'HHaha', '1', '2019-11-14 14:16:03', 'room419'),
(689, 9, 41, 'Ahhahahah', '1', '2019-11-14 14:16:05', 'room419'),
(690, 41, 9, 'hi', '1', '2019-11-15 05:52:13', 'room419'),
(691, 41, 9, 'hrllo', '1', '2019-11-15 05:52:48', 'room419'),
(692, 41, 9, 'hii', '1', '2019-11-15 05:54:34', 'room419'),
(693, 12, 9, 'hillo', '1', '2019-11-15 05:56:27', 'room912'),
(694, 12, 9, 'gfvhb', '1', '2019-11-15 05:57:08', 'room912'),
(695, 12, 9, 'ji', '1', '2019-11-15 05:57:20', 'room912'),
(696, 12, 9, 'ki', '1', '2019-11-15 05:58:02', 'room912'),
(697, 12, 9, 'fguhh', '1', '2019-11-15 05:58:18', 'room912'),
(698, 12, 25, 'hii', '0', '2019-11-15 05:59:25', 'room2512'),
(699, 12, 25, 'hello', '0', '2019-11-15 07:13:08', 'room2512'),
(700, 12, 25, 'hey', '0', '2019-11-15 07:14:40', 'room2512'),
(701, 12, 9, 'hello', '1', '2019-11-15 07:19:59', 'room912'),
(702, 12, 9, 'Hii', '1', '2019-11-15 08:55:01', 'room912'),
(703, 12, 9, 'Khkh', '1', '2019-11-15 08:55:24', 'room912'),
(704, 12, 9, 'hi tarun', '1', '2019-11-15 08:57:12', 'room912'),
(705, 12, 9, 'Kya bheju', '1', '2019-11-15 09:00:03', 'room912'),
(706, 12, 9, 'hello', '1', '2019-11-15 09:09:37', 'room912'),
(707, 12, 9, 'heii', '1', '2019-11-15 09:09:50', 'room912'),
(708, 9, 12, 'Hahs', '1', '2019-11-15 09:09:58', 'room912'),
(709, 9, 12, 'Bshs', '1', '2019-11-15 09:09:59', 'room912'),
(710, 9, 12, 'Jsjah', '1', '2019-11-15 09:10:00', 'room912'),
(711, 9, 12, 'Ava', '1', '2019-11-15 09:10:09', 'room912'),
(712, 9, 12, 'Gshshs', '1', '2019-11-15 09:10:31', 'room912'),
(713, 9, 12, 'Yes', '1', '2019-11-15 09:11:21', 'room912'),
(714, 9, 12, 'Hello', '1', '2019-11-15 09:12:00', 'room912'),
(715, 12, 9, 'giopt', '1', '2019-11-15 09:12:14', 'room912'),
(716, 12, 9, 'ger', '1', '2019-11-15 09:12:21', 'room912'),
(717, 20, 19, 'hi', '1', '2019-11-15 11:22:04', 'room1920'),
(718, 20, 19, 'are you able to see this message', '1', '2019-11-15 11:22:14', 'room1920'),
(719, 19, 20, 'Yes', '1', '2019-11-15 11:24:20', 'room1920'),
(720, 48, 19, 'hy', '1', '2019-11-18 16:09:53', 'room4819'),
(721, 19, 48, 'ok', '1', '2019-11-18 16:12:05', 'room4819'),
(722, 51, 59, 'test', '0', '2019-11-20 03:02:25', 'room5159'),
(723, 72, 20, 'hello Jameel. i am enjoying the conference\n and am interested in receiving copies of  a\n few of the presentations that were made.  will they be accessible online by any chance?  if not, is their a way to coordinate getting copies.', '0', '2019-11-21 19:50:19', 'room7220'),
(724, 20, 72, 'Hi Ricardo. once we have the confirmation from speakers for sharing the presentation. I will send it to you', '0', '2019-11-21 21:45:16', 'room7220'),
(725, 41, 9, 'hello', '0', '2019-11-24 13:53:34', 'room419'),
(726, 41, 9, 'byeeeeee1', '0', '2019-11-24 13:53:49', 'room419'),
(727, 41, 9, 'tata', '0', '2019-11-24 13:53:53', 'room419'),
(728, 19, 108, 'hy', '0', '2019-11-27 10:01:41', 'room19108');

-- --------------------------------------------------------

--
-- Table structure for table `chat_room`
--

CREATE TABLE `chat_room` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `reciver_id` int(11) NOT NULL,
  `room_id` varchar(255) NOT NULL,
  `last_message` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat_room`
--

INSERT INTO `chat_room` (`id`, `sender_id`, `reciver_id`, `room_id`, `last_message`, `created_at`, `updated_at`) VALUES
(3, 1, 3, 'room13', 'HHHHHHHHHHHHHHHH', '2019-11-08 10:18:26', '2019-11-08 11:07:08'),
(4, 3, 1, 'room13', 'HHHHHHHHHHHHHHHH', '2019-11-08 10:18:26', '2019-11-08 11:07:08'),
(5, 9, 3, 'room93', NULL, '2019-11-09 08:55:02', '2019-11-09 08:55:02'),
(6, 3, 9, 'room93', NULL, '2019-11-09 08:55:02', '2019-11-09 08:55:02'),
(7, 9, 1, 'room91', NULL, '2019-11-09 08:57:34', '2019-11-09 08:57:34'),
(8, 1, 9, 'room91', NULL, '2019-11-09 08:57:34', '2019-11-09 08:57:34'),
(9, 9, 12, 'room912', 'ger', '2019-11-09 09:45:26', '2019-11-15 09:12:21'),
(10, 12, 9, 'room912', 'ger', '2019-11-09 09:45:26', '2019-11-15 09:12:21'),
(11, 9, 25, 'room925', 'Ggggg', '2019-11-09 10:17:40', '2019-11-09 10:17:57'),
(12, 25, 9, 'room925', 'Ggggg', '2019-11-09 10:17:40', '2019-11-09 10:17:57'),
(13, 21, 12, 'room2112', 'Cool......You will enjoy it lot', '2019-11-09 11:34:36', '2019-11-13 09:35:46'),
(14, 12, 21, 'room2112', 'Cool......You will enjoy it lot', '2019-11-09 11:34:36', '2019-11-13 09:35:46'),
(15, 21, 38, 'room2138', NULL, '2019-11-09 11:38:54', '2019-11-09 11:38:54'),
(16, 38, 21, 'room2138', NULL, '2019-11-09 11:38:54', '2019-11-09 11:38:54'),
(17, 21, 21, 'room2121', 'hh', '2019-11-11 05:58:45', '2019-11-11 14:17:43'),
(18, 21, 21, 'room2121', 'hh', '2019-11-11 05:58:45', '2019-11-11 14:17:43'),
(19, 9, 27, 'room927', NULL, '2019-11-11 06:00:08', '2019-11-11 06:00:08'),
(20, 27, 9, 'room927', NULL, '2019-11-11 06:00:08', '2019-11-11 06:00:08'),
(21, 21, 36, 'room2136', 'hii', '2019-11-11 06:12:16', '2019-11-12 12:29:00'),
(22, 36, 21, 'room2136', 'hii', '2019-11-11 06:12:16', '2019-11-12 12:29:00'),
(23, 9, 28, 'room928', 'Hii', '2019-11-11 06:35:14', '2019-11-11 06:35:21'),
(24, 28, 9, 'room928', 'Hii', '2019-11-11 06:35:14', '2019-11-11 06:35:21'),
(25, 21, 9, 'room219', 'to hjj', '2019-11-11 07:04:41', '2019-11-12 05:53:18'),
(26, 9, 21, 'room219', 'to hjj', '2019-11-11 07:04:41', '2019-11-12 05:53:18'),
(27, 12, 12, 'room1212', 'hii', '2019-11-11 07:44:50', '2019-11-11 10:08:41'),
(28, 12, 12, 'room1212', 'hii', '2019-11-11 07:44:50', '2019-11-11 10:08:41'),
(29, 9, 19, 'room919', NULL, '2019-11-11 09:57:21', '2019-11-11 09:57:21'),
(30, 19, 9, 'room919', NULL, '2019-11-11 09:57:21', '2019-11-11 09:57:21'),
(31, 9, 9, 'room99', NULL, '2019-11-11 10:16:43', '2019-11-11 10:16:43'),
(32, 9, 9, 'room99', NULL, '2019-11-11 10:16:43', '2019-11-11 10:16:43'),
(33, 9, 0, 'room90', NULL, '2019-11-11 10:21:48', '2019-11-11 10:21:48'),
(34, 0, 9, 'room90', NULL, '2019-11-11 10:21:48', '2019-11-11 10:21:48'),
(35, 41, 36, 'room4136', 'Type a message...', '2019-11-11 11:51:40', '2019-11-11 11:58:01'),
(36, 36, 41, 'room4136', 'Type a message...', '2019-11-11 11:51:40', '2019-11-11 11:58:01'),
(37, 36, 12, 'room3612', 'fried', '2019-11-11 11:59:29', '2019-11-12 11:40:58'),
(38, 12, 36, 'room3612', 'fried', '2019-11-11 11:59:29', '2019-11-12 11:40:58'),
(39, 36, 9, 'room369', 'gvr', '2019-11-11 12:06:36', '2019-11-11 16:17:10'),
(40, 9, 36, 'room369', 'gvr', '2019-11-11 12:06:36', '2019-11-11 16:17:10'),
(41, 21, 27, 'room2127', NULL, '2019-11-11 14:25:01', '2019-11-11 14:25:01'),
(42, 27, 21, 'room2127', NULL, '2019-11-11 14:25:01', '2019-11-11 14:25:01'),
(43, 9, 37, 'room937', 'Hey', '2019-11-11 14:53:07', '2019-11-11 15:34:23'),
(44, 37, 9, 'room937', 'Hey', '2019-11-11 14:53:07', '2019-11-11 15:34:23'),
(45, 41, 9, 'room419', 'tata', '2019-11-11 15:40:48', '2019-11-24 13:53:53'),
(46, 9, 41, 'room419', 'tata', '2019-11-11 15:40:48', '2019-11-24 13:53:53'),
(47, 41, 37, 'room4137', 'hi', '2019-11-11 15:42:20', '2019-11-11 15:46:04'),
(48, 37, 41, 'room4137', 'hi', '2019-11-11 15:42:20', '2019-11-11 15:46:04'),
(49, 37, 21, 'room3721', 'test', '2019-11-11 15:47:22', '2019-11-13 09:31:53'),
(50, 21, 37, 'room3721', 'test', '2019-11-11 15:47:22', '2019-11-13 09:31:53'),
(51, 37, 36, 'room3736', 'hi', '2019-11-11 16:32:00', '2019-11-11 16:32:02'),
(52, 36, 37, 'room3736', 'hi', '2019-11-11 16:32:00', '2019-11-11 16:32:02'),
(53, 21, 41, 'room2141', 'hi', '2019-11-12 06:46:58', '2019-11-12 13:27:02'),
(54, 41, 21, 'room2141', 'hi', '2019-11-12 06:46:58', '2019-11-12 13:27:02'),
(55, 21, 25, 'room2125', 'ghhh', '2019-11-12 06:58:39', '2019-11-12 12:11:25'),
(56, 25, 21, 'room2125', 'ghhh', '2019-11-12 06:58:39', '2019-11-12 12:11:25'),
(57, 41, 25, 'room4125', 'hfhfjcjfjf', '2019-11-12 07:08:14', '2019-11-12 09:37:28'),
(58, 25, 41, 'room4125', 'hfhfjcjfjf', '2019-11-12 07:08:14', '2019-11-12 09:37:28'),
(59, 25, 36, 'room2536', 'hi abhishek', '2019-11-12 07:31:42', '2019-11-12 08:45:25'),
(60, 36, 25, 'room2536', 'hi abhishek', '2019-11-12 07:31:42', '2019-11-12 08:45:25'),
(61, 25, 37, 'room2537', '\n', '2019-11-12 07:32:43', '2019-11-14 08:51:41'),
(62, 37, 25, 'room2537', '\n', '2019-11-12 07:32:43', '2019-11-14 08:51:41'),
(63, 19, 12, 'room1912', 'hope you are doing good', '2019-11-12 08:56:42', '2019-11-13 08:39:41'),
(64, 12, 19, 'room1912', 'hope you are doing good', '2019-11-12 08:56:42', '2019-11-13 08:39:41'),
(65, 19, 28, 'room1928', 'hello', '2019-11-12 08:56:54', '2019-11-13 07:19:21'),
(66, 28, 19, 'room1928', 'hello', '2019-11-12 08:56:54', '2019-11-13 07:19:21'),
(67, 19, 20, 'room1920', 'Yes', '2019-11-12 08:57:48', '2019-11-15 11:24:20'),
(68, 20, 19, 'room1920', 'Yes', '2019-11-12 08:57:48', '2019-11-15 11:24:20'),
(69, 37, 12, 'room3712', 'gi', '2019-11-12 11:05:06', '2019-11-13 08:04:14'),
(70, 12, 37, 'room3712', 'gi', '2019-11-12 11:05:06', '2019-11-13 08:04:14'),
(71, 41, 12, 'room4112', 'are', '2019-11-12 11:09:10', '2019-11-12 11:38:21'),
(72, 12, 41, 'room4112', 'are', '2019-11-12 11:09:10', '2019-11-12 11:38:21'),
(73, 41, 41, 'room4141', NULL, '2019-11-12 11:33:29', '2019-11-12 11:33:29'),
(74, 41, 41, 'room4141', NULL, '2019-11-12 11:33:29', '2019-11-12 11:33:29'),
(75, 21, 42, 'room2142', 'hey', '2019-11-12 12:14:44', '2019-11-12 12:14:47'),
(76, 42, 21, 'room2142', 'hey', '2019-11-12 12:14:44', '2019-11-12 12:14:47'),
(77, 41, 38, 'room4138', 'hey', '2019-11-12 12:22:37', '2019-11-12 12:22:44'),
(78, 38, 41, 'room4138', 'hey', '2019-11-12 12:22:37', '2019-11-12 12:22:44'),
(79, 41, 39, 'room4139', NULL, '2019-11-12 12:22:58', '2019-11-12 12:22:58'),
(80, 39, 41, 'room4139', NULL, '2019-11-12 12:22:58', '2019-11-12 12:22:58'),
(81, 21, 0, 'room210', NULL, '2019-11-12 12:55:18', '2019-11-12 12:55:18'),
(82, 0, 21, 'room210', NULL, '2019-11-12 12:55:18', '2019-11-12 12:55:18'),
(83, 19, 21, 'room1921', 'hii', '2019-11-13 07:33:55', '2019-11-13 09:32:19'),
(84, 21, 19, 'room1921', 'hii', '2019-11-13 07:33:55', '2019-11-13 09:32:19'),
(85, 37, 0, 'room370', NULL, '2019-11-13 07:57:00', '2019-11-13 07:57:00'),
(86, 0, 37, 'room370', NULL, '2019-11-13 07:57:00', '2019-11-13 07:57:00'),
(87, 12, 20, 'room1220', 'how you doing?', '2019-11-13 08:37:41', '2019-11-13 08:37:58'),
(88, 20, 12, 'room1220', 'how you doing?', '2019-11-13 08:37:41', '2019-11-13 08:37:58'),
(89, 12, 28, 'room1228', 'cool', '2019-11-13 08:39:15', '2019-11-13 08:50:11'),
(90, 28, 12, 'room1228', 'cool', '2019-11-13 08:39:15', '2019-11-13 08:50:11'),
(91, 25, 12, 'room2512', 'hey', '2019-11-14 08:49:31', '2019-11-15 07:14:40'),
(92, 12, 25, 'room2512', 'hey', '2019-11-14 08:49:31', '2019-11-15 07:14:40'),
(93, 48, 19, 'room4819', 'ok', '2019-11-18 16:09:50', '2019-11-18 16:12:05'),
(94, 19, 48, 'room4819', 'ok', '2019-11-18 16:09:50', '2019-11-18 16:12:05'),
(95, 20, 71, 'room2071', NULL, '2019-11-19 20:06:43', '2019-11-19 20:06:43'),
(96, 71, 20, 'room2071', NULL, '2019-11-19 20:06:43', '2019-11-19 20:06:43'),
(97, 51, 66, 'room5166', NULL, '2019-11-20 02:59:10', '2019-11-20 02:59:10'),
(98, 66, 51, 'room5166', NULL, '2019-11-20 02:59:10', '2019-11-20 02:59:10'),
(99, 51, 59, 'room5159', 'test', '2019-11-20 03:02:22', '2019-11-20 03:02:25'),
(100, 59, 51, 'room5159', 'test', '2019-11-20 03:02:22', '2019-11-20 03:02:25'),
(101, 72, 20, 'room7220', 'Hi Ricardo. once we have the confirmation from speakers for sharing the presentation. I will send it to you', '2019-11-21 19:47:46', '2019-11-21 21:45:16'),
(102, 20, 72, 'room7220', 'Hi Ricardo. once we have the confirmation from speakers for sharing the presentation. I will send it to you', '2019-11-21 19:47:46', '2019-11-21 21:45:16'),
(103, 19, 108, 'room19108', 'hy', '2019-11-27 10:01:39', '2019-11-27 10:01:41'),
(104, 108, 19, 'room19108', 'hy', '2019-11-27 10:01:39', '2019-11-27 10:01:41');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `content` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `user_id`, `content`, `created_at`, `updated_at`) VALUES
(1, 5, 41, 'hey', '2019-11-24 13:56:52', '2019-11-24 13:56:52');

-- --------------------------------------------------------

--
-- Table structure for table `email_verifications`
--

CREATE TABLE `email_verifications` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `verify_code` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_verifications`
--

INSERT INTO `email_verifications` (`id`, `email`, `verify_code`, `created_at`, `updated_at`) VALUES
(5, 'ankit.mittal@mobulous.com', 'GZM6', '2019-09-28 04:51:55', '2019-10-31 07:54:47'),
(19, 'grv1@yopmail.com', 'C437', '2019-10-01 13:14:21', '2019-10-01 13:14:21'),
(20, 'grv0000@yopmail.com', '2M3H', '2019-10-01 13:15:10', '2019-10-01 13:15:10'),
(23, 'abhi@gmail.com', 'E23I', '2019-10-01 13:41:16', '2019-10-01 13:41:16'),
(28, 'rp@gmail.com', '0Y7H', '2019-10-09 09:48:33', '2019-10-09 09:48:33'),
(31, 'tarunchauhan382@gmail.com', '73W6', '2019-10-23 07:31:28', '2019-10-23 07:31:28'),
(32, 'meer.nayn@outlook.com', 'OKYJ', '2019-10-23 08:01:10', '2019-10-23 08:01:10'),
(33, 'meer.suhaib@outlook.com', '6EQN', '2019-10-23 08:02:08', '2019-10-23 08:03:40'),
(39, 'ankit05.mittal@gmail.com', 'RUSJ', '2019-10-31 08:01:28', '2019-10-31 09:07:50'),
(40, 'garima@yopmail.com', '916D', '2019-10-31 10:31:14', '2019-10-31 10:31:14'),
(42, 'dev@gmail.com', 'GRY4', '2019-10-31 11:12:00', '2019-10-31 11:12:00'),
(48, 'mohd.anas@mobulous.com', '2M36', '2019-11-05 05:37:17', '2019-11-05 05:37:17'),
(49, 'testmobulous222@gmail.com', '7IZM', '2019-11-05 05:55:31', '2019-11-05 06:01:47'),
(50, 'pathan@yopmail.com', 'SZE6', '2019-11-05 05:57:42', '2019-11-05 05:57:42'),
(51, 'gauravkmr0000@gmail.com', '1WLZ', '2019-11-05 06:07:05', '2019-11-05 06:07:05'),
(52, 'jahanzaibsami@gmail.com', 'MZCT', '2019-11-06 05:12:55', '2019-11-06 05:16:27'),
(60, 'tarun11@gmail.com', 'YG0B', '2019-11-15 06:23:05', '2019-11-15 06:23:05'),
(62, 'shariq.abdul@valiantbmedia.com', 'YN3T', '2019-11-18 08:59:16', '2019-11-19 10:41:42'),
(65, 'john.henry@gmail.com', 'PXLC', '2019-11-18 17:14:00', '2019-11-18 17:14:00'),
(71, 'skallepu@onepeterson.com', 'UHJT', '2019-11-19 13:09:05', '2019-11-19 13:11:22'),
(72, 'narenas@classicontrols.com', 'UISD', '2019-11-19 13:17:06', '2019-11-19 13:20:46'),
(74, 'sheldon.davis@ceslintlgroup.com', 'TEXA', '2019-11-19 13:29:29', '2019-11-19 13:33:23'),
(76, 'shivonidal@gmail.com', 'ADNY', '2019-11-19 13:46:44', '2019-11-19 13:48:30'),
(79, 'shanif@atni.com', '35JB', '2019-11-19 14:02:01', '2019-11-19 18:50:15'),
(80, 'shanif@atni.con', 'SPIY', '2019-11-19 14:05:37', '2019-11-19 14:05:37'),
(81, 'yorkblakemccau@gmail.com', 'GBYN', '2019-11-19 14:26:18', '2019-11-19 15:21:49'),
(90, 'narvan.singh@bakerhughes.com', '8GKR', '2019-11-19 16:12:58', '2019-11-21 17:47:36'),
(100, 'fgdgd@dfgh.ggg', 'A8RF', '2019-11-20 05:27:53', '2019-11-20 05:27:53'),
(101, 't@g.km', 'HODP', '2019-11-20 06:34:39', '2019-11-20 06:34:39'),
(102, 'sghasgajh@dfd.ffd', 'Z1B4', '2019-11-20 06:38:24', '2019-11-20 06:38:24'),
(103, 'hdddddd@gf.hgfhgfhfgh', 'W50Y', '2019-11-20 06:42:01', '2019-11-20 06:42:01'),
(104, 'wdfrg@hgfh', 'U1GQ', '2019-11-20 06:46:00', '2019-11-20 06:46:00'),
(105, 'fgfgfg@gfgfg.dfd', 'WIQ7', '2019-11-20 06:48:13', '2019-11-20 06:48:13'),
(108, 'rc@gysbi.com', 'VGZE', '2019-11-20 10:36:45', '2019-11-20 10:36:45'),
(109, 'farheen@gmail.com', 'LQ8G', '2019-11-20 11:36:14', '2019-11-20 11:36:14'),
(110, 'francisca.cruz@zemarine.com', 'OJTE', '2019-11-20 11:48:02', '2019-11-20 11:48:26'),
(114, 'kevin.saroop@oegoffshore.com', 'BRSV', '2019-11-20 13:16:00', '2019-11-20 13:16:00'),
(115, 'qwerty@gmail.com', 'IO9H', '2019-11-20 13:19:20', '2019-11-20 13:19:20'),
(116, 'qwertyu@gmail.com', 'UAJF', '2019-11-20 13:22:31', '2019-11-20 13:22:31'),
(117, 'poiuyt@hjkl.com', 'QK4N', '2019-11-20 13:26:16', '2019-11-20 13:26:16'),
(118, 'qwer@gmail.com', 'A70B', '2019-11-20 13:28:40', '2019-11-20 13:28:40'),
(120, 'qwert@gg.kk', 'Y9U0', '2019-11-20 13:31:08', '2019-11-20 13:31:08'),
(121, 'thuranthiran.nadarajah@saipem.com', 'IKDQ', '2019-11-20 13:38:18', '2019-11-21 15:21:25'),
(125, 'craneman9999@gmail.com', 'Q2MR', '2019-11-20 13:43:27', '2019-11-20 13:44:56'),
(128, 'rajesh@yopmail.com', 'JF1P', '2019-11-20 15:18:40', '2019-11-20 15:18:40'),
(132, 'shawn.Combden@woodplc.com', '34GU', '2019-11-20 17:05:21', '2019-11-20 17:41:59'),
(137, 'trisha.skoryk@gmail.com', 'DIGX', '2019-11-20 23:43:10', '2019-11-20 23:45:27'),
(138, 'farheengauhar@mobulous.com', '5R6D', '2019-11-21 05:11:26', '2019-11-21 05:11:26'),
(144, 'vikas_mohan@ongcvidesh.in', 'QRJH', '2019-11-21 16:15:34', '2019-11-22 19:19:11'),
(145, 'bothrakomal@yahoo.com', '3D6W', '2019-11-22 05:41:20', '2019-11-22 05:41:48'),
(146, 'komalbotbra26@gmail.com', 'C91X', '2019-11-22 05:42:07', '2019-11-22 05:42:07'),
(148, 'ambalikaghosh93@gmail.com', 'UWVH', '2019-11-22 07:24:52', '2019-11-22 07:24:52');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `about` text,
  `venu` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `banner` varchar(200) DEFAULT NULL,
  `datefrom` varchar(100) DEFAULT NULL,
  `dateto` varchar(100) DEFAULT NULL,
  `event_code` varchar(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `userid`, `name`, `about`, `venu`, `email`, `phone`, `logo`, `banner`, `datefrom`, `dateto`, `event_code`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'MMC 2019', 'This is an official mobile app for Marrakech Mining Convention 2019. It will provide you with the latest updates about our event, keeping you informed and connected. Use this app to access resources, network with other attendees, and share your experience at our event.', 'Mövenpick Hotel Boulevard Mohammed VI, Hivernage 40000 Marrakech, Morocco', 'info@mmc.com', '9876543212', 'eventlogo/1572425513_mmc2019logo.png', 'eventbanner/1572425513_mmc-2019-microscience.jpg', '2019-11-04', '2019-11-27', 'AZSXDC321', 0, '2019-10-30 08:51:53', '2019-11-27 05:51:43'),
(2, 7, 'Cricket World Cup', '11 Teams, 1 Cup... Full fun to watch. An entertaining show is happening around us by 1 November.', 'Indira Gandhi Stadium, Noida sector - 23, Uttar Pradesh - 203405', 'grv@yopmail.com', '8090999009', 'eventlogo/1572874528_IMG_0447.PNG', 'eventbanner/1572874528_6aa32160eba21a70905d81546d26033f.jpg', '11/01/2019', '11/30/2019', 'ASDF', 1, '2019-11-04 13:35:28', '2019-11-04 13:58:52'),
(3, 6, 'demo', 'Demo demo Demo demo Demo demo Demo demo Demo demo Demo demo Demo demo Demo demo Demo demo Demo demo Demo demo', 'Noida', 'demo@gmail.com', '9876543212', 'eventlogo/1572930905_mmc2019logo.png', 'eventbanner/1572930905_IMG_0447.PNG', '11/27/2019', '11/30/2019', 'ASDF123', 0, '2019-11-05 05:15:05', '2019-11-05 06:45:39'),
(4, 1, 'Test event', 'ECXJKCBSkdvSKJDBV.kdbvKJDSBV.Ksjbdv.SKJBKSJDV.KSJDBV.KJSDV.KSJ', 'MARRIOT HOTEL NEW DELHI', 'meer.nayn@gmail.com', '+918882507699', 'eventlogo/1573018657_images1.jpg', 'eventbanner/1573018657_images.jpg', '10/01/2019', '10/17/2019', 'testing123', 0, '2019-11-06 05:37:37', '2019-11-06 05:53:28'),
(5, 1, 'Test event', 'hkkkkkkkk jjjjjjj  jjjjjjjjj jjjjjjjjjjjjjjj', 'MARRIOT HOTEL NEW DELHI', 'meer.nayn@gmail.com', '9596775491', 'eventlogo/1573019163_images.jpg', 'eventbanner/1573019163_images1.jpg', '10/09/2019', '10/24/2019', 'testing123', 0, '2019-11-06 05:46:03', '2019-11-16 15:27:11'),
(6, 1, 'Story Writers/Novel Writers/Screenplay Gathered around New Delhi\'s Red Fort to celebrate their values.', 'Story Writers/Novel Writers/Screenplay Gathered around New Delhi\'s Red Fort to celebrate their values.\r\nPlease come and celebrated with us to see the best inside the selected Prizes and values they have made in the market area.', 'Cannaught Place, New Delhi - 110004', 'story@yopmail.com', '96589666588', 'eventlogo/1573193557_index.jpeg', 'eventbanner/1573193557_Writing-Out-NEW-min.jpg', '11/15/2019', '11/22/2019', 'QWERTY', 0, '2019-11-08 06:12:37', '2019-11-16 15:27:00'),
(7, 1, 'Scheduled Event in a Row', 'To check whether the event process is in the same as I have noticed earlier.', 'Noida sector - 63, Uttar Pradesh', 'logo@yopmail.com', '9874568965', 'eventlogo/1573204468_logo3.jpg', 'eventbanner/1573204468_event1.jpeg', '11/14/2019', '11/22/2019', 'QWERTY', 0, '2019-11-08 09:14:28', '2019-11-16 15:27:08'),
(8, 1, 'Atlease 50 Exibitors/ 50 Sponsors/ 50 Speakers', 'Atlease 50 Exibitors/ 50 Sponsors/ 50 SpeakersAtlease 50 Exibitors/ 50 Sponsors/ 50 SpeakersAtlease 50 Exibitors/ 50 Sponsors/ 50 SpeakersAtlease 50 Exibitors/ 50 Sponsors/ 50 SpeakersAtlease 50 Exibitors/ 50 Sponsors/ 50 SpeakersAtlease 50 Exibitors/ 50 Sponsors/ 50 SpeakersAtlease 50 Exibitors/ 50 Sponsors/ 50 SpeakersAtlease 50 Exibitors/ 50 Sponsors/ 50 Speakers', 'Yamuna vihar, New Delhi', 'email@gmail.com', '9655564655', 'eventlogo/1573207057_logo4.png', 'eventbanner/1573207057_event2.jpeg', '11/20/2019', '11/29/2019', 'QWERTY', 0, '2019-11-08 09:57:37', '2019-11-16 15:27:04'),
(9, 1, 'wfgawfe', 'qeqwe', 'werwer wer', 'sfga@dfjk.com', '2343424234', 'eventlogo/1573217587_fs.PNG', 'eventbanner/1573217587_blanche-dental-clinic-138008.jpg', '11/08/2019', '11/20/2019', 'qwerty', 2, '2019-11-08 12:53:07', '2019-11-08 13:01:01'),
(10, 1, 'GIPEX', 'The Department of Energy, Go-Invest and Valiant Business Media are most excited to host the 2nd edition of GIPEX, an exclusive convention in the capital city of Guyana. We are pleased to announce that GIPEX 2018 has exceeded expectations in the level of participation, interest and support for the summit in its inaugural year with global participation of 600 delegates and 200 international and national oil & gas companies, cementing GIPEX as South America’s largest petroleum summit up to date.', 'Guyana Marriott Hotel Georgetown Block Alpha, Battery Road, Kingston Georgetown', 'info@guyanaoilexpo.com', '+918882507699', 'eventlogo/1574005189_GIPEX.jpg', 'eventbanner/1574005068_coverET.jpg', '11/20/2019', '11/22/2019', 'GIPEX2019', 1, '2019-11-16 15:48:02', '2019-11-20 09:30:12'),
(11, 1, 'EMC', 'European Mining Convention EMC - 2019 aims at connecting the European Mining Industry with the rest of the world to reframe sustainability with innovation and experience. This event will demonstrate the latest technology and innovative solutions to various stakeholders in the industry to take this opportunity to expand connections, launch new products and forge new business partnerships.\r\n\r\nEMC aims to bring together leading International Mining & Quarrying companies, Mining Ministries, Geological Research Authorities & Surveys, Mining Chambers & Associations, Service & Technology Providers, Mining Thought Leaders, Investors, Consulting Organisations and Researchers to exchange their experiences and explore numerous business opportunities through excellent networking & exhibiting platform. EMC - 2019 also provides a top-notch interdisciplinary forum for various stakeholders in the mining industry to present and discuss the most recent innovations, trends & concerns, practical challenges encountered and the solutions adopted in the field of Mining, Material, and Metallurgical industries.', 'The Convention Centre Dublin Ireland', 'suhaib.mir@vakiantbmedia.com', '+918882507699', 'eventlogo/1574421625_198-x-98.jpg', 'eventbanner/1574421625_Emc.jpg', '12/03/2019', '12/04/2019', 'EMC2019', 1, '2019-11-22 11:05:54', '2019-11-22 11:20:25'),
(12, 1, 'And Even', 'Event Des', 'Noida sector 16, Uttar pradesh', 'grv@yopmail.com', '9876543210', 'eventlogo/1574659565_notifyiconnew.png', 'eventbanner/1574659565_notifyiconnew.png', '11/26/2019', '11/27/2019', '12345', 0, '2019-11-25 05:26:05', '2019-11-27 05:52:07');

-- --------------------------------------------------------

--
-- Table structure for table `event_details`
--

CREATE TABLE `event_details` (
  `id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `feature_id` int(11) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `form_name` varchar(100) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_details`
--

INSERT INTO `event_details` (`id`, `event_id`, `feature_id`, `type`, `form_name`, `updated_at`, `created_at`) VALUES
(6, 1, 1, 'about', 'About us', '2019-10-30 09:05:30', '2019-10-30 09:05:30'),
(7, 1, 2, 'schedule', 'Schedules', '2019-10-30 09:05:39', '2019-10-30 09:05:39'),
(8, 1, 3, 'exhibitors', 'Exhibitors', '2019-10-30 09:05:52', '2019-10-30 09:05:52'),
(9, 1, 4, 'sponsers', 'Sponsors', '2019-10-30 09:06:00', '2019-10-30 10:41:40'),
(10, 1, 5, 'speakers', 'Speakers', '2019-10-30 09:06:06', '2019-10-30 09:06:06'),
(11, 1, 6, 'floor_plan', 'Floor Plan', '2019-10-30 09:06:16', '2019-10-30 09:06:16'),
(12, 2, 1, 'about', 'About This Event', '2019-11-04 13:36:06', '2019-11-04 13:36:06'),
(13, 2, 1, 'about', 'I Want Another Update', '2019-11-04 13:36:44', '2019-11-04 13:36:44'),
(15, 2, 2, 'schedule', 'Schedules', '2019-11-04 13:38:13', '2019-11-04 13:38:13'),
(16, 2, 3, 'exhibitors', 'Exhibitors', '2019-11-04 13:38:33', '2019-11-04 13:38:33'),
(17, 2, 4, 'sponsers', 'Sponsors', '2019-11-04 13:38:43', '2019-11-04 13:38:43'),
(18, 2, 5, 'speakers', 'Speakers', '2019-11-04 13:38:50', '2019-11-04 13:38:50'),
(19, 2, 6, 'floor_plan', 'Floor Maps', '2019-11-04 13:38:59', '2019-11-04 13:38:59'),
(21, 3, 1, 'about', 'About', '2019-11-05 05:15:32', '2019-11-05 05:15:32'),
(22, 3, 1, 'about', 'About more', '2019-11-05 05:15:44', '2019-11-05 05:15:44'),
(23, 3, 2, 'schedule', 'Schedules', '2019-11-05 06:25:25', '2019-11-05 06:25:25'),
(24, 4, 1, 'about', 'company name', '2019-11-06 05:38:07', '2019-11-06 05:38:07'),
(25, 4, 2, 'schedule', 'schedule', '2019-11-06 05:38:35', '2019-11-06 05:38:35'),
(26, 4, 3, 'exhibitors', 'exhibitor', '2019-11-06 05:38:46', '2019-11-06 05:38:46'),
(27, 4, 4, 'sponsers', 'sponser', '2019-11-06 05:39:00', '2019-11-06 05:39:00'),
(28, 4, 6, 'floor_plan', 'floor plan', '2019-11-06 05:39:11', '2019-11-06 05:39:11'),
(29, 5, 1, 'about', 'aBOUT US', '2019-11-06 05:46:21', '2019-11-06 05:46:21'),
(30, 5, 3, 'exhibitors', 'EXIBITOR', '2019-11-06 05:46:37', '2019-11-06 05:46:37'),
(31, 5, 6, 'floor_plan', 'FP', '2019-11-06 05:46:45', '2019-11-06 05:46:45'),
(32, 5, 2, 'schedule', 'schedule', '2019-11-07 05:40:08', '2019-11-07 05:40:08'),
(33, 6, 1, 'about', 'About This Event', '2019-11-08 06:13:14', '2019-11-08 06:13:14'),
(34, 6, 2, 'schedule', 'Schedules', '2019-11-08 06:13:32', '2019-11-08 06:13:32'),
(35, 6, 2, 'schedule', 'Schedules_1', '2019-11-08 06:13:55', '2019-11-08 06:13:55'),
(36, 6, 2, 'schedule', 'Schedules_2', '2019-11-08 06:14:04', '2019-11-08 06:14:04'),
(37, 6, 2, 'schedule', 'Schedules_3', '2019-11-08 06:14:13', '2019-11-08 06:14:13'),
(38, 6, 3, 'exhibitors', 'Ministries', '2019-11-08 06:14:25', '2019-11-08 06:14:25'),
(39, 6, 3, 'exhibitors', 'Story Writers', '2019-11-08 06:14:38', '2019-11-08 06:14:38'),
(40, 6, 3, 'exhibitors', 'Screenplay Writers', '2019-11-08 06:14:49', '2019-11-08 06:14:49'),
(41, 6, 4, 'sponsers', 'They Provide Money', '2019-11-08 06:15:07', '2019-11-08 06:15:07'),
(42, 6, 4, 'sponsers', 'A Big Sponsors name That can damage the UI design', '2019-11-08 06:15:27', '2019-11-08 06:15:27'),
(43, 6, 5, 'speakers', 'These are the Speakers', '2019-11-08 06:15:45', '2019-11-08 06:15:45'),
(44, 6, 5, 'speakers', 'A big name of the Speaker to make UI uncomfortable', '2019-11-08 06:16:07', '2019-11-08 06:16:07'),
(45, 6, 6, 'floor_plan', 'Map view of the Park where we have the Event', '2019-11-08 06:16:32', '2019-11-08 06:16:32'),
(46, 7, 1, 'about', 'Event Description', '2019-11-08 09:16:13', '2019-11-08 09:16:13'),
(47, 7, 2, 'schedule', 'Seminars', '2019-11-08 09:16:36', '2019-11-08 09:16:36'),
(48, 7, 2, 'schedule', 'Matches Between Teams', '2019-11-08 09:17:02', '2019-11-08 09:17:02'),
(49, 7, 2, 'schedule', 'Quiz Competition', '2019-11-08 09:17:19', '2019-11-08 09:17:19'),
(50, 7, 5, 'speakers', 'Controllers', '2019-11-08 09:18:07', '2019-11-08 09:18:07'),
(51, 8, 1, 'about', 'About this Event', '2019-11-08 09:58:09', '2019-11-08 09:58:09'),
(52, 8, 2, 'schedule', 'Schedules', '2019-11-08 09:58:20', '2019-11-08 09:58:20'),
(53, 8, 3, 'exhibitors', 'Exhibitors', '2019-11-08 09:58:30', '2019-11-08 09:58:30'),
(54, 8, 4, 'sponsers', 'Sponsors', '2019-11-08 09:58:38', '2019-11-08 09:58:38'),
(55, 8, 5, 'speakers', 'Speakers', '2019-11-08 09:58:45', '2019-11-08 09:58:45'),
(56, 8, 6, 'floor_plan', 'Floor Plan', '2019-11-08 09:58:57', '2019-11-08 09:58:57'),
(57, 9, 4, 'sponsers', 's', '2019-11-08 12:53:12', '2019-11-08 12:53:12'),
(58, 9, 4, 'sponsers', 'p', '2019-11-08 12:53:17', '2019-11-08 12:53:17'),
(59, 9, 4, 'sponsers', 'o', '2019-11-08 12:53:20', '2019-11-08 12:53:20'),
(69, 10, 4, 'sponsers', 'Hosted By', '2019-11-16 17:42:58', '2019-11-16 17:42:58'),
(70, 10, 4, 'sponsers', 'Organizers', '2019-11-16 17:43:21', '2019-11-16 17:43:21'),
(84, 10, 3, 'exhibitors', 'Supporting Partners', '2019-11-16 18:06:31', '2019-11-16 18:06:31'),
(85, 10, 3, 'exhibitors', 'Strategic Partner', '2019-11-16 18:07:15', '2019-11-16 18:07:15'),
(86, 10, 3, 'exhibitors', 'Lead Sponsors', '2019-11-16 18:07:48', '2019-11-16 18:07:48'),
(87, 10, 3, 'exhibitors', 'Diamond Sponsors', '2019-11-16 18:08:05', '2019-11-16 18:08:05'),
(88, 10, 3, 'exhibitors', 'Gold Sponsors', '2019-11-16 18:08:43', '2019-11-16 18:08:43'),
(89, 10, 3, 'exhibitors', 'Silver Sponsors', '2019-11-16 18:09:00', '2019-11-16 18:09:00'),
(90, 10, 3, 'exhibitors', 'Bronze Sponsors', '2019-11-16 18:09:34', '2019-11-16 18:09:34'),
(91, 10, 3, 'exhibitors', 'Associate Sponsors', '2019-11-16 18:09:58', '2019-11-16 18:09:58'),
(92, 10, 3, 'exhibitors', 'Gold Exhibitor', '2019-11-16 18:10:19', '2019-11-16 18:10:19'),
(93, 10, 3, 'exhibitors', 'Exhibitors', '2019-11-16 18:10:37', '2019-11-16 18:10:37'),
(94, 10, 4, 'sponsers', 'Media Partners', '2019-11-16 18:12:39', '2019-11-16 18:12:39'),
(95, 10, 5, 'speakers', 'Speakers', '2019-11-16 18:13:00', '2019-11-16 18:13:00'),
(101, 10, 2, 'schedule', 'Schedule', '2019-11-17 10:27:11', '2019-11-17 10:27:11'),
(102, 10, 6, 'floor_plan', 'Floor Plan', '2019-11-17 10:27:21', '2019-11-17 10:27:21'),
(103, 10, 1, 'about', 'About Event', '2019-11-17 10:27:41', '2019-11-17 10:27:41'),
(105, 11, 4, 'sponsers', 'Organizers', '2019-11-27 05:55:45', '2019-11-27 05:55:45'),
(106, 11, 3, 'exhibitors', 'Supporting Partners', '2019-11-27 05:56:15', '2019-11-27 05:56:15'),
(107, 11, 3, 'exhibitors', 'Lead Sponsors', '2019-11-27 05:56:54', '2019-11-27 05:56:54'),
(108, 11, 3, 'exhibitors', 'Bronze Sponsors', '2019-11-27 05:57:05', '2019-11-27 05:57:05'),
(109, 11, 3, 'exhibitors', 'Exhibitors', '2019-11-27 05:57:40', '2019-11-27 05:57:40'),
(110, 11, 4, 'sponsers', 'Media Partners', '2019-11-27 05:58:08', '2019-11-27 05:58:08'),
(111, 11, 5, 'speakers', 'Speakers', '2019-11-27 05:58:32', '2019-11-27 05:58:32'),
(113, 11, 2, 'schedule', 'Schedule', '2019-11-27 05:59:03', '2019-11-27 05:59:03'),
(114, 11, 6, 'floor_plan', 'Floor Plan', '2019-11-27 05:59:12', '2019-11-27 05:59:12'),
(115, 11, 1, 'about', 'About Event', '2019-11-27 05:59:34', '2019-11-27 05:59:34');

-- --------------------------------------------------------

--
-- Table structure for table `exhibitors`
--

CREATE TABLE `exhibitors` (
  `id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `detailsid` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `booth` varchar(250) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `description` longtext,
  `toprated` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exhibitors`
--

INSERT INTO `exhibitors` (`id`, `event_id`, `userid`, `detailsid`, `name`, `booth`, `image`, `website`, `email`, `phone`, `description`, `toprated`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 8, 'Ministry of Energy, Mines and Sustainable Development, Kingdom of Morocco', '102', 'exhibitorlogo/1572426681_Minister-of-Energy-Mines-and-Sustainable-Development-300x144.jpg', 'www.mem.gov.ma', 'mem@gmail.com', '9876543212', 'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum', 0, '2019-10-30 09:11:21', '2019-10-30 09:11:21'),
(5, 2, 7, 16, 'AC tech', 'Booth no - 12', 'exhibitorlogo/1572875360_NG_3_blog-header_2160x1080px-1080x540.jpeg', 'www.ac.in', 'ac@yopmail.com', '90988989138', 'Ankit Mittal a greate backend developer', 1, '2019-11-04 13:49:20', '2019-11-04 13:49:20'),
(6, 2, 7, 16, 'XC Pvt. Ltd', 'Booth no - 40', 'exhibitorlogo/1572875419_Ocp.jpg', 'www.story.com', 'xc@yopmail.com', '745745646', 'Along with Amnas Zain', 0, '2019-11-04 13:50:19', '2019-11-04 13:50:19'),
(7, 4, 1, 26, 'test exhibitor', '123', 'exhibitorlogo/1573018883_images1.jpg', 'www.test.com', 'test@tets.com', '9596775491', '.kjSBDc.KJSBDK>JSBD.KJSDB.kjSBD.kjbad', 1, '2019-11-06 05:41:23', '2019-11-06 05:41:23'),
(8, 5, 1, 30, 'CVB', '4', 'exhibitorlogo/1573019264_images.jpg', 'www.test.com', 'meer.nayn@gmail.com', '+918882507699', 'HJJKKKK', 1, '2019-11-06 05:47:44', '2019-11-06 05:47:44'),
(9, 5, 1, 30, 'HHHHHHH', '12', 'exhibitorlogo/1573019360_images.jpg', 'www.test.com', 'meer.nayn@gmail.com', '+918882507699', 'ASDDDD', 0, '2019-11-06 05:49:20', '2019-11-06 05:49:20'),
(10, 6, 1, 38, 'Exhibitor_COmpany_1', '1', 'exhibitorlogo/1573195373_logo1.png', 'www.freefunkar.com', 'gaiop@yopmaiilc.om', '998564646546', 'Description to Text valuie', 0, '2019-11-08 06:42:53', '2019-11-08 06:42:53'),
(11, 6, 1, 38, 'Exhibitor_COmpany_2', '2000', 'exhibitorlogo/1573195442_logo2.jpeg', 'www.galgotia.com', 'grv@yopmail.com', '5464654646', 'Description of thr ebskjnksdfbksd  Description of thr ebskjnksdfbksd  Description of thr ebskjnksdfbksd  Description of thr ebskjnksdfbksd  Description of thr ebskjnksdfbksd  Description of thr ebskjnksdfbksd  Description of thr ebskjnksdfbksd  Description of thr ebskjnksdfbksd  Description of thr ebskjnksdfbksd  Description of thr ebskjnksdfbksd', 1, '2019-11-08 06:44:02', '2019-11-08 06:44:02'),
(12, 8, 1, 53, 'Exhibitor_COmpany_1', '1', 'exhibitorlogo/1573207430_logo1.png', 'www.one.com', 'one@yopmail.com', '9999999998', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:03:50', '2019-11-08 10:03:50'),
(13, 8, 1, 53, 'Exhibitor_COmpany_2', '2', 'exhibitorlogo/1573207461_logo2.jpeg', 'www.two.com', 'sdb@fdskg.jksdf', '6546465465456', 'asfdafsasd safd ds dsfg asgasf', 0, '2019-11-08 10:04:21', '2019-11-08 10:04:21'),
(14, 8, 1, 53, 'Exhibitor_COmpany_3', '3', 'exhibitorlogo/1573207488_logo3.jpg', 'dfgfdg.dydhy.ertyert', 'reterz@fdhfg.gfyh', '43653654654', 'f sgfg fdsh hg rtyttryt yrtyrt ytr ytytwe ye yr', 0, '2019-11-08 10:04:48', '2019-11-08 10:04:48'),
(15, 8, 1, 53, 'Exhibitor_COmpany_4', '4', 'exhibitorlogo/1573207517_logo3.png', 'dsf.dfgfd.dfg', 'fdgdsf@frhrgtj.ftrge', '3464326546546', 'fgfds gesf geryewr yerw tyewr tyewry erw erwy wery y rey  eww', 0, '2019-11-08 10:05:17', '2019-11-08 10:05:17'),
(16, 8, 1, 53, 'Exhibitor_COmpany_5', '5', 'exhibitorlogo/1573207545_logo4.png', 'dfsg.detgery.erter', 'fegf@gfhgf.ewrew', '356547567865', 'df gh fyh t  tett ewytw ytr ytr y ytr yt ty ty try rteyeyyy', 0, '2019-11-08 10:05:45', '2019-11-08 10:05:45'),
(17, 8, 1, 53, 'Exhibitor_COmpany_6', '6', 'exhibitorlogo/1573207578_man2.jpeg', 'fdgdfgh.hyrty.etytry', 'rtyrte@ty.ret', '23554654654', 'gf hg yyytyruyut yreu vgdfh h rty tr gh h f hffj g rthrt', 0, '2019-11-08 10:06:18', '2019-11-08 10:06:18'),
(18, 8, 1, 53, 'Exhibitor_COmpany_7', '7', 'exhibitorlogo/1573207612_man4.jpeg', 'fdsyrty.eyt.etytr', 'fhgfdhdf@hfhj.fdg', '76867877777765', 'fg sdghdh rt ytyryu yikt uiyt tyuyutyutyu tr yru yet', 0, '2019-11-08 10:06:52', '2019-11-08 10:06:52'),
(19, 8, 1, 53, 'Exhibitor_COmpany_8', '8', 'exhibitorlogo/1573207644_man3.jpeg', 'www.fegrh.fvui', 'fgjhfgd@rtretyrt.rty', '43677657865', 'FDfhgf jdfj ryuery uryreuyter ytyre uyt ti  tryutye utyei ty i', 1, '2019-11-08 10:07:24', '2019-11-08 10:07:24'),
(20, 8, 1, 53, 'Exhibitor_COmpany_9', '9', 'exhibitorlogo/1573207679_man4.jpeg', 'trert.ertret.try', 'fsdtgfd@fgjhg.try', '346546745656', 'ghr yhr ey ty t', 1, '2019-11-08 10:07:59', '2019-11-08 10:07:59'),
(21, 8, 1, 53, 'Exhibitor_COmpany_10', '10', 'exhibitorlogo/1573207708_man5.jpeg', 'fhg.tyhrt.tfrytr', 'fsdtgdjh@rtuy.try', '6578658756', 'fvg dut67 5867m6665fd j', 0, '2019-11-08 10:08:28', '2019-11-08 10:08:28'),
(22, 8, 1, 53, 'Exhibitor_COmpany_11', '11', 'exhibitorlogo/1573207737_logo1.png', 'dfsghh.rtfuyr.tryrtyr', 'rftge@ghgf.gftyh', '65867876865', 'dfh rtg rt yytu tyuyr utyrryt tryt yt yuyt uty ut u yt', 0, '2019-11-08 10:08:57', '2019-11-08 10:08:57'),
(23, 8, 1, 53, 'Exhibitor_COmpany_12', '12', 'exhibitorlogo/1573207759_logo2.jpeg', 'dfst.trytr.trytry', 'xcvhfg@fghfg.frter', '56867876876', '56 hhghjkjs ghhiuh iueftiureuhThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:09:19', '2019-11-08 10:09:19'),
(24, 8, 1, 53, 'Exhibitor_COmpany_13', '13', 'exhibitorlogo/1573207780_logo3.jpg', 'sdgf.tyu.rtyr', 'hfd@gfjh.yhg', '765765', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:09:40', '2019-11-08 10:09:40'),
(25, 8, 1, 53, 'Exhibitor_COmpany_14', '14', 'exhibitorlogo/1573207817_logo3.png', 'efyretf.rtytry.try', 'dghgdgf@gfjh.gtf', '5767876876876', 'df gh rtgy tr yrt yty tr yry rtyr uyyu rtu ur ureu yThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:10:17', '2019-11-08 10:10:17'),
(26, 8, 1, 53, 'Exhibitor_COmpany_15', '15', 'exhibitorlogo/1573207850_logo4.png', 'sdfhy.gtyrtuy.try', 'gfd@ghgf.fgh', '6786867897', 'sfg dsfh dshy teyttr ytwytyuj uyre yt ytwtteyw reytyytwe ye', 0, '2019-11-08 10:10:50', '2019-11-08 10:10:50'),
(27, 8, 1, 53, 'Exhibitor_COmpany_16', '16', 'exhibitorlogo/1573207883_man2.jpeg', 'fd.fgtuyhgj.dyh', 'fhgdgf@thytrdj.df', '769687685457', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:11:23', '2019-11-08 10:11:23'),
(28, 8, 1, 53, 'Exhibitor_COmpany_17', '17', 'exhibitorlogo/1573207910_logo3.png', 'rre.tryry.rtyt', 'fhujrd@gjuy.com', '69877687567', 'cxfg dhrtg uyrt ytu tThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:11:50', '2019-11-08 10:11:50'),
(29, 8, 1, 53, 'Exhibitor_COmpany_18', '18', 'exhibitorlogo/1573207935_logo2.jpeg', 'frhy.fgtuyh.trfy', 'ghgf@fgjhg.com', '67897887', 'hj r This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:12:15', '2019-11-08 10:12:15'),
(30, 8, 1, 53, 'Exhibitor_COmpany_19', '19', 'exhibitorlogo/1573207957_logo3.png', 'ghgf.dfyrtyrt.dtytr', 'fhfg@ghgf.dgh', '7987987987987', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 1, '2019-11-08 10:12:37', '2019-11-08 10:12:37'),
(31, 8, 1, 53, 'Exhibitor_COmpany_20', '20', 'exhibitorlogo/1573207987_logo1.png', 'fghgf.rftuytu.dtyt', 'fdgfd@gfjhg.com', '87967865876', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:13:07', '2019-11-08 10:13:07'),
(32, 8, 1, 53, 'Exhibitor_COmpany_21', '21', 'exhibitorlogo/1573208018_logo4.png', 'dfg.yuityuyhuytu.rrty', 'fghgfd@trutytry.rte', '6986789757664', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:13:38', '2019-11-08 10:13:38'),
(33, 8, 1, 53, 'Exhibitor_COmpany_22', '22', 'exhibitorlogo/1573208056_logo1.png', 'dfg.yttrytrtryrt.yhtry', 'rtgyhfjgik@ryuty.rete', '879885764', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:14:16', '2019-11-08 10:14:16'),
(34, 8, 1, 53, 'Exhibitor_COmpany_23', '23', 'exhibitorlogo/1573208080_logo3.jpg', 'fdgfd.tytrytr.tyrty', 'dfhfgjhgki@gfjhg.dfgy', '799685765675', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:14:40', '2019-11-08 10:14:40'),
(35, 8, 1, 53, 'Exhibitor_COmpany_24', '24', 'exhibitorlogo/1573208107_logo2.jpeg', 'fdhgfd.tytrytr.tytry', 'fgdytuyt@ghfg.fgth', '7987865765', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:15:07', '2019-11-08 10:15:07'),
(36, 8, 1, 53, 'Exhibitor_COmpany_25', '25', 'exhibitorlogo/1573208137_logo3.png', 'vcxhfg.ftgyrtuy.rte', 'fdhfghghj@yuiyi.ftgre', '9865865478', 'fghgfjht nmThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:15:37', '2019-11-08 10:15:37'),
(37, 8, 1, 53, 'Exhibitor_COmpany_26', '26', 'exhibitorlogo/1573208165_logo3.jpg', 'fdgfd.yuytu.rtytry', 'tryrt@rtyu.tyry', '97876857665', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:16:05', '2019-11-08 10:16:05'),
(38, 8, 1, 53, 'Exhibitor_COmpany_27', '27', 'exhibitorlogo/1573208195_logo4.png', 'dfghf.rytreyrt.rty', 'grtuyhytut@futy.fty', '98678658656', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:16:35', '2019-11-08 10:16:35'),
(39, 8, 1, 53, 'Exhibitor_COmpany_28', '28', 'exhibitorlogo/1573208236_logo1.png', 'dfhfg.ftuyrtyrt.tryr', 'hgk@jutyu.tryr', '96654766575', '7This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 1, '2019-11-08 10:17:16', '2019-11-08 10:17:16'),
(40, 8, 1, 53, 'Exhibitor_COmpany_29', '29', 'exhibitorlogo/1573208269_man2.jpeg', 'dhg.dtyreeruyy.ery t', 'ghjfg@tyiyu.rtyry', '97565854754', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:17:49', '2019-11-08 10:17:49'),
(41, 8, 1, 53, 'Exhibitor_COmpany_30', '30', 'exhibitorlogo/1573208298_logo2.jpeg', 'asd.reytrerytry.ety', 'ruytyr@ruy.dhy', '97678567654654', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:18:18', '2019-11-08 10:18:18'),
(42, 8, 1, 53, 'Exhibitor_COmpany_31', '31', 'exhibitorlogo/1573208327_logo1.png', 'dfsg.etyrttryr.er rtyr', 'tytr@yujyt.com', '4577896789689', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:18:47', '2019-11-08 10:18:47'),
(43, 8, 1, 53, 'Exhibitor_COmpany_32', '32', 'exhibitorlogo/1573208359_logo3.jpg', 'edtg.trfuyrty.ty', 'rtyrte@ty.ret', '9786758', '65hgj hThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:19:19', '2019-11-08 10:19:19'),
(44, 8, 1, 53, 'Exhibitor_COmpany_33', '33', 'exhibitorlogo/1573208383_logo3.jpg', 'dfg.rtuy.ftgyh', 'rtyrte@ty.ret', '68967568', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:19:43', '2019-11-08 10:19:43'),
(45, 8, 1, 53, 'Exhibitor_COmpany_34', '34', 'exhibitorlogo/1573208406_logo3.png', 'www.fegrh.fvui', 'reterz@fdhfg.gfyh', '9655564655', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:20:06', '2019-11-08 10:20:06'),
(46, 8, 1, 53, 'Exhibitor_COmpany_35', '35', 'exhibitorlogo/1573208437_logo1.png', 'www.goppm.com', 'email@gmail.com', '5687678567', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:20:37', '2019-11-08 10:20:37'),
(47, 8, 1, 53, 'Exhibitor_COmpany_36', '36', 'exhibitorlogo/1573208461_logo2.jpeg', 'www.galgotia.com', 'reterz@fdhfg.gfyh', '9631525421454', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:21:01', '2019-11-08 10:21:01'),
(48, 8, 1, 53, 'Exhibitor_COmpany_37', '37', 'exhibitorlogo/1573208486_logo3.jpg', 'www.facebook.com', 'fegf@gfhgf.ewrew', '5464654646', 'sdfg dfh hhfgThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:21:26', '2019-11-08 10:21:26'),
(49, 8, 1, 53, 'Exhibitor_COmpany_38', '38', 'exhibitorlogo/1573208521_man2.jpeg', 'www.galgotia.com', 'email@mainleast.com', '879885764', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:22:01', '2019-11-08 10:22:01'),
(50, 8, 1, 53, 'Exhibitor_COmpany_39', '39', 'exhibitorlogo/1573208544_logo2.jpeg', 'www.fegrh.fvui', 'etadmin@gmail.com', '7014552245', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:22:24', '2019-11-08 10:22:24'),
(51, 8, 1, 53, 'Exhibitor_COmpany_40', '40', 'exhibitorlogo/1573208565_logo3.png', 'www.goppm.com', 'email@mainleast.com', '879885764', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:22:45', '2019-11-08 10:22:45'),
(52, 8, 1, 53, 'Exhibitor_COmpany_41', '41', 'exhibitorlogo/1573208588_logo3.jpg', 'www.galgotia.com', 'reterz@fdhfg.gfyh', '5464654646', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:23:08', '2019-11-08 10:23:08'),
(53, 8, 1, 53, 'Exhibitor_COmpany_42', '42', 'exhibitorlogo/1573208623_man4.jpeg', 'https://facebook.com', 'rtyrte@ty.ret', '879885764', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:23:43', '2019-11-08 10:23:43'),
(54, 8, 1, 53, 'Exhibitor_COmpany_43', '43', 'exhibitorlogo/1573208644_logo3.png', 'www.fegrh.fvui', 'rtyrte@ty.ret', '9631525421454', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:24:04', '2019-11-08 10:24:04'),
(55, 8, 1, 53, 'Exhibitor_COmpany_44', '44', 'exhibitorlogo/1573208665_logo4.png', 'https://facebook.com', 'email@mainleast.com', '9631525421454', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:24:25', '2019-11-08 10:24:25'),
(56, 8, 1, 53, 'Exhibitor_COmpany_45', '45', 'exhibitorlogo/1573208731_logo4.png', 'www.facebook.com', 'etadmin@gmail.com', '9655564655', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:25:31', '2019-11-08 10:25:31'),
(57, 8, 1, 53, 'Exhibitor_COmpany_46', '46', 'exhibitorlogo/1573208753_logo3.png', 'www.goppm.com', 'reterz@fdhfg.gfyh', '5464654646', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:25:53', '2019-11-08 10:25:53'),
(58, 8, 1, 53, 'Exhibitor_COmpany_47', '47', 'exhibitorlogo/1573208774_man2.jpeg', 'https://twitter.com', 'email@mainleast.com', '9631525421454', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:26:14', '2019-11-08 10:26:14'),
(59, 8, 1, 53, 'Exhibitor_COmpany_48', '48', 'exhibitorlogo/1573208801_man3.jpeg', 'www.facebook.com', 'reterz@fdhfg.gfyh', '879885764', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:26:41', '2019-11-08 10:26:41'),
(60, 8, 1, 53, 'Exhibitor_COmpany_49', '49', 'exhibitorlogo/1573208826_man4.jpeg', 'www.galgotia.com', 'rtyrte@ty.ret', '7014552245', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:27:06', '2019-11-08 10:27:06'),
(61, 8, 1, 53, 'Exhibitor_COmpany_50', '50', 'exhibitorlogo/1573208846_man3.jpeg', 'www.fegrh.fvui', 'email@gmail.com', '5464654646', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:27:26', '2019-11-08 10:27:26'),
(62, 10, 1, 84, 'Georgetown Chamber of Commerce & Industry', 'AE14', 'exhibitorlogo/1573930731_sp1.jpg', 'www.gcci.gy', 'info@gcci.gy', '+592-226-3519, 225-5846', 'The Georgetown Chamber of Commerce and Industry was established under the name “The Chamberof Commerce of the City of Georgetown” in the year 1889. The formation of the chamber begun at a meeting with Royal Agricultural Commercial Society and a Commercial Committee held on the 20 the December 1888. Under the initiative of Mr. J. Errest Tinner a sphere of useful discussions were held and it was decided to merge these two bodies. On the 17th June, 1889 at a meeting of Merchants and others it was decided to form a Chamber of Commerce for the City, which should be recognized by law. The Memorandum and Articles of Association were drafted. These were submitted at a General Meeting held on the 8th July, 1889 and the President, Vice-Presidents and Council were elected.', 0, '2019-11-16 18:58:51', '2019-11-16 18:58:51'),
(63, 10, 1, 84, 'Private Sector Comnission', 'AE7', 'exhibitorlogo/1573931092_sp2.jpg', 'www.psc.org.gy', '*****@*****.com', '+592 223 0875', 'The Private Sector Commission of Guyana was established in 1992 by five Private Sector Associations with the aim of bringing together all Private Sector Organs and Business Entities under the purview of being one National Body.\r\n\r\nThe Private Sector Commission is governed by a Council which comprises of the Head of all Members Sectoral Organizations and a number of elected members.  The Council is headed by a Chairman who can serve a maximum of two consecutive one-year terms.  Any Chairman who has served two consecutive terms may be eligible for subsequent re-election.\r\n\r\nThe overall activities of the Commission are coordinated by an Executive Management Committee which comprises the following elected officials: Chairman, Vice Chairman, Honorary Secretary, and Honorary Treasurer.  The Executive Director is also a member of the committee by appointment.', 0, '2019-11-16 19:04:53', '2019-11-16 19:04:53'),
(66, 10, 1, 86, 'Hess', 'LS1', 'exhibitorlogo/1573933349_Hess.jpg', 'www.hess.com', '*****@*****.com', '212-997-8500', 'Hess Corporation is a leading global independent energy company engaged in the exploration and production of crude oil and natural gas globally. Hess operates across four continents with operations in the U.S. Europe, Asia and South America. Hess has industry-leading positions in the U.S. as an operator in the leading shale play of the Bakken and is also as one of the largest producers in the deepwater Gulf of Mexico. In addition Hess is a key producer in Malaysia, Thailand, Denmark and Libya. The company is engaged in the exploration, development and production of oil and gas resources offshore Guyana where we are participating in one of the industry’s largest oil discoveries in the past decade, with the first phase of a planned multi-phase development of the Stabroek Block in Guyana underway. To find out more about how Hess is working to become the world’s most trusted energy partner.', 0, '2019-11-16 19:42:29', '2019-11-16 19:42:29'),
(67, 10, 1, 85, 'Exxon Mobil', 'SP', 'exhibitorlogo/1573933476_newreqexon1.jpg', 'www.ExxonMobil.com/Guyana', 'guyanastaff@exxonmobil.com', '592-231-2866', 'ExxonMobil is the world\'s largest publicly traded international oil and gas company. It holds an industry-leading inventory of global oil and gas resources. It is the world\'s largest refiner and marketer of petroleum products, and its chemical company ranks among the world\'s largest. ExxonMobil applies science and innovation to find better, safer and cleaner ways to deliver the energy the world needs. ExxonMobil\'s upstream business encompasses high-quality exploration opportunities across all resource types and geographies, an industry-leading resource base, a portfolio of world-class projects, and a diverse set of producing assets. It has an active exploration or production presence in 36 countries. ExxonMobil is proud to play a leading role in providing the energy the world needs to support economic growth, technological advancement and the well-being of communities around the globe.', 0, '2019-11-16 19:44:36', '2019-11-16 19:44:36'),
(69, 10, 1, 86, 'CNOOC', 'LS3', 'exhibitorlogo/1573933825_cnooc_redblue_cmyk.jpg', 'www.intl.cnoocltd.com', '*****@*****.com', '592-227-8319', 'CNOOC International, the international division of CNOOC Limited, is a global energy company with operations in Asia, Africa, the Americas and Europe. Our successful growth as an upstream oil and gas producer has been achieved by consistently finding and developing world-class assets. Our business is focused on safely exploring and producing from conventional offshore, unconventional and oil sands assets. As part of the CNOOC Group of companies, which span the entire energy supply chain, our size and capability create a competitive advantage that ensures we deliver long-term value. Our growth strategy is to safely and sustainably develop our high-quality portfolio to deliver energy for all.', 0, '2019-11-16 19:50:25', '2019-11-16 19:50:25'),
(70, 10, 1, 86, 'GTT', 'LS2', 'exhibitorlogo/1573934001_GTT.png', 'www.gtt.co.gy', '*****@*****.com', '(592) 231 9541', 'For almost 30 years, GTT has delivered innovative telecommunications to Guyana. We are proud to be an integral part of the community and culture of our people. Whether it is by being the first high speed internet provider, and the first mobile services provider, by providing the digital backbone of e-commerce, or by being the leader in the fiber optic technology, we are committed to the development of our country across the consumer and business landscape. GTT is the only provider with direct subsea cables\' connectivity to Guyana . These are via the fully protected SGSCS which lands in Georgetown , and has full redundancy via Suriname and the Americas II which connects to Ring in Cayenne. GTT is the preferred technology provider of the Oil and Gas Industry in Guyana. For almost 30 years, GTT has delivered innovative telecommunications to Guyana. We are proud to be an integral part of the community and culture of our people. Whether it is by being the first high speed internet pro.', 0, '2019-11-16 19:53:21', '2019-11-16 19:53:21'),
(71, 10, 1, 87, 'Total E&P Americas', 'D1', 'exhibitorlogo/1573934213_d1.jpg', 'www.total.com', '*****@*****.com', '+33 (0)1 47 44 45 46', 'Total’s ambition is to become the responsible energy major. To become the Responsible Energy Major means providing energy that is affordable, reliable and clean. Energy is a vital, constantly changing resource that has accompanied major shifts in society throughout time. And energy must continue to adapt if it is to play a key role in addressing the complex challenges facing the world today. We want to contribute to these changes because energy is Total’s history: its past, its present and its future.', 0, '2019-11-16 19:56:53', '2019-11-16 19:56:53'),
(72, 10, 1, 88, 'Baker Hughes', 'G1', 'exhibitorlogo/1573934468_baker-huges-updated.jpg', 'www.bakerhughes.com', '*****@*****.com', '**********', 'We are the leading energy technology company. We design, manufacture and service transformative technologies to help take energy forward. For more than a century, our inventions have revolutionized energy. We harness the power of engineering, data, and science to redefine what\'s possible. We are global, but we\'re local too. With operations in more than 120 countries we work in partnership with our customers, wherever they are, to deliver better outcomes. We are proud that our people and our businesses are part of the fabric of the communities in which they work.', 0, '2019-11-16 20:01:08', '2019-11-16 20:01:08'),
(73, 10, 1, 88, 'Technip FMC', 'G3', 'exhibitorlogo/1573934630_TechnipFMC.jpg', 'www.technipfmc.com/en/', '*****@*****.com', '+44 (0) 203 429 3950', 'TechnipFMC is a global leader in subsea, onshore, offshore, and surface technologies.\r\nWith our proprietary technologies and production systems, integrated expertise and comprehensive solutions, we are transforming our clients’ project economics.\r\nWe are uniquely positioned to deliver greater efficiency across project lifecycles from concept to project delivery and beyond. Through innovative technologies and improved efficiencies, our offering unlocks new possibilities for our clients in developing their oil and gas resources.', 0, '2019-11-16 20:03:50', '2019-11-16 20:03:50'),
(74, 10, 1, 88, 'Sol Guyana', 'G5', 'exhibitorlogo/1573934845_Sol.jpg', 'www.solpetroleum.com', '*****@*****.com', '(592) 233-0582', 'The world was first introduced to the Sol brand in 2005, when the company was born and the Sol logo was unveiled. Sol (the Spanish word for sun) embodies the spirit of the Caribbean region. Our name and our sunburst logo are direct representations of our team’s energy and commitment to being a central element of all aspects of life in the Caribbean. The Sol visual brand reflects the vibrancy of the Caribbean; our prominent orange and blue colour palette were designed to personify the warm hues of the Caribbean landscape.', 0, '2019-11-16 20:07:25', '2019-11-16 20:07:25'),
(79, 10, 1, 88, 'Clariant', 'G6', 'exhibitorlogo/1573935489_clai.jpg', 'www.clariant.com/deepwater', '*****@*****.com', '**********', 'Clariant Oil Services is the fastest growing international supplier of specialty oilfield production chemicals and services to the oil and gas industry. Our product offerings have an impressive reach extending from Offshore and Deep Water, Conventional and Unconventional Oil & Gas, Paraffin Control Technologies to VERITRAX™, our Intelligent Chemical Management System. Our global presence provides a unique position from which to address and resolve customer challenges anywhere in the world. We use chemistry, systems and experts to understand our customers\' needs, then create high-value, innovative solutions that help our customers win. We strive to develop and deliver the most economical solutions with an unwavering commitment to high standards for health, safety and the environment and work to implement your custom chemical program with the utmost respect to the workforce and community.', 0, '2019-11-16 20:18:09', '2019-11-16 20:18:09'),
(80, 10, 1, 88, 'Tullow', 'G4', 'exhibitorlogo/1573935617_g6.jpg', 'www.tullowoil.com', 'info@tullowoil.com', '+44 (0)203 249 9000', 'Tullow Oil is a leading independent oil and gas exploration and production company. The Group has interests in 80 exploration and production licences across 15 countries which are managed as three Business Teams: West Africa, East Africa and New Ventures. Tullow draws upon multiple resources and relationships to create value. Our business works across all stages of the oil life cycle from exploration to production. Our focus is on delivering value for all three stakeholder groups: our investors, our host countries and our people.', 0, '2019-11-16 20:20:17', '2019-11-16 20:20:17'),
(82, 10, 1, 88, 'Inter Oil Guyana', 'G14', 'exhibitorlogo/1573936169_inter.png', 'www.interoilint.com', 'info@interoilgroup.com', '+44 (0)20 7186 9990', 'InterOil Guyana Inc., is a local Oil & Gas Service provider, which commenced its operations in April 2019, as a joint venture between InterOil Group Ltd., a company with over 40+ years of experience as best in class service providers to the Oil & Gas industry, and Mings Products & Services Ltd., a Guyanese company known for its investments in community development and human resources. The partnership will develop a Shore Base and Logistics Service Centre with integrated managerial and technical resources, a focus on optimizing commercial relationships, technical know-how, HSE (Health, Safety and Environment) and operational efficiency to contribute a sustainable and cost-effective development to serve Guyana’s rapidly expanding Oil & Gas Industry. Phase I of the facility is set to complete by December 2020.', 0, '2019-11-16 20:29:29', '2019-11-16 20:29:29'),
(83, 10, 1, 88, 'Strategic Recruitment Solutions Inc.', 'G8 & G11', 'exhibitorlogo/1573936291_g10.png', 'www.srsgy.com', 'admin@srsgy.com', '+592-608-2960', 'Strategic Recruitment Solutions Inc. (SRS) is 100% Guyanese owned recruiting and HR services company that provides recruitment and placement services to individuals in Guyana. Formed in 2018, we have progressed across all sectors and industries, ensuring our services are accessible to all job seekers and employers. We are known for our consistent professionalism, transparency, inclusivity, and efficiency when hiring and recruiting. In the world of constant evolution and innovation, our tech-savvy recruitment consultancy services provide the necessary mechanisms for qualified personnel and organizations—to cope with the daily dynamics of meeting specialized needs. Our company has helped transformed the long-standing ways of hiring and recruiting individuals in Guyana by shaping and automating the human resource management processes. Our company is primarily designed to serve: candidates who are struggling to find the right organization to work for; and organizations who are', 0, '2019-11-16 20:31:31', '2019-11-16 20:31:31'),
(85, 10, 1, 88, 'JHI', 'G12', 'exhibitorlogo/1573936472_jhi.png', 'www.jhiassociates.com', '*****@*****.com', '705.888.2756', 'JHI is the premier Guyana-focused deepwater exploration company. With leadership actively exploring offshore Guyana since 1998, our world-class technical team has experience around the Atlantic Margin, including with Guyana’s basin-opening discoveries. Along with our Supermajor partners ExxonMobil and Total, and Guyana-based Mid-Atlantic Oil & Gas, Inc., we look forward to writing the next chapter of exploration success in Guyana with the Canje Block.', 0, '2019-11-16 20:34:32', '2019-11-16 20:34:32'),
(86, 10, 1, 88, 'Repsol', 'G10', 'exhibitorlogo/1573936653_rep.jpg', 'www.repsol.com', '*****@*****.com', '(+34) 91 7538100 / 91 7538000', 'Repsol is a global energy company present throughout the whole energy value chain. The company employs more than 25,000 people and its products are sold in more than 90 countries, reaching 10 million customers. Repsol produces over 700,000 barrels of oil equivalent per day and has one of the most efficient refining systems in Europe. The company operates low-emissions electricity generation assets, including photovoltaic and offshore wind power projects. Repsol is a leader in mobility and a pioneer in the development of initiatives that contribute to new solutions and energy sources for transportation. It has over 4,700 service stations in Spain, Portugal, Peru, Italy, and Mexico, offering the most efficient and high quality fuels. Technology and digitalization are key pillars of the company, whose cutting-edge projects in various disciplines have made it a leader in innovation, sustainability, and efficiency in the energy sector.', 0, '2019-11-16 20:37:33', '2019-11-16 20:37:33'),
(87, 10, 1, 88, 'CWSG Inc.', 'G9', 'exhibitorlogo/1573936775_cwsg.jpg', 'www.cwslsolutions.com', 'info@cwsltt.com', '(868) 673-0009', 'We are a Group of Companies, Oil and Gas service providers, working together to provide world class Oil and Gas supply base with two berths to become operational in 2020. ASCO a UK based O&G Supply Base Company, will be the Main Operator providing demonstrated obsession in safety and service delivery readily available to develop the local workforce with the use of local suppliers and local content, a model applied throughout global operations in countries such as Australia, Norway, Canada and Trinidad and Tobago. This supply base will provide integrated logistics services to offshore operators which will include supply of fuel, water, bulks, quayside operations, materials management, pipe yard services, waste management, road transportation, marine services and warehousing facilities. Companies under this umbrella comes with a wealth of experience and expertise in various fields such as Engineering, Procurement, Construction, Welding, Fabricating, Mechanical, Electrical, Instrumentation, Communications, Controls, Industrial Coatings etc. all ready to work to build Local Capacity in Guyana through training and development in these areas. It is envisaged that works normally contracted to foreign firms would be done locally with local workforce and suppliers. We are therefore poised to contribute in the development of Guyana.', 0, '2019-11-16 20:39:35', '2019-11-16 20:39:35'),
(88, 10, 1, 89, 'Schlumberger', 'S1', 'exhibitorlogo/1573937078_Schlumberger.jpg', 'www.slb.com', '*****@*****.com', '**********', 'Schlumberger is the world\'s leading provider of technology for reservoir characterization, drilling, production, and processing to the oil and gas industry. With product sales and services in more than 120 countries and employing approximately 100,000 people who represent over 140 nationalities, Schlumberger supplies the industry\'s most comprehensive range of products and services, from exploration through production, and integrated pore-to-pipeline solutions that optimize hydrocarbon recovery to deliver reservoir performance.', 0, '2019-11-16 20:44:38', '2019-11-16 20:44:38'),
(89, 10, 1, 89, 'Halliburton', 'S3', 'exhibitorlogo/1573937388_s2.jpg', 'www.halliburton.com', '*****@*****.com', '+44 1224 777000', 'Founded in 1919, Halliburton celebrates its 100 years of service as one of the world\'s largest providers of products and services to the energy industry. With 60,000 employees, representing 140 nationalities in more than 80 countries, the company helps its customers maximize value throughout the lifecycle of the reservoir – from locating hydrocarbons and managing geological data, to drilling and formation evaluation, well construction and completion, and optimizing production throughout the life of the asset.', 0, '2019-11-16 20:49:48', '2019-11-16 20:49:48'),
(90, 10, 1, 89, 'Century Tamara Energy Services Inc.', 'S4', 'exhibitorlogo/1573937533_s3.jpg', 'www.ctes-inc.com', 'info@ctes-inc.com', '(592) 226-1406', 'Century Tamara Energy Services Inc. (CTES) is an oil & gas technical and support services company registered in Guyana. Our vision is to become the leading energy company in Southern and Central America, offering cost effective bespoke solutions to our existing and potential clients and partners; our technical partner is the Century Group, operating out of Nigeria, and West Africa.', 0, '2019-11-16 20:52:13', '2019-11-16 20:52:13'),
(91, 10, 1, 90, 'Tiger Rentals', 'B1', 'exhibitorlogo/1573937705_ttnew.jpg', 'www.tigertankstrinidad.com', 'sales@tigertankstrinidad.com', '(1 868) 651-1544 / 651-0130/ 651-1460', 'Tiger Rentals Guyana Incorporated is part of the Tiger Tanks Trinidad Unlimited family whose parent company is Tiger Tanks Offshore Rental Limited of Beaumont, Texas, USA, in business since 1997. Tiger Tanks brings to the energy industry a wealth of experience, innovation and access to the latest industry technological developments, methods and techniques. Tiger Tanks specialises in the manufacture and provision of Transport Containers, Storage Vessels, Cargo Carrying Equipment (CCEs) and tanks, vacuum tankers, pumps, steam cleaners, and specialized safety equipment. In addition Tiger Tanks offers high quality services including Waste Management with state of the art Thermal technologies, Emergency and Spill Response, Confined Space and Tank Cleaning (offshore/onshore), Cooling Tower Cleaning, Tank Repairs and Installations, Solid and Liquid hazardous and non-hazardous waste transport, Port and Logistics Handling and Management and Environmental Consultancy.', 0, '2019-11-16 20:55:05', '2019-11-16 20:55:05'),
(94, 10, 1, 91, 'IAL Engineering', 'A8', 'exhibitorlogo/1573938602_a2.jpg', 'www.ialengineering.com', '*****@*****.com', '(868) 657-8561/4', 'One of the first Oil Wells drilled in the World was in Trinidad in 1857. First Commercial Oil was in 1908. Our Organization was established in 1935 to serve the Energy Industry. Today, with 80+ years in Operation Our range of Integrated Services include Tubular Licensing Technology & Field Services, Manufacturing of Wellheads & Premium Accessories, Valve Servicing and Welding & Fabrication … managed with both LLOYD’s & API Certified Facilities. For the past 15 years we have been exporting Premium Accessories to over 50 Countries ..as far as Australia, and are now pleased to be part of the Guyana Frontier …CNC Machines already Operating with a new Facility under Construction.', 0, '2019-11-16 21:10:02', '2019-11-16 21:10:02'),
(95, 10, 1, 91, 'Airswift', 'A7', 'exhibitorlogo/1573938748_a1.jpg', 'www.airswift.com', '*****@*****.com', '**********', '“Airswift are a global workforce solutions provider to the energy, process and infrastructure industries. We serve as a strategic partner to our clients, offering a turnkey workforce solution to capture and deliver the top talent needed to complete successful projects. With 800 employees in over 60 offices worldwide, 7,000 contractors and a candidate database of 500,000, our geographical reach and pool of available talent, experience and expertise is unmatched in the industry. Airswift has been successfully operating in Guyana for the last 3 years with its Airswift Guyana company that has been incorporated in Georgetown. Airswift Guyana has been managed by the experienced Guyanese national staff.\"', 0, '2019-11-16 21:12:28', '2019-11-16 21:12:28'),
(96, 10, 1, 91, 'Global Edge', 'A1', 'exhibitorlogo/1573938886_edge.jpg', 'www.theglobaledgeconsultants.com', '*****@*****.com', '+592-640-8885', 'The Global Edge Guyana is a local staffing firm with world-class standards in recruitment, supporting and promoting local employment and development. A part of The Global Edge Group, a global recruitment firm that is headquartered in Houston, with other operations throughout Europe, Asia and Africa, we introduce high quality service by providing full-service recruitment, workforce solutions and global mobility. We are here to connect you to your next employment opportunity within the Oil and Gas sector worldwide. Our Managers and well-trained Recruitment Consultants can provide the highest quality service and guidance through your job application process. Connect with our Recruitment Consultants today to learn more about the latest local and international employment opportunities.', 0, '2019-11-16 21:14:46', '2019-11-16 21:14:46'),
(97, 10, 1, 91, 'Jaguar Oilfied Services Guyana', 'A2', 'exhibitorlogo/1573939023_jagup.png', 'www.jaguargy.com', 'info@jaguargy.com', '592-265-7445', 'Jaguar Oilfield Services Guyana, an indigenous market leader. Setting the benchmark on safety, quality and value with internationally recognized accreditations from ISO 9001, ABS and LEEA. Continuously striving for improvement through efficiency and technological advancement. The organization is set up with two specialist divisions: Inspection and Maintenance, Products and Rentals. Providing world class inspection services and products, delivered by Guyanese and readily available in-country.', 0, '2019-11-16 21:17:03', '2019-11-16 21:17:03'),
(98, 10, 1, 91, 'Tripple D\'s', 'A9', 'exhibitorlogo/1573939164_trip.jpg', 'www.tripledsltd.com', 'helpdesk@tripledsltd.com', '(868) 636-5144, 0333', 'Triple D\'s Inc was incorporated in 2018, the company stated off with a fleet of just 3 Tractor Truck units with its first clients being Schlumberger Guyana. Today, Triple D\'s Inc has grown to offer services beyond Heavy Equipment Transportation which includes equipment rental, shuttle services, personal lease rentals, warehouse solutions, corporate house accommodations and port facilities. Our fleet has now grown and our clients have now expanded to include Halliburton, Baker Hughes, Repsol and Ramps Logistics just to name a few. To accommodate our rapidly expanding business, Triple D\'s expanded its human capital base to include a diverse group of competent Guyanese individuals who have all been trained and mandated to deliver superior levels of customer service and support. Presently, we employ a staff of over 50 permanent team members. Our experience in Customer Service and Fleet Management has grown to a level that provides assurance to all our customers that we will meet and exceed all of their requirements. As our business continues to develop, we aim to be the leading provider of land transportation in the country, keeping safety and exception service are or forefront', 0, '2019-11-16 21:19:24', '2019-11-16 21:19:24'),
(99, 10, 1, 91, 'Gulf Engineering', 'A5', 'exhibitorlogo/1573939273_a4.jpg', 'www.gulfenggy.com', 'sales@gulfenggy.com', '+592 673 4853 / 695 4853', 'Gulf Engineering Services (Guyana) Inc. is a Guyanese entity with its facility located at Tract G4 - G5 Land of Canaan, East Bank Demerara, Republic of Guyana, W.I. GESGI carries out its business and operations in alignment with its Trinidad counterpart Gulf Engineering Services Limited, an ISO 9001:2015 and API Q1 certified facility, that has a proven track record of over 42 years for providing quality threaded products in accordance to: licensee’s and OEM specifications; Customer’s requirements; machine shop related services to downhole drilling tools and equipment for the oil and gas exploration and production operations in Trinidad and now seeks to provide for local content in Guyana.', 0, '2019-11-16 21:21:13', '2019-11-16 21:21:13');
INSERT INTO `exhibitors` (`id`, `event_id`, `userid`, `detailsid`, `name`, `booth`, `image`, `website`, `email`, `phone`, `description`, `toprated`, `created_at`, `updated_at`) VALUES
(100, 10, 1, 91, 'Cataleya Energy Limited', 'Space not taken', 'exhibitorlogo/1573939620_a6.jpg', 'www.cataleyaenergy.com', '*****@*****.com', '**********', 'Cataleya Energy Limited has funded a project in a remote village in the South American country of Guyana to tackle drought and create economic opportunities for residents in the Central Rupununi community.\r\n\r\nThe US$1.25 million reservoir has the capacity to hold 4.5 million cubic metres of water. It was funded by Cataleya Energy to reduce vulnerability to drought and build resilience in Nappi Village and surrounding communities.', 0, '2019-11-16 21:27:00', '2019-11-16 21:27:00'),
(101, 10, 1, 91, 'Dentons', 'A4', 'exhibitorlogo/1573939753_dens.jpg', 'www.dentons.com/en', '*****@*****.com', '**********', 'Dentons, the world’s largest law firm, is the only global magic circle law firm to have established offices in Guyana and the wider Caribbean Community (CARICOM). Through the Guyana office, the firm offers a solid oil and gas team including upstream, midstream, and downstream which unmatched capabilities given our experience locally, regionally and globally. The firm is a leader on the Acritas Global Elite Brand Index, a BTI Client Service 30 Award winner and recognized by prominent businesses and legal publications for its innovation in client service. Dentons’ polycentric approach, commitment to inclusion and diversity, and world-class talent challenge the status quo to advance client interests in the communities in which we live and work.', 0, '2019-11-16 21:29:13', '2019-11-16 21:29:13'),
(102, 10, 1, 91, 'Oceaneering', 'A3', 'exhibitorlogo/1573939903_oce.jpg', 'www.oceaneering.com', '*****@*****.com', '713.329.4500', 'At Oceaneering, we do things differently, creatively, and smarter. As your trusted partner in Guyana, Oceaneering pushes boundaries to solve your toughest challenges. Our unmatched experience and truly innovative portfolio of technologies and solutions give us the flexibility to adapt and evolve, regardless of market conditions.\r\nOur Corporate Social Responsibility is built around our Core Values. We believe in managing our business in a way that promotes health, safety, and the environment as well as strong ethics while supporting the communities in which we live and work.\r\nWe look forward to continuing to grow our local Guyanese workforce.', 0, '2019-11-16 21:31:43', '2019-11-16 21:31:43'),
(103, 10, 1, 92, 'Saipem', 'G2', 'exhibitorlogo/1573940180_saip_exhib.jpg', 'www.saipem.com', '*****@*****.com', '+39 0244231', 'Saipem is a leading company in engineering, drilling and construction of major projects in the energy and infrastructure sectors. It is “One-Company” organized in five business divisions (Offshore E&C, Onshore E&C, Offshore Drilling, Onshore Drilling and XSIGHT, dedicated to conceptual design). Saipem is a global solution provider with distinctive skills and competences and high-tech assets, which it uses to identify solutions aimed at satisfying customer requirements. Listed on the Milan Stock Exchange, it is present in over 70 countries worldwide and has 32 thousand employees of 120 different nationalities.', 0, '2019-11-16 21:36:20', '2019-11-16 21:36:20'),
(105, 10, 1, 88, 'SBM Offshore', 'G7', 'exhibitorlogo/1573944778_sbm.jpg', 'www.sbmoffshore.com', '*****@*****.com', '**********', 'Our vision statement: SBM Offshore believes the oceans will provide the world with safe, sustainable and affordable energy for generations to come. We share our experience to make it happen. SBM Offshore is a leader in leased floating production solutions and mooring systems, for the offshore energy industry, over the full product lifecycle. Group companies employ approximately 4,350 people worldwide, including around 650 contractors, which are spread over offices in key markets, operational shore bases and the offshore fleet of vessels, including Liza Destiny FPSO and its shore base in Georgetown. Offshore Guyana, SBM Offshore will operate both the Liza Destiny and Liza Unity FPSOs, coming onstream in 2020 and 2022 respectively. With over 320 cumulative years’ experience in operations SBM Offshore has unrivalled operational experience in this field and a delivery track record across the full lifecycle. SBM Offshore product portfolio includes: FPSOs, semi-submersibles, TLPs, float.', 0, '2019-11-16 22:52:58', '2019-11-16 22:52:58'),
(106, 10, 1, 88, 'Mid Atlantic Oil & Gas Inc.', 'G13', 'exhibitorlogo/1573944938_mid.jpg', 'www.midatlanticoilandgas.com', '*****@*****.com', '**********', 'Mid Atlantic Oil & Gas Inc. (MOGI) is a Guyana Petroleum Exploration Company which was incorporated and registered in early 2013. On March 4, 2015, MOGI was awarded the CANJE Petroleum Prospecting License (PPL) along with JHI Associates Inc., a privately held Canadian Company. In February 2016, Exxon Mobil farmed into the CANJE PPL and was appointed Operator and in late 2018 TOTAL joined the consortia. Our partnership is committed to an Exploration Program that is sustainable, local content oriented, with the highest levels of HSE (health, safety, environment) and adherence to the laws, regulations etc. governing such licenses in Guyana.', 0, '2019-11-16 22:55:38', '2019-11-16 22:55:38'),
(108, 10, 1, 90, 'DAI', 'B2', 'exhibitorlogo/1573945139_dai.jpg', 'www.clbdguyana.com', '*****@*****.com', '+592-223-7781 or +592-608-5256', 'The Centre for Local Business Development (the Centre) is Guyana’s leading source for oil and gas business and industry information, policy advocacy, and professional networking. Since 2017, the Centre has been meeting the growing demands of Guyanese and international businesses by offering training, mentoring for growth and procurement linkages. The Centre’s Supplier Registration Portal (SRP) is the premier platform for oil and gas suppliers as they seek to contract, partner, and purchase goods and services from Guyanese businesses. With new offerings like the Health, Safety, Security and the Environment (HSSE) programme and its online Computer-based Training (CBT), the Centre continues to expand its services to advance local business. With decades of transformative growth ahead, the Centre is at the forefront of Guyanese and international business.', 0, '2019-11-16 22:58:59', '2019-11-16 22:58:59'),
(109, 10, 1, 91, 'Tucker Energy', 'A10', 'exhibitorlogo/1573945267_tucker.jpg', 'www.tuckerenergy.com', '*****@*****.com', '**********', 'Tucker Energy Services is part of a multi-service organization, providing Solutions, Services and Products to the energy industry. Our goal is to supply people, products and equipment that deliver the best in Quality, Service and Value to our customers. Established in 1935 as Trinidad Oilfield Services, the company offered cementing, perforating, and logging services in Trinidad. The company has developed significantly over the years providing a greater number of products and services to the energy industry. Being ISO 9001:2015 Certified combined with our HSE Management System and STOW (Safe?to?Work) Certification, Tucker Energy Services strives to be the service provider of choice via the provision of safe and quality services.', 0, '2019-11-16 23:01:07', '2019-11-16 23:01:07'),
(111, 10, 1, 93, 'GLOBAL TECHNOLOGY INC.', 'E1', 'exhibitorlogo/1573977688_gte.jpg', 'www.gtechweb.com', 'info@gtechweb.com', '(592)-225-3364, (592)-225-4657', 'Operating for over 20 years, Global Technology Inc. providessolutions for Technology, Communication, Training, Staffing and Diving/ROV needs locally.We provide the full line of Bartec-Vodec explosion-proof products, radios, RFID, tablets, Apple products, software design, support and maintenance. Our communication solution includes VSAT, VOIP, VPN and minutes for marine and onshore applications. Global-Technology’s Safety and Computer training essential for offshore and onshore workers includes confined space entry, fall protection, H2S Alive, software certification etc. We provide an all-inclusive staffing service (recruitment, payroll, training, invoicing) for all operation types. Our Diving/ROV solution supports construction (WROVs), inspections and FPSO repairs (Air Diving).', 0, '2019-11-17 08:01:28', '2019-11-17 08:01:28'),
(112, 10, 1, 93, 'D&J Group', 'E6', 'exhibitorlogo/1573978248_dje.jpg', 'www.thedjgroup.co', '*****@*****.com', '**********', 'To offer unmatched services in Guyana where our we gather repeat clients who, because of our service, grow. deliverables rather than cross-media customer service.\r\n\r\nTo be regarded as a responsible, reliable and respectable business conglomerate and to be the partner of choice for International and regional customers.', 0, '2019-11-17 08:10:48', '2019-11-17 08:10:48'),
(113, 10, 1, 93, 'THE HARDWARE DEPOT', 'E2', 'exhibitorlogo/1573978515_hde.jpg', 'www.hardwaredepotgy.com', '*****@*****.com', '**********', 'The Hardware Depot is a large wholesaler of industrial and commercial supplies,\r\nselling exclusively to the government and private companies in Guyana.\r\nWe have been in operation for 20 years, With access to more than 2000 range of products.In health & safety and more specifically with specialized equipment used in the electrical, water, telecommunications, mining, manufacturing and construction sectors.\r\n\r\nWith satisfaction we serve major companies like Banks DIH, Demerara Distillery Limited (DDL),\r\nGuyana Power & Light (GPL), Guyana Water Inc (GWI), Guyana Telephone & Telegraph (GTT),\r\nAurora Gold Mine (AGM), Rusal, Guyana sugar corporation (GUYSUCO). Just to name a few with over 5000 pair of safety footwear annually,\r\nalong with other occupational safety gears an equipment.', 0, '2019-11-17 08:15:15', '2019-11-17 08:15:15'),
(114, 10, 1, 93, 'CARILEC', 'E14', 'exhibitorlogo/1573978666_ce.jpg', 'www.carilec.org', 'admin@carilec.org', '1 758-452-0140', 'The Caribbean Electric Utility Services Corporation (CARILEC) is an association of electric energy solutions providers and other stakeholders operating in the electricity industry in the Caribbean region, Central and South Americas and Globally. The CARILEC Secretariat endeavors to improve communication among its members, providing technical information, training, capacity building, conference and other services. The Secretariat plays a leading role in electric utility advocacy, growth and sustainability in the Region.', 0, '2019-11-17 08:17:46', '2019-11-17 08:17:46'),
(116, 10, 1, 93, 'The Guyana Oil Company Limited', 'E4 & E5', 'exhibitorlogo/1573980853_goe.jpg', 'www.guyoil.gy', 'admin@guyoil.gy', '(592)225-1595-8', 'The Guyana Oil Company Limited is a proud Guyanese owned and Guyanese operated company, serving our nation for over 42 years.\r\n\r\nWe are committed at all times to conducting our business in a responsible and sustainable manner as it is integral to the way we do business. We are committed to operating with the highest standards for the benefit of all stakeholders, our environment and the wider community.\r\n\r\nEmbedded in our corporate role, is to protect the public from predatory pricing by ensuring that the Guyanese people receive quality products at the most competitive prices.\r\n\r\nAll the profits from GUYOIL remain in Guyana and are used for the development of our country. So when you buy fuel and lubricants from GUYOIL, you are directly contributing to building schools, hospitals, roads or to projects that benefit all Guyanese.', 0, '2019-11-17 08:54:13', '2019-11-17 08:54:13'),
(117, 10, 1, 93, 'Beston', 'E7', 'exhibitorlogo/1573980998_be.jpg', 'www.beston.consulting', '*****@*****.com', '1 868 663 7142', 'Beston is committed to growth. Since our inception in 1992, we have built a reputation for delivering efficient solutions to complex challenges. Approaching problems from unexpected angles is one of our strengths, and we are well versed at interpreting needs and consolidating ideas to offer value-added consultancy services for our clients and collaborators, regardless of project scope. Always consistent, Beston’s focus on building excellence – whether in skills, methodologies, or people – leads to the delivery of world-class results.', 0, '2019-11-17 08:56:38', '2019-11-17 08:56:38'),
(119, 10, 1, 93, 'Silverline Systems', 'E10', 'exhibitorlogo/1573981505_sle.jpg', 'www.silverlineoffice.com', 'sales@silverlineoffice.com', '(246) 436-1134', 'VISION: To be the preferred choice of leading Caribbean corporations, for the creation of aesthetically pleasing and productive commercial interiors that are comfortable, exciting and safe.\r\nMISSION: To be propelled by visionary leaders & a highly trained dynamic team, we are committed to optimizing benefits and function of elegant commercial interiors. Serving exceptionally for 24 years in space planning, designing, project management, supplying quality and aesthetically pleasing products that satisfy the evolving needs of the Human Resource at work on time.\r\nSERVICES: Office Designs & Layouts, Supply Coordination, Project Management, Installations, After Sale Service', 0, '2019-11-17 09:05:05', '2019-11-17 09:05:05'),
(121, 10, 1, 93, 'NSB-Omega Guyana', 'E16', 'exhibitorlogo/1573982446_nsbe.jpg', 'www.nsbomega.gy', 'info@nsbomega.gy', '(592) 227 3370', 'NSB Omega (Guyana) was formed in 2018 to provide workforce solutions to the emerging oil and gas industry in Guyana. NSB Omega (Guyana) is a majority Guyanese and woman-owned company and can provide comprehensive workforce solutions tailored to meet the needs of its clients. Services include: recruitment; payroll; crewing; immigration and consulting.', 0, '2019-11-17 09:20:46', '2019-11-17 09:20:46'),
(123, 10, 1, 93, 'Classic Controls, Inc.', 'E9', 'exhibitorlogo/1573982685_qwer.jpg', 'www.classiccontrols.com', '*****@*****.com', '863.644.3642', 'Classic Controls, Inc. was founded in 1991 as an instrumentation, valve, and control system supplier, representing best in class manufacturers for Florida, Georgia, and the entire Caribbean. We offer complete support services for the solutions we provide including product technical support, valve automation & repair, and system integration.', 0, '2019-11-17 09:24:45', '2019-11-17 09:24:45'),
(124, 10, 1, 93, 'U-mobile cellular Inc.', 'E17 &  E18', 'exhibitorlogo/1573982994_umob.jpg', 'www.u.com.my', '*****@*****.com', '+6018 388 1318', 'U Mobile is a data-centric and multiple award-winning mobile data service company in Malaysia.\r\n\r\nWe have grown from strength to strength over the past few years and our subscriber base has crossed the 7 million mark. We’ve made it our mission to challenge the status quo and give Malaysians what they value – mobile and digital services that enable you to pursue your passions, realise your potential and feel truly unlimited. With our commitment to expand our #BARULAHBEST 4G network, we are in the best position to deliver on that promise!', 0, '2019-11-17 09:29:54', '2019-11-17 09:29:54'),
(125, 10, 1, 93, 'Arrow Oilfield Services Inc.', 'E3', 'exhibitorlogo/1573983277_aofe.jpg', 'www.arrowosi.com', 'sales@arrowosi.com', '+011 (592) 264 2765', 'Arrow Oilfield Services Inc. is a 100% locally owned and operated Oil and Gas Service Company. Our aim is to become pioneers within the Industry by offering a quality service through development of our local capability while keeping people safe and preserving our local environment. We specialize in Maintenance, Repair and Operate (MRO) supplies and services, Strategic Sourcing and Procurement Services, Facilities Management and Construction. We are proud to be a part of the Guyanese community supporting the development of our local Oil and Gas Industry.', 0, '2019-11-17 09:34:37', '2019-11-17 09:34:37'),
(126, 10, 1, 93, 'VSH United Guyana Inc.', 'E13', 'exhibitorlogo/1573983456_vshe.jpg', 'www.vshguyana.com', 'salesguy@vshunited.com', '+(592) 223 8444', 'VSH Guyana is the official distributor for Red Wing Safety in Guyana. We have been actively selling premium safety shoes and garments for over 5 years. Our clientele consists of all sectors including Oil & Gas, mining, natural resources, construction, government agencies etc. As official distributors for Red Wing Shoes, we offer a 12 month manufactures warranty on all Red Wing Shoes. Our staff is experienced in identifying the best product to suit each organization.', 0, '2019-11-17 09:37:36', '2019-11-17 09:37:36'),
(127, 10, 1, 93, 'Eve Anderson Recruitment Limited', 'E20', 'exhibitorlogo/1573983609_vee.jpg', 'www.eveandersonrecruitment.com', '*****@*****.com', '+1 868 625 1588', 'Eve Anderson Recruitment is the leading recruitment and human resource advisory agency in the Caribbean catering to local and international clients focused on recruiting and retaining top talent across several industries for both permanent and contract roles, managing their payroll and supporting key human resource functions required to build and grow their business.', 0, '2019-11-17 09:40:09', '2019-11-17 09:40:09'),
(128, 10, 1, 93, 'GENESIS MARINE GUYANA INC', 'E19', 'exhibitorlogo/1573983756_geneee.jpg', 'www.genesisgroupslp.com', 'info@genesisgroupslp.com', '+ 507 209 4126', 'Genesis Group. was founded in 2006 and quickly positioned itself as a key protagonist among a selected group of shipping agents serving, the Oil & Gas industry, following its successful launch, the company embarked on a period of aggressive growth and expansion throughout Venezuela, Panama and the greater Caribbean region. In 2016 to provide management services for Shipping, Logistics & Projects (SLP) related businesses in the Americas and in response to considerable interest from its Principals and stakeholders, Genesis Group incorporated a new company in Georgetown, Guyana to support the needs of the emerging Oil & Gas sector. Genesis Marine (Guyana) Inc.', 0, '2019-11-17 09:42:36', '2019-11-17 09:42:36'),
(129, 10, 1, 93, 'Fugro', 'E28', 'exhibitorlogo/1573984153_feeee.jpg', 'www.fugro.com', '*****@*****.com', '+91 22 2762 9500', 'Fugro is the world’s leading Geo-data specialist, collecting and analysing comprehensive information about the Earth and the structures built upon it. Through integrated data acquisition, analysis and advice, we unlock insights from Geo-data to help our clients design, build and operate their assets in a safe, sustainable and efficient manner.', 0, '2019-11-17 09:49:13', '2019-11-17 09:49:13'),
(130, 10, 1, 93, 'Marine Consultants (Trinidad) Limited', 'E34', 'exhibitorlogo/1573984340_mce.jpg', 'www.mcl.co.tt', 'mcl@mcl.co.tt', '1-868-625-1309 / 2887', 'MARINE CONSULTANTS (TRINIDAD) LIMITED, registered in 1971, was founded by three Master Mariners, two of whom are still Directors. The company was formed to provide consultancy services to organizations operating in the maritime environment using the wealth of knowledge acquired from the combined training and experience of the founding partners. Over the years the scale and scope of the company has grown, and we now draw on a network of local and international expertise that offers world-class products and services to the domestic and regional maritime sectors. Our customers can rely on us to understand their business processes in order to offer appropriate solutions that add both value and assurance to their stakeholders. Marine Consultants caters to the Oil & Gas Industry, Ships, Ports, Pleasure Crafts and National Security.', 0, '2019-11-17 09:52:20', '2019-11-17 09:52:20'),
(131, 10, 1, 93, 'L & S Surveying Services Guyana INC.', 'E33', 'exhibitorlogo/1573985480_lse.jpg', 'www.lssurvey.com', 'info@lssurvey.com', '(868) 652 1314', 'L & S Surveying Services Limited is one of the leading surveying and mapping service companies in Trinidad and Tobago. Since our formation in 2000, we have worked on some of the largest survey and construction projects in the country, and we have continually sought to provide the innovation and flexibility required to provide the solutions needed in a continually changing industry. As a result we have introduced the following services, Underground Utility Detection Services, 3D Laser Scanning and Aerial Photogrammetric and LiDAR Mapping. In 2016 we registered our company L & S Surveying Services Guyana Inc., with our office at #50 Brickdam, Stabroek, Georgetown. Our vision has been “to provide a full range of surveying and related engineering services in keeping with the needs of the market to deliver superior service to our clients”.', 0, '2019-11-17 10:11:20', '2019-11-17 10:11:20'),
(132, 10, 1, 93, 'EnerMech Guyana INC', 'E25', 'exhibitorlogo/1573985817_emeeee.jpg', 'www.enermech.com', 'africa.sales@enermech.com', '+27 21 512 3160', 'Everyday we deliver fully integrated solutions to the global energy and infrastructure markets. We provide a range of mechanical, electrical and instrumentation services to support assets and projects, of any size, wherever they are in the world.\r\nOur innovative, technical and commercial solutions support you across the full asset life cycle. With more than 3000 people globally, we have the right capabilities and vision to serve the ever-changing needs of our Industry.\r\nWe are focused on maximizing economic values for our customers, and we have a proven track record of successful delivery. In working with EnerMech, no matter how big or small the project, you get a team that is dedicated, capable and focused on cost-effective and time-efficient solutions, delivering excellence every step of the way.', 0, '2019-11-17 10:16:57', '2019-11-17 10:16:57'),
(133, 10, 1, 93, 'Nalco Champion Guyana Inc.', 'E26', 'exhibitorlogo/1573985983_nce.jpg', 'www.ecolab.com/nalco-champion', '*****@*****.com', '**********', 'Nalco Champion Energy Services division offers a singular focus on providing specialty chemistry programs and related services for upstream, midstream and downstream oil and gas operations. Through onsite problem solving and the application of innovative technologies, we deliver sustainable solutions to overcome complex challenges in the world\'s toughest energy frontiers. Our businesses include WellChem Technologies, Oilfield Chemicals and Downstream, covering the entire petroleum value chain.', 0, '2019-11-17 10:19:43', '2019-11-17 10:19:43'),
(134, 10, 1, 93, 'Downhole Products', 'E8', 'exhibitorlogo/1574007490_dpeeee.png', 'www.downholeproducts.com', '*****@*****.com', '**********', 'MISSION STATEMENT:\r\nOur commitment to you is to be the most responsive in providing reliable and creative drilling & completion solutions.\r\n\r\nCORE VALUES:\r\nWe are guided by four strong values to achieve our VISION: We Set the Industry Standard', 0, '2019-11-17 16:18:10', '2019-11-17 16:18:10'),
(135, 10, 1, 93, 'Parker Hannifin', 'E11 & E12', 'exhibitorlogo/1574007921_peee.png', 'www.parker.com', 'parkerind@parker.com', '+91-22-41242828', 'Parker Hannifin is a Fortune 250 global leader in motion and control technologies. For 100 years the company has engineered the success of its customers in a wide range of diversified industrial and aerospace markets.', 0, '2019-11-17 16:25:21', '2019-11-17 16:25:21'),
(136, 10, 1, 93, 'GAICO', 'E21 & E22', 'exhibitorlogo/1574008273_gaieeeee.png', 'www.gaico.inc.gy', '*****@*****.com', '**********', 'N/A', 0, '2019-11-17 16:31:13', '2019-11-17 16:31:13'),
(137, 10, 1, 93, 'Rubis', 'E23', 'exhibitorlogo/1574008637_rueee.jpeg', 'www.rubis-caribbean.com', '*****@*****.com', '(246) 417-6300', 'We are an established international brand with over 20 years’ experience. RUBIS distributes high quality products including petroleum and aviation fuels, LPG and lubricants. You’ll find us throughout the Eastern Caribbean including Antigua, Barbados, Dominica, Grenada, Guyana, St Lucia and St Vincent as well as the Western and French Caribbean, Bermuda, Africa and Europe. We have a commitment to the communities in which we operate. As always, RUBIS is dedicated to serving you.', 0, '2019-11-17 16:37:17', '2019-11-17 16:37:17'),
(138, 10, 1, 93, 'Ramps Logistics', 'E24', 'exhibitorlogo/1574009263_rameeee.png', 'www.rampslogistics.com', 'info@rampslogistics.com', '+1868 627-5664', 'Ramps is a leading provider of freight forwarding and supply chain management services. For over 30 years we have been offering transportation and logistics solutions. Our customized technology and solutions support the way our customers want to do business, wherever they are in the world. We have offices in Guyana, Trinidad, Houston, Miami, Mexico and Suriname.', 0, '2019-11-17 16:47:43', '2019-11-17 16:47:43'),
(139, 10, 1, 93, 'Tower Cranes', 'E29', 'exhibitorlogo/1574009600_tceee.jpg', 'www.towercranesguyana.com', '*****@*****.com', '**********', 'Vision: Training today for tomorrow’s future.\r\nMission: Institute develops broadly educated, highly skilled and adaptable citizens to be successful in careers that significantly contribute to the communities they serve locally, nationally and globally.\r\nTower Cranes of Guyana is a Guyanese company that offers several products including Sales, Rentals, Erection and Dismantle of Tower Cranes. Inspection, Caricom Approved Training and certification of cranes and heavy equipment is our core business. TCGI chose to venture into this industry in Guyana, as we see a need for skilled training and certification in this nation.\r\nTCGI is looking forward to building strong relations with the Guyanese nationals and business partners. \"', 0, '2019-11-17 16:53:20', '2019-11-17 16:53:20'),
(140, 10, 1, 93, 'Western Scientific', 'E32', 'exhibitorlogo/1574010330_wse.png', 'http://www.westscitech.com/', '*****@*****.com', '592 225-3362', 'N/A', 0, '2019-11-17 17:05:30', '2019-11-17 17:05:30'),
(141, 10, 1, 93, 'Target Solutions', 'E30', 'exhibitorlogo/1574010554_teeeeeeeem.png', 'www.targetsolutionstt.com', 'sales@targetsolutionstt.com', '(868) 235-5551', 'N/A', 0, '2019-11-17 17:09:14', '2019-11-17 17:09:14'),
(142, 10, 1, 93, 'Council for technical vocation', 'AE1', 'exhibitorlogo/1574010864_ctvteeeeeee.png', 'www.ctvet.org.gy', 'council4tvet@moe.gov.gy', '+592-227-8784/9', 'Our Motto:\r\nExcellence in Standards and Quality.\r\n\r\n\r\nOur Vision:\r\nA National TVET System created to meet the changing demands of global competitiveness.\r\n\r\n\r\nOur Mission:\r\nThe Mission is focused on “the provision of a Guyanese workforce with requisite knowledge, skills and attitudes contributing to increased productivity and economic development”.', 0, '2019-11-17 17:14:24', '2019-11-17 17:14:24'),
(143, 10, 1, 93, 'Guyana National Industrial company', 'AE2', 'exhibitorlogo/1574011669_gnice.jpg', 'www.gnicgy.com', 'gnicceo@guyana.net.gy', '(592) 226 0882', 'To optimize all business potentials in the area of Engineering and Shipping for the benefit of our customers, Employees, Shareholders, the Communities in which we serve and the entire Guyanese Nation.', 0, '2019-11-17 17:27:49', '2019-11-17 17:27:49'),
(144, 10, 1, 93, 'GMC', 'AE3', 'exhibitorlogo/1574012051_gmce.jpeg', 'www.newgmc.com', 'info@newgmc.com', '(592) 226-8255 ; 227-1630', 'Guyana Marketing Corporation is the agency of the Ministry of Agriculture that provides marketing services to stakeholders in the non-traditional agricultural sector.\r\n\r\nWe hope you find it a useful resource for doing business with and from Guyana, in the non-traditional agricultural sector: fresh fruits & vegetables and processed agricultural products.\r\n\r\nTo our farmers, agro-processors, exporters and other agri-business investors, we also hope you find this website a useful resource for information on proper post-harvest handling, packaging, marketing and exporting.', 0, '2019-11-17 17:34:11', '2019-11-17 17:34:11'),
(145, 10, 1, 93, 'Source One Oil & Gas Marine Supplies', 'AE4', 'exhibitorlogo/1574012411_saeee.jpeg', 'www.********.com', '*****@*****.com', '**********', 'N/A', 0, '2019-11-17 17:40:11', '2019-11-17 17:40:11'),
(146, 10, 1, 93, 'HAB International', 'AE5', 'exhibitorlogo/1574012611_habe.jpeg', 'www.habinternational.net', 'customerservice@habint.net', '+592 231 9578', 'HAB International Inc. was established in 2001 as a supporting company to Swiss Machinery. Swiss was seeking a solution to service their downtime customers. HAB became that solution by introducing an overnight EXPRESS service from Miami to Georgetown, Guyana.\r\n\r\nFollowing divine guidance, HAB expanded to deliver a complete shipping service for our clients with the following services:\r\n\r\n    Air Freight\r\n    Ocean Freight\r\n    Online Shopping\r\n    Online Account\r\n    Procurement\r\n    Supply Chain\r\n    and so much more…', 0, '2019-11-17 17:43:31', '2019-11-17 17:43:31'),
(147, 10, 1, 93, 'Falcon Logistics Inc.', 'AE6', 'exhibitorlogo/1574012815_fale.jpeg', 'www.falconguyana.com', 'info@falconguyana.com', '592-227-0390', 'We are a 100% Guyanese company! We started our operations in 2013 with our primary focus on offering competitive Logistics and Support Services to the Oil and Gas Sector in Guyana.\r\n\r\nOur team players have over 35 years of logistics experience in serving the extractive resource sector in Guyana.', 0, '2019-11-17 17:46:55', '2019-11-17 17:46:55'),
(148, 10, 1, 93, 'Private Sector Commission', 'AE7', 'exhibitorlogo/1574013217_pse.jpeg', 'www.psc.org.gy', '*****@*****.com', '**********', 'The Private Sector Commission of Guyana was established in 1992 by five Private Sector Associations with the aim of bringing together all Private Sector Organs and Business Entities under the purview of being one National Body.\r\n\r\nThe Private Sector Commission is governed by a Council which comprises of the Head of all Members Sectoral Organizations and a number of elected members.  The Council is headed by a Chairman who can serve a maximum of two consecutive one-year terms.  Any Chairman who has served two consecutive terms may be eligible for subsequent re-election.\r\n\r\nThe overall activities of the Commission are coordinated by an Executive Management Committee which comprises the following elected officials: Chairman, Vice Chairman, Honorary Secretary, and Honorary Treasurer.  The Executive Director is also a member of the committee by appointment.', 0, '2019-11-17 17:53:37', '2019-11-17 17:53:37'),
(149, 10, 1, 93, 'Guyana Oil & Gas Support Services', 'AE8 , AE9 & A10', 'exhibitorlogo/1574013678_goge.jpeg', 'www.guyanaservice.com', 'info@guyanaservice.com', '+ (592) 227.0097', 'N/A', 0, '2019-11-17 18:01:18', '2019-11-17 18:01:18'),
(150, 10, 1, 93, 'John Fernandes Ltd.', 'AE11, AE12 & AE13', 'exhibitorlogo/1574013877_joeeee.jpg', 'www.jf-ltd.com', 'enquiries@johnfernandesltd.com', '(592) 227-3344,', 'John Fernandes Limited, established in 1959, has become the industry leader in pier operation, stevedore contracting and shipping agency representation in Guyana. With the exciting developments in the Guyana Oil and Gas industry, we have leveraged our core competencies to provide support services utilizing our established infrastructural, mechanical and human capital. We have strategically aligned ourselves with selected international and regional partners to better service the varied demands of the local energy sector. Our partnerships include: G Port Inc. Rig Bound (Guyana) Inc. ,TLC (Guyana) Inc. ,Solus (Guyana) Inc. ,IRP Fire and Safety (Guyana) Inc', 0, '2019-11-17 18:04:37', '2019-11-17 18:04:37'),
(151, 10, 1, 93, 'Georgetown Chamber of Commerce & Industry', 'AE14', 'exhibitorlogo/1574014212_gccieeeeee.jpeg', 'www.gcci.gy', 'gccicommerce2009@gmail.com', '+592-226-3519', 'The Georgetown Chamber of Commerce and Industry was established under the name “The Chamberof Commerce of the City of Georgetown” in the year 1889. The formation of the chamber begun at a meeting with Royal Agricultural Commercial Society and a Commercial Committee held on the 20 the December 1888. Under the initiative of Mr. J. Errest Tinner a sphere of useful discussions were held and it was decided to merge these two bodies. On the 17th June, 1889 at a meeting of Merchants and others it was decided to form a Chamber of Commerce for the City, which should be recognized by law. The Memorandum and Articles of Association were drafted. These were submitted at a General Meeting held on the 8 th July, 1889 and the President, Vice-Presidents and Council were elected.', 0, '2019-11-17 18:10:12', '2019-11-17 18:10:12'),
(152, 10, 1, 93, 'Precision Products', 'E31', 'exhibitorlogo/1574020823_prece.jpeg', 'www.********.com', '*****@*****.com', '**********', 'N/A', 0, '2019-11-17 20:00:23', '2019-11-17 20:00:23'),
(153, 10, 1, 93, 'University of Guyana', 'M1', 'exhibitorlogo/1574021170_uge.png', 'www.uog.edu.gy', '*****@*****.com', '**********', 'The University of Guyana was established in April 1963 and began its operations in October of the same year with a batch of 164 students in temporary premises loaned from Queen.\r\nProgrammes were at first confined to the Arts, Natural Sciences, and Social Sciences. However, in 1967, a Faculty of Education was established and this was followed by the establishment of the Faculties of Technology in 1969, Agriculture in 1977, and in 1981, the Faculty of Health Sciences, prior to the establishment of which programmes in Health Sciences were offered within the Faculty of Natural Sciences.', 0, '2019-11-17 20:06:10', '2019-11-17 20:06:10'),
(154, 10, 1, 93, 'Department of Energy, Mministry of the Presidency', 'M1', 'exhibitorlogo/1574021960_deeeeeeee.png', 'www.motp.gov.gy', '*****@*****.com', '**********', 'The Department of Energy was created on August 1, 2018 to effectively manage the hydrocarbon resources of the Cooperative Republic of Guyana. The May 2015 and subsequent discoveries of oil and associated gases, have resulted in Guyana being regarded amongst the top reserve holders worldwide. This has the potential for improved standard of living for all Guyanese and transformational and impactful development to occur.\r\n\r\nAs the Department responds to the rapid rate of developments within the sector, it invites suitably qualified individuals to apply to fill vacancies in the following areas by submitting: (i) a cover letter written as a Statement of Interest, and (ii) a curriculum vitae\r\n• Economics\r\n• Econometrics\r\n• Law\r\n• Procurement\r\n• Monitoring and Evaluation\r\n• Administration\r\n• Environmental Safeguards\r\n\r\n1. Eligibility Requirements\r\n\r\nTo be considered for employment, applicants must meet the following requirements:\r\n\r\n? Possess a Bachelor’s degree or higher relevant to the field of study.\r\n? Have excellent academic performance as demonstrated by university or institution records.\r\n? Be proficient in English language.\r\n? Additional consideration would be given to past professional experience in the field for which a candidate in applying.\r\n\r\n2. The DoE offers a competitive remuneration package\r\nPersons desirous of having their applications considered, can submit same on or before March 30th, 2019.', 0, '2019-11-17 20:19:20', '2019-11-17 20:19:20'),
(155, 10, 1, 93, 'Guyana Office for Investment', 'M2', 'exhibitorlogo/1574022246_goieeeee.png', 'www.goinvest.gov.gy', 'ceosec@goinvest.gov.gy', '+592-225-0658, 227-0653', 'To contribute to Guyana’s economic development by promoting and facilitating local and foreign private-sector investment and exports in accordance with the country’s approved investment and export strategies.\r\n\r\nThe Guyana Office for Investment (GO-Invest) was established under the Public Corporations Act (1994) in 1994 as a semi-autonomous body and comes under the direct purview of the Ministry of Business. The CEO answers to a Board of Directors which is composed on representatives of both the private and public sectors.\r\n\r\nGO-Invest is divided into two divisions, one responsible for Investment Facilitation and Promotion and the other for Export Promotion. With these divisions, GO-Invest offers a full complement of services to local and foreign investors and exporters.', 0, '2019-11-17 20:24:06', '2019-11-17 20:24:06'),
(156, 10, 1, 84, 'Guyana Office for Investment', 'M2', 'exhibitorlogo/1574022487_goieeeee.png', 'www.goinvest.gov.gy', 'ceosec@goinvest.gov.gy', '+592-225-0658, 227-0653', 'To contribute to Guyana’s economic development by promoting and facilitating local and foreign private-sector investment and exports in accordance with the country’s approved investment and export strategies.\r\n\r\nThe Guyana Office for Investment (GO-Invest) was established under the Public Corporations Act (1994) in 1994 as a semi-autonomous body and comes under the direct purview of the Ministry of Business. The CEO answers to a Board of Directors which is composed on representatives of both the private and public sectors.\r\n\r\nGO-Invest is divided into two divisions, one responsible for Investment Facilitation and Promotion and the other for Export Promotion. With these divisions, GO-Invest offers a full complement of services to local and foreign investors and exporters.', 0, '2019-11-17 20:28:07', '2019-11-17 20:28:07'),
(157, 10, 1, 93, 'National Hardware Limited', 'AE10', 'exhibitorlogo/1574022654_nhle.jpeg', 'www.********.com', '*****@*****.com', '**********', 'N/A', 0, '2019-11-17 20:30:54', '2019-11-17 20:30:54'),
(158, 10, 1, 93, 'Canadian High Commission', 'B3', 'exhibitorlogo/1574022931_cae.jpg', 'www.international.gc.ca', '*****@*****.com', '(1-800-622-6232)', 'N/A', 0, '2019-11-17 20:35:31', '2019-11-17 20:35:31'),
(159, 10, 1, 93, 'Europe Caribbean Line', 'E15', 'exhibitorlogo/1574178153_ECL.png', 'www.europecaribbeanline.com', 'info@vertraco.nl', '+31 (0)10 285 87 00', 'ECL offers a unique liner service between Europe, the southern Caribbean and the north coast of South America. The liner service is suitable for the transportation of project cargo, break bulk, dry bulk, and vehicles in all shapes and sizes. To this aim, the monthly South Caribbean Service with our multi-purpose vessels sails from Antwerp, Hull, and Gijon to Georgetown, Paramaribo, Point Lisas, and Matanzas. Every two weeks, we offer sailings from Vlissingen to Georgetown and Paramaribo with our Reefer Service. This service yields the fastest transit times and is efficient.', 0, '2019-11-19 15:42:33', '2019-11-19 15:42:33'),
(162, 11, 1, 106, 'IMQS', 'B1', 'exhibitorlogo/1574847488_Imqs.png', 'www.imqs.ie', 'info@imqs.ie', '0894927942', 'The Irish Mining and Quarrying Society was founded in 1958 to provide a focal point for all those working in the extractive industry in Ireland. Our aim is to promote, safeguard and represent the natural resources and extractive industries. The membership is drawn from all sectors of the industry, ranging from exploration geologist to equipment suppliers and, as such, is recognised as a significant representational body with over 200 full Society members. It has a unique role in Ireland as a forum and network for contact between the different branches of the industry, enabling it to reflect the industry views in a co-ordinated manner.', 0, '2019-11-27 09:38:08', '2019-11-27 09:38:08'),
(163, 11, 1, 107, 'Aurum Exploration', 'LS', 'exhibitorlogo/1574847679_Aurum.jpg', 'www.aurumexploration.com', 'info@aurumexploration.com', '+353 (0)46 929 3278', 'Aurum Exploration, is a renowned mineral exploration consultancy company, delivering high quality, professional and innovative geological services to the global mineral exploration industry.\r\n\r\nAt Aurum we pride ourselves in developing dynamic, flexible and cooperative partnerships with our clients to ensure their exploration project is a success. Our long-standing clients return to us again and again to leverage our vast experience, expertise and knowledge as we deliver a full spectrum of exploration services in Europe, Africa and the Middle East.', 0, '2019-11-27 09:41:19', '2019-11-27 09:41:19'),
(164, 11, 1, 108, 'Minova', 'E3 & E4', 'exhibitorlogo/1574847960_minova.jpg', 'www.minovaglobal.com', 'globalmarketing@minovaglobal.com', '+44 1226 280 567', 'Minova has a 137-year track record of developing and delivering innovative ground support products for the mining, construction and energy industries. We are known for our high quality products, technical expertise and problem solving. This is a heritage to be proud of and to build on.', 0, '2019-11-27 09:46:00', '2019-11-27 09:46:00'),
(165, 11, 1, 108, 'Orica', 'E3 & E4', 'exhibitorlogo/1574848294_orica.png', 'www.orica.com', '*****@*****.com', '+61 3 9665 7111', 'Orica is the world’s largest provider of commercial explosives and innovative blasting systems to the mining, quarrying, oil and gas and construction markets, a leading supplier of sodium cyanide for gold extraction, and a specialist provider of ground support services in mining and tunnelling.\r\n\r\nOur purpose is to make our customers successful, every day, all around the world. We take pride in operating safely, responsibly and sustainably. Together, these enable us to grow and create enduring value for our shareholders.\r\n\r\nFounded in 1874, Orica has more than 140 years of experience and investment in innovation. We are the global leader in mining and civil services, with a diverse workforce of around 11,500 employees, servicing customers across more than 100 countries.  Orica is listed on the Australian Securities Exchange (ASX: ORI).', 0, '2019-11-27 09:51:34', '2019-11-27 09:51:34'),
(166, 11, 1, 108, 'Nanotech Industrial Solutions', 'Not Taken', 'exhibitorlogo/1574848752_ifws.png', 'www.nisusacorp.com', 'info@nisusacorp.com', '1-732-313-0020', 'Nanotech Industrial Solutions is the only global manufacturer of submicron spherical particles of Inorganic Fullerene-like Tungsten Disulfide IF-WS2. The unique technology is exclusively licensed to NIS for commercialization worldwide from the Weizmann Institute of Science.\r\n\r\nNIS IF-WS2 particles are designed to have dozens of concentric layers so they excel under extreme pressure, load and impact, and decrease friction by up to 30%. This leads to reduced operating temperatures, metal wear, and overall production cost.', 0, '2019-11-27 09:59:12', '2019-11-27 09:59:12'),
(167, 11, 1, 108, 'ZERO-TRIP Innovations', 'B6', 'exhibitorlogo/1574848862_zero.png', 'www.zero-trip.com', '*****@*****.com', '**********', 'N/A', 0, '2019-11-27 10:01:02', '2019-11-27 10:01:02'),
(168, 11, 1, 109, 'Solmax', 'E36', 'exhibitorlogo/1574849121_solmax.jpg', 'www.solmax.com', '*****@*****.com', '(+1) 450 929 1234', 'Solmax is the world’s largest geosynthetics manufacturer. Our products contain and drain, shielding the soil, water, and air from toxins and pollutants in applications as critical as the landfills of the world’s most populated cities and mines operating in fragile ecosystems. With plants in North America, Europe, Asia, and the Middle East, our products are sold in over 60 countries, trusted by some of the biggest names in mining, oil and gas, water and waste management, and civil engineering. Our leadership is built on 40+ years’ of product innovation and excellence. We aim to reshape the industry, leading change by bringing to market innovative products that are more reliable, stronger, resistant to contaminants, and affordable, even in developing countries.', 0, '2019-11-27 10:05:21', '2019-11-27 10:05:21'),
(169, 11, 1, 109, 'Geobrugg AG', 'E24', 'exhibitorlogo/1574849392_geobrugg.jpg', 'www.geobrugg.com', '*****@*****.com', '+43 664 915 42 91', 'Safety is our nature - true to this philosophy, Geobrugg develops and manufactures protection systems made of high-tensile steel wire. These systems protect against natural hazards such as rockfall, landslides, debris flow and avalanches. They ensure safety in mining and tunneling, as well as on motorsport tracks and stop other impacts from falling or flying objects. More than 65 years of experience and close collaboration with research institutes and universities make Geobrugg a pioneer and leading expert in these fields. Geobrugg has 340 specialists worldwide, production facilities on four continents, as well as branches and partners in over 50 countries. This proximity allows us, to ensure customers quick project implementation with a range of services that includes: needs assessments, consulting, design, testing and system installation. Geobrugg operates as an independent corporation within the Brugg Group, and is headquartered in Romanshorn, Switzerland.', 0, '2019-11-27 10:09:52', '2019-11-27 10:09:52'),
(170, 11, 1, 109, 'AZIWELL AS', 'E16', 'exhibitorlogo/1574849623_Aziwell.jpg', 'www.aziwell.no', 'post@aziwell.no', '+47 41 41 88 18', 'Aziwell is a Norwgian engineering company established early 2015 aiming to revolutionize rock drilling. Through a cooperation with Innovation Norway a set of development projects was started leading to a breakthrough early 2017 when the first prototypes of AziDrill – Directional Core Barrel and OriCore – Core Orienter was successfully tested by a drilling company. The Aziwell engineering team has more than 19 years of experience within R&D in directional core drilling, bore hole survey as well as oil and gas process technology. Aziwell now provides directional drilling services and bore hole survey for the mining and infrastructure industry worldwide. Our technologies provides reduced drilling costs, more information per drilled meter and accurate steering of bore holes.', 0, '2019-11-27 10:13:43', '2019-11-27 10:13:43'),
(171, 11, 1, 109, 'ALS Global', 'E11', 'exhibitorlogo/1574849879_als.jpg', 'www.alsglobal.com', '*****@*****.com', '**********', 'ALS Minerals is the leading full-service provider of testing services for the global mining industry in four key service areas: • Geochemistry • Metallurgy • Mine Site Laboratories • Inspection Our integrated network of 71 laboratories around the world provides unparalleled global coverage for a dynamic industry that seeks consistency, reliability, quality, mobility, versatility and technical leadership from service providers. The ALS Minerals service offering covers the entire resource life-cycle from exploration, feasibility, production, design, development through to trade, and finally rehabilitation.', 0, '2019-11-27 10:17:59', '2019-11-27 10:17:59'),
(172, 11, 1, 109, 'Concrete Canvas', 'E10', 'exhibitorlogo/1574850101_concrete.jpg', 'www.concretecanvas.com', 'info@concretecanvas.com', '+44 (0) 345 680 1908', 'Concrete Canvas is uniquely suited for installations in remote mine sites where logistical efficiency is key, and is well suited to use in extreme environments. It has been installed in mining locations with sub-zero temperatures such as northern British Columbia in Canada and at 4000m altitude in the Chilean Andes.', 0, '2019-11-27 10:21:41', '2019-11-27 10:21:41'),
(173, 11, 1, 109, 'Al Rowad', 'E21', 'exhibitorlogo/1574850254_alrowad.jpg', 'www.rowadplastic.com', 'marketing@rowadplastic.com', '+966 11 2651966', 'Rowad international geosynthetic co. ltd. as a saudi arabian based company specializing in the manufacture of HDPE & LLDPE Geomembrane linings and related systems. ROWAD was the first company of its type to be established in the Arab and GCC Countries in 2006.', 0, '2019-11-27 10:24:14', '2019-11-27 10:24:14'),
(174, 11, 1, 106, 'Geoscience Ireland', 'S3', 'exhibitorlogo/1574850428_Geo.png', 'www.geoscience.ie', 'sean.finlay@gsi.ie', '+353 1 678 2842', 'Geoscience Ireland (GI) is a business cluster of 40 Member Companies, delivering integrated expertise in water, minerals, environmental and infrastructure development to clients in over 70 countries.\r\n\r\nGI is supported by Geological Survey Ireland (a division of Dept. Communication, Climate Action and Environment), Enterprise Ireland and Dept. Foreign Affairs and Trade. The GI Member Companies provide design, consultancy and contracting services to multilateral agencies, governments and the private sector.', 1, '2019-11-27 10:27:08', '2019-11-27 10:27:08'),
(175, 11, 1, 109, 'General Kinematics', 'E15', 'exhibitorlogo/1574850568_general.jpg', 'www.generalkinematics.com', 'info@generalkinematics.com', '+91 6363 34 9454', 'For over fifty years, General Kinematics has led the Mining, Minerals, Aggregate industry in our innovation of vibratory and vibrating process equipment. GK’s core strengths rest in our ability to create energy efficient, high-quality vibratory mining equipment to solve the toughest of process problems. Our proven track record as mining equipment manufacturers & suppliers can be found in our extensive vibratory installation base of over 40,000 units in 35 countries. GK stands behind all our vibrating mining equipment, with unmatched after-sales service and support throughout our global network.', 0, '2019-11-27 10:29:28', '2019-11-27 10:29:28'),
(176, 11, 1, 109, 'Dynaset', 'E9', 'exhibitorlogo/1574850775_dynasty.jpg', 'www.dynaset.com', 'info@dynaset.com', '+358 3 3488 200', 'Dynaset is the global leading manufacturer of hydraulic generators, power washers and compressors. Dynaset hydraulic equipment converts a mobile machine\'s hydraulic power into electricity, high pressure water, compressed air, magnet and vibration. Dynaset products are used for hundreds of applications in various industries all around the world.', 0, '2019-11-27 10:32:55', '2019-11-27 10:32:55');
INSERT INTO `exhibitors` (`id`, `event_id`, `userid`, `detailsid`, `name`, `booth`, `image`, `website`, `email`, `phone`, `description`, `toprated`, `created_at`, `updated_at`) VALUES
(178, 11, 1, 109, 'QME Mining', 'E23', 'exhibitorlogo/1574850975_qme.jpg', 'www.qme.ie', 'info@qme.ie', '+353 4690 73709', 'QME supplies complete solutions to the Mining and Tunnelling industries worldwide. QME are the providers of complete mining & tunnelling technical services solutions, extensive fleet of mining & tunnelling CE certified equipment available, existing organisation of certified key management personnel including large experienced mining & tunnelling workforce, full in-house engineering capabilities including full rebuild/overhaul facilities, flexible equipment rental solutions available and extensive network of service and OEM parts suppliers worldwide.', 0, '2019-11-27 10:36:15', '2019-11-27 10:36:15'),
(179, 11, 1, 109, 'Megaplast India Pvt. Ltd.', 'E13', 'exhibitorlogo/1574851085_megaplast.jpg', 'www.megaplast.in', 'info@megaplast.in', '+91 22-61066000', 'Megaplast are a leading manufacturer of Geomembrane, Films and Carrier Bags (HDPE/LDPE/PE). A dependable PE products manufacturer, with an expertise of more than 10 years. We have gained a reputation for reliability, for providing product innovation, quality consistency, price stability and trustworthiness. Manufactured from India its products are sold in over in different countries around the world also high quality markets like Australia ,Europe, Middle East , USA ,Africa and are used by the biggest names in the mining, petroleum, waste management, water, and civil engineering sectors.', 0, '2019-11-27 10:38:05', '2019-11-27 10:38:05'),
(180, 11, 1, 109, 'GEOREKA Software', 'E25', 'exhibitorlogo/1574851675_georeka.jpg', 'www.georeka.com', 'info@geo-reka.com', '**********', 'GEOREKA software provides powerful 3D geological modelling tools combined with an amazing geology viewer. It provides one of the best value-for-money 3D geological software packages in the industry. Combining technologies from other industries we are also able to provide some of the most advanced features. GEOREKA Software started in October 2012. Since then it has steadily impressed people worldwide. It is now being used in North America, Western-Europe, Russia, Australia and is expanding to South America, Afrika, Eastern-Europe and Asia. GEOREKA was developed with the idea that good 3D geological modelling software should be available to all, not just a few experts.', 0, '2019-11-27 10:47:55', '2019-11-27 10:47:55'),
(181, 11, 1, 109, 'ABMEC UK', 'E17', 'exhibitorlogo/1574851884_ambec.jpg', 'www.abmec.org.uk', 'enquiries@abmec.org.uk', '+44 (0)1924 860258', 'The Association of British Mining Equipment has a history dating back almost 100 year. ABMEC provides a means for its members to accomplish things collectively, that they could not do as effectively or efficiently working alone. In total, member companies represent over $1 billion worth of export mining equipment, with expertise in surface and underground mining of minerals, including coal, gypsum and potash. Their machinery and services are utilised all over the world, meeting stringent environmental requirements and extreme climate challenges. British expertise in engineering is demonstrated with the high quality of workmanship coupled with excellent safety standard.', 0, '2019-11-27 10:51:24', '2019-11-27 10:51:24'),
(182, 11, 1, 109, 'Global Mining Guidelines Group', 'E38', 'exhibitorlogo/1574852008_gmg.jpg', 'www.gmggroup.org', 'info@gmggroup.org', '450-829-9000', 'Global Mining Guidelines Group (GMG) is a network of representatives from mining companies, OEMs, OTMs, research organizations, consultants and regulators around the world who collaborate to tackle challenges facing our industry. Our corporate members are influencers working together to accelerate the speed at which the global mining industry innovates. Our international guidelines are developed through industry-wide collaboration and assist the global mining community in implementing practices to improve operations and/or implement new technologies to ensure a safer, more ef?cient industry. GMG is multi-stakeholder and operator-driven, operates on a global scale, working to secure representation from mining regions around the world, and is an open platform.', 0, '2019-11-27 10:53:28', '2019-11-27 10:53:28'),
(183, 11, 1, 109, 'European Federation of Geologists', 'E7', 'exhibitorlogo/1574852215_efg.png', 'www.eurogeologists.eu', 'glen.burridge@eurogeologists.eu', '+32 2 788 76 36', 'The European Federation of Geologists (EFG) is a non-governmental organisation that was established in 1980 and includes today 26 national association members. EFG is a professional organisation whose main aims are to contribute to a safer and more sustainable use of the natural environment, to protect and inform the public and to promote a more responsible exploitation of natural resources. EFG’s members are National Associations, NAs, whose principal objectives are based in similar aims. The guidelines to achieve these aims are the promotion of excellence in the application of geology and the creation of public awareness of the importance of geoscience for the society.', 0, '2019-11-27 10:56:55', '2019-11-27 10:56:55'),
(184, 11, 1, 109, 'PW Mining International Ltd.', 'E22', 'exhibitorlogo/1574852334_pwmining.jpg', 'www.pwmil.com', 'pwg@pwmil.com', '+233 302 518112 - 6', 'Of Irish origin, PW has been a major force in the field of civil engineering and contract mining in Africa since 1974. As a Mining and Civil Engineering contractor PW Mining International Limited has the capability to deliver on any scale of mining project, this capability covers all aspects of contract mining and civil engineering earth works and related infrastructure on greenfield mine sites as well as developments of existing operations. Working to international standards for Health and Safety and Environmental issues PW Mining International is an experienced operator relied upon by many of the world’s leading mining companies. Experience cannot be underestimated and this is what we are able to bring to any mining operation. Like many other clients you can use our practical knowledge and experience to supplement your theoretical plans for operations. With a substantial stock of equipment PW Mining International is geared up and ready to go. We invite you to talk to us about your ambitions in the sector.', 0, '2019-11-27 10:58:54', '2019-11-27 10:58:54'),
(185, 11, 1, 109, 'ATCO', 'E2', 'exhibitorlogo/1574852587_atco.jpg', 'www.atco.com', '*****@*****.com', '(403) 292-7500', 'We are ATCO Frontec, a part of the ATCO Group of Companies, a $23-billion global enterprise that provides innovative and integrated solutions for Modular Structures, Logistic Services, Electricity, Pipelines & Liquids and Retail Energy. Our Frontec International division specializes in operational logistic support services, remote site turn-key accommodation and disaster and emergency management solutions. Our products and services: Modular Solutions – Office Space Offices, lunchrooms, lavatories • Manufacturing in Canada, USA, Mexico, Chile and Australia Modular Solutions – Workforce Housing • Flexible accommodation options for 2 – 20,000 • Temporary or permanent options • Rental/ customer specific designs Specialty Buildings • Apollo Rapid Deployment Soft-wall Camps Lodging & Camp Services • Catering & food services • Facility maintenance, housekeeping & janitorial • Accommodation management & administration Site Services, Operations & Maintenance • Airfield services • Fire protection and first response • Fuel management and distribution • Water/wastewater services • Solid and hazardous waste management • Power supply', 0, '2019-11-27 11:03:07', '2019-11-27 11:03:07'),
(186, 11, 1, 109, 'DGEG Portugal', 'E39', 'exhibitorlogo/1574852745_dgeg.jpg', 'www.dgeg.gov.pt', 'energia@dgeg.gov.pt', '(351) 217 922 700', 'The Directorate General for Energy and Geology (DGEG) is the Portuguese Public Administration body whose mission is to contribute to the design, promotion and evaluation of policies related to energy and geological resources, with a view to sustainable development and guaranteeing the safety of the supply. The mission of DGEG includes, of course, the need to make citizens aware of the importance of such policies, in the context of the desired economic and social development for the country, informing them of the instruments available for the implementation of political decisions and disseminating the results of its monitoring and implementation.', 0, '2019-11-27 11:05:45', '2019-11-27 11:05:45'),
(187, 11, 1, 109, 'Scantech International Pty', 'E1', 'exhibitorlogo/1574852880_scantech.jpg', 'www.scantech.com.au', 'salesw@scantech.com.au', '+61 8 8350 0200', 'Scantech is the world-leader in the application of on-line real-time measurement technology for bulk materials. The Company has developed a broad range of industrial instrumentation utilising various measurement technologies, including microwave and nuclear methods. The application of our products is principally in the resource sector including cement, coal and minerals industries.', 0, '2019-11-27 11:08:00', '2019-11-27 11:08:00'),
(188, 11, 1, 109, 'Hamilton Manufacturing Inc.', 'E8', 'exhibitorlogo/1574853067_hmi.jpg', 'www.hmi-mfg.com', '*****@*****.com', '1 (208) 733-9689', 'HMI, Hamilton Manufacturing, Inc. is a family owned and operated dust and erosion control products, hydromulch, and cellulose insulation manufacturing facility since 1962. Our environmentally friendly products are Bio-preferred approved, non-toxic and competitively priced. Our highly qualified team provides the versatility that is needed to evaluate, design, and execute projects of all types and sizes in various environments around the world.', 0, '2019-11-27 11:11:07', '2019-11-27 11:11:07'),
(189, 11, 1, 109, 'Apt Information Systems', 'E18', 'exhibitorlogo/1574853167_apt.png', 'www.apt.gr', 'info@apt.gr', '+30 210 69 85 910', 'Apt Information Systems is a technology provider offering integrated digitalization solutions. APT has developed Control Room Management Suite (CRMS) a 360 mining company digitalization platform providing a multi-layered business integration and management tool to digitally handle all operational parameters, correlate real-time technical and financial data and provide robust and comprehensive reporting.', 0, '2019-11-27 11:12:47', '2019-11-27 11:12:47'),
(190, 11, 1, 109, 'Ministry of Economic Affairs and Employment of Finland', 'E5', 'exhibitorlogo/1574853447_ministryfin.jpg', 'www.tem.fi', 'kirjaamo@tem.fi', '+358 29 516001', 'In minerals policy, the aim of the Ministry of Economic Affairs and Employment is to secure Finland’s raw material supply and ensure sustainable use of national resources, international competitiveness, responsible use of natural resources and vitality of Finnish regions. Minerals policy helps to promote research on and production of raw materials for the needs of the society. It also helps to ensure correct management of resources, taking into account the opportunities generated by the circular economy.', 0, '2019-11-27 11:17:27', '2019-11-27 11:17:27'),
(191, 11, 1, 109, 'Sleipner OY', 'E5', 'exhibitorlogo/1574853577_sleip.jpg', 'www.sleipner.fi', 'sleipner@sleipner.fi', '+358 400 497674', 'Sleipner is made for moving and carrying, yet its roots are strongly in its honorable history. In 1996 an excavator operator, Ossi Kortesalmi, came up with his own smart solution for an excavator transport system. The first prototype of Sleipner was born. It was built for a 90 tonne excavator, and it was meant to last. It is still in use. Since the first years, the Sleipner product family has grown to provide solution for excavators up to 565 tonnes and drills and bulldozers up to 120 tonnes. They are all ready to face mobilization challenges in the global mining, quarrying and earthmoving industries.', 0, '2019-11-27 11:19:37', '2019-11-27 11:19:37'),
(192, 11, 1, 109, 'Vumos OY', 'E5', 'exhibitorlogo/1574854761_vumos.png', 'www.vumos.fi', 'info@vumos.fi', '+358 20 734 9842', 'The headquarters of the Vumos concern are located at Renforsin Ranta (a business park in Kajaani) and our main market area is Scandinavia. The company was established in 2008. Our main products include bagging services for industries, machinery contracting and various property maintenance as well as landscaping services. The keyword of Vumos\' services is responsibility. This means responsibility to human beings, to the environment and also towards economy. Constant training helps to ensure the safety of our employees. We provide all our services with a high level of quality and professionalism and are always listening to our customers.', 0, '2019-11-27 11:39:21', '2019-11-27 11:39:21'),
(193, 11, 1, 109, 'Mining Finland', 'E5', 'exhibitorlogo/1574854892_miningfin.jpg', 'www.miningfinland.com', 'harry.sandstrom@miningfinland.com', '+358 400 709 899', 'Mining Finland is a mining industry growth programme and part of the national Team Finland effort. The programme is managed and coordinated by Geological Survey of Finland (GTK) in co-operation with Finpro. The program brings together Finnish companies offering high-quality products and services to the mining industry internationally. We organize networking to connect these companies with international customers and potential partners. Additionally we link Finnish junior mining companies with potential international investors and inform international exploration companies about Finnish mineral potential and mining opportunities.', 0, '2019-11-27 11:41:32', '2019-11-27 11:41:32'),
(194, 11, 1, 109, 'EPSE', 'E5', 'exhibitorlogo/1574855021_epse.png', 'www.epse.fi', '*****@*****.com', '+385 40 752 0530', 'Global EcoProcess Services (EPSE) is a privately owned Finnish company specializing in the treatment of industrial hazardous waste and mining waste waters.', 0, '2019-11-27 11:43:41', '2019-11-27 11:43:41'),
(195, 11, 1, 109, 'Atarfil Geomembranes', 'E6', 'exhibitorlogo/1574855112_atarfil.png', 'www.atarfil.com', 'comercial@atarfil.com', '+34 958 43 92 00', 'Atarfil is a multinational manufacturer of plastic geomembranes produced with polyolefins (high-density polyethylene HDPE, linear low-density polyethylene LLDPE, very low density polyethylene VLDPE and polypropylene PP) for use in Safe Containment applications to ensure Environmental protection in the storage and encapsulation of Domestic, Industrial and Mining Waste or, in general, in waterproofing applications which carry a high level of responsibility related to Hydraulic Works, such as storage basins, sewers and water treatment. We offer a wide range of products and a large production capacity, which ensures both delivery times and quantities, which are extremely important factors for certain applications or geographic areas.', 0, '2019-11-27 11:45:12', '2019-11-27 11:45:12'),
(196, 11, 1, 109, 'Forcit', 'E34', 'exhibitorlogo/1574855380_forcit.png', 'www.forcit.fi', 'sales.defence@forcit.fi', '+358 207 440 400', 'OY FORCIT AB is a Finnish company which operates in the chemical sector. Forcit develops, manufactures, and sells explosives, mainly on the Nordic market. Special products are also exported to markets outside the Nordic countries. Forcit, which also offers consultation and training, is regarded as a competent and reliable actor. The parent company of the group is OY FORCIT AB. The operation is divided into three business areas: Forcit Explosives, Forcit Defence and Forcit Consulting. Forcit Explosives serves the civil explosive industry, while Forcit Defence serves the Defence Forces and produces defence products. Forcit Consulting serves the explosives users by providing them with measurement, control, planning and training services. The group comprises the subsidiaries Forcit Sweden AB, Forcit Norway AS, Forcit International Oy, Oy Finnrock Ab, Bergsäker AB, Bergcon AS and Forcit International Ltd,. Parts of the group are also the associated companies Suomen Tärinämittaus Oy and Vipnordic AB. The Chairman of the Board is Mr. Lauri Stadigh and the Managing Director is Mr. Joakim Westerlund. The group employs approximately 360 people. Forcit’s turnover in 2018 was ca. 107 million euros.', 0, '2019-11-27 11:49:40', '2019-11-27 11:49:40'),
(197, 11, 1, 109, 'Carrier Europe', 'E33', 'exhibitorlogo/1574855483_carrier.png', 'http://www.carriereurope.be', '*****@*****.com', '(32) 67-400130', 'Carrier Europe SCA was established in 1995 to better serve the European marketplace. Like its parent company, the division is known for innovative technology in the field of bulk materials handling and processing equipment. Carrier Europe SCA specializes in the manufacture of vibrating conveyors, feeders, screeners, fluid bed dryers and coolers, and vibrating spiral elevators with features incorporated to screen, separate, dry, cool, blend, and perform various other processing functions. Markets Served: Through the years, Carrier has developed processing equipment to meet the needs of a broad range of European industries, including chemicals, synthetics, foundry, glass, food, dairy, pharmaceuticals, explosives, wood, coal, metals, scrap, and recycling. We pride ourselves on our extensive customer base and our commitment to excellence.', 0, '2019-11-27 11:51:23', '2019-11-27 11:51:23'),
(198, 11, 1, 109, 'HiSeis Pty Ltd', 'E14', 'exhibitorlogo/1574856081_Hise.png', 'www.hiseis.com.au', 'admin@hiseis.com', '+61 8 9470 9866', 'HiSeis provides end to end high definition seismic services focusing on hard rock exploration, and in particular; exploration around existing mine sites and mine life planning. Our proposition is to enable investors and owners to better appreciate and plan the exploration and development of subsurface mineralization. Market demand has been demonstrated through delivering either a beta version of the service or full commercial service to clients and sponsors. We are able to overcome many of the historical challenges associated with the application of seismic techniques to hard rock environments, and we can deliver on key customer requirements for the service. With our intimate knowledge of the capabilities and limitations of seismic imaging and a holistic approach to the image production process we can deliver a previously unattainable result to your organisation. HiSeis applies a unique combination of novel data acquisition equipment, innovative data processing algorithms and proprietary know how in seismic survey design and interpretation. We are able to deliver results in complex geological environments where conventional seismic has previously been an expensive waste of time.', 0, '2019-11-27 12:01:21', '2019-11-27 12:01:21'),
(199, 11, 1, 109, 'Agrigear Tyre and Wheel Specialists', 'E19', 'exhibitorlogo/1574856246_agrigear.png', 'www.agrigear.ie.', '*****@*****.com', '042-9666444', 'Agrigear Tyre and Wheel Specialists, located in County Cavan, Ireland was established in 1980 by Fred Clarke and boasts a span of 40 years’ experience in wheel manufacturing and tyre expertise. With the largest selection of industrial, OTR off-road and agricultural tyres, along with immediate unmatched availability anywhere in Ireland, Agrigear Tyre and Wheel Specialists is Ireland’s leading supplier of tyres and wheels. The BKT tyre range is about high quality at a competitive price. This, coupled with Agrigear’s after-sales service which is further supported by BKT’s European technical and design teams based in Italy, ensures that customers in Ireland get an excellent product and service. Alongside its tyre business, Agrigear also offers a wheel rim design, manufacturing and modification service where ideal wheel configurations are offered to suit a customer’s own requirements. In addition, a wheel rim reconditioning service is also available where a customer can have a set of wheels cleaned and re-painted to look as good as new. Agrigear is the appointed Irish importer and distributor of BKT tyres throughout Ireland and offers a full technical backup service on all their products.', 0, '2019-11-27 12:04:06', '2019-11-27 12:04:06'),
(200, 11, 1, 109, 'Dassault Systemes', 'E41', 'exhibitorlogo/1574856364_dassault.png', 'www.3ds.com', '*****@*****.com', '+33 (0)1 6162 6162', 'Dassault Systèmes, the 3DEXPERIENCE Company, provides business and people with virtual universes to imagine sustainable innovations. Its world-leading solutions transform the way products are designed, produced, and supported. Dassault Systèmes’ collaborative solutions foster social innovation, expanding possibilities for the virtual world to improve the real world. The group brings value to over 250, 000 customers of all sizes, in all industries, in more than 140 countries. For more information, visit www.3ds.com GEOVIA As part of Dassault Systèmes, GEOVIA provides 3DEXPERIENCE® Universes to Model and Simulate our Planet from the vast expanse of the geosphere to the smallest details of urban settlements. GEOVIA is home to world-renowned mining solutions including Surpac, GEMS, Minex, Whittle, MineSched and InSite.', 0, '2019-11-27 12:06:04', '2019-11-27 12:06:04'),
(201, 11, 1, 109, 'Nordic Gold Inc.', 'E27', 'exhibitorlogo/1574856459_nordic.png', 'www.nordic.gold', 'info@nordic.gold', '604-609-6189', 'Nordic Gold Inc. (TSXV:NOR) wholly owns the Laiva Gold Mine. The Laiva Gold Mine is a fully permitted open pit operation with a 2 million tonne per annum process plant in place and two open pits. The sunk capital of the project to date is in excess of CAD 300 million, the majority being spent on the start of the art process plant built by Metso and Outotec. The mine is fully permitted and ready for production.', 0, '2019-11-27 12:07:39', '2019-11-27 12:07:39'),
(202, 11, 1, 109, 'AutoStem Europe B.V.', 'E28', 'exhibitorlogo/1574856573_autost.png', 'www.autostemtechnology.com', '*****@*****.com', '+27 (082) 978 8548', 'AutoStem is the world\'s safest and most advanced non-detonating blasting technology for primary and secondary blasting. The revolutionary alternative to dangerous high explosives with application in mining, tunnelling, trenching, benching, civil infrastructure, quarrying and underwater. Fully CE accredited as Product of Pyrotechnic. AutoStem is the global leader in the development of alternative explosive technology offering a safer, environmentally friendly and user-friendly and cost-effective replacement for explosives. The AutoStem technology is the reinvention of explosives.', 0, '2019-11-27 12:09:33', '2019-11-27 12:09:33'),
(203, 11, 1, 109, 'Ess- Engineering software steyr GmbH', 'B2', 'exhibitorlogo/1574856705_ess.png', 'www.essteyr.com', '*****@*****.com', '+43 7252 20446', 'We are ESS. An award-winning deep-tech startup located in the heart of Austria. Our company was founded in 2015 as a spin-off from research projects, experiments, and intensive work in the field of Computational Fluid Dynamics (CFD). We develop next-level simulation software that helps to design and optimize more sustainable and reliable products, production lines, and processes', 0, '2019-11-27 12:11:45', '2019-11-27 12:11:45'),
(204, 11, 1, 109, 'Ambisense', 'E29', 'exhibitorlogo/1574856850_ambi.png', 'www.ambisense.net', 'info@ambisense.net', '+353 1 9072790', 'Since 2014 Ambisense has been driving innovation within the environmental space. Our technology platform optimizes the delivery of environmental risk assessment on some of the world’s largest infrastructure projects across industrial, O&G and waste verticals. Ambilytics encompasses both IoT and machine learning solutions, combining information from remotely deployed field devices with contextual data sources such as weather, satellite, geophysical and operational data to build machine learning models. These models identify the relationships, patterns, and drivers hidden within the data and allow our customers to forecast and predict the behaviour of targeted environmental pollutants.', 0, '2019-11-27 12:14:10', '2019-11-27 12:14:10');

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE `favourites` (
  `id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `fav` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favourites`
--

INSERT INTO `favourites` (`id`, `event_id`, `post_id`, `user_id`, `fav`, `created_at`, `updated_at`) VALUES
(1, 10, 6, 58, 1, '2019-11-20 12:40:18', '2019-11-20 12:40:18'),
(3, 10, 5, 19, 1, '2019-11-20 15:15:31', '2019-11-20 15:15:31'),
(5, 10, 6, 108, 1, '2019-11-20 15:24:51', '2019-11-20 15:24:51'),
(6, 10, 5, 108, 1, '2019-11-20 15:24:53', '2019-11-20 15:24:53'),
(7, 10, 5, 111, 1, '2019-11-20 16:09:37', '2019-11-20 16:09:37'),
(8, 10, 4, 111, 1, '2019-11-20 16:09:45', '2019-11-20 16:09:45'),
(9, 10, 4, 19, 1, '2019-11-21 13:23:30', '2019-11-21 13:23:30'),
(11, 10, 5, 41, 1, '2019-11-24 13:57:24', '2019-11-24 13:57:24'),
(12, 10, 6, 41, 1, '2019-11-24 13:57:29', '2019-11-24 13:57:29'),
(13, 10, 6, 19, 1, '2019-11-27 14:52:37', '2019-11-27 14:52:37');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `usename` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `name`, `usename`, `created_at`, `updated_at`) VALUES
(1, 'about', 'About Us', '2019-10-30 09:05:12', '2019-10-30 09:05:12'),
(2, 'schedule', 'Schedules', '2019-10-30 09:05:12', '2019-10-30 09:05:12'),
(3, 'exhibitors', 'Exhibitors', '2019-10-30 09:05:12', '2019-10-30 09:05:12'),
(4, 'sponsers', 'Sponsors', '2019-10-30 09:05:12', '2019-10-30 10:41:24'),
(5, 'speakers', 'Speakers', '2019-10-30 09:05:12', '2019-10-30 09:05:12'),
(6, 'floor', 'Floor Plan', '2019-10-30 09:05:12', '2019-10-30 09:05:12');

-- --------------------------------------------------------

--
-- Table structure for table `floor_plans`
--

CREATE TABLE `floor_plans` (
  `id` int(11) NOT NULL,
  `event_Id` int(11) DEFAULT NULL,
  `detailsid` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `floor_image` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `floor_plans`
--

INSERT INTO `floor_plans` (`id`, `event_Id`, `detailsid`, `name`, `floor_image`, `created_at`, `updated_at`) VALUES
(1, 1, 11, NULL, 'floorimages/1572428163_ffffff.png', '2019-10-30 09:36:03', '2019-10-30 09:36:03'),
(2, 1, 11, NULL, 'floorimages/1572428163_RoomSketcher-Order-Floor-Plans-2D-Floor-Plan.jpg', '2019-10-30 09:36:03', '2019-10-30 09:36:03'),
(3, 2, 19, NULL, 'floorimages/1572875708_ffffff.png', '2019-11-04 13:55:08', '2019-11-04 13:55:08'),
(4, 2, 19, NULL, 'floorimages/1572875708_IMG_0447.PNG', '2019-11-04 13:55:08', '2019-11-04 13:55:08'),
(5, 2, 19, NULL, 'floorimages/1572875708_IMG_1009.PNG', '2019-11-04 13:55:08', '2019-11-04 13:55:08'),
(6, 2, 19, NULL, 'floorimages/1572875708_Mohamed-Abdel-Vetah.png', '2019-11-04 13:55:08', '2019-11-04 13:55:08'),
(7, 2, 19, NULL, 'floorimages/1572875708_privacy.png', '2019-11-04 13:55:08', '2019-11-04 13:55:08'),
(8, 4, 28, NULL, 'floorimages/1573018978_floor.jpg', '2019-11-06 05:42:58', '2019-11-06 05:42:58'),
(9, 5, 31, NULL, 'floorimages/1573019314_floor.jpg', '2019-11-06 05:48:34', '2019-11-06 05:48:34'),
(10, 6, 45, NULL, 'floorimages/1573195060_index.jpeg', '2019-11-08 06:37:40', '2019-11-08 06:37:40'),
(11, 6, 45, NULL, 'floorimages/1573195061_man2.jpeg', '2019-11-08 06:37:41', '2019-11-08 06:37:41'),
(12, 6, 45, NULL, 'floorimages/1573195061_man3.jpeg', '2019-11-08 06:37:41', '2019-11-08 06:37:41'),
(13, 6, 45, NULL, 'floorimages/1573195061_man4.jpeg', '2019-11-08 06:37:41', '2019-11-08 06:37:41'),
(14, 6, 45, NULL, 'floorimages/1573195061_man5.jpeg', '2019-11-08 06:37:41', '2019-11-08 06:37:41'),
(15, 8, 56, NULL, 'floorimages/1573209612_logo4.png', '2019-11-08 10:40:12', '2019-11-08 10:40:12'),
(17, 10, 102, NULL, 'floorimages/1574006562_img-1.png', '2019-11-17 16:02:42', '2019-11-17 16:02:42'),
(18, 11, 114, NULL, 'floorimages/1574922123_EMC_FloorPlan_.jpg', '2019-11-28 06:22:03', '2019-11-28 06:22:03');

-- --------------------------------------------------------

--
-- Table structure for table `join_events`
--

CREATE TABLE `join_events` (
  `id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `join_events`
--

INSERT INTO `join_events` (`id`, `event_id`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 9, 1, '2019-10-30 09:39:50', '2019-10-30 09:40:26'),
(2, 1, 12, 1, '2019-10-30 10:08:08', '2019-10-30 10:08:38'),
(3, 1, 25, 1, '2019-10-30 10:25:08', '2019-10-30 10:25:21'),
(4, 1, 27, 1, '2019-10-30 12:27:07', '2019-10-30 12:29:25'),
(5, 1, 19, 1, '2019-10-30 13:14:39', '2019-10-30 13:14:58'),
(6, 1, 28, 1, '2019-10-31 05:17:38', '2019-10-31 05:18:29'),
(7, 1, 21, 1, '2019-10-31 12:14:43', '2019-10-31 12:15:04'),
(8, 1, 33, 1, '2019-11-04 10:38:30', '2019-11-04 10:50:15'),
(9, 2, 21, 1, '2019-11-04 13:59:38', '2019-11-04 13:59:44'),
(10, 2, 12, 1, '2019-11-05 06:12:51', '2019-11-05 06:12:55'),
(11, 3, 12, 1, '2019-11-05 06:16:41', '2019-11-05 06:16:48'),
(12, 3, 25, 1, '2019-11-05 06:37:25', '2019-11-05 06:37:46'),
(13, 2, 27, 1, '2019-11-05 10:17:14', '2019-11-05 10:17:18'),
(14, 3, 27, 1, '2019-11-05 10:23:26', '2019-11-05 10:23:34'),
(15, 1, 20, 1, '2019-11-05 14:38:50', '2019-11-05 14:39:17'),
(16, 1, 35, 0, '2019-11-05 14:45:11', '2019-11-05 14:45:11'),
(17, 5, 28, 1, '2019-11-06 05:54:07', '2019-11-06 05:54:16'),
(18, 5, 19, 1, '2019-11-06 05:58:25', '2019-11-06 05:58:34'),
(19, 5, 27, 1, '2019-11-06 07:10:43', '2019-11-06 07:10:50'),
(20, 6, 12, 1, '2019-11-08 06:49:08', '2019-11-08 06:53:32'),
(21, 7, 12, 1, '2019-11-08 09:24:04', '2019-11-08 09:24:11'),
(22, 7, 36, 1, '2019-11-08 09:36:43', '2019-11-08 09:37:34'),
(23, 8, 36, 1, '2019-11-08 10:41:41', '2019-11-08 10:41:47'),
(24, 8, 12, 1, '2019-11-08 10:45:29', '2019-11-08 10:45:33'),
(25, 8, 37, 1, '2019-11-08 11:04:02', '2019-11-08 11:04:07'),
(26, 8, 38, 1, '2019-11-08 11:05:48', '2019-11-08 11:05:52'),
(27, 8, 39, 1, '2019-11-08 11:12:43', '2019-11-08 11:12:47'),
(28, 8, 40, 1, '2019-11-08 11:17:14', '2019-11-08 11:17:18'),
(29, 8, 41, 1, '2019-11-08 11:32:33', '2019-11-08 11:32:41'),
(30, 8, 42, 1, '2019-11-08 11:43:31', '2019-11-08 11:46:19'),
(31, 8, 21, 1, '2019-11-08 12:00:28', '2019-11-08 13:20:22'),
(32, 6, 21, 1, '2019-11-08 12:34:00', '2019-11-08 13:20:49'),
(33, 6, 43, 1, '2019-11-08 12:50:19', '2019-11-08 12:50:27'),
(34, 6, 9, 1, '2019-11-08 12:59:08', '2019-11-08 12:59:14'),
(35, 8, 43, 1, '2019-11-08 13:28:08', '2019-11-08 13:28:12'),
(36, 6, 36, 1, '2019-11-11 11:55:17', '2019-11-11 11:55:23'),
(37, 8, 25, 1, '2019-11-11 12:47:09', '2019-11-11 12:47:21'),
(38, 1, 37, 1, '2019-11-11 13:36:52', '2019-11-11 13:37:06'),
(39, 8, 9, 1, '2019-11-11 13:59:06', '2019-11-11 13:59:11'),
(40, 6, 41, 1, '2019-11-12 07:24:32', '2019-11-12 07:24:36'),
(41, 6, 25, 1, '2019-11-12 08:50:07', '2019-11-12 08:50:13'),
(42, 3, 19, 0, '2019-11-12 08:59:21', '2019-11-12 08:59:21'),
(43, 10, 19, 1, '2019-11-16 15:55:17', '2019-11-16 15:55:23'),
(44, 2, 19, 0, '2019-11-16 16:33:45', '2019-11-16 16:33:45'),
(45, 10, 20, 1, '2019-11-16 21:52:22', '2019-11-16 21:52:34'),
(46, 10, 45, 1, '2019-11-18 04:22:22', '2019-11-18 04:22:56'),
(47, 2, 9, 1, '2019-11-18 07:27:47', '2019-11-18 07:29:57'),
(48, 10, 9, 1, '2019-11-18 09:27:28', '2019-11-18 09:27:40'),
(49, 10, 21, 1, '2019-11-18 09:30:06', '2019-11-18 09:30:15'),
(50, 10, 12, 1, '2019-11-18 09:53:44', '2019-11-18 09:53:52'),
(51, 10, 47, 0, '2019-11-18 14:55:27', '2019-11-18 14:55:27'),
(52, 10, 48, 1, '2019-11-18 16:08:45', '2019-11-18 16:09:11'),
(53, 10, 49, 1, '2019-11-18 17:18:43', '2019-11-18 17:18:55'),
(54, 10, 51, 1, '2019-11-19 12:39:41', '2019-11-19 12:40:04'),
(55, 10, 52, 0, '2019-11-19 12:47:56', '2019-11-19 12:47:56'),
(56, 10, 54, 0, '2019-11-19 12:58:23', '2019-11-19 12:58:23'),
(57, 10, 55, 1, '2019-11-19 13:22:56', '2019-11-19 21:26:26'),
(58, 10, 59, 1, '2019-11-19 13:53:37', '2019-11-19 13:56:33'),
(59, 10, 61, 1, '2019-11-19 14:27:26', '2019-11-20 11:32:18'),
(60, 2, 55, 0, '2019-11-19 14:39:47', '2019-11-19 14:39:47'),
(61, 10, 62, 1, '2019-11-19 14:45:18', '2019-11-19 14:45:33'),
(62, 10, 63, 1, '2019-11-19 14:48:39', '2019-11-19 16:29:54'),
(63, 10, 64, 1, '2019-11-19 14:52:07', '2019-11-20 13:42:26'),
(64, 10, 65, 1, '2019-11-19 15:01:21', '2019-11-19 15:01:51'),
(65, 10, 66, 1, '2019-11-19 15:35:52', '2019-11-19 15:36:22'),
(66, 10, 67, 0, '2019-11-19 15:37:56', '2019-11-19 15:37:56'),
(67, 10, 69, 1, '2019-11-19 17:27:30', '2019-11-19 17:28:16'),
(68, 10, 70, 1, '2019-11-19 18:03:45', '2019-11-19 18:04:48'),
(69, 10, 71, 1, '2019-11-19 19:45:04', '2019-11-19 19:49:40'),
(70, 10, 72, 1, '2019-11-19 19:47:24', '2019-11-19 19:49:51'),
(71, 10, 73, 1, '2019-11-19 21:11:19', '2019-11-20 10:38:24'),
(72, 10, 74, 1, '2019-11-19 21:27:49', '2019-11-19 21:28:40'),
(73, 10, 75, 1, '2019-11-19 22:09:30', '2019-11-19 22:10:03'),
(74, 10, 76, 1, '2019-11-20 00:20:53', '2019-11-20 00:21:30'),
(75, 10, 77, 1, '2019-11-20 02:11:31', '2019-11-20 02:12:00'),
(76, 10, 78, 1, '2019-11-20 02:52:31', '2019-11-20 02:52:54'),
(77, 10, 96, 1, '2019-11-20 11:50:09', '2019-11-20 11:50:57'),
(78, 10, 58, 1, '2019-11-20 12:39:29', '2019-11-20 12:39:37'),
(79, 10, 97, 1, '2019-11-20 12:56:27', '2019-11-20 12:57:49'),
(80, 10, 103, 1, '2019-11-20 13:31:56', '2019-11-20 14:46:11'),
(81, 10, 53, 1, '2019-11-20 13:38:39', '2019-11-20 17:01:16'),
(82, 10, 104, 0, '2019-11-20 13:40:28', '2019-11-20 13:40:28'),
(83, 10, 105, 1, '2019-11-20 13:47:38', '2019-11-20 13:49:32'),
(84, 10, 106, 1, '2019-11-20 14:49:35', '2019-11-20 14:49:50'),
(85, 10, 107, 1, '2019-11-20 15:07:51', '2019-11-20 15:08:11'),
(86, 10, 108, 1, '2019-11-20 15:14:56', '2019-11-20 15:15:06'),
(87, 10, 110, 0, '2019-11-20 15:52:02', '2019-11-20 15:52:02'),
(88, 10, 111, 1, '2019-11-20 15:56:01', '2019-11-20 16:09:16'),
(89, 10, 112, 1, '2019-11-20 17:08:51', '2019-11-20 17:39:15'),
(90, 10, 113, 1, '2019-11-20 17:22:35', '2019-11-20 17:22:46'),
(91, 10, 114, 1, '2019-11-20 17:36:04', '2019-11-20 17:36:57'),
(92, 10, 115, 0, '2019-11-20 17:39:53', '2019-11-20 17:39:53'),
(93, 10, 116, 1, '2019-11-20 18:14:18', '2019-11-20 18:14:56'),
(94, 10, 41, 1, '2019-11-21 07:44:30', '2019-11-21 07:46:21'),
(95, 11, 19, 1, '2019-11-22 11:09:31', '2019-11-22 11:09:38'),
(96, 11, 108, 1, '2019-11-22 11:13:53', '2019-11-22 11:14:02'),
(97, 11, 12, 1, '2019-11-22 14:42:14', '2019-11-25 05:22:16'),
(98, 10, 124, 1, '2019-11-22 15:30:00', '2019-11-22 15:30:33'),
(99, 11, 21, 1, '2019-11-22 16:10:47', '2019-11-22 16:10:56'),
(100, 12, 12, 1, '2019-11-25 05:26:51', '2019-11-25 05:26:56'),
(101, 12, 21, 1, '2019-11-25 09:04:50', '2019-11-25 09:04:56'),
(102, 2, 125, 0, '2019-11-26 19:49:41', '2019-11-26 19:49:41'),
(103, 11, 125, 0, '2019-11-26 19:49:48', '2019-11-26 19:49:48'),
(104, 11, 126, 1, '2019-11-29 07:49:19', '2019-11-29 07:49:30'),
(105, 11, 127, 1, '2019-11-29 10:06:08', '2019-11-29 10:08:08');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(6, '2016_06_01_000004_create_oauth_clients_table', 2),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `notify_id` int(11) DEFAULT NULL,
  `content` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `notify_id`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 'New post has submited on MMC 2019 event by Tarun', '2019-11-15 06:57:23', '2019-11-15 06:57:23'),
(2, 6, 'New post has submited on Story Writers/Novel Writers/Screenplay Gathered around New Delhi\'s Red Fort to celebrate their values. event by Tarun', '2019-11-15 06:58:26', '2019-11-15 06:58:26'),
(3, 8, 'New post has submited on Atlease 50 Exibitors/ 50 Sponsors/ 50 Speakers event by Tarun', '2019-11-15 07:09:19', '2019-11-15 07:09:19'),
(4, 10, 'GIPEX event invitation calling you, Book now', '2019-11-16 15:48:02', '2019-11-16 15:48:02'),
(5, 10, 'New post has submited on GIPEX event by Luis Wilhelm', '2019-11-19 15:38:28', '2019-11-19 15:38:28'),
(6, 10, 'New post has submited on GIPEX event by jameel', '2019-11-19 20:01:00', '2019-11-19 20:01:00'),
(7, 10, 'New post has submited on GIPEX event by jameel', '2019-11-20 00:54:14', '2019-11-20 00:54:14'),
(8, 10, 'John Quelch like on GIPEX event post', '2019-11-20 12:40:18', '2019-11-20 12:40:18'),
(9, 10, 'Mir Suhaib like on GIPEX event post', '2019-11-20 15:15:29', '2019-11-20 15:15:29'),
(10, 10, 'Mir Suhaib like on GIPEX event post', '2019-11-20 15:15:31', '2019-11-20 15:15:31'),
(11, 10, 'Mir Suhaib like on GIPEX event post', '2019-11-20 15:15:54', '2019-11-20 15:15:54'),
(12, 10, 'Adnan Hassan like on GIPEX event post', '2019-11-20 15:24:51', '2019-11-20 15:24:51'),
(13, 10, 'Adnan Hassan like on GIPEX event post', '2019-11-20 15:24:53', '2019-11-20 15:24:53'),
(14, 10, 'Faheem Wani like on GIPEX event post', '2019-11-20 16:09:37', '2019-11-20 16:09:37'),
(15, 10, 'Faheem Wani like on GIPEX event post', '2019-11-20 16:09:45', '2019-11-20 16:09:45'),
(16, 10, 'Mir Suhaib like on GIPEX event post', '2019-11-21 13:23:30', '2019-11-21 13:23:30'),
(17, 11, 'EMc event invitation calling you, Book now', '2019-11-22 11:05:54', '2019-11-22 11:05:54'),
(18, NULL, 'Abhishek Kumar comment on GIPEX event post', '2019-11-24 13:56:52', '2019-11-24 13:56:52'),
(19, 10, 'Abhishek Kumar like on GIPEX event post', '2019-11-24 13:57:13', '2019-11-24 13:57:13'),
(20, 10, 'Abhishek Kumar like on GIPEX event post', '2019-11-24 13:57:24', '2019-11-24 13:57:24'),
(21, 10, 'Abhishek Kumar like on GIPEX event post', '2019-11-24 13:57:29', '2019-11-24 13:57:29'),
(22, 12, 'And Even event invitation calling you, Book now', '2019-11-25 05:26:05', '2019-11-25 05:26:05'),
(23, 10, 'Mir Suhaib like on GIPEX event post', '2019-11-27 14:52:37', '2019-11-27 14:52:37');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('000c964fde2278928cdad76352dad40c5d0b0c228d43b54c68aab09ec9ae9b1c87f5e7a6761ba958', 50, 2, NULL, '[]', 0, '2019-11-19 10:20:33', '2019-11-19 10:20:33', '2020-11-19 10:20:33'),
('0027c82ab73c66d9e32f18e550dfb3502a9d00a210f97112e1cb5d27c7fc5142eedb135d90d86897', 12, 2, NULL, '[]', 1, '2019-11-11 11:37:57', '2019-11-11 11:37:57', '2020-11-11 11:37:57'),
('0098412dfe751afb6d7fc13af522a258c0687f78a755c67263dd323a63f54ed5df0f7321e2199c7c', 19, 2, NULL, '[]', 0, '2019-10-30 13:14:39', '2019-10-30 13:14:39', '2020-10-30 13:14:39'),
('00a89f80487d682b2d5d78b9bfa0d782c8dc5a5dd8883b48770454c04d492ae06c3f060555debf69', 82, 2, NULL, '[]', 0, '2019-11-20 05:26:01', '2019-11-20 05:26:01', '2020-11-20 05:26:01'),
('00bdc103f66694045f679a1e1b61348da63650e2531038d09bb7390c05c4ca1495b06e24e1016476', 113, 2, NULL, '[]', 0, '2019-11-20 17:22:22', '2019-11-20 17:22:22', '2020-11-20 17:22:22'),
('01922fdcc811b691db656ff71007def7874e5c018e5944cf790d61a380804f3f77d11e02dfbee45f', 9, 2, NULL, '[]', 1, '2019-11-11 12:18:38', '2019-11-11 12:18:38', '2020-11-11 12:18:38'),
('01c4d51a0422bd9685a314a33633ec326b87003abcc6fc5dc9ff31e23b868dea60c870853bebc033', 12, 2, NULL, '[]', 0, '2019-10-31 11:29:34', '2019-10-31 11:29:34', '2020-10-31 11:29:34'),
('024befbf2e161852709c588ceabcefc4adcc2a9471ed5f53b93fa109fd513c6621784398e66b4607', 100, 2, NULL, '[]', 0, '2019-11-20 13:26:05', '2019-11-20 13:26:05', '2020-11-20 13:26:05'),
('02a497fa0c47b53276aac6245f9129c1a74ada8000d30652336d6a97117bed873c45363b9a869871', 36, 2, NULL, '[]', 1, '2019-11-08 09:36:29', '2019-11-08 09:36:29', '2020-11-08 09:36:29'),
('02eac8e94a5c189c25368d1f68aea39cfd275bd1fe1cec5a4a251c036d67e0c8e4437b94e5954fd8', 12, 2, NULL, '[]', 0, '2019-11-14 07:42:11', '2019-11-14 07:42:11', '2020-11-14 07:42:11'),
('0369e4dbc2bdb3fda403d028e7483c6aa8446cb5e88807dc1e1a6af26254f9bd47cd3b7ef76acb23', 9, 2, NULL, '[]', 0, '2019-11-18 07:27:26', '2019-11-18 07:27:26', '2020-11-18 07:27:26'),
('03b561dbb8fce2dc70aaec66d6b1ab0374ca422edd505dec22a2c6396d3d2b83aa4cf2c0559af22f', 19, 2, NULL, '[]', 0, '2019-11-20 09:29:13', '2019-11-20 09:29:13', '2020-11-20 09:29:13'),
('05661f183cfb24573f4bac712f38b37e95d0aa3b6bebedb751b0b0cde91a1f35c8047719a544e671', 9, 2, NULL, '[]', 0, '2019-10-30 11:07:32', '2019-10-30 11:07:32', '2020-10-30 11:07:32'),
('0663ddecbc03a16acc1ce8304dade3681544dbcf725d4dc4fa3c7dedc0b3819352b9bf271e72d09d', 117, 2, NULL, '[]', 0, '2019-11-21 07:32:43', '2019-11-21 07:32:43', '2020-11-21 07:32:43'),
('06688fb8ae40ccca87286cb106622d2358f119bd59851928e0489a83be7afa3f9a2fe3d00883d266', 9, 2, NULL, '[]', 0, '2019-10-18 10:11:23', '2019-10-18 10:11:23', '2020-10-18 10:11:23'),
('070d4d24eeb1ada1bc3614b3880654c3148fb30b3a22ec848837285e71342405a358a17b32d5f508', 12, 2, NULL, '[]', 0, '2019-10-31 14:54:52', '2019-10-31 14:54:52', '2020-10-31 14:54:52'),
('0747efcab3ce3afd3eb7561fd38b85b45581b7b8f5f8b3783ab48a536d8fe0d2235d95195f772d01', 96, 2, NULL, '[]', 0, '2019-11-20 11:50:09', '2019-11-20 11:50:09', '2020-11-20 11:50:09'),
('075bc8a37e8a0a4630031b27c6d05833f00611d0e29be24b2a6e6e7554e9b8dd57436c4f4fb74bc7', 47, 2, NULL, '[]', 0, '2019-11-18 14:55:13', '2019-11-18 14:55:13', '2020-11-18 14:55:13'),
('076c1fa9c5d1fb08053d8ab04026620460a14db3eee380cb021778150a7862b43d36656ffcf57be6', 53, 2, NULL, '[]', 0, '2019-11-19 12:51:21', '2019-11-19 12:51:21', '2020-11-19 12:51:21'),
('07a43c8c8f6854490b18fdc39b1d2df08412d94889f59f7879ab2738160c8fd4bf4928075725d098', 9, 2, NULL, '[]', 0, '2019-10-23 10:11:07', '2019-10-23 10:11:07', '2020-10-23 10:11:07'),
('08598e3d9073f44884ac8814ac236a2853b6cfbd6c11073e3f687b7f77df429a23e51cd743792e75', 55, 2, NULL, '[]', 0, '2019-11-19 14:47:56', '2019-11-19 14:47:56', '2020-11-19 14:47:56'),
('08e5ad09d65be93811f5452bb81fcc7e4860144699e86ee44367e1d8b2e874e8081a98494618534a', 19, 2, NULL, '[]', 0, '2019-10-31 07:54:06', '2019-10-31 07:54:06', '2020-10-31 07:54:06'),
('09f54a120d36034155127b26f14cd181785ce6cb31e41368f3bfba7a3632b99f43e27ff897f1b32c', 9, 2, NULL, '[]', 0, '2019-10-18 10:30:40', '2019-10-18 10:30:40', '2020-10-18 10:30:40'),
('0a791fa6b589525508ac4af10515079944667b14ec92353fa564819f4bceb82003b4fb935568c13e', 125, 2, NULL, '[]', 0, '2019-11-26 19:47:48', '2019-11-26 19:47:48', '2020-11-26 19:47:48'),
('0b8af389ac3a669aa3fcf9bb7a2a714271dd647c5f39f284dc9a0320a8845b6d88ec49b8e8eeb667', 93, 2, NULL, '[]', 0, '2019-11-20 07:33:28', '2019-11-20 07:33:28', '2020-11-20 07:33:28'),
('0b919cec92a0be5c47f19495fa5eb33a4651860cb034face7f5250584c980938b2139e25ed150a2f', 19, 2, NULL, '[]', 0, '2019-11-18 21:14:34', '2019-11-18 21:14:34', '2020-11-18 21:14:34'),
('0c108ef4a03c2d8184422ae5aa792aea74d4f23a657b80a346e74a894293504f78ac6fe01ef903d2', 12, 2, NULL, '[]', 0, '2019-10-22 12:14:59', '2019-10-22 12:14:59', '2020-10-22 12:14:59'),
('0c179977ab6168d1ae6ab9a2211256f6c09ce7a786d23562e12b05d27cc554dba6d78a5dbd40a4bb', 19, 2, NULL, '[]', 0, '2019-11-14 07:49:55', '2019-11-14 07:49:55', '2020-11-14 07:49:55'),
('0c41b2ff3d63fe73fa0a8fdf34085babeb38c391b3116a1b3e0a828915b36c1dc597736e6b25f284', 127, 2, NULL, '[]', 0, '2019-11-29 10:05:58', '2019-11-29 10:05:58', '2020-11-29 10:05:58'),
('0cef7d138b8192541edd4f7751d2285a2754d1128757eb689b04bf9f21b507b36acfb38c7adf4635', 12, 2, NULL, '[]', 1, '2019-11-11 07:01:36', '2019-11-11 07:01:36', '2020-11-11 07:01:36'),
('0d0b6b06afe7858b9c9ac7e287f8dcaf797850921a7dd91831091cc4fdec306a6d80d1e4ad312c1f', 21, 2, NULL, '[]', 1, '2019-10-24 10:30:06', '2019-10-24 10:30:06', '2020-10-24 10:30:06'),
('0da21c5c3c0fa7933185c859120b232f17a827571cbf23e8036c8a75ce22c17d683baa72c22c8d95', 9, 2, NULL, '[]', 1, '2019-11-15 15:37:18', '2019-11-15 15:37:18', '2020-11-15 15:37:18'),
('0e370411e91e7745f43f81c92e8f32b435a909945e4d312a4581739331aa8d20b5cb2b4828e854b0', 46, 2, NULL, '[]', 0, '2019-11-18 13:26:53', '2019-11-18 13:26:53', '2020-11-18 13:26:53'),
('0e6401f1c743f3f7655f67a0848aa8f09a32cc1251e47cb63c0aea322f06d8e3e376e1b2c756bfa6', 46, 2, NULL, '[]', 0, '2019-11-20 09:02:37', '2019-11-20 09:02:37', '2020-11-20 09:02:37'),
('0ea057589e99937f37391c6d86db55282f88b6f934588fbee84a48d493fad870af770d5a01a66a3e', 81, 2, NULL, '[]', 0, '2019-11-20 05:24:27', '2019-11-20 05:24:27', '2020-11-20 05:24:27'),
('0efc972d2d349b96b030e31929f48a0c894b4bc5ec6fcd34f26094955bbe6bfbcbebb3d64325bf77', 12, 2, NULL, '[]', 1, '2019-11-11 13:53:52', '2019-11-11 13:53:52', '2020-11-11 13:53:52'),
('0f29d08d3ddcbbf0f7727f8759544de32d2ab15d3e9a5b3c4471b66f5c9c480f0007543835715974', 31, 2, NULL, '[]', 1, '2019-10-31 11:16:49', '2019-10-31 11:16:49', '2020-10-31 11:16:49'),
('128f11e1acffc33e2a2906ed9defb4445753670e2344befe9db3f29be89c7cebce6096db26a2d1ca', 70, 2, NULL, '[]', 0, '2019-11-19 18:03:14', '2019-11-19 18:03:14', '2020-11-19 18:03:14'),
('130059987892b3971716369b947748d407ed3b332e7c671dab9c68654e872cd15a51d06835056915', 21, 2, NULL, '[]', 0, '2019-11-21 07:41:32', '2019-11-21 07:41:32', '2020-11-21 07:41:32'),
('140af88be99b6b26a6d3c077df6cd7e8682be9146b48ce047d15bd4a0256483e566bb9392d5fa3c6', 9, 2, NULL, '[]', 0, '2019-10-18 09:48:31', '2019-10-18 09:48:31', '2020-10-18 09:48:31'),
('1473743f68baad433e4d832570d1724bb0513f60e007ac3d8c0e38a53f3acd8699937efc06b1d8aa', 57, 2, NULL, '[]', 0, '2019-11-19 13:50:12', '2019-11-19 13:50:12', '2020-11-19 13:50:12'),
('14df868f01d370b0b3bdaa300c44792806dc18ae842fb503e96a17e0d9fbf61bb8e8b17da821f9dc', 50, 2, NULL, '[]', 0, '2019-11-19 10:28:19', '2019-11-19 10:28:19', '2020-11-19 10:28:19'),
('1650826eb8379bcee20a85a6024647825181c7caf22275c618cd2da1a7c24d4b0e609aee1314035c', 21, 2, NULL, '[]', 0, '2019-10-14 11:09:10', '2019-10-14 11:09:10', '2020-10-14 11:09:10'),
('16659f2525e612e838db600b94719422734fd56768f5d76347a60d8ba8bdd079fe3b6c995409d683', 117, 2, NULL, '[]', 0, '2019-11-21 06:15:22', '2019-11-21 06:15:22', '2020-11-21 06:15:22'),
('16bf0376020fc8f8717d5d1a64fd81b5e8363e8dd9b5af1eaed33cb5d0a9c0ff49e1d5aa7c0a3602', 117, 2, NULL, '[]', 0, '2019-11-21 09:23:57', '2019-11-21 09:23:57', '2020-11-21 09:23:57'),
('172fd321913a67939d8a963e549291383e11eb7a684168e805ab0f44a39a714af6ab6998eeeeeb56', 107, 2, NULL, '[]', 0, '2019-11-20 15:07:21', '2019-11-20 15:07:21', '2020-11-20 15:07:21'),
('17cd91377846ce398f5b2f6f304f97ebe5dc88807c52f21ac128e57d8515ac9a7ac3f158347e3c6c', 61, 2, NULL, '[]', 0, '2019-11-20 03:14:19', '2019-11-20 03:14:19', '2020-11-20 03:14:19'),
('185447e5400790975c75eaee58b1245c3e614a1fe4bcb359d399f70bff225667e8ef70aeb6446fed', 36, 2, NULL, '[]', 0, '2019-11-11 12:44:15', '2019-11-11 12:44:15', '2020-11-11 12:44:15'),
('18dbc5a2957989e3680208d674630d2b09d2287be13dc6c212aae42920c1b881f8379e4883d9c3e5', 12, 2, NULL, '[]', 0, '2019-10-24 05:34:25', '2019-10-24 05:34:25', '2020-10-24 05:34:25'),
('192689ce777b50d0ddd3cc0d4b47ba12374fabf5be1cbbf115c201ecb67a8f7048f060cefd12992f', 54, 2, NULL, '[]', 0, '2019-11-19 12:57:43', '2019-11-19 12:57:43', '2020-11-19 12:57:43'),
('19821186866c64c89b52d99bf002e8f96ab40663be111e9e68c10d43267bb1631cf5591ea1ee1931', 28, 2, NULL, '[]', 0, '2019-10-31 05:16:46', '2019-10-31 05:16:46', '2020-10-31 05:16:46'),
('19adfd96af66311f12a38747640ea649b687b124a8b55966a06e51adf5e2c8500a93d39ba8de00fe', 46, 2, NULL, '[]', 0, '2019-11-19 09:00:12', '2019-11-19 09:00:12', '2020-11-19 09:00:12'),
('1a03ce978e10f25ab9ed44731bc5d1e564c295ed26c3810adf5e25d9e5a8656ada1cf21d06bbb9d8', 46, 2, NULL, '[]', 0, '2019-11-18 11:38:29', '2019-11-18 11:38:29', '2020-11-18 11:38:29'),
('1a36e503f0166c1ce3d443bbe7f10a190c6768b0e01916efda4e37e0fd3f796fe90e42ff73b99090', 12, 2, NULL, '[]', 1, '2019-11-11 09:22:01', '2019-11-11 09:22:01', '2020-11-11 09:22:01'),
('1add499b61f502d72c2f72ecae2cb4177269b6b10cf0a85c4c522c9e2d2db8daa1ce8647b0a9fac1', 41, 2, NULL, '[]', 0, '2019-11-12 11:08:54', '2019-11-12 11:08:54', '2020-11-12 11:08:54'),
('1b50a1937a2130ade80acf1dd1ee7be0ec58d15bff732713859e3083de71447a4917fe606532d270', 21, 2, NULL, '[]', 1, '2019-11-12 09:57:52', '2019-11-12 09:57:52', '2020-11-12 09:57:52'),
('1b7bfb9019f33bf529d4d74a2e12232d98d2899777f0b2060a245e0b5581ea7a66d6168072452424', 12, 2, NULL, '[]', 1, '2019-11-12 07:45:43', '2019-11-12 07:45:43', '2020-11-12 07:45:43'),
('1c009c8eb470fb5f65137f3e085e75fe7e139f2b1baf2f4a6df62d463219a5ddef98a9c21be8d5d0', 21, 2, NULL, '[]', 0, '2019-10-31 12:14:29', '2019-10-31 12:14:29', '2020-10-31 12:14:29'),
('1c9c54a3852560e0298d814ed3a4d156b9c1fb59966d3a86e5566a179a8e2858cbbcc205531ba913', 12, 2, NULL, '[]', 1, '2019-11-13 07:53:47', '2019-11-13 07:53:47', '2020-11-13 07:53:47'),
('1d6dc9d0edee981e4c3aed54b65aa5961e05453a85977a9d7ccba8013dd560cfae17888b29e65122', 19, 2, NULL, '[]', 0, '2019-11-01 12:52:02', '2019-11-01 12:52:02', '2020-11-01 12:52:02'),
('1d6f2ede983559ed7a60e602bdfbc2a04bf15cf6ec7104b432ab4a41ff6b1cbf839388557ea2eada', 35, 2, NULL, '[]', 0, '2019-11-05 06:09:44', '2019-11-05 06:09:44', '2020-11-05 06:09:44'),
('1d83e75c1dde283bf92079c65d6edce7fc39a080ec67b83820f2ec4f914f3edc4bcf0f09fabc7bf2', 91, 2, NULL, '[]', 0, '2019-11-20 06:46:00', '2019-11-20 06:46:00', '2020-11-20 06:46:00'),
('1da2b5ff5070affb10f455ec29bb8ba94f8c0f757b724c939089865ee8cfcc2d544a2ee8b45d4b53', 19, 2, NULL, '[]', 1, '2019-11-19 10:32:34', '2019-11-19 10:32:34', '2020-11-19 10:32:34'),
('1de32c8eb74208b86661ea0a72a11005314d361918e81e0338ea4602f7f2221424030d60f2598d05', 46, 2, NULL, '[]', 0, '2019-11-18 11:37:04', '2019-11-18 11:37:04', '2020-11-18 11:37:04'),
('1e1a9008d2eb7588255d514b9fbcc35967174a3bd9b5580aed75248a4b7ca1e1e79c71637890e37e', 21, 2, NULL, '[]', 0, '2019-10-21 12:53:07', '2019-10-21 12:53:07', '2020-10-21 12:53:07'),
('1e46b81363ce7750a1ea88fa93005165b0c5bf6f107b23fc7cc191205099dc1bb080a396124f3cc3', 119, 2, NULL, '[]', 1, '2019-11-21 07:32:24', '2019-11-21 07:32:24', '2020-11-21 07:32:24'),
('1eee6ce18cf6ca6c1e00b01035d4b52ae8f9fc4c9ab5ab24c2fcba48a26049cc0b54a0da732d74e9', 12, 2, NULL, '[]', 0, '2019-11-22 14:41:40', '2019-11-22 14:41:40', '2020-11-22 14:41:40'),
('1fb36ee97d64f42ac324de4de1c78837fd49f2333dcd960704291cd20ce3856a42b27d450a21ea97', 12, 2, NULL, '[]', 0, '2019-10-01 13:13:07', '2019-10-01 13:13:07', '2020-10-01 13:13:07'),
('200b099e2cab8597b65328f13805dd9653fdc9bba17c2541de3b0784a2b1bf7ca4314373bc90abb2', 12, 2, NULL, '[]', 0, '2019-10-01 11:21:30', '2019-10-01 11:21:30', '2020-10-01 11:21:30'),
('20815bd4561a085f2da17e8ea85cdf692e01cfcea3baa29e21636891bd8cb97c1f5ffa145ae73630', 41, 2, NULL, '[]', 0, '2019-11-11 15:19:18', '2019-11-11 15:19:18', '2020-11-11 15:19:18'),
('20acf4bb1f5e77c58a2f807dc43473689314118a0537c7c206ba318460748eb8b3ca38cb58cee608', 75, 2, NULL, '[]', 0, '2019-11-19 22:09:09', '2019-11-19 22:09:09', '2020-11-19 22:09:09'),
('2132a5d2ead724f0e79f2311e7f99a0e6eda3dba35ebcb6317afbfb3716bb905e1c2c17f48fdb6e8', 21, 2, NULL, '[]', 1, '2019-11-08 12:47:18', '2019-11-08 12:47:18', '2020-11-08 12:47:18'),
('213807fa8c1e47bfb067c7cacbc7e0422ff5e6e8708bc7e5cc99b57124e7dcf8760521177d237af2', 94, 2, NULL, '[]', 0, '2019-11-20 09:28:10', '2019-11-20 09:28:10', '2020-11-20 09:28:10'),
('2162f20ad2b9cab7ef5f39bc91395ae756b8214486226bdd72b58b721393e6162f94adf6388c3ef8', 37, 2, NULL, '[]', 1, '2019-11-12 11:04:52', '2019-11-12 11:04:52', '2020-11-12 11:04:52'),
('2278a1653619d85c9bc40e18a57d4d3c05e904902fede543e6b74a11dfbeb0e4e149d7c59d257d04', 84, 2, NULL, '[]', 0, '2019-11-20 05:41:41', '2019-11-20 05:41:41', '2020-11-20 05:41:41'),
('22c62ac305430fddb0705d42cf3143b6b9ee627ab6b33dd894c163cc54f28edbf52d3a66bba69548', 46, 2, NULL, '[]', 0, '2019-11-18 12:16:51', '2019-11-18 12:16:51', '2020-11-18 12:16:51'),
('22e2f5b3edb1120d2e79f1a5d33b2d667c7e3fb49d419295fb174bfa0a2a21b265478e0caaca0435', 117, 2, NULL, '[]', 1, '2019-11-22 06:17:00', '2019-11-22 06:17:00', '2020-11-22 06:17:00'),
('233d7677214c4544ceaa055627135f50fbbf4b49259a3c69413a97b33e3850db8ab7af4dbfd8ba4e', 12, 2, NULL, '[]', 0, '2019-10-23 13:20:01', '2019-10-23 13:20:01', '2020-10-23 13:20:01'),
('244b77fe9542c53a77456bf5c657bcf27f53edf896b7bdd47e25a39406cad0c57a4b25c280a0735b', 12, 2, NULL, '[]', 1, '2019-10-23 11:03:29', '2019-10-23 11:03:29', '2020-10-23 11:03:29'),
('256ea7410ead77f64cf2e8a85a028919823c5850cd66573de18696875cfaf3d9b39791bc3ee2c668', 51, 2, NULL, '[]', 0, '2019-11-19 12:39:06', '2019-11-19 12:39:06', '2020-11-19 12:39:06'),
('257738101e9696f3b9fdd4e2dc291195474d090f7e1a4158612e9cbbc4fd404f74519ec4614bdf5f', 20, 2, NULL, '[]', 0, '2019-11-01 21:00:45', '2019-11-01 21:00:45', '2020-11-01 21:00:45'),
('262064f61c0950f058296c74f8850907d551c90b0413e2d2dff4cedb360a0146610b6e58e28c025c', 117, 2, NULL, '[]', 1, '2019-11-25 05:52:14', '2019-11-25 05:52:14', '2020-11-25 05:52:14'),
('270a0dc1cf081221f8352de554d9a06b56375f33676dd336c79cb88b5987864122302f45b75387ae', 12, 2, NULL, '[]', 0, '2019-10-01 13:11:46', '2019-10-01 13:11:46', '2020-10-01 13:11:46'),
('27893442b6e8f6b2981a75d6e25e9c75b3b7244c532f5bb5cf3bbe2b52f4f7e8482e7c474f2b5e9e', 21, 2, NULL, '[]', 0, '2019-11-11 09:46:21', '2019-11-11 09:46:21', '2020-11-11 09:46:21'),
('280784acf92a7a9d882d0ddbbbb7e4fed264f7d990f22fdc6a98a4008576f499600f674197a15315', 19, 2, NULL, '[]', 0, '2019-10-30 14:26:16', '2019-10-30 14:26:16', '2020-10-30 14:26:16'),
('2857c43bedf99027e5bde2a3b352134bfc1782be3125160a1e3337a33010de7b1fea84aee60f8d20', 97, 2, NULL, '[]', 0, '2019-11-20 12:56:13', '2019-11-20 12:56:13', '2020-11-20 12:56:13'),
('285dfa25b43b650f91fc053c43e1654fa84633e0479f75c79a6a861e9068af7e8aae302f557db703', 117, 2, NULL, '[]', 1, '2019-11-21 09:33:59', '2019-11-21 09:33:59', '2020-11-21 09:33:59'),
('2873e98065e1601aa3d68029bb10dcf043aae830cc354dcd834903db063272589208af82ba575db3', 24, 2, NULL, '[]', 0, '2019-10-23 10:10:16', '2019-10-23 10:10:16', '2020-10-23 10:10:16'),
('2909c872b13e86264b5c569a5952150380613deffb0e5d542e7c16dddc887d6226cb7c0f9ca74117', 12, 2, NULL, '[]', 1, '2019-11-15 09:11:54', '2019-11-15 09:11:54', '2020-11-15 09:11:54'),
('291850db846f8f544138f762d2c8777011cdcbec1c7fd8225215326c6ece416cdc413af6e28aa549', 21, 2, NULL, '[]', 1, '2019-10-22 15:21:02', '2019-10-22 15:21:02', '2020-10-22 15:21:02'),
('29642886de1c4b42dea108b6257414788a31d5ff16e35463f100c8414e3a299721e8a36ada1d704a', 19, 2, NULL, '[]', 0, '2019-10-31 11:34:06', '2019-10-31 11:34:06', '2020-10-31 11:34:06'),
('29af3184fe7b48b02d16312d863dd7e422f4a94636f415c3d7f5b10f2e31551b912fafbf4b0707c9', 80, 2, NULL, '[]', 0, '2019-11-20 05:21:12', '2019-11-20 05:21:12', '2020-11-20 05:21:12'),
('29ffe7ce3a27ab1d39c497b5a0fada6799596e336cc4865171332c969bc4b8ee9c39b3514a649f6d', 21, 2, NULL, '[]', 0, '2019-10-23 10:36:52', '2019-10-23 10:36:52', '2020-10-23 10:36:52'),
('2a4ea38bb802bd75bd4dddff078fec897a2dcd5ef1c89f27f74f45a8d56cff6cd636926bf3918ea6', 117, 2, NULL, '[]', 0, '2019-11-21 06:47:36', '2019-11-21 06:47:36', '2020-11-21 06:47:36'),
('2ae0d4b6b87de8617873940962076226b2a58de937eb54be1c8cd753b97bfd7fabff94a764f71730', 34, 2, NULL, '[]', 0, '2019-11-05 05:36:22', '2019-11-05 05:36:22', '2020-11-05 05:36:22'),
('2affdfdac266569a5a1309ad84c4605eb215b68c9cb5ddb0bc12df1b61530ce93280ef8dfb347497', 46, 2, NULL, '[]', 0, '2019-11-18 12:50:27', '2019-11-18 12:50:27', '2020-11-18 12:50:27'),
('2c0e8cb5940f37f8639a42da0578471817595b9bf5cd42c91c2dea51b50278e803a3c9de358cf1a9', 19, 2, NULL, '[]', 0, '2019-11-19 04:47:34', '2019-11-19 04:47:34', '2020-11-19 04:47:34'),
('2c84bc0700f745c1aef296ae3249d52e9bfa4ea15ed6555c7723cd36b6868a95c45d831bfcd03442', 12, 2, NULL, '[]', 0, '2019-10-31 06:47:05', '2019-10-31 06:47:05', '2020-10-31 06:47:05'),
('2c8bd2046d79fb4e0eadf656be06eab7bd257ca52c3c0ca9545a355149f466113245d6db5a3b2f9c', 95, 2, NULL, '[]', 0, '2019-11-20 11:36:13', '2019-11-20 11:36:13', '2020-11-20 11:36:13'),
('2c92faca530b6f00ede1be4071a336b8ae0e16c63e90cd97ca2df5a18ef76463826c1d8e129aa39a', 39, 2, NULL, '[]', 1, '2019-11-08 11:12:11', '2019-11-08 11:12:11', '2020-11-08 11:12:11'),
('2c9adeebe7a4c05be30e75acbaec8c186696b18cc0d1b92f8d814f287eb868f8568b2bcc4d603c86', 12, 2, NULL, '[]', 0, '2019-10-23 10:49:27', '2019-10-23 10:49:27', '2020-10-23 10:49:27'),
('2ca64b9cbefd73827cecc7e280fffb344e6a1e7e1be685b37a30b0a335c0c77a6807a95e6778e797', 44, 2, NULL, '[]', 1, '2019-11-14 23:27:18', '2019-11-14 23:27:18', '2020-11-14 23:27:18'),
('2cfb7d6256a0d928f1a40997b673a516bf4484f317f896634bc82560b187a813afb8b0ad3bfc65b0', 117, 2, NULL, '[]', 0, '2019-11-21 06:33:40', '2019-11-21 06:33:40', '2020-11-21 06:33:40'),
('2de8c88febbc01dc79d09828bcdb4503a4bb796c01b386b04b945eaf3c122b87133771a988c4a5b5', 12, 2, NULL, '[]', 0, '2019-10-30 10:08:08', '2019-10-30 10:08:08', '2020-10-30 10:08:08'),
('2e2e48e668ba57e472cf4ac01d0ecaa35c0c4c3337858ec588450a16fa12f8e81dd15cc1932ed7f6', 19, 2, NULL, '[]', 0, '2019-11-22 11:58:00', '2019-11-22 11:58:00', '2020-11-22 11:58:00'),
('2e4e4dfec0b2f0cae536876b51844727b994d69c1912c6281eea7276f9c733df6af2e8d4159be544', 33, 2, NULL, '[]', 1, '2019-11-12 13:12:05', '2019-11-12 13:12:05', '2020-11-12 13:12:05'),
('2f21fcdea9fd9d8ab7a568ec41d150315f3994e74c2532978e137182a12ce8f0cd107b07d6fea450', 117, 2, NULL, '[]', 0, '2019-11-21 05:09:36', '2019-11-21 05:09:36', '2020-11-21 05:09:36'),
('2fc9ec78a5bcf0c48b83df6ba5dded27c3dec6ff23f6c2ac9372380f6ae1b84a24f4e85a8f288c2b', 5, 2, NULL, '[]', 1, '2019-09-28 04:03:19', '2019-09-28 04:03:19', '2020-09-28 09:33:19'),
('3046006599961ef9b23864fcec874741fb9ce7a927850c52047d33c5fd1adaa1cd83415ec0469cdb', 117, 2, NULL, '[]', 1, '2019-11-25 05:04:34', '2019-11-25 05:04:34', '2020-11-25 05:04:34'),
('30f57d8c1814bbabf60d50b34d7759d707cef9dda8ba1f5679d0a8d91ff0e1dc029b8d4e5bdd38af', 64, 2, NULL, '[]', 0, '2019-11-19 14:51:55', '2019-11-19 14:51:55', '2020-11-19 14:51:55'),
('32af57508675632aacf1cf3252af5566bb7c8858046b794ab67b7fc2fe1e9c492acf2c84648b2c82', 9, 2, NULL, '[]', 0, '2019-11-11 14:05:58', '2019-11-11 14:05:58', '2020-11-11 14:05:58'),
('32e94d50e2bcd5ebbe641cfa09cf39355b349d35a6e48c700474b00b38ace2cbfce92bab988f36c7', 37, 2, NULL, '[]', 1, '2019-11-12 13:08:57', '2019-11-12 13:08:57', '2020-11-12 13:08:57'),
('33731a0a5c0b2bf46a55791ffb4ac3850845d459f8c1dc1c4bb3cf9371aea5f5b2b487c2cc60c1d7', 15, 2, NULL, '[]', 0, '2019-10-01 12:53:12', '2019-10-01 12:53:12', '2020-10-01 12:53:12'),
('34925c8c053c1b4a903c67cfb9fec56b176a9e1456cc4afe3f28d1fd579343defd47c192865cba72', 21, 2, NULL, '[]', 0, '2019-10-11 08:52:45', '2019-10-11 08:52:45', '2020-10-11 08:52:45'),
('349f74f8777709f1a702ed03d06fbc385fde08ec2878771d4021fa9692a8331860ea994da21d0fa4', 21, 2, NULL, '[]', 1, '2019-11-12 07:42:56', '2019-11-12 07:42:56', '2020-11-12 07:42:56'),
('35cb269dfa46dcad0b3fb66b5205e1dd8353f2e1c8461a54321785c97211823341d4b1aac336efee', 21, 2, NULL, '[]', 0, '2019-10-21 06:22:42', '2019-10-21 06:22:42', '2020-10-21 06:22:42'),
('35cbf0130268137feb068f7270d5768a5dbb9062caf0accd1b4de9c1c5085a8e04ee24f975193666', 12, 2, NULL, '[]', 1, '2019-10-23 09:10:04', '2019-10-23 09:10:04', '2020-10-23 09:10:04'),
('37431a27d9cb29b6d27e90661485fa06feafa460d219d43635eeb55bbb93b550ea61bfdab1f83e57', 22, 2, NULL, '[]', 0, '2019-10-16 06:53:49', '2019-10-16 06:53:49', '2020-10-16 06:53:49'),
('3877a711f121164a410ef20eb3f1757cb3dd50e612af266c05363906f540de3afc1ee8b74157c6a8', 21, 2, NULL, '[]', 0, '2019-11-18 06:22:34', '2019-11-18 06:22:34', '2020-11-18 06:22:34'),
('38b21eadbf2c08bfae61068ef7ac90060a379c950736ba5e509f54cf64809ac42d6677f5f32ae274', 79, 2, NULL, '[]', 0, '2019-11-20 05:16:47', '2019-11-20 05:16:47', '2020-11-20 05:16:47'),
('399367d1e7967bf19db69b71e9997b168c5757a05f9a0c960dfc12d4ec9bcb8fbd149ae76ed6c101', 21, 2, NULL, '[]', 1, '2019-11-11 11:58:33', '2019-11-11 11:58:33', '2020-11-11 11:58:33'),
('399e46bbcdf1fea81790931b17b81d99918945b3ec061c1c66ddb5c02301b77f97e069fcfe9e7c38', 55, 2, NULL, '[]', 0, '2019-11-19 13:22:56', '2019-11-19 13:22:56', '2020-11-19 13:22:56'),
('3a2e87739eefca91554d7f92a0832c313b48c13a0bd35c1c69f7af5f6734d7be0761db6d621268bb', 9, 2, NULL, '[]', 1, '2019-10-23 10:35:59', '2019-10-23 10:35:59', '2020-10-23 10:35:59'),
('3a588e92f7f446d1be7f0e912dcd48e4769a19ed5b2afd75795fb983c605d782d50a3e53c4756ae0', 9, 2, NULL, '[]', 0, '2019-10-24 18:53:40', '2019-10-24 18:53:40', '2020-10-24 18:53:40'),
('3a5b897489189e024b2ab8aa54864d2a5f628ef56b4d8a14490c3d6014644caa82ca60221f206fba', 19, 2, NULL, '[]', 1, '2019-11-18 16:38:29', '2019-11-18 16:38:29', '2020-11-18 16:38:29'),
('3aaaa16026a2fd2913d54370579ce46192c0cf4ef14497ee210fc4c73fbfc86ff6003bb94d192b85', 117, 2, NULL, '[]', 0, '2019-11-21 11:24:32', '2019-11-21 11:24:32', '2020-11-21 11:24:32'),
('3ab0d9e3710ba298ba45b7f68d8d2c339ada087efb31fe019a441268efbae4f256da4dd52893f5ab', 21, 2, NULL, '[]', 1, '2019-10-23 11:26:41', '2019-10-23 11:26:41', '2020-10-23 11:26:41'),
('3af32459e305f42e5ce7c4641471ee4583c2912b7f32c7bb206d80ec3c66d9c92a365d6544796ac9', 124, 2, NULL, '[]', 0, '2019-11-22 15:30:00', '2019-11-22 15:30:00', '2020-11-22 15:30:00'),
('3c5aef4722e440e7efeeafa86ec8a07553fb589f2e76ab98e3c7d26bdde584dbf0b38df6a75c5fd2', 19, 2, NULL, '[]', 0, '2019-10-01 16:00:24', '2019-10-01 16:00:24', '2020-10-01 16:00:24'),
('3dfa8e9ec95216598a8cb927a4642977d7e0697ec0c09d758620161de8f5514691dcf486d3cb160d', 19, 2, NULL, '[]', 0, '2019-11-16 15:55:16', '2019-11-16 15:55:16', '2020-11-16 15:55:16'),
('3ef27a8e1a2abcbaf6c6e7871a29553c4aaa84ed3c52fa4ee35cf20a1ca3ce69c00476f763c41bb2', 12, 2, NULL, '[]', 0, '2019-10-01 11:21:01', '2019-10-01 11:21:01', '2020-10-01 11:21:01'),
('3fdcb133afc5c8240b1848ed4bca569bc6bd3f8bde24c5be9492f81dd24ef9cbc341448a55c07fae', 41, 2, NULL, '[]', 0, '2019-11-12 06:45:15', '2019-11-12 06:45:15', '2020-11-12 06:45:15'),
('404101efcfac92bfad431cde5d9a0f3857c741891846bfc4300b45959b1580a00f92edae154e0fa6', 117, 2, NULL, '[]', 0, '2019-11-21 07:09:30', '2019-11-21 07:09:30', '2020-11-21 07:09:30'),
('40dfed2ecb9f2e55f6646c9601772d3a255f7fd9e149ec2bfd9296b43053ec3237da6873240ce1a5', 106, 2, NULL, '[]', 0, '2019-11-20 14:49:21', '2019-11-20 14:49:21', '2020-11-20 14:49:21'),
('41573217fe179c73a2f319fd443da3199b9b7654b2348a730eff029cee49bb09ae2c60b3102209c2', 95, 2, NULL, '[]', 0, '2019-11-20 11:37:21', '2019-11-20 11:37:21', '2020-11-20 11:37:21'),
('41b0ff47c7e639a36bff380d1c734c253a2852e2285309a3dcb7b13d64b0351571d7c9670ea058e7', 27, 2, NULL, '[]', 0, '2019-10-30 12:26:57', '2019-10-30 12:26:57', '2020-10-30 12:26:57'),
('41d19c08a45e372e6218f505e608e6ae1a3df65a2c07ccd6d84aa500f9ece7dc0ac45b4159a84dae', 12, 2, NULL, '[]', 1, '2019-11-15 07:13:47', '2019-11-15 07:13:47', '2020-11-15 07:13:47'),
('42a335ef01460fd02de93143609c0f2d4daadd750f2bd92911633b6f9948cc68dbba578fd81b32cc', 9, 2, NULL, '[]', 1, '2019-11-11 09:51:18', '2019-11-11 09:51:18', '2020-11-11 09:51:18'),
('4400833b774b809c62e96c78b18f9e422d787ce33f884701cfd529cea55ccd9e80e814cae64524b4', 88, 2, NULL, '[]', 0, '2019-11-20 06:34:18', '2019-11-20 06:34:18', '2020-11-20 06:34:18'),
('44eaf9e57d51b92dc2aa05a390c4738382969bc4f1c4085718bcc92ea4e492164dd99fb3b1b6bd99', 21, 2, NULL, '[]', 1, '2019-11-11 09:41:24', '2019-11-11 09:41:24', '2020-11-11 09:41:24'),
('45044402614c09dc6abbcabf84e9152e7a72c0447647fcb625ccd45ec9e8fccf7f09054fcf89e23a', 61, 2, NULL, '[]', 1, '2019-11-19 22:59:10', '2019-11-19 22:59:10', '2020-11-19 22:59:10'),
('4525b37c984531eee2f81ac6980fec790b6af65fa9991dda8c3e7092d0696f27bdf3ec5862f5114d', 41, 2, NULL, '[]', 1, '2019-11-08 11:31:23', '2019-11-08 11:31:23', '2020-11-08 11:31:23'),
('45520d50029fe329ab63cea60abf33c8a4f262bec907e146f039618a24ca11ac440fda2518b76eb5', 19, 2, NULL, '[]', 1, '2019-11-04 15:06:55', '2019-11-04 15:06:55', '2020-11-04 15:06:55'),
('45647fa3296267d8d530b9966964d22ee77772a1206b9b4bcb67359da89e287bd783712c7848f5e7', 19, 2, NULL, '[]', 0, '2019-11-12 11:12:58', '2019-11-12 11:12:58', '2020-11-12 11:12:58'),
('45668eedd9a7b84be438c2bff1c1946e1960442d33477469222e5e073f49b87905778d879f0611c7', 21, 2, NULL, '[]', 1, '2019-11-12 11:02:59', '2019-11-12 11:02:59', '2020-11-12 11:02:59'),
('45b002577a3663f8838de96e8e762fb31eff315ca64f9d014ef11414680c7ffcfc757dba14dc86d2', 9, 2, NULL, '[]', 1, '2019-11-14 14:28:49', '2019-11-14 14:28:49', '2020-11-14 14:28:49'),
('4602dcd9cfb0a0200c7093c7211314e381453fc65b1323acce419adf4bcc672c8901c50e660cd9e0', 78, 2, NULL, '[]', 0, '2019-11-20 02:51:42', '2019-11-20 02:51:42', '2020-11-20 02:51:42'),
('4640b9ab2854f43f12b6e5cffc2e38e3491191af0d100d0668f19d9a5ab18216eea6976e0590ad24', 19, 2, NULL, '[]', 0, '2019-11-18 09:44:06', '2019-11-18 09:44:06', '2020-11-18 09:44:06'),
('474d1d2a70d48a57ae292743386934c897df96ee05540104a3b211a946efa7ef065a7576328d27f7', 73, 2, NULL, '[]', 0, '2019-11-19 21:11:02', '2019-11-19 21:11:02', '2020-11-19 21:11:02'),
('480292a5dc28a783744b8ede3d8a7dc9472598143d629ce7dd9a54075b116f1ff4b4993bbdf69d87', 5, 2, NULL, '[]', 0, '2019-09-28 03:53:55', '2019-09-28 03:53:55', '2020-09-28 09:23:55'),
('48161fb6f3dd7c377f709aa1ba13b92a80b422b8f14e4b3e9a7525c4439e9d33279b95aef5e48822', 19, 2, NULL, '[]', 0, '2019-10-24 06:15:53', '2019-10-24 06:15:53', '2020-10-24 06:15:53'),
('493ab8d131a3a8f68f717f57d94807a922dd2d46989ea1049e6a5e4a4c29f2dc44c61f76465d9289', 55, 2, NULL, '[]', 1, '2019-11-19 14:36:24', '2019-11-19 14:36:24', '2020-11-19 14:36:24'),
('4a40cf2ed6640efe37f931082e1ba83c4f557aac09ebdedbd1e5d1e5c7c62b8f38cd48390962975e', 117, 2, NULL, '[]', 0, '2019-11-21 07:15:14', '2019-11-21 07:15:14', '2020-11-21 07:15:14'),
('4aa758793bc191360d209111c214c1fb6148ad3700bed81060ff587cdac58fed4b5c8eab282a7840', 21, 2, NULL, '[]', 1, '2019-10-23 07:28:33', '2019-10-23 07:28:33', '2020-10-23 07:28:33'),
('4c7d27f65c795f58ede92a7ecc683e71d34c2b83f53ad88cdab23b7c08a9aef56c3090b7a01b1eb4', 72, 2, NULL, '[]', 0, '2019-11-19 19:47:10', '2019-11-19 19:47:10', '2020-11-19 19:47:10'),
('4d1979a37060a3a508a2b00d8e55ca485cd922d8ab3371bac41f1852d6027b140e09d62821dff35e', 117, 2, NULL, '[]', 1, '2019-11-21 11:40:16', '2019-11-21 11:40:16', '2020-11-21 11:40:16'),
('4d99b28fa91235d41f806ff4bf0017e426d1fd0baf50bb40d8f50cb80ce8550d3a32076a6c415c9c', 41, 2, NULL, '[]', 0, '2019-11-12 11:22:36', '2019-11-12 11:22:36', '2020-11-12 11:22:36'),
('4f3407658dc0262c14826b0fa2f3aa01bd2f82ab30713c5ceb11bfba62507fe75895a13e206e130b', 12, 2, NULL, '[]', 0, '2019-10-25 06:35:47', '2019-10-25 06:35:47', '2020-10-25 06:35:47'),
('4f98f04b9d6d2809dc7249bdf86b38603e359388ae72e4f792c84f9486286ffda6129ccfae8f2ed0', 36, 2, NULL, '[]', 0, '2019-11-11 11:52:17', '2019-11-11 11:52:17', '2020-11-11 11:52:17'),
('4fc57974436beedb61b34ba4dcbf174ce4c2521d2de5e7daf8c12573e8ff8ade832c0efb1e3be0a3', 19, 2, NULL, '[]', 0, '2019-11-18 12:24:46', '2019-11-18 12:24:46', '2020-11-18 12:24:46'),
('504211ccb49678cc9a0b82920548e1b4bada38ff325d0cc762d87309633b09c08f7839df2f0087f0', 12, 2, NULL, '[]', 0, '2019-11-04 07:54:04', '2019-11-04 07:54:04', '2020-11-04 07:54:04'),
('50715f31f8d49652e72da59b3256d08c96fe16cd8c7e0cfa8b63ff874964a8ba1e70b8a9b1448164', 117, 2, NULL, '[]', 0, '2019-11-21 06:43:34', '2019-11-21 06:43:34', '2020-11-21 06:43:34'),
('50a823f7d7ca52e316b94dc2f38b5ee1f5d3231ef6b153cf4d0ae6ad1ac17bb94c7f24ae5301e46c', 21, 2, NULL, '[]', 0, '2019-10-09 10:39:13', '2019-10-09 10:39:13', '2020-10-09 10:39:13'),
('50c8f4c44522ad19264e0649b0fe1d597eddf44383c6b337fbc86d0b3ec12849566998bd48328380', 12, 2, NULL, '[]', 1, '2019-11-11 14:12:12', '2019-11-11 14:12:12', '2020-11-11 14:12:12'),
('50dded796d6d5e0776584f29735fd6e88864f655f2e5c30b269c5f3cd846e90dd0d8bbf7c8fb2c8b', 25, 2, NULL, '[]', 0, '2019-11-12 08:58:46', '2019-11-12 08:58:46', '2020-11-12 08:58:46'),
('50e7890dee59bfc02a6c3abfe4f9ef9eee8269d653b5e1cc9424c684276832aca07dec8f815393f7', 21, 2, NULL, '[]', 0, '2019-11-21 07:40:10', '2019-11-21 07:40:10', '2020-11-21 07:40:10'),
('517ee6eb18660ce281c66c635ff0db30a00cee35c4a4cbe1294b3bf88ac7134bdf6f9362f06e9b2c', 15, 2, NULL, '[]', 0, '2019-10-01 12:24:52', '2019-10-01 12:24:52', '2020-10-01 12:24:52'),
('528012f97e852c5faae3d162abca8de7658fbf0a07ffddfd1e3d1b6f6f5366ed57325b7fefff1af4', 21, 2, NULL, '[]', 1, '2019-10-24 05:47:25', '2019-10-24 05:47:25', '2020-10-24 05:47:25'),
('531300fd51455860edb9d0b09767a0ab4f030136f00b84e2582354778a0d0591d9339b0b606d0924', 12, 2, NULL, '[]', 0, '2019-11-18 10:31:13', '2019-11-18 10:31:13', '2020-11-18 10:31:13'),
('534e2e12e094e60629c5b50b1e96222a1a8ac7d69bc3ad65f17f3ec36b41b571e7942212e0036480', 19, 2, NULL, '[]', 0, '2019-10-30 14:31:57', '2019-10-30 14:31:57', '2020-10-30 14:31:57'),
('5364e4897bafa6ed7486dd412492d93df8fe3b5e1784dbaac900192454519fe2f9333a4cb77df1f2', 5, 2, NULL, '[]', 0, '2019-09-28 02:39:47', '2019-09-28 02:39:47', '2020-09-28 08:09:47'),
('53a8a3fa0998c05eb4f952668d8fdb6326e9f72b8da1e1dbf76b90a714d2e8eb149624e909feda12', 21, 2, NULL, '[]', 1, '2019-11-09 11:33:41', '2019-11-09 11:33:41', '2020-11-09 11:33:41'),
('53be022ca6075ceac8692ee62b359e48f0c913283e9db94ed95770788d398485efe90d62f4bd6983', 12, 2, NULL, '[]', 1, '2019-11-06 08:13:16', '2019-11-06 08:13:16', '2020-11-06 08:13:16'),
('53c0e6843009e8fac0add96b0ec87c94a6e61818cb55b0fb329f61498003ce8820cbd51008f8ce21', 9, 2, NULL, '[]', 0, '2019-10-30 09:39:50', '2019-10-30 09:39:50', '2020-10-30 09:39:50'),
('53e9cab7c92792131af90bf84acfa80d6389d5fee85eccda78a9df50034a69f05c6f83d72d627ae8', 9, 2, NULL, '[]', 1, '2019-11-15 06:56:13', '2019-11-15 06:56:13', '2020-11-15 06:56:13'),
('54129f142e0f7a40617b74fc122f1f3e164c94febd3d34498830b9dbf966a9705121e1af0162351f', 21, 2, NULL, '[]', 1, '2019-11-12 07:42:17', '2019-11-12 07:42:17', '2020-11-12 07:42:17'),
('545abb6430f938c9f9cc5c53dcd0acc23b27ae64503eb3c8024176498f1cea24be618ec9186a1b7f', 21, 2, NULL, '[]', 0, '2019-10-22 16:57:41', '2019-10-22 16:57:41', '2020-10-22 16:57:41'),
('548dd824ca1cec087815233d1ed62e8e8eef710184a5ee9dacfbcf14b87ee926e7f2420e29b8cf61', 21, 2, NULL, '[]', 1, '2019-11-11 10:04:46', '2019-11-11 10:04:46', '2020-11-11 10:04:46'),
('54adea90ba9ca68daa86e4b9e393cd9a5218b7f4017b3e2a4d1de30e3eb107f79f3a0f4f2817cf66', 114, 2, NULL, '[]', 0, '2019-11-20 17:35:52', '2019-11-20 17:35:52', '2020-11-20 17:35:52'),
('5567392422ed5ccba352a532c12dc69bc2740c45b514fc49c4a1fd6f9791ad9ddd5c7876fe37366d', 61, 2, NULL, '[]', 1, '2019-11-19 14:27:14', '2019-11-19 14:27:14', '2020-11-19 14:27:14'),
('55a27c0b464114bc31d951786c2d15cc4358ad4416c180d5391c3f0992d760bf6211a9245ed4c87b', 112, 2, NULL, '[]', 0, '2019-11-20 17:08:36', '2019-11-20 17:08:36', '2020-11-20 17:08:36'),
('55e828fc2984cd795c9847a73d5eea87ebc9939b4a549e952f74d5872ecdbd6db7a02ace325abe8d', 19, 2, NULL, '[]', 0, '2019-11-04 14:51:10', '2019-11-04 14:51:10', '2020-11-04 14:51:10'),
('5610c274b32f3c772e31513d2da599dd7d8eb06b31497eb3f8325cf80b77f30669a2111aaf80ee18', 9, 2, NULL, '[]', 0, '2019-10-22 14:59:50', '2019-10-22 14:59:50', '2020-10-22 14:59:50'),
('563495f70fc0b2f9b6f794f8f7986af65ede2d278f9473f7b75be593a34ca4193555484e9e7265e3', 45, 2, NULL, '[]', 0, '2019-11-18 04:22:22', '2019-11-18 04:22:22', '2020-11-18 04:22:22'),
('563d893622b6caf8895535c5c7f40ea6d5f7d4f01936c0fc4d03c853d80cc6b0b71e8987efa217b8', 41, 2, NULL, '[]', 1, '2019-11-15 05:52:00', '2019-11-15 05:52:00', '2020-11-15 05:52:00'),
('57051510cd77af4a5506a3e8d6274b8d71951df0af8e61b56a48c07ff799106ae4bbc49d36eff52b', 12, 2, NULL, '[]', 0, '2019-11-04 10:03:58', '2019-11-04 10:03:58', '2020-11-04 10:03:58'),
('57c0e67a1446862777aa62cef66990f428c0bafda718d8b0cefc364eccbaa2316d8f50a163b92d33', 12, 2, NULL, '[]', 1, '2019-11-15 05:56:16', '2019-11-15 05:56:16', '2020-11-15 05:56:16'),
('594a9a17e5df6a71e5fa238c69d7e7864f6705458cbb48ce9eb5fa480c3f6529200fef0aaf811e4f', 9, 2, NULL, '[]', 1, '2019-11-11 09:21:26', '2019-11-11 09:21:26', '2020-11-11 09:21:26'),
('59830a819cd332300413eee972a3dbc7e7df44b087eadfea3f72e380560b419efb4fba37760f5d55', 117, 2, NULL, '[]', 0, '2019-11-21 06:27:08', '2019-11-21 06:27:08', '2020-11-21 06:27:08'),
('5a1dd4bc6065e323a7327ecbed2ece3fae620ec7a657886ed9ebe5e43a6ce1940f3fcf3f53a893c2', 19, 2, NULL, '[]', 0, '2019-10-29 12:46:29', '2019-10-29 12:46:29', '2020-10-29 12:46:29'),
('5a47a203abaa81595dd1208292f28d765b8f5437e398ba421748ed72ac31cf211a104aecdacc662b', 9, 2, NULL, '[]', 1, '2019-11-15 08:53:26', '2019-11-15 08:53:26', '2020-11-15 08:53:26'),
('5ad2d1ca9e23de36be454327560389fb4e4869fe4eb95d08fc8d51ea807a325ddc4311b8fd758ffa', 25, 2, NULL, '[]', 1, '2019-11-15 07:14:18', '2019-11-15 07:14:18', '2020-11-15 07:14:18'),
('5ad82d3b526f4429315aab857bf6772d058250acd9cb1de432dda5602996e15858bf0569f9772b29', 12, 2, NULL, '[]', 0, '2019-10-23 09:31:56', '2019-10-23 09:31:56', '2020-10-23 09:31:56'),
('5cc397260534c1f21d14676e3c61cd6fbe2ec1ac24c293c1da60044f856d487e1785e4541874a1dc', 104, 2, NULL, '[]', 0, '2019-11-20 13:40:27', '2019-11-20 13:40:27', '2020-11-20 13:40:27'),
('5ccbb8eb590d8a0eab3789975b503fb57d2b0348c25d15544340ded623296f915f8547627a6bd8d2', 37, 2, NULL, '[]', 1, '2019-11-13 07:38:48', '2019-11-13 07:38:48', '2020-11-13 07:38:48'),
('5d5d92cc4d17c9000078b0a5544e226e0f27929c5705a8530eee114f02e8610a6631b0e7a7553994', 9, 2, NULL, '[]', 0, '2019-10-24 07:51:34', '2019-10-24 07:51:34', '2020-10-24 07:51:34'),
('5d853427c7ce401747de02c64de566b6587f2831a4d9803d5b3c11d98afe00f89e178041244c9513', 36, 2, NULL, '[]', 1, '2019-11-11 16:29:46', '2019-11-11 16:29:46', '2020-11-11 16:29:46'),
('5d958aba70c94e0de47997afc6ec2f2f5f4c6c3f6593b520efb13574c7e156258ccb422bb355be20', 117, 2, NULL, '[]', 1, '2019-11-22 06:17:51', '2019-11-22 06:17:51', '2020-11-22 06:17:51'),
('5df4249d1fbd20988df9b3dfa48b9573ae41db7ac00fb056218c75fd7c38495e19dc72cb58c4bda7', 19, 2, NULL, '[]', 0, '2019-11-25 09:43:33', '2019-11-25 09:43:33', '2020-11-25 09:43:33'),
('5e798c0b4015f71003d0738b77deb465a23965afbb31525d115c0f8f6fd66d70e37c564076256100', 12, 2, NULL, '[]', 1, '2019-11-05 06:12:27', '2019-11-05 06:12:27', '2020-11-05 06:12:27'),
('5e86be772c75020f2066c3c5f394e613eb9d632a76242119edc2ef7477618bb5774cd0bad7280611', 101, 2, NULL, '[]', 0, '2019-11-20 13:28:37', '2019-11-20 13:28:37', '2020-11-20 13:28:37'),
('5e990c2151caa1a68cc83f93c724bca5d7e3d11a9acc39dd0e5c2957d4ba16dbb17c41490fec1179', 9, 2, NULL, '[]', 0, '2019-11-15 08:59:40', '2019-11-15 08:59:40', '2020-11-15 08:59:40'),
('5ea56e190c623a2b3d67b73a72c236ff347ced66152bc331f5ee71e8e3d59e5eadf27074b6928fef', 48, 2, NULL, '[]', 0, '2019-11-18 16:08:18', '2019-11-18 16:08:18', '2020-11-18 16:08:18'),
('5f3165c4ea0360729b4ea680d617f0d49e17ba282a002e7af983908db99ec99b9af16fb1c49c8e53', 117, 2, NULL, '[]', 1, '2019-11-22 11:38:24', '2019-11-22 11:38:24', '2020-11-22 11:38:24'),
('5f332ee3f9c8cd55179631d47eb7af8e74b7d57ddaad6e95e87a6900e037b4857bedf3a613f44d6e', 19, 2, NULL, '[]', 0, '2019-10-31 08:04:03', '2019-10-31 08:04:03', '2020-10-31 08:04:03'),
('5fa8e38ae5df698d4020cdf2eeecc820b284491926bfe4d23ec88305ea74b3bccddc3aed73377474', 12, 2, NULL, '[]', 0, '2019-11-08 12:29:20', '2019-11-08 12:29:20', '2020-11-08 12:29:20'),
('5fb0d95a1cb77c60a7c09f9340fe7070cf7dd6c458b3521d1d3cdc78467910890ffd97a0221c165f', 12, 2, NULL, '[]', 0, '2019-11-11 12:36:21', '2019-11-11 12:36:21', '2020-11-11 12:36:21'),
('5ffca2e452cd1dff3f2fa7643d353aacc164a8b4627c82680ef3d9dabe14a6b657f511c323aef8ac', 19, 2, NULL, '[]', 0, '2019-10-07 06:52:01', '2019-10-07 06:52:01', '2020-10-07 06:52:01'),
('600abb3f5e68a14f25f0294e2aabf850a1272a01fe98eb6c581ea8c1da7fc9836b92f97e7a3dfbcf', 12, 2, NULL, '[]', 1, '2019-11-06 08:09:15', '2019-11-06 08:09:15', '2020-11-06 08:09:15'),
('604146b995f686e067861490718eae1ba4dcee8c6e698501230cda5ecc142edba3115c5e404367ce', 98, 2, NULL, '[]', 0, '2019-11-20 13:19:19', '2019-11-20 13:19:19', '2020-11-20 13:19:19'),
('6106496d553df065c29454f770960e05b149ab1d619627909b3b009ebd084339b56bafe8cd4683d0', 12, 2, NULL, '[]', 0, '2019-10-24 05:36:25', '2019-10-24 05:36:25', '2020-10-24 05:36:25'),
('612fd4bec0322a057d0630c7038b00d96ff6cfded65fa75f625829888e2ce8de3a04110986b6f06a', 117, 2, NULL, '[]', 1, '2019-11-29 07:43:08', '2019-11-29 07:43:08', '2020-11-29 07:43:08'),
('61304fd3648d961b73292d684d0ea75e58dcc9e187e21a84955617cdd7df518f086d2798e00e3d85', 9, 2, NULL, '[]', 1, '2019-10-23 05:24:24', '2019-10-23 05:24:24', '2020-10-23 05:24:24'),
('61e0f87d700e078f9faf2fe20b528db0aa6fc811f8b98b3b850a49127dfdfd8f60983eda3b182e36', 109, 2, NULL, '[]', 1, '2019-11-20 15:46:11', '2019-11-20 15:46:11', '2020-11-20 15:46:11'),
('6215526f8093e1bf2928d5f12d192769da11f7d8505d9233ba70fd3bbbdce4445513cb7299e530b0', 117, 2, NULL, '[]', 0, '2019-11-21 06:52:33', '2019-11-21 06:52:33', '2020-11-21 06:52:33'),
('6244703ffcfa5d12e14e2f86ba38ecadc6d5411cfd1dd34597f81c3417129384f288ec51976ab5bb', 12, 2, NULL, '[]', 0, '2019-10-30 12:31:28', '2019-10-30 12:31:28', '2020-10-30 12:31:28'),
('6273d51fa0854786cfa0c63490b7e79980ba72a3ec3583bced915b4441e28c78d0cc275d405ace86', 19, 2, NULL, '[]', 0, '2019-10-30 09:54:52', '2019-10-30 09:54:52', '2020-10-30 09:54:52'),
('627cee5ee3ab1829b2325e9d876da48c6a0fb10fa3e1b71047a32ef315838c8fd97b24216fa52197', 19, 2, NULL, '[]', 0, '2019-11-25 10:57:40', '2019-11-25 10:57:40', '2020-11-25 10:57:40'),
('6372b4867f39f5e916449ef929f88c628da21cc9c76bcfb81b8f4e06b0a3c1886136a000b14c84b8', 26, 2, NULL, '[]', 0, '2019-10-25 06:45:43', '2019-10-25 06:45:43', '2020-10-25 06:45:43'),
('638830a4a889cdece62aaefb63cbafc929c9ec8e9f7be683275cdb540940af67e6cde5eae93ebde1', 19, 2, NULL, '[]', 1, '2019-11-18 12:42:10', '2019-11-18 12:42:10', '2020-11-18 12:42:10'),
('6399033959ef8cdba90002eba356f4e5e05e36552710c8ebf864e3cf85e572247ea5c5d4332d3fb2', 9, 2, NULL, '[]', 1, '2019-11-14 14:08:30', '2019-11-14 14:08:30', '2020-11-14 14:08:30'),
('63a980f07f12855757419b61fe0194f8df1537402cb6f5d9d6a88142f843a31712499402a73a477f', 9, 2, NULL, '[]', 0, '2019-10-16 09:27:30', '2019-10-16 09:27:30', '2020-10-16 09:27:30'),
('63e7fda809b999d5a154bac1fb90a703439a87066fd83c96ee267653e8e0b46d08f60fb2e5f7f5aa', 74, 2, NULL, '[]', 0, '2019-11-19 21:27:35', '2019-11-19 21:27:35', '2020-11-19 21:27:35'),
('648735e3bcdff3b8526f53a54937d8e2ca816c039d04de0330062fdaf9a1a7e6132f3bcae20cbd03', 19, 2, NULL, '[]', 0, '2019-11-04 14:54:07', '2019-11-04 14:54:07', '2020-11-04 14:54:07'),
('65b5cd93f449fa386a8408527cfa69926254b98261f6c79c23ae9e3ac381b6443a9abbd119988e5e', 25, 2, NULL, '[]', 0, '2019-11-11 13:11:35', '2019-11-11 13:11:35', '2020-11-11 13:11:35'),
('66d074ac248e5954fe7ab8ecaf23328bcfe0a078ef89870ea3a8c325231d296b09704faac57bc988', 117, 2, NULL, '[]', 0, '2019-11-21 07:31:47', '2019-11-21 07:31:47', '2020-11-21 07:31:47'),
('66f691fee5b5b0c680abed273fd677143b25c017d2844a5164f6f1021e9d4c408499ce2dba1efa98', 41, 2, NULL, '[]', 1, '2019-11-12 07:15:39', '2019-11-12 07:15:39', '2020-11-12 07:15:39'),
('6720f364f525a676b7b9bc71b8eba244bb86d86cfd5f38e4610e31d18ac0013ec4a803a1997501b2', 12, 2, NULL, '[]', 0, '2019-10-23 11:17:42', '2019-10-23 11:17:42', '2020-10-23 11:17:42'),
('6760e5a1be8f6d948edae390ba5952abdd6262f578e42efff833e7ce89e7971d74a56eaff8cc7aff', 9, 2, NULL, '[]', 0, '2019-11-19 09:05:41', '2019-11-19 09:05:41', '2020-11-19 09:05:41'),
('688bb3968f43eeab8ce185d3bb55730fefc55bfd5d6b3d9ebc30ea707758b7d4aa17ac813e0c4a1e', 9, 2, NULL, '[]', 0, '2019-11-20 10:49:24', '2019-11-20 10:49:24', '2020-11-20 10:49:24'),
('68c1cfeca65639feeaafa3d4913b9cce8e99680b1a68074ae809b8b03799b27a829cf7423ebf31a1', 46, 2, NULL, '[]', 0, '2019-11-19 09:03:00', '2019-11-19 09:03:00', '2020-11-19 09:03:00'),
('6a0d7343938c434906d17a2db7d704b6b93effa4fc8a4e650c0572103ec5f4b6eb66e140793c6c40', 21, 2, NULL, '[]', 0, '2019-11-13 09:26:12', '2019-11-13 09:26:12', '2020-11-13 09:26:12'),
('6a4b3082a40e957241c3fce4f3b5b6ed9ebc78808ae5f7908098c81d99670b11a4cfecd34acf7837', 37, 2, NULL, '[]', 0, '2019-11-13 08:02:04', '2019-11-13 08:02:04', '2020-11-13 08:02:04'),
('6a66163cedab53aef5130697e173ab52ee8b12721c6fb0f24c3a84449b15388e4355b7d3bbece545', 15, 2, NULL, '[]', 0, '2019-10-01 12:47:15', '2019-10-01 12:47:15', '2020-10-01 12:47:15'),
('6a9a8f19756f73118faea5e16fb97c59b7682cda70eefafcaf19bc62ea1bcb15a4d1c2109211260f', 37, 2, NULL, '[]', 0, '2019-11-12 07:34:10', '2019-11-12 07:34:10', '2020-11-12 07:34:10'),
('6ac7c38773d3ba48f63aac07bebd30cf73098c696853e24d0f2aa18e579c8e4f3b539c5884221198', 21, 2, NULL, '[]', 0, '2019-10-11 09:11:52', '2019-10-11 09:11:52', '2020-10-11 09:11:52'),
('6afe3b564f46a9055e0575fb63920eb3212424a07ac0177bbce7b03b8030b47a2531d699b94485a9', 25, 2, NULL, '[]', 0, '2019-10-24 18:43:29', '2019-10-24 18:43:29', '2020-10-24 18:43:29'),
('6cb63302b2530b47529c25f993febbfc3b1770bdf0db65f3159fda61c17d8126644e0cf779d75e34', 8, 2, NULL, '[]', 0, '2019-10-01 09:24:46', '2019-10-01 09:24:46', '2020-10-01 09:24:46'),
('6d601d9679f4e802e854c3f224aa141234c4c21fbf372d14c1d3fa45db1ff2907d094e7742e2e77f', 19, 2, NULL, '[]', 0, '2019-10-30 09:55:34', '2019-10-30 09:55:34', '2020-10-30 09:55:34'),
('6d8477d40aecbbf2d73790f327b2ed1580ac4a71963aab762267b659d6fcaa20fc5cc3f53133c541', 23, 2, NULL, '[]', 0, '2019-10-23 10:07:30', '2019-10-23 10:07:30', '2020-10-23 10:07:30'),
('6de18a9fa8d1b553c00c801b3a966015eefca0cccb7eb3fcc82177607d566ce81c59b52d3522a9dc', 21, 2, NULL, '[]', 0, '2019-10-14 11:47:38', '2019-10-14 11:47:38', '2020-10-14 11:47:38'),
('6e1de9fb14fcbf8280081c9d9941cc6bb77f1d9f4a9b47e117ffbee8f0fe482367c31382a8686e50', 46, 2, NULL, '[]', 0, '2019-11-20 06:50:39', '2019-11-20 06:50:39', '2020-11-20 06:50:39'),
('6e65a8e134d6d9cf30ca4ae6719933a74fb93e078a0c6409a8ce9a1b05ee908939936dad474f0d17', 66, 2, NULL, '[]', 0, '2019-11-19 15:35:06', '2019-11-19 15:35:06', '2020-11-19 15:35:06'),
('6e91de623d3bd9ddab90e8bd9fc21259693b198d230488f6058b97a34cda52a0452cfb1300801be7', 117, 2, NULL, '[]', 0, '2019-11-21 07:23:13', '2019-11-21 07:23:13', '2020-11-21 07:23:13'),
('6ed249fe6b85a78933fa038889e989cde1e33393dbed8ef8cb4292b78c128739e7642978577f3f8a', 18, 2, NULL, '[]', 0, '2019-10-01 14:07:24', '2019-10-01 14:07:24', '2020-10-01 14:07:24'),
('6f2e7c178ac70fe7730812c2c484d678341839c56993818d83951217f31364d7e7ae7fd37a3c1a1f', 9, 2, NULL, '[]', 0, '2019-11-20 11:03:30', '2019-11-20 11:03:30', '2020-11-20 11:03:30'),
('703a72baf8a8ffc6a471628543e444ad9217c18145d18c7daa8ccf2a38ec055aaa7efba6d14ee444', 20, 2, NULL, '[]', 0, '2019-11-05 14:37:45', '2019-11-05 14:37:45', '2020-11-05 14:37:45'),
('709537e3c8ec663ba073f15e470652cbd3f990ca48b740f6dec0422310de5347b8320107227c2b56', 25, 2, NULL, '[]', 0, '2019-11-12 09:36:23', '2019-11-12 09:36:23', '2020-11-12 09:36:23'),
('70eafc8f6673e3f5c24f2273a61b9569c10d1ce881e41e6fb3b39b7f938406b7ad730c7eb59f820d', 32, 2, NULL, '[]', 1, '2019-10-31 11:18:44', '2019-10-31 11:18:44', '2020-10-31 11:18:44'),
('7165f0d7ac44ac36d0c7a80fc732c3dbdc6e2a86309f1335cb474d84491f7b0052e15353151dd7ae', 103, 2, NULL, '[]', 0, '2019-11-20 13:31:41', '2019-11-20 13:31:41', '2020-11-20 13:31:41'),
('71bda0eee798b0c358116f32b6dbc2761a946ed45087040840f0d994ffc6890e8575453fb33016ec', 43, 2, NULL, '[]', 0, '2019-11-08 12:50:19', '2019-11-08 12:50:19', '2020-11-08 12:50:19'),
('73d5ca97a35aebce0dc4086d119d86e4ff555974e41712ed35d864fdba6bac92ae13e2a7f949a1f4', 117, 2, NULL, '[]', 0, '2019-11-21 06:23:09', '2019-11-21 06:23:09', '2020-11-21 06:23:09'),
('7420758da41c6dd28622a3262339020b5390a32306204699ece716db55a7c0301e492afbb0f0b1f0', 9, 2, NULL, '[]', 0, '2019-11-12 18:29:21', '2019-11-12 18:29:21', '2020-11-12 18:29:21'),
('74260d2f30ebe0285dba005bc12fad9ed03632dc8305cc0186c052894194a239e1e0bd3f187b4e54', 12, 2, NULL, '[]', 0, '2019-11-25 05:20:26', '2019-11-25 05:20:26', '2020-11-25 05:20:26'),
('7427b8d84705abfe14a0e53c0cd7c68576465fa956bea12b74923e5fcaaf519dae25fcb24ae02dd2', 12, 2, NULL, '[]', 0, '2019-10-24 05:24:20', '2019-10-24 05:24:20', '2020-10-24 05:24:20'),
('744333343c5ff1892d597d9d5edaf81be1c79cf7a164fe0299a4da65a1e4a8792ce21076a2b71349', 33, 2, NULL, '[]', 1, '2019-11-04 10:38:18', '2019-11-04 10:38:18', '2020-11-04 10:38:18'),
('74cf3a13abd27597fc4445e3dcbe604b11164f76957016177696581f3d82e1dd620c1385346dd554', 19, 2, NULL, '[]', 0, '2019-10-07 06:10:13', '2019-10-07 06:10:13', '2020-10-07 06:10:13'),
('7560beffb6d2df0e30129bad137fffb2df5bbf17aea0d4ba2f058c4b752389dcfd48d9c8748331e7', 46, 2, NULL, '[]', 0, '2019-11-18 13:21:51', '2019-11-18 13:21:51', '2020-11-18 13:21:51'),
('7582f3332076a5368924dc63ae317423bfae2a05c424734b94c4ec6f9adfbaeb9f552f220258d7cd', 12, 2, NULL, '[]', 1, '2019-11-14 13:59:57', '2019-11-14 13:59:57', '2020-11-14 13:59:57'),
('75d0ef9ea903c188e75184604fceea661d386cf66c31a9d81c8aba08d5fab5537817b6ccee2d534f', 117, 2, NULL, '[]', 1, '2019-11-21 09:30:12', '2019-11-21 09:30:12', '2020-11-21 09:30:12'),
('7694ade7ab5dfa9f159631e0d4c6506cf6ff73fb9118bc07083926895f29579c124ad069e47e0dd1', 21, 2, NULL, '[]', 1, '2019-11-12 07:40:10', '2019-11-12 07:40:10', '2020-11-12 07:40:10'),
('7722a20154f4a3656861d3047f3248db142bf4e147a45a934f9a4f0b01c832fa7a1e467a3309aa86', 25, 2, NULL, '[]', 0, '2019-11-14 08:02:43', '2019-11-14 08:02:43', '2020-11-14 08:02:43'),
('77a29670a6323d98d4399bd2c164860d6508f8e8a826df248c502d1be395262af0ffd046eee32c1c', 9, 2, NULL, '[]', 1, '2019-11-15 05:52:44', '2019-11-15 05:52:44', '2020-11-15 05:52:44'),
('77ccdadcdb5ab33fdacda2d1541be213a1ee9866e45e9c5875c842434e9b5935dad518f7fac7fe6d', 21, 2, NULL, '[]', 0, '2019-10-14 10:09:37', '2019-10-14 10:09:37', '2020-10-14 10:09:37'),
('77ff23ec6bbf20555471bc8340154143c8a9f541515a3d38b84e78d03b78cc01e663e4f53fc0e697', 50, 2, NULL, '[]', 0, '2019-11-19 10:30:17', '2019-11-19 10:30:17', '2020-11-19 10:30:17'),
('781d93cb70f2506b08cc3847144e0c56c708f249fef8a098ed885c0932418dc0e5ada8a5b415bf09', 117, 2, NULL, '[]', 1, '2019-11-21 11:40:48', '2019-11-21 11:40:48', '2020-11-21 11:40:48'),
('7870befb1c815b22f572e1ee532508d00369a508162b59c60ff5a5cd5ea68b6254c86ff8492ac96e', 21, 2, NULL, '[]', 0, '2019-11-13 07:41:00', '2019-11-13 07:41:00', '2020-11-13 07:41:00'),
('79034f2ab50ea8faed5be0e4d837e7d08db79ef38cdd31dc965596fe6f7b340d60838fd983b3fd76', 12, 2, NULL, '[]', 1, '2019-10-23 18:02:02', '2019-10-23 18:02:02', '2020-10-23 18:02:02'),
('792d8491e9dc32879784e721782a232089095965e20e95dc6d4cd32bea2143f276e95b959fc862b6', 86, 2, NULL, '[]', 0, '2019-11-20 06:15:50', '2019-11-20 06:15:50', '2020-11-20 06:15:50'),
('79cbfd858033e074c634f2a515286ba0983afe093ca564f93b8e7e6feb6193fc6a737e719e7679a8', 41, 2, NULL, '[]', 1, '2019-11-12 09:20:55', '2019-11-12 09:20:55', '2020-11-12 09:20:55'),
('7a402928d8c73e66ecc6f8a23ccb81bfddaaf819a55919433125e4e6563dbcaafce42e1debdaa818', 37, 2, NULL, '[]', 1, '2019-11-12 11:18:03', '2019-11-12 11:18:03', '2020-11-12 11:18:03'),
('7ac8be3c6b7f0b6e3e49eed3a8544672c191f358aa47eb0908cb2ac22bdb14f0f0c57ed04c0c5c4a', 12, 2, NULL, '[]', 1, '2019-11-08 07:17:45', '2019-11-08 07:17:45', '2020-11-08 07:17:45'),
('7c490ece9aa8fbb86ef31829d679e463dbf71476a9efc2147a5b52daf61e26c77c7d1d786cd90e2d', 41, 2, NULL, '[]', 1, '2019-11-21 07:44:02', '2019-11-21 07:44:02', '2020-11-21 07:44:02'),
('7d3cadcfd71fb5f7c4798fa2c249cf5b9dba5c41f49a9df42f3105420cc884c2a4ee72500cb27147', 19, 2, NULL, '[]', 0, '2019-11-13 05:21:26', '2019-11-13 05:21:26', '2020-11-13 05:21:26'),
('7d5a6aea0735d86702bd9ef8885b6230e96b74aa077489edb910bc4fbef0da9960b3c28426f5efe6', 9, 2, NULL, '[]', 0, '2019-10-21 05:26:44', '2019-10-21 05:26:44', '2020-10-21 05:26:44'),
('7d6f75ea1de90d7ca9de5ba92bb31110aca9d00af346e8acdddccec773a438a00a4e2978003f9953', 12, 2, NULL, '[]', 0, '2019-11-04 09:00:47', '2019-11-04 09:00:47', '2020-11-04 09:00:47'),
('7d7051740fe6099f1027033395a0f6fc288e2ee1b0e6274f9630c957441fbb348714a83eac18cab7', 31, 2, NULL, '[]', 1, '2019-10-31 12:01:31', '2019-10-31 12:01:31', '2020-10-31 12:01:31'),
('7d8e94714681170ada5449a2e560c1071a8d40627705efd2a106d469ff66cde6908d36884eb6db82', 12, 2, NULL, '[]', 1, '2019-11-18 10:34:19', '2019-11-18 10:34:19', '2020-11-18 10:34:19'),
('7fa4178adbdf4d0b34a76133aa6d8fc90821c1d57dc87d259893c40f4f8cf1e07aa1faa4ae28e64b', 62, 2, NULL, '[]', 0, '2019-11-19 14:44:52', '2019-11-19 14:44:52', '2020-11-19 14:44:52'),
('803dd415eb304bc84b123f071d008d38ab0f105a4cc1c9250c4437aa44ee16bda69d3dd04ce6062a', 21, 2, NULL, '[]', 0, '2019-10-14 09:53:41', '2019-10-14 09:53:41', '2020-10-14 09:53:41'),
('812f3d9b62cc75dd10054e23aa70094d395014895efef752f8f88d71df7f96e90951a4bfe996cad9', 9, 2, NULL, '[]', 0, '2019-11-15 15:38:53', '2019-11-15 15:38:53', '2020-11-15 15:38:53'),
('818bc2b70204fc53480dfba2d865aa204f72f0e14f1dd89d7b6f9b0b8a706c11789ba3333c336618', 19, 2, NULL, '[]', 0, '2019-11-20 13:59:02', '2019-11-20 13:59:02', '2020-11-20 13:59:02'),
('81c2d345468f0224d115bc1c365c7594f9418fef10be8c9087dd39737f44984aefd1c69b108dbe7e', 21, 2, NULL, '[]', 1, '2019-11-08 12:37:29', '2019-11-08 12:37:29', '2020-11-08 12:37:29'),
('81cab93df059508442452c18545a445f9766e49ac20e2f76f06ebf57104c6cfb8a9d8a12ad3ae824', 19, 2, NULL, '[]', 0, '2019-10-31 09:23:28', '2019-10-31 09:23:28', '2020-10-31 09:23:28');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('82acfb1563d838047e68f1ba0dc2cbbec04888bcd0026ab8a9e1f287777d003850339e59228b05d1', 12, 2, NULL, '[]', 1, '2019-11-11 07:41:24', '2019-11-11 07:41:24', '2020-11-11 07:41:24'),
('84ddf71501b1387ffa3ba3b6bf300a78608e2e0e4e8e741063658e6c74919255e612cd889f011c25', 21, 2, NULL, '[]', 1, '2019-11-01 13:17:20', '2019-11-01 13:17:20', '2020-11-01 13:17:20'),
('860c56b39341613c55ba31962db3f21a2ddc183c251fd131b361b2a245f964c1897e057d223f8304', 21, 2, NULL, '[]', 1, '2019-11-04 12:04:55', '2019-11-04 12:04:55', '2020-11-04 12:04:55'),
('8681a4341c3a7f5525261d37127b65b75cfca089260aa1a24146045a506bae6b5a2096b4f4a2fa0c', 36, 2, NULL, '[]', 1, '2019-11-11 16:06:53', '2019-11-11 16:06:53', '2020-11-11 16:06:53'),
('86e9dfb86a06d9f5089f2b5753e6fa438c45e2e7484f3bbb666cd151eedccb6504344eaba02905f8', 117, 2, NULL, '[]', 0, '2019-11-21 06:10:29', '2019-11-21 06:10:29', '2020-11-21 06:10:29'),
('881823475fabc56ef72be1e837dc0141cc0cb2ab57bdbb4cd4512dbfa2b77cc34101ca141451a192', 9, 2, NULL, '[]', 0, '2019-11-15 07:17:21', '2019-11-15 07:17:21', '2020-11-15 07:17:21'),
('883de0eea60eb5eaa67ce89752993025d140c6d82ac21055994f1dad536fa2f2b2657f4514de0f57', 71, 2, NULL, '[]', 0, '2019-11-19 19:44:46', '2019-11-19 19:44:46', '2020-11-19 19:44:46'),
('88953b48cac3fd3ada41870d2c0ad0bdf9c103377f299847a47c2928bbc0397c87126c99ebdce9fa', 9, 2, NULL, '[]', 0, '2019-10-22 13:58:28', '2019-10-22 13:58:28', '2020-10-22 13:58:28'),
('88a5e8315d4ed39ae17d1a3939c0a0c5b6826304f52ff408f1b1bcc279d3b1ccc639108473e1c129', 76, 2, NULL, '[]', 0, '2019-11-20 00:20:40', '2019-11-20 00:20:40', '2020-11-20 00:20:40'),
('88cc22331846a5edac2d880fc2c5dff440f29538b33b83462e5a8278eaaec1b5544cd9781f838a14', 9, 2, NULL, '[]', 0, '2019-10-23 07:32:19', '2019-10-23 07:32:19', '2020-10-23 07:32:19'),
('8969115d302cb1a49dc314f924abed72b58c8ee54f9902fb3bf0e5439b9df4076731987bc371c1da', 108, 2, NULL, '[]', 0, '2019-11-20 15:14:48', '2019-11-20 15:14:48', '2020-11-20 15:14:48'),
('8a9f616b73dd0a6de0f458efe06840f9394be7b23d2aab3c99d2f1853f881d82cd6c177d9b6a0eb9', 49, 2, NULL, '[]', 0, '2019-11-18 17:18:15', '2019-11-18 17:18:15', '2020-11-18 17:18:15'),
('8abc0803bda1c4cc89ec607017883ec44453913a006c78ec9c42682ee688a9d531893f629993ccbe', 68, 2, NULL, '[]', 0, '2019-11-19 16:00:57', '2019-11-19 16:00:57', '2020-11-19 16:00:57'),
('8ad8a4496f29d90c0298aced305c30a78065ed7fdedd51226b97a85eda773d0802b81d9dddb3ed20', 21, 2, NULL, '[]', 1, '2019-11-12 08:39:25', '2019-11-12 08:39:25', '2020-11-12 08:39:25'),
('8bac296e6d1505b200b5773a8418f4e77536a9891c160c5106d33f8d4677825a3bce163fcd214d67', 59, 2, NULL, '[]', 0, '2019-11-19 13:55:22', '2019-11-19 13:55:22', '2020-11-19 13:55:22'),
('8bb1f6314c296cebc329c3a647a3309ac687b59b5e3557e62be46e749f66c8ad24cb3da2810e64d1', 58, 2, NULL, '[]', 0, '2019-11-19 13:45:13', '2019-11-19 13:45:13', '2020-11-19 13:45:13'),
('8bfab1725caa0acf723d5e12ff8b90373af16c9b10b8c0839ff8b66c6620d16c089d5aa06b77b6a6', 16, 2, NULL, '[]', 0, '2019-10-01 13:19:57', '2019-10-01 13:19:57', '2020-10-01 13:19:57'),
('8c18fdddd9cdbaf5a85ce26efed93551c1362e0d8c55dd11d9aa6002b43cd2007e927d825172af1e', 117, 2, NULL, '[]', 0, '2019-11-21 11:18:34', '2019-11-21 11:18:34', '2020-11-21 11:18:34'),
('8c6d721e8462368fdc52a6d7fe6b3aef69c6241ed275c0f1e8631bb7fe2065b60ea8fef45f59c1ca', 18, 2, NULL, '[]', 0, '2019-10-01 14:08:08', '2019-10-01 14:08:08', '2020-10-01 14:08:08'),
('8eb5da30c9552f169c5c4a30a945f7f93c94c77f5bcc2911c8cbf661aaace46cdc22e437ae1da5d6', 28, 2, NULL, '[]', 0, '2019-11-06 05:18:35', '2019-11-06 05:18:35', '2020-11-06 05:18:35'),
('8f4e94fd8e9c5212144b50424048abfc7589989df221fe7740b37cec340f7b8f8350b3d8363b1c5f', 5, 2, NULL, '[]', 0, '2019-09-28 02:31:06', '2019-09-28 02:31:06', '2020-09-28 08:01:06'),
('8fc9f0f5e1617718f79bf009f88eb4b50af61e2e4a198f62e8d337de7671b146046d2b8351c1a359', 12, 2, NULL, '[]', 0, '2019-10-01 11:23:06', '2019-10-01 11:23:06', '2020-10-01 11:23:06'),
('904fa5d15b1d84806dbf8aa06407c400851853d8b7fc997fb3686ed39916a4a470a3ee17b108c1a3', 12, 2, NULL, '[]', 1, '2019-11-11 09:25:57', '2019-11-11 09:25:57', '2020-11-11 09:25:57'),
('90786e5dfb3555f9c58f563ba6679ed18fdca5da3fdd3788be073dab8297d6296d57a4dd173274f0', 117, 2, NULL, '[]', 1, '2019-11-21 11:41:46', '2019-11-21 11:41:46', '2020-11-21 11:41:46'),
('91f11fbb3f59d709e52387787788b8444006535332b36fda11b11ee509ab7c326279a001987d27ca', 19, 2, NULL, '[]', 0, '2019-10-31 09:25:45', '2019-10-31 09:25:45', '2020-10-31 09:25:45'),
('9220bb17876ca64b527746356cefcdc87dbe5469b32751a00eeea5f389d73ace0635cb2ad5cec09d', 5, 2, NULL, '[]', 0, '2019-09-28 02:39:14', '2019-09-28 02:39:14', '2020-09-28 08:09:14'),
('923cd632cf06aba638cb1efff82e7128fb128e0b65ffb052b67560d6c1ccdbc6f12a708be2411ff4', 21, 2, NULL, '[]', 1, '2019-11-12 13:13:47', '2019-11-12 13:13:47', '2020-11-12 13:13:47'),
('9275bbc0ae0ec0858a72be10254d890eacaf9084d619c7d7d9078571bd017cf67ed8f46c874a0841', 41, 2, NULL, '[]', 1, '2019-11-12 12:02:38', '2019-11-12 12:02:38', '2020-11-12 12:02:38'),
('93071ab77c62ff14bbfc509f061fee687b84dcddc6e8e0e0b0d28d1944be3e180000adbf802ebcc5', 46, 2, NULL, '[]', 0, '2019-11-18 11:43:06', '2019-11-18 11:43:06', '2020-11-18 11:43:06'),
('931326091e9468be11e830583cc92d4414ff75f6e4f75b85658d688dc6deaae2185e13bd037119e9', 20, 2, NULL, '[]', 0, '2019-11-19 10:38:39', '2019-11-19 10:38:39', '2020-11-19 10:38:39'),
('937f1a6dab3bf8713311e1f8fada01746974df951bdf6bfb34986189913bf9fd13513cba9f32a166', 21, 2, NULL, '[]', 0, '2019-10-10 08:35:50', '2019-10-10 08:35:50', '2020-10-10 08:35:50'),
('93ec63122e1956da5d0efbe025f225f59c35ffffa118220811d1a3c89046400a884419ec88098892', 126, 2, NULL, '[]', 0, '2019-11-29 07:48:50', '2019-11-29 07:48:50', '2020-11-29 07:48:50'),
('94b421242b03af73d934c5e7c6d5fc913a8423ea334302c90e49f7a43812257cbf6f8b4a731f3715', 121, 2, NULL, '[]', 0, '2019-11-21 07:46:23', '2019-11-21 07:46:23', '2020-11-21 07:46:23'),
('9578ada7312c4f9c9362897d9faf664de5b46f80e6ddab28b246c1105458f44e406dbde1dd407005', 21, 2, NULL, '[]', 1, '2019-11-12 05:45:11', '2019-11-12 05:45:11', '2020-11-12 05:45:11'),
('95b8d2e2cff4984d9208bc575d7e80edd9aaf0d43f81eddc5b6ea36bb196b5b2f246a496cb1c393f', 105, 2, NULL, '[]', 0, '2019-11-20 13:47:20', '2019-11-20 13:47:20', '2020-11-20 13:47:20'),
('95c9de4d407b941c8bd56a18dde6a196e6f086f5ab648927d6ae439779da5bc26bf61119178744aa', 21, 2, NULL, '[]', 0, '2019-10-31 14:55:57', '2019-10-31 14:55:57', '2020-10-31 14:55:57'),
('961f9c5554f5bb15751d32dc372c33fb08f5b88e338699751441c914077f0c426f65be3c7c26e9ac', 95, 2, NULL, '[]', 0, '2019-11-20 11:38:26', '2019-11-20 11:38:26', '2020-11-20 11:38:26'),
('967e96ded0497ea420afc73cf7094151b8ab211fd6a34f7856ffe3e81fb27da331a0b7a56bff8ace', 21, 2, NULL, '[]', 0, '2019-10-09 13:32:34', '2019-10-09 13:32:34', '2020-10-09 13:32:34'),
('97ff3c53173badee0524210e6ce1982896bb06b610f23c4ac4115b2960cc0dc47aa011854995faed', 117, 2, NULL, '[]', 0, '2019-11-21 06:38:37', '2019-11-21 06:38:37', '2020-11-21 06:38:37'),
('98537734f5478ca2d8e6d3fa56e5f8b7938788f7c40b1b23d271f0a55e07ab7117b069cb47da493d', 12, 2, NULL, '[]', 1, '2019-11-06 08:10:57', '2019-11-06 08:10:57', '2020-11-06 08:10:57'),
('98eb4181b4e25dc2eeee4f7f736cae005307f1df332de6b0138a7b5e6c794424cc2684ac512562d1', 12, 2, NULL, '[]', 0, '2019-11-04 09:10:12', '2019-11-04 09:10:12', '2020-11-04 09:10:12'),
('994daeb81fa119486be0ee2a67fbbfb067676aa57f2b3fcfea6353eac425bd99bf95fd03b444105d', 21, 2, NULL, '[]', 0, '2019-10-09 09:53:19', '2019-10-09 09:53:19', '2020-10-09 09:53:19'),
('9b1dd981f3a6b32a4140b902bfff577599f1ad2f3840dbb9675ad3245ad941e8551994eeb5881cfd', 123, 2, NULL, '[]', 0, '2019-11-22 05:43:21', '2019-11-22 05:43:21', '2020-11-22 05:43:21'),
('9b4edbd54c33765408ad44012f50b042e2db973ec744216223c8968be8f1427fa78d7ae8df387780', 57, 2, NULL, '[]', 0, '2019-11-19 13:49:34', '2019-11-19 13:49:34', '2020-11-19 13:49:34'),
('9b524095e9b62574271f6147b5b6f644b53d0e9ae6c79d2aa3bcc8a849ecb21455b7309819ed8071', 9, 2, NULL, '[]', 0, '2019-10-18 10:31:43', '2019-10-18 10:31:43', '2020-10-18 10:31:43'),
('9b7b4c8bbbc6e4a6e1c4a7f0e3977c44ea226c7e04ffa9e5e2cd89b2683964179dda4232b54e675b', 9, 2, NULL, '[]', 0, '2019-10-01 10:00:04', '2019-10-01 10:00:04', '2020-10-01 10:00:04'),
('9c66619955fdf8bc6175e7611ea2a412ed40e2b2ddad3cb38e5ca80be2d578f8c4821d186d41dd35', 21, 2, NULL, '[]', 1, '2019-11-15 11:14:00', '2019-11-15 11:14:00', '2020-11-15 11:14:00'),
('9d0b98558581df027183885d604a3b6ea30932e305c39144259e60cde6207c62b16c4947b3a74570', 90, 2, NULL, '[]', 0, '2019-11-20 06:42:01', '2019-11-20 06:42:01', '2020-11-20 06:42:01'),
('9d5d864b78bb57fb52c626390bc4ffbacf4125ee40a625dcc5c9d9c06efedb9a16bee3aeb4a953aa', 12, 2, NULL, '[]', 1, '2019-11-06 08:11:22', '2019-11-06 08:11:22', '2020-11-06 08:11:22'),
('9e2e6f42d43eb0782b8a8e97b3abdac14a8d4e3b888385bf48f4edd43861984b84340e1d611dc582', 117, 2, NULL, '[]', 0, '2019-11-29 10:18:50', '2019-11-29 10:18:50', '2020-11-29 10:18:50'),
('9ed80be2ec0e874d785acb1d15b8566ef8899c8d3668104b11228a1e3c60734a99b65a80a8c025b5', 117, 2, NULL, '[]', 1, '2019-11-29 07:38:49', '2019-11-29 07:38:49', '2020-11-29 07:38:49'),
('9eddf2b063b511f30fa1b59e62186e439eca313217df2016a5685e69ba416f80c48536f97ca6686f', 19, 2, NULL, '[]', 1, '2019-10-23 07:57:48', '2019-10-23 07:57:48', '2020-10-23 07:57:48'),
('9f4d4e8f00c1db230e4483f9950160c208859076303dcaf1246b5c79a2cf68dc40cd1508cbe49cd5', 67, 2, NULL, '[]', 0, '2019-11-19 15:37:29', '2019-11-19 15:37:29', '2020-11-19 15:37:29'),
('a00f3824917d8ec3aa58ef3f9fad471aabedfaa32cfbe9024af69452bf009536b9a8905d1293693e', 118, 2, NULL, '[]', 0, '2019-11-21 05:27:44', '2019-11-21 05:27:44', '2020-11-21 05:27:44'),
('a0138ca288b0f95c06b550e2a1db31a28861beb0039c5b93b02ffa32fd8312ec3217db1ae75f8f24', 117, 2, NULL, '[]', 0, '2019-11-21 07:33:10', '2019-11-21 07:33:10', '2020-11-21 07:33:10'),
('a09047c96ad821663e61740b99008efa647c01e285ace75ba703514fedf6efefa4689b4c506c3804', 46, 2, NULL, '[]', 0, '2019-11-18 11:38:47', '2019-11-18 11:38:47', '2020-11-18 11:38:47'),
('a12a8d8a17fb6afefca84126be1c8dde1b941ae57ce0c841cb37e5420da514e78a0a8c3af273dd07', 41, 2, NULL, '[]', 1, '2019-11-12 08:40:20', '2019-11-12 08:40:20', '2020-11-12 08:40:20'),
('a15041b503698030ab79e6d728d34352432acaf52b577b6ef1ce0f01b984423661412a0c32979284', 19, 2, NULL, '[]', 0, '2019-10-30 04:49:42', '2019-10-30 04:49:42', '2020-10-30 04:49:42'),
('a1d897b22d079cecc3437c678657299eaf5642e5f7905e3a7c6b46f5a431d04c688d608de432b508', 12, 2, NULL, '[]', 0, '2019-10-23 08:54:27', '2019-10-23 08:54:27', '2020-10-23 08:54:27'),
('a2971d827499b6acb60e03502fd333e5b2100e14bc220bd7f010ff498119d72b5a111cdb30d03d74', 9, 2, NULL, '[]', 0, '2019-10-11 06:00:13', '2019-10-11 06:00:13', '2020-10-11 06:00:13'),
('a2c9311d0b4e722109cc190692c9ab5ee537b52bfebcff676056454c09280993e6d162e8439e5d19', 55, 2, NULL, '[]', 0, '2019-11-19 14:37:52', '2019-11-19 14:37:52', '2020-11-19 14:37:52'),
('a2d62614cc3d26fb81eb3cb605b98300c3bb34351ee5479291cf2ea0fd5cba075dc49469d439ddd7', 21, 2, NULL, '[]', 0, '2019-10-14 11:43:32', '2019-10-14 11:43:32', '2020-10-14 11:43:32'),
('a3761e724e09170cbbbd20f8916c14a2cb78e306ceee9906b9f1693e67c2356f3a8a191cf0b0088c', 117, 2, NULL, '[]', 0, '2019-11-21 06:41:52', '2019-11-21 06:41:52', '2020-11-21 06:41:52'),
('a43114e522b970b0568402900da2d04e76301c95754f45093391e3777992983032f55786275e171c', 117, 2, NULL, '[]', 0, '2019-11-21 11:18:29', '2019-11-21 11:18:29', '2020-11-21 11:18:29'),
('a466a974432ccb9fd6f5df8b5dec79f0fdadc1a1464a9e1a7a308cc7768b46fdb304b8aa36cb4958', 19, 2, NULL, '[]', 0, '2019-10-23 05:53:56', '2019-10-23 05:53:56', '2020-10-23 05:53:56'),
('a54cfdb41585866b3018c43fc3f58559c537a36737ec796280b361f7ca222b38d9762f45fb7ceb95', 21, 2, NULL, '[]', 0, '2019-10-24 10:46:14', '2019-10-24 10:46:14', '2020-10-24 10:46:14'),
('a5e7f38f0cd4ac9c2e0ce6b77d8c96c4904058366e47f87212815666ac527fe03bf59fa094ce6347', 41, 2, NULL, '[]', 1, '2019-11-14 14:13:38', '2019-11-14 14:13:38', '2020-11-14 14:13:38'),
('a618fe79980344a9dc4f4764bcb8bfd9d1c4549041c17d4351ca4022215fe6db527dfe075862e29d', 61, 2, NULL, '[]', 0, '2019-11-20 11:32:09', '2019-11-20 11:32:09', '2020-11-20 11:32:09'),
('a7130bc318582bcab2e60a2efcf5b8794921b35acc4f73284afc276358c9d4ce3f78196f8c33061b', 50, 2, NULL, '[]', 0, '2019-11-19 10:26:45', '2019-11-19 10:26:45', '2020-11-19 10:26:45'),
('a7aac4c096d90f6a2a2c4778cec18a61e1ba4ab029b27ad2368b3ada98b90e06e7a858d698edf21b', 21, 2, NULL, '[]', 0, '2019-10-14 07:34:36', '2019-10-14 07:34:36', '2020-10-14 07:34:36'),
('a7c249e8bda87431afc55cb9a8881b157ede65f1330b559f7e2d78ca3015b9dbe6b573354648ff09', 110, 2, NULL, '[]', 0, '2019-11-20 15:50:11', '2019-11-20 15:50:11', '2020-11-20 15:50:11'),
('a85397816c44195f7ed8a5c1ce8cfce7d50422c9fa65f3af2b2a7c2cd161938988223053edfaa97f', 26, 2, NULL, '[]', 0, '2019-10-25 08:26:42', '2019-10-25 08:26:42', '2020-10-25 08:26:42'),
('a9bdb8f66f09b2ba639e9f694859ed29e3ae556a432c4f1fe9ccca34aeddafb7bf78cdd18314a839', 12, 2, NULL, '[]', 1, '2019-11-08 06:47:40', '2019-11-08 06:47:40', '2020-11-08 06:47:40'),
('aa190dfdacfc93f1e38b131a8ef9225bf183a0bf693bae501fc1c29bb252cd3ebef83d2ff6ac138d', 9, 2, NULL, '[]', 0, '2019-11-08 06:49:59', '2019-11-08 06:49:59', '2020-11-08 06:49:59'),
('aa80398829c55c3dffd886d32f07f16cc2e34628d7510ab7351b345fe96cafdf53859c32b54d6e46', 21, 2, NULL, '[]', 0, '2019-10-09 13:39:40', '2019-10-09 13:39:40', '2020-10-09 13:39:40'),
('aa841f59198c9aba866703142b3c74685f308e94a8e1c3e328ed8c1a0cd18e8fd890be8167a618f9', 21, 2, NULL, '[]', 0, '2019-11-01 07:13:11', '2019-11-01 07:13:11', '2020-11-01 07:13:11'),
('ab465201c48b3ba5f18c11c3b9d417ee9680ef025f906f3342fef53819e795e0ba31978675fc1000', 19, 2, NULL, '[]', 0, '2019-11-19 10:34:28', '2019-11-19 10:34:28', '2020-11-19 10:34:28'),
('ab70bdbcf8e4397d3f04333d121e0526985ee8d5e35a2684312910afa590ef333064c4c3481365e4', 19, 2, NULL, '[]', 1, '2019-11-18 17:14:51', '2019-11-18 17:14:51', '2020-11-18 17:14:51'),
('ac77833ee36d3c5dddf148d282bafb2bec2395b64693123db88c24960234006b886d15b38cea9955', 9, 2, NULL, '[]', 0, '2019-11-04 08:36:21', '2019-11-04 08:36:21', '2020-11-04 08:36:21'),
('ac9a4c57fd9de89890a2cab442cb561da54790398272aa900a6181d8ce285b354c2d153673fdeabc', 9, 2, NULL, '[]', 0, '2019-10-16 05:48:49', '2019-10-16 05:48:49', '2020-10-16 05:48:49'),
('ace1f49e73842d7186660e6effa024d6020e1e4e71004e186bf0f1339d955e1a564e7df4b4c2ca18', 12, 2, NULL, '[]', 0, '2019-10-23 12:49:33', '2019-10-23 12:49:33', '2020-10-23 12:49:33'),
('ad1c3dea2b582098bd4ee56a3ffddbfc02a3660c37a36cb7051e5d7f45df8ccf73772991714b329b', 9, 2, NULL, '[]', 0, '2019-11-08 10:47:31', '2019-11-08 10:47:31', '2020-11-08 10:47:31'),
('ad2cc711293c1711e25e5372aa3ac7b6278192ddae6e0de6721f049c6642f435084e6bd2b0a0f9be', 25, 2, NULL, '[]', 1, '2019-11-12 07:30:49', '2019-11-12 07:30:49', '2020-11-12 07:30:49'),
('ad6cb36cc162140db9c9f816a4f43968bf9a2bbca70a6ca81c82832fb9b35a06da3f4f1adc3e6a76', 21, 2, NULL, '[]', 1, '2019-10-31 11:24:45', '2019-10-31 11:24:45', '2020-10-31 11:24:45'),
('adac998cda75bf84fb4fe38f489d3a6e5362fbd8a8719248336568977b44d2eb8c5c27068ac02891', 21, 2, NULL, '[]', 0, '2019-10-14 09:23:03', '2019-10-14 09:23:03', '2020-10-14 09:23:03'),
('aeb165f1989f18adbbc80d72fc91f3fe61e031534ae9421ab0cc4d1be5c53bffa5c7db5906e658d0', 41, 2, NULL, '[]', 0, '2019-11-12 07:54:38', '2019-11-12 07:54:38', '2020-11-12 07:54:38'),
('afe35342d2232e1af57136999f08537bf04f218cb50d79a0fc65306bd3e9d0ad978305b9398546f0', 9, 2, NULL, '[]', 0, '2019-10-30 05:18:16', '2019-10-30 05:18:16', '2020-10-30 05:18:16'),
('b027dfdff683e4843e162e1cba3b88e367aaeb6c385666a3bbc70f22c69d16c96d2447235c93394e', 46, 2, NULL, '[]', 0, '2019-11-19 09:02:03', '2019-11-19 09:02:03', '2020-11-19 09:02:03'),
('b0a134b1122259b3c6a6eb26a772643cb781f60ba9c195c572fb274a94504039aea73f581913e4fd', 117, 2, NULL, '[]', 1, '2019-11-21 11:29:19', '2019-11-21 11:29:19', '2020-11-21 11:29:19'),
('b1165b0daf5536000eca68a3cf4efc5cd6cf63a4ae40bbbbb9f84205e65e05740648f62f89ca43bd', 21, 2, NULL, '[]', 1, '2019-10-25 06:18:36', '2019-10-25 06:18:36', '2020-10-25 06:18:36'),
('b14fe7defedc623b447b1490cd127a1d0c02e670abf1c3973a348dacbe60096804a4ff6c5167a0d8', 21, 2, NULL, '[]', 1, '2019-11-12 11:25:32', '2019-11-12 11:25:32', '2020-11-12 11:25:32'),
('b2e58e12d7250b1a91a7751a302007fbce1c50b5c4a8fca1581f082387b0a8bcf54c24c2e905ccf1', 21, 2, NULL, '[]', 0, '2019-10-09 13:35:40', '2019-10-09 13:35:40', '2020-10-09 13:35:40'),
('b353297075a1bfac6a24cc5a954a9701af472040cb116e3271be4522046acd571d3cc8f798bc1ee8', 41, 2, NULL, '[]', 0, '2019-11-12 13:08:31', '2019-11-12 13:08:31', '2020-11-12 13:08:31'),
('b36d2fedbb3329c4f89549b5465cc012e359072ce00b7be1e1a58b70b2c05ed37c997e07492d6efd', 12, 2, NULL, '[]', 1, '2019-11-12 11:00:18', '2019-11-12 11:00:18', '2020-11-12 11:00:18'),
('b3779f2cab7373d81af06387a0116750373075ea667c0385fa1c7482e5d12ec2a06e3202441c4fcf', 99, 2, NULL, '[]', 0, '2019-11-20 13:22:30', '2019-11-20 13:22:30', '2020-11-20 13:22:30'),
('b3912b978f362f01dab9a89d023d9fd5901306a352f4296bd4fbfdfdcac2299e15cea411e4cd337c', 21, 2, NULL, '[]', 0, '2019-10-11 09:22:45', '2019-10-11 09:22:45', '2020-10-11 09:22:45'),
('b3a8366d4f29987f302cd07be36eb69b7bb455f756faeafdcded109a4ab9f913794fc670cd3e10ed', 21, 2, NULL, '[]', 1, '2019-10-23 11:10:29', '2019-10-23 11:10:29', '2020-10-23 11:10:29'),
('b4003c99d13145d53c173a6ad5a62a529c4783070271e99060951dca3b2a2015e7171f6d48519bd0', 9, 2, NULL, '[]', 0, '2019-10-01 10:00:46', '2019-10-01 10:00:46', '2020-10-01 10:00:46'),
('b41bf95780d3fb7169da74bd60c36ad7dba50191b0b0deb17c0b344c80b660f97963639f0c8725c3', 12, 2, NULL, '[]', 0, '2019-11-11 15:09:00', '2019-11-11 15:09:00', '2020-11-11 15:09:00'),
('b4418546e6279ce78ff32f8b793c12a6cbf65bd0bf63132d6d409ca6ca183c49fdd23abb9215399d', 20, 2, NULL, '[]', 0, '2019-10-07 06:34:28', '2019-10-07 06:34:28', '2020-10-07 06:34:28'),
('b4519fb4cf087fc5421dd7ed8133f4fb5f338f8addd5b3d5f54f57a669657bcaf30deee3e357e8f1', 117, 2, NULL, '[]', 0, '2019-11-21 09:24:22', '2019-11-21 09:24:22', '2020-11-21 09:24:22'),
('b4e3857ed361d3125eedca7ec820aaeaea55ed6657aec588c91b709c886abaf5b5f0e6ba60b6fb4e', 21, 2, NULL, '[]', 1, '2019-11-06 13:56:49', '2019-11-06 13:56:49', '2020-11-06 13:56:49'),
('b502dc133d97d07b77552817da08ccb43f88ded0994a1947d8a26edf54d3899a0c7927d10506f5bc', 12, 2, NULL, '[]', 1, '2019-11-08 11:55:11', '2019-11-08 11:55:11', '2020-11-08 11:55:11'),
('b54f4c5a128676fac1825e5243a4e8d1abb38942e372a8a5c77145df514046c07e5ae0f1f832f410', 21, 2, NULL, '[]', 0, '2019-10-22 14:20:41', '2019-10-22 14:20:41', '2020-10-22 14:20:41'),
('b5b7b572ac57ca3490086c51be6350223cac5eb6dd57b2b2f3b73060fd54d8dbefed13a41ea1cb39', 9, 2, NULL, '[]', 0, '2019-10-01 11:07:26', '2019-10-01 11:07:26', '2020-10-01 11:07:26'),
('b5b83db7f9643c775798ff39cd52a71e6a2dba5a4941ff4dd7f13ca88c9ddbf78e258131879569d5', 12, 2, NULL, '[]', 0, '2019-10-30 10:14:29', '2019-10-30 10:14:29', '2020-10-30 10:14:29'),
('b60a7447e725d0e8d8520945bc9102014c8a072255283240956541e1fa91b72b0ac4fc5c8d70f7a7', 117, 2, NULL, '[]', 0, '2019-11-21 07:25:32', '2019-11-21 07:25:32', '2020-11-21 07:25:32'),
('b66060ba51e4099eb94276636e93526918b81ab624653f7cfb1aec6744acffe2206cbb0567cb3dc9', 21, 2, NULL, '[]', 0, '2019-11-01 13:48:52', '2019-11-01 13:48:52', '2020-11-01 13:48:52'),
('b871837a8a2f36ebff6b41ea831d212935186a3b0338f26940224ec0738263f825d2f0474507c227', 19, 2, NULL, '[]', 0, '2019-11-19 08:00:39', '2019-11-19 08:00:39', '2020-11-19 08:00:39'),
('b9497e41ca951e22c95775350e6ab0ef49dae15e0b892a76ab2e9f5ce1d56f73ee7fbd712b376108', 117, 2, NULL, '[]', 0, '2019-11-21 06:08:13', '2019-11-21 06:08:13', '2020-11-21 06:08:13'),
('b96984d4c529fedd68a34ccd2d2c08cddf3e7761173a6ac13a4c23666474f7dad83f8b9a50013512', 85, 2, NULL, '[]', 0, '2019-11-20 06:10:01', '2019-11-20 06:10:01', '2020-11-20 06:10:01'),
('bab7f08c8a2bfa13de4be2266c374df87941ec5b3a23bceee66d5fd3ca8b419d5769c426cfe101fd', 57, 2, NULL, '[]', 0, '2019-11-19 13:32:01', '2019-11-19 13:32:01', '2020-11-19 13:32:01'),
('bac9a34bc5af873b4d0e86e5018c484f7c5b72fb3f3b2311d277af6bfef46a06518073393f8a3b72', 9, 2, NULL, '[]', 1, '2019-11-13 07:47:14', '2019-11-13 07:47:14', '2020-11-13 07:47:14'),
('bad2c457ede4880594b3f667cb0f1925707e92f07af77900ea85b52f9822550d6252f547c59937e5', 41, 2, NULL, '[]', 1, '2019-11-08 11:32:20', '2019-11-08 11:32:20', '2020-11-08 11:32:20'),
('bad82d6428d0556953c3a200e3b03dad6015785494224dfa97146e7c46774df14882251197ce7185', 9, 2, NULL, '[]', 0, '2019-11-14 23:28:16', '2019-11-14 23:28:16', '2020-11-14 23:28:16'),
('bae3318ba2c89269b715d3d2cec63bea67025875158774f7bf1ed0341a4e02a47c10bb85e4b16336', 30, 2, NULL, '[]', 1, '2019-10-31 11:06:38', '2019-10-31 11:06:38', '2020-10-31 11:06:38'),
('bb5598fe0d2e60cf3fde0f3056ce81370c0ea16ed007c3a0b4a3458356f26a0592c836e8ef4ad584', 21, 2, NULL, '[]', 1, '2019-11-04 07:29:04', '2019-11-04 07:29:04', '2020-11-04 07:29:04'),
('bbcf452596c8997b37282c846b9cc638c5133326e58049ea466147474693ae47be514af72169cee7', 117, 2, NULL, '[]', 0, '2019-11-21 07:27:17', '2019-11-21 07:27:17', '2020-11-21 07:27:17'),
('bbf1308b47d11d59bedb02115f4e477af2d5cdd9ff75990cd59bda83061ee51009120073df2be1b4', 12, 2, NULL, '[]', 0, '2019-11-01 08:15:16', '2019-11-01 08:15:16', '2020-11-01 08:15:16'),
('bc0372c9db4631d586d4fb56c76b55a0908048e5e91458a4b8c5e40a9e450381483328e3b9628c2e', 21, 2, NULL, '[]', 0, '2019-10-22 13:54:48', '2019-10-22 13:54:48', '2020-10-22 13:54:48'),
('bc9a62e07f6fa5c5752920591d8db3ac18af87b5c741e1c95ffe5cb225396957d781deec63dd7f97', 21, 2, NULL, '[]', 1, '2019-10-31 11:56:52', '2019-10-31 11:56:52', '2020-10-31 11:56:52'),
('bc9ba6433fd61232a907bff13e0fa110f761cf32f9b6dda1e3df0c47dbc17adadc2ae4b2a21a7f22', 87, 2, NULL, '[]', 0, '2019-11-20 06:20:08', '2019-11-20 06:20:08', '2020-11-20 06:20:08'),
('bd3d0b30a26577945a8c8ce47b4afe4688374da57fc568673545aef45afe1d72adbbbd1bd7fc4f28', 21, 2, NULL, '[]', 0, '2019-11-12 09:19:44', '2019-11-12 09:19:44', '2020-11-12 09:19:44'),
('be004b01040a80a1fd2c53a014e4984ecfe0d323909e59e656fe19c7b559fee8763e4de37d5fe28e', 59, 2, NULL, '[]', 0, '2019-11-19 13:53:23', '2019-11-19 13:53:23', '2020-11-19 13:53:23'),
('be89e54453d821940758db08c000a3916af5d73f39ba518195b868c617e73c758d2be9959da8c9bb', 57, 2, NULL, '[]', 0, '2019-11-19 13:52:21', '2019-11-19 13:52:21', '2020-11-19 13:52:21'),
('beb888152b4da2b01c3176ba3072c0d456addfea4b2f39ac261be57b1220ca6e53f5dfaed1036fde', 93, 2, NULL, '[]', 0, '2019-11-20 07:38:31', '2019-11-20 07:38:31', '2020-11-20 07:38:31'),
('bef6a3de9b0c33023d184b7b3d207b5f0183bbb7698a2711f9dfb511139f1c4fdab49744e3b2a2bd', 17, 2, NULL, '[]', 0, '2019-10-01 13:57:36', '2019-10-01 13:57:36', '2020-10-01 13:57:36'),
('bf4661f48eecf5f2adbb6b1e4a09835e3e0c442e9635c33df924bd8126665d11e357624c877003af', 21, 2, NULL, '[]', 1, '2019-11-12 11:52:09', '2019-11-12 11:52:09', '2020-11-12 11:52:09'),
('bfd68dae47290ed64b082441f2bc99510bd0575606bcedee176dace7af4e8a39b3d9f7c8e74bfdd8', 37, 2, NULL, '[]', 0, '2019-11-13 07:48:38', '2019-11-13 07:48:38', '2020-11-13 07:48:38'),
('c0a352885eb8483ed4cc0169c5d5d61c931eb8c83884d798c972251446ac08504f9dfb615c412619', 92, 2, NULL, '[]', 0, '2019-11-20 06:48:13', '2019-11-20 06:48:13', '2020-11-20 06:48:13'),
('c0bc59f3a9001a11146d9285d163563818e6259584c6d870362c8bc224fa801b863964a5ce2a1d80', 117, 2, NULL, '[]', 0, '2019-11-22 11:00:24', '2019-11-22 11:00:24', '2020-11-22 11:00:24'),
('c0eeef8003ecb11ef19d05d2492c8d8718b492bce155407dbe58812abb8d28f2e9d2b08abca9bcc7', 21, 2, NULL, '[]', 1, '2019-10-22 14:31:45', '2019-10-22 14:31:45', '2020-10-22 14:31:45'),
('c1097d74e3cc62f6d4d56465b0de646325f24dc71413af32b4537b348518e0a5a98f3834e73e4c77', 28, 2, NULL, '[]', 0, '2019-11-13 05:51:32', '2019-11-13 05:51:32', '2020-11-13 05:51:32'),
('c126d3e9511960d0c698d24d0b82f86b05e6800f3aed5dee1f4e8b6996695b931ea8d0528e76381a', 19, 2, NULL, '[]', 0, '2019-11-18 21:10:08', '2019-11-18 21:10:08', '2020-11-18 21:10:08'),
('c200236709099ae45314d15dd0f30eee0a04dd4f435d71836509c55cecc605411d0acb2e39797d41', 117, 2, NULL, '[]', 1, '2019-11-29 07:10:33', '2019-11-29 07:10:33', '2020-11-29 07:10:33'),
('c21f3cb102fb47b8ef9fbe6328039766f95be6270f956b69096a8ba5892f39d9d54604bfd1f8bfb9', 21, 2, NULL, '[]', 0, '2019-10-15 08:49:35', '2019-10-15 08:49:35', '2020-10-15 08:49:35'),
('c271671957e03a36c4a2b6d84f705db19de350fc6f621b71503a9f5f4aeffedc8746de64be748736', 41, 2, NULL, '[]', 1, '2019-11-11 11:51:05', '2019-11-11 11:51:05', '2020-11-11 11:51:05'),
('c2c70afcf4d72ee55a1134fe2a232c7d8c9155e6bb40f584da1444d3d94d058c6bfa0a87ff5946d1', 12, 2, NULL, '[]', 0, '2019-10-30 11:14:23', '2019-10-30 11:14:23', '2020-10-30 11:14:23'),
('c47f2b4cf59b0f8e2de85cc40eef7fefce2d6da1bae61f88a531eded3a9bb3767c49a02498c18a0e', 21, 2, NULL, '[]', 0, '2019-10-14 10:39:10', '2019-10-14 10:39:10', '2020-10-14 10:39:10'),
('c498d1d52b7a0c944a5c0b31e9172ce0d7f09a8c281d0fb604285d6dce4828ace542cfbfaa4511ba', 19, 2, NULL, '[]', 0, '2019-11-18 21:06:38', '2019-11-18 21:06:38', '2020-11-18 21:06:38'),
('c4dccc3165e863bf3538f001626d107780a3dd61b37e7c90c91ecc417b037c443f6a3596b4eb7403', 37, 2, NULL, '[]', 1, '2019-11-11 13:05:48', '2019-11-11 13:05:48', '2020-11-11 13:05:48'),
('c550ffc2e9dc4e05ca402653fc58ffd97fac5954034278af9fd5a80dc618b04ad520fa2bf43d9df9', 46, 2, NULL, '[]', 0, '2019-11-19 09:39:06', '2019-11-19 09:39:06', '2020-11-19 09:39:06'),
('c56efa1401e7bd654da2668b6ab62599db72d7484c83d0b706f05100185560a6e6de5d6fcf93255f', 19, 2, NULL, '[]', 0, '2019-11-18 21:12:41', '2019-11-18 21:12:41', '2020-11-18 21:12:41'),
('c598279cb92d5e942b8b59244f51d49570e74ce2f46df9afc78ffa618c79576329fe68849d4bdda9', 21, 2, NULL, '[]', 0, '2019-10-14 10:47:49', '2019-10-14 10:47:49', '2020-10-14 10:47:49'),
('c5d9f2e3cb29d51684bc26181453b07638bdc2a64639ff50b46e2839f8a9cb2e15d81f9e29755929', 25, 2, NULL, '[]', 0, '2019-10-24 18:48:16', '2019-10-24 18:48:16', '2020-10-24 18:48:16'),
('c5e2175c1f4d5ba3f9e9893f3023558754edd14af865030b328256a2a9df719d900444daf6374082', 25, 2, NULL, '[]', 0, '2019-11-12 06:59:07', '2019-11-12 06:59:07', '2020-11-12 06:59:07'),
('c5e6673d7079d92b7d5d978f47d9c20af6b5e8ee0ea2c917e2037904787378b0e20ae80c3e9d998e', 21, 2, NULL, '[]', 1, '2019-11-05 07:42:49', '2019-11-05 07:42:49', '2020-11-05 07:42:49'),
('c99f9c53953770d54fc54d5614fb03731a5645c0a62f494d2d21bb2ca6adc981f3afcb8e450f5851', 21, 2, NULL, '[]', 0, '2019-10-09 09:59:18', '2019-10-09 09:59:18', '2020-10-09 09:59:18'),
('ca36cf8708ac96d3a7e6e1c96a48316d7f1fdd7a076501e28c81746a5da7eb54af02269a32c0a6bc', 36, 2, NULL, '[]', 0, '2019-11-11 16:33:31', '2019-11-11 16:33:31', '2020-11-11 16:33:31'),
('ca69b62cb787da504acc84a2ddf7ccb0a8a00327371bc38c0c9df715a7528a4f38dfce73fa479de0', 19, 2, NULL, '[]', 0, '2019-11-13 05:23:57', '2019-11-13 05:23:57', '2020-11-13 05:23:57'),
('cafa351e7a6b01cd7d37c9d5d174b30eea7bc9e90c1bc1cda9ce0ce46afe62da223d8b895aa4ca85', 46, 2, NULL, '[]', 0, '2019-11-18 13:13:22', '2019-11-18 13:13:22', '2020-11-18 13:13:22'),
('cb0c9551d3d996ea2648fe7fc9271d21d3370456543e426a0a9c76871e83c922a1b9e9825d11840f', 30, 2, NULL, '[]', 0, '2019-10-31 11:05:21', '2019-10-31 11:05:21', '2020-10-31 11:05:21'),
('cb1dbb37639dd3b4c43e99f0f829b98eb833ec86e514edad292aa5e33762d111305962dfa0883b5f', 12, 2, NULL, '[]', 0, '2019-10-31 12:05:20', '2019-10-31 12:05:20', '2020-10-31 12:05:20'),
('cc1d2de08755c1378e71260b0bb2b84512e946b941f2a6fee455d92af92d779a9e49ee101a798442', 9, 2, NULL, '[]', 1, '2019-11-11 12:34:25', '2019-11-11 12:34:25', '2020-11-11 12:34:25'),
('cc2de17249b70196b27226a936ab7ed3bb2d9830b4758d2b72fd98de0acb18aaf34541d3d343fdb1', 9, 2, NULL, '[]', 0, '2019-11-11 16:10:53', '2019-11-11 16:10:53', '2020-11-11 16:10:53'),
('cc2fef392a5f04d6b6551008ff563372748a7f5cfc7a1938c61c757f8c373fcf464d70f58a1d6675', 35, 2, NULL, '[]', 0, '2019-11-05 14:44:03', '2019-11-05 14:44:03', '2020-11-05 14:44:03'),
('cd41736752cfd11c3d86ad55c3ef23a6040c9b40ced2498602d4688b51db39fd4d7a7e945726b920', 21, 2, NULL, '[]', 0, '2019-10-14 10:20:03', '2019-10-14 10:20:03', '2020-10-14 10:20:03'),
('cd58143c56a56730e957fd7efcead3591579846b705ef75bdecfa37030ad6e907710ecf03029fe23', 21, 2, NULL, '[]', 0, '2019-11-01 13:29:34', '2019-11-01 13:29:34', '2020-11-01 13:29:34'),
('cda0dfc1a43adad1708d33600206ff3871e997edd9abfd80497ace61f69a91684be434df52cda513', 12, 2, NULL, '[]', 0, '2019-10-23 12:33:53', '2019-10-23 12:33:53', '2020-10-23 12:33:53'),
('ce0aedfb1926bf10fff8800a46a876860cad5c4a197a447e99ed8023322ce8927b2d14f52094b4c0', 19, 2, NULL, '[]', 0, '2019-10-01 16:02:09', '2019-10-01 16:02:09', '2020-10-01 16:02:09'),
('ce0d583eaee584353c88da0667a92373c2f8fdc1e35d3931b7d124d813979e68e78644c3141157bb', 21, 2, NULL, '[]', 0, '2019-10-21 14:31:41', '2019-10-21 14:31:41', '2020-10-21 14:31:41'),
('ce4f7e2d9e05fd0bedde7fdf0f6c470645a5cb263d2e5c060b21bd2ed5cb3c1d7e75c32fe4b29bd0', 42, 2, NULL, '[]', 1, '2019-11-08 11:40:58', '2019-11-08 11:40:58', '2020-11-08 11:40:58'),
('ce505a2c4158b7eecb2f8fca374a405bd362faf87d2ba7f1f73e519c76498245ae4198ef5a9c4b47', 19, 2, NULL, '[]', 0, '2019-11-12 08:56:04', '2019-11-12 08:56:04', '2020-11-12 08:56:04'),
('ce94faa8edd0536697b235044444a3dcf22eb97a8e528c34a7942a1d1135489ad547282482a8282b', 12, 2, NULL, '[]', 0, '2019-10-30 11:04:19', '2019-10-30 11:04:19', '2020-10-30 11:04:19'),
('cf0eb60e2ebff5268ee2e79b26eb5d4273026d6a36e135b26242164799cd1cbb3b10cb96dea35238', 20, 2, NULL, '[]', 0, '2019-11-15 11:21:36', '2019-11-15 11:21:36', '2020-11-15 11:21:36'),
('cf2cbe7b6da7d5bde6c7c13e776b77d1ddc8a5f1c1d220a102eed55dfee84325a16365631952e23f', 7, 2, NULL, '[]', 0, '2019-10-01 09:21:56', '2019-10-01 09:21:56', '2020-10-01 09:21:56'),
('cf557c17671e5084cc7f67859ddeae575f82447b3648e71c4d84a1d54b60fdcb91c93db358fb5736', 12, 2, NULL, '[]', 0, '2019-10-01 12:07:43', '2019-10-01 12:07:43', '2020-10-01 12:07:43'),
('d041fe296df907a70fefa2ceae50d3b077a7e969a358accd0d739893a379c910e1b53a8fd1ebda1d', 9, 2, NULL, '[]', 0, '2019-10-31 06:17:45', '2019-10-31 06:17:45', '2020-10-31 06:17:45'),
('d0e9d1599d6dae25d03806e2db1ec3ffbc813baa371ab03680b9ed7a5775f9619b1daf069b60932e', 77, 2, NULL, '[]', 0, '2019-11-20 02:11:08', '2019-11-20 02:11:08', '2020-11-20 02:11:08'),
('d120a50a6352b878a7834439eaa5fe66b5bb906bd209e3683e76a7604b5a6130742c79a8e729c8b6', 21, 2, NULL, '[]', 0, '2019-10-14 11:06:04', '2019-10-14 11:06:04', '2020-10-14 11:06:04'),
('d121af3f3f5a8c3a82ffa6f1322c115e4bb195b3e8331a3f9dcf2edda6d037e9e7bb13d61149494d', 41, 2, NULL, '[]', 0, '2019-11-12 07:50:16', '2019-11-12 07:50:16', '2020-11-12 07:50:16'),
('d1bc9cfc02c3b95e919459ce9e0cf1bd7dc324b011ef9958a8e75c788164d57584a6be82c702d55d', 19, 2, NULL, '[]', 0, '2019-11-20 13:36:34', '2019-11-20 13:36:34', '2020-11-20 13:36:34'),
('d1fdb10aa0f95e116a1e9b1c6c87b4522e469b5a787c15b557e0681e180de4c0eeb6b25fb3828463', 41, 2, NULL, '[]', 0, '2019-11-12 07:08:04', '2019-11-12 07:08:04', '2020-11-12 07:08:04'),
('d20298dd7df56926ab10bd16ffebbe9a72c145f2fbee466dd4e69f0a40e8ae82efc1fede5a9205af', 56, 2, NULL, '[]', 0, '2019-11-19 13:26:15', '2019-11-19 13:26:15', '2020-11-19 13:26:15'),
('d38d33cd4d4a56e89c7ca7d0eae6619c2454318cacb8c81dfd9da88f5e0f2acfeab7560eedf3a30a', 83, 2, NULL, '[]', 0, '2019-11-20 05:27:21', '2019-11-20 05:27:21', '2020-11-20 05:27:21'),
('d39042d97bbc4cd83fda1abcaaaa4262352d50159d09ad09a8446ff3fac35b070893198551bb2d4c', 12, 2, NULL, '[]', 0, '2019-11-18 09:57:39', '2019-11-18 09:57:39', '2020-11-18 09:57:39'),
('d3a75dfd15d7df9a36e8069c6f00f7c43fce6f11982306633d9dff66362fe8d195977f9dec8cd6c0', 21, 2, NULL, '[]', 1, '2019-11-09 09:27:55', '2019-11-09 09:27:55', '2020-11-09 09:27:55'),
('d3df80c95d78e8700af135df30588579f41effac8af7b0f9709db85f96fc06c5346c65c26c39f0ba', 19, 2, NULL, '[]', 0, '2019-11-05 15:45:16', '2019-11-05 15:45:16', '2020-11-05 15:45:16'),
('d3e7f5f768eaf9abbfdd3667cd4875fdd1a6385b7be3488fd82dc0522226c835c4d58528eb1d34eb', 12, 2, NULL, '[]', 0, '2019-11-18 05:39:05', '2019-11-18 05:39:05', '2020-11-18 05:39:05'),
('d467cb83f06efef1793ea452bcf9befed2c6eab6289a2eff4689f284b4f05f93018b6bedb595c24e', 42, 2, NULL, '[]', 1, '2019-11-08 11:41:30', '2019-11-08 11:41:30', '2020-11-08 11:41:30'),
('d56b257f26e24fd566c7e0194645da5ca532baf53516aaef1e4bbff86b916146e07d95f9a74aa2a4', 18, 2, NULL, '[]', 0, '2019-10-01 14:09:06', '2019-10-01 14:09:06', '2020-10-01 14:09:06'),
('d5e95312ddb683e262d41fa4a84703d76b6f81e95efebd13c9c755776bf43e01b376dbc00655450e', 19, 2, NULL, '[]', 0, '2019-11-18 09:55:56', '2019-11-18 09:55:56', '2020-11-18 09:55:56'),
('d647dd125117b6c750d209d721719f416c8bfeb8d9f41d5d0bac45a75fc313503e1f5114d3b803c8', 12, 2, NULL, '[]', 0, '2019-11-11 07:04:17', '2019-11-11 07:04:17', '2020-11-11 07:04:17'),
('d6d0fdd0594bc43f65d7f71250e1dfc176bc8087762d785228892a7b4e3234d69cca0d3958f2265c', 19, 2, NULL, '[]', 0, '2019-11-18 09:08:30', '2019-11-18 09:08:30', '2020-11-18 09:08:30'),
('d6eba2456bd793beb26ba437c0a43463bccfb92cc13c1b3702bcdfd5db2ea1bf936ce8e9de1903cb', 27, 2, NULL, '[]', 0, '2019-10-30 12:32:06', '2019-10-30 12:32:06', '2020-10-30 12:32:06'),
('d705eaee07759437b6fbdb4b123f929b622ebd16bbc618209b0e5a371107df547d2b8d3ce88969be', 21, 2, NULL, '[]', 1, '2019-11-12 05:42:40', '2019-11-12 05:42:40', '2020-11-12 05:42:40'),
('d8507d4cbfc62a8c05c97cf382864a8f68948d2e953820fbd4f83f6b6bf4dc3ca2d8a040dd6b25ca', 21, 2, NULL, '[]', 0, '2019-11-18 06:19:58', '2019-11-18 06:19:58', '2020-11-18 06:19:58'),
('d85295c22637c112a2250a12cfa55b7ff7aef246aff75201dd0a2b38f9b07ab15cacf5448b742fa6', 117, 2, NULL, '[]', 0, '2019-11-28 11:34:03', '2019-11-28 11:34:03', '2020-11-28 11:34:03'),
('d8b482a1ee81c856ece52fc60dadddc9a28a1c703244d51f4fd8dcce2c37ebf2ead96de65d648c5d', 21, 2, NULL, '[]', 1, '2019-10-16 10:02:35', '2019-10-16 10:02:35', '2020-10-16 10:02:35'),
('d90eba03376f58e67f6f519fde117364fb01345a38c628eea619d7cc66b81c467388affa7da46a38', 117, 2, NULL, '[]', 0, '2019-11-21 06:55:55', '2019-11-21 06:55:55', '2020-11-21 06:55:55'),
('d98c1c8907d46dce5b80a5c18a723e0c3e22faba5d7ba9bab714a5bf6c80b4309ddc6fac45a2b6c1', 21, 2, NULL, '[]', 1, '2019-11-11 11:49:49', '2019-11-11 11:49:49', '2020-11-11 11:49:49'),
('d9d26244ea7fc8a85cfb2adb4d44780287040ba9899847648c12ae822610616008844c7bbf778c81', 115, 2, NULL, '[]', 0, '2019-11-20 17:39:52', '2019-11-20 17:39:52', '2020-11-20 17:39:52'),
('da4291865687742b6d34ae1f6e0b3f302199de9d939c94fa9469466408c19ddeb09d2593fd08b5f3', 12, 2, NULL, '[]', 0, '2019-10-23 11:38:35', '2019-10-23 11:38:35', '2020-10-23 11:38:35'),
('db58fb5efcb1863476b9866df5e1a07979e53c9542b47235b05b9fa4642751d11b7d04a9f220edda', 38, 2, NULL, '[]', 1, '2019-11-08 11:05:38', '2019-11-08 11:05:38', '2020-11-08 11:05:38'),
('db731e45b1b1ca217b282c99059a5e393b4e9c3378399a1ea28746caa918c7ee6b420a94cba9dcb7', 55, 2, NULL, '[]', 0, '2019-11-19 21:26:13', '2019-11-19 21:26:13', '2020-11-19 21:26:13'),
('db784637b859e2cefe113cffc6e49cd244d34b103e4630522205ee6fab6105cf3e9b85201ac15c11', 27, 2, NULL, '[]', 1, '2019-11-05 07:49:17', '2019-11-05 07:49:17', '2020-11-05 07:49:17'),
('dba848cc85c9f5452eae2d34bef00ab565d9082d411b96398a36a503a15c467ba90521445e733327', 21, 2, NULL, '[]', 0, '2019-11-01 06:25:22', '2019-11-01 06:25:22', '2020-11-01 06:25:22'),
('dc67e3e24ed35560a04508cc095447997617374badce85032241896ef8dcd26dc47d667e89e40226', 9, 2, NULL, '[]', 0, '2019-10-18 09:38:54', '2019-10-18 09:38:54', '2020-10-18 09:38:54'),
('dc8c34148eafe6c6695d08ce0c96b58c2be6771fea3041231d6c8d216584295024aa1298ffd2a9d8', 37, 2, NULL, '[]', 1, '2019-11-08 11:03:45', '2019-11-08 11:03:45', '2020-11-08 11:03:45'),
('dddc5ecf86a82722daf3bf373c4070c15b433f0c523f31c050566a5dfe18050b91c145f4eee00952', 122, 2, NULL, '[]', 0, '2019-11-21 07:51:54', '2019-11-21 07:51:54', '2020-11-21 07:51:54'),
('de53a1f95a20d724bb2e392f95c0af707aef83f64a44816e483967db7fa39d227cb920ad759cac9e', 12, 2, NULL, '[]', 0, '2019-11-13 10:16:44', '2019-11-13 10:16:44', '2020-11-13 10:16:44'),
('de77e753b169a5c5284d825a76450fcbbd47079966c5774d041f705aef4c34f756ac410ea86a9628', 21, 2, NULL, '[]', 1, '2019-11-08 12:53:42', '2019-11-08 12:53:42', '2020-11-08 12:53:42'),
('deeb4cb8df6db601c32945303b26845ee43b05bf2dfd34e416102d610d72504d67b3e4444a6190f4', 12, 2, NULL, '[]', 0, '2019-10-23 13:19:21', '2019-10-23 13:19:21', '2020-10-23 13:19:21'),
('df113b8ed1bc84e718a7f6397a507fa7beac3ce97cee144434313006205d5402cd074bd02c9bcbe7', 69, 2, NULL, '[]', 0, '2019-11-19 17:27:19', '2019-11-19 17:27:19', '2020-11-19 17:27:19'),
('df1865e72eb1e45a6e4baa05b282bf261a540a8a2864a8c35edc810fe22e91b2c28cc3e273bdd41d', 50, 2, NULL, '[]', 0, '2019-11-19 10:15:49', '2019-11-19 10:15:49', '2020-11-19 10:15:49'),
('df6d9c12fefd249586bc94aedde58c66dc3a6bebce0e20d5be4749d6cbb3e610fad8c69041cb7018', 21, 2, NULL, '[]', 0, '2019-10-15 09:56:53', '2019-10-15 09:56:53', '2020-10-15 09:56:53'),
('df902efca282e76c38c6bf87db6092c225218185ff48a6cccc416455ccc4916b23f88e7b5dfa0134', 117, 2, NULL, '[]', 1, '2019-11-21 11:25:26', '2019-11-21 11:25:26', '2020-11-21 11:25:26'),
('dfe625131482c1721773bbf52f9361193e0aa8688f18256d1fc182451de8af038dc786124f8e09a1', 12, 2, NULL, '[]', 0, '2019-11-18 05:59:52', '2019-11-18 05:59:52', '2020-11-18 05:59:52'),
('e14ecca9754ea23a201f904c6590e5f5c0991e1b3d08a65e3d388705cb6f408967191b8697c6d5ad', 9, 2, NULL, '[]', 1, '2019-11-11 09:25:25', '2019-11-11 09:25:25', '2020-11-11 09:25:25'),
('e1820ee30434b3ae8d360aa9f9028873c112df69986b0e9804ee54f1a8e53bc021c748a3c028f0d9', 25, 2, NULL, '[]', 1, '2019-11-15 05:59:05', '2019-11-15 05:59:05', '2020-11-15 05:59:05'),
('e1956bff7a54daf7d45e8309f16bd6bd8da529ac9ac8e8d606d5793babdd70db12a187b6cee431ad', 9, 2, NULL, '[]', 1, '2019-11-11 07:10:03', '2019-11-11 07:10:03', '2020-11-11 07:10:03'),
('e2bc91d3f71b1567f1302cf283f4795681eda6ed015ad2ff138cd508fdf5ba12e5a66e79220f1f83', 52, 2, NULL, '[]', 0, '2019-11-19 12:47:39', '2019-11-19 12:47:39', '2020-11-19 12:47:39'),
('e3ab708d0058698279df20d2ef3060b65673c5188b19627b533fdb57fe60e1080513d21fadab5fa7', 117, 2, NULL, '[]', 0, '2019-11-29 10:59:02', '2019-11-29 10:59:02', '2020-11-29 10:59:02'),
('e46d61ebebc41b02522fb67daf23d44874dce200100e8b63442ceabd5c1c1199fee0d4ec081db4b7', 21, 2, NULL, '[]', 0, '2019-10-21 06:25:12', '2019-10-21 06:25:12', '2020-10-21 06:25:12'),
('e4b0eea7e909b269a5256be78618390ad497a07db27cb4f53862f2a732aa48d68935499b90d6b0c1', 23, 2, NULL, '[]', 0, '2019-10-23 09:52:21', '2019-10-23 09:52:21', '2020-10-23 09:52:21'),
('e4edda0edd9b241c068ce46f37afc258d368fe2a4ccf1b4f18a379870d4f02f7cba25fc028e39b61', 89, 2, NULL, '[]', 0, '2019-11-20 06:38:18', '2019-11-20 06:38:18', '2020-11-20 06:38:18'),
('e7a8bf1b2d03b03883fdfe96d8992ab1230eafaa280dbc0b35809be9e0b5b8592b631d04a8c558fd', 25, 2, NULL, '[]', 1, '2019-11-12 09:42:26', '2019-11-12 09:42:26', '2020-11-12 09:42:26'),
('ea862316488ec14847f62e7b74d087765e1eadc2d3c85a5a6d29dfaff304daf3b7d63d548a22f2e9', 21, 2, NULL, '[]', 0, '2019-11-18 06:18:01', '2019-11-18 06:18:01', '2020-11-18 06:18:01'),
('eaaed082d75f4728804cf9a85c9eb086c0939b9fe7a7763b8cc430b7c2981bfc2198e46d1c72161e', 12, 2, NULL, '[]', 1, '2019-11-14 14:09:42', '2019-11-14 14:09:42', '2020-11-14 14:09:42'),
('eb566f8a9312794e015b24e33a5fea92fead814b0d787f01be79d548d390e8a82b89b109dcbcedb8', 12, 2, NULL, '[]', 1, '2019-11-14 07:41:42', '2019-11-14 07:41:42', '2020-11-14 07:41:42'),
('ebc0d059cb3048b9303955673d951d106214eaee8f292512cf5a08932f1a36b08ad074a456a7a843', 25, 2, NULL, '[]', 1, '2019-11-11 12:44:19', '2019-11-11 12:44:19', '2020-11-11 12:44:19'),
('ebf2a4bd51a624f3cccf007ffa269357c1398f0df99add8c597bd0cfb42cbe09bd071ab1eb82903e', 12, 2, NULL, '[]', 0, '2019-10-22 13:22:50', '2019-10-22 13:22:50', '2020-10-22 13:22:50'),
('ece6d386a83502b1b96dced418a6c533af6c64aad084df76d171ec3e069cd08063e6d9638d0776cc', 116, 2, NULL, '[]', 0, '2019-11-20 18:14:09', '2019-11-20 18:14:09', '2020-11-20 18:14:09'),
('ed0c9940a555021cea40b95268aaae83c87129c1c2e409db605c5ef0fb4e319379ebbb107ab8d5bd', 12, 2, NULL, '[]', 0, '2019-11-11 09:20:09', '2019-11-11 09:20:09', '2020-11-11 09:20:09'),
('ed6997a7e1c11302ceeff623da045f6fa4c5f97922163c39825f238316f2d5fc5a420758c6c4af51', 25, 2, NULL, '[]', 0, '2019-10-30 10:23:25', '2019-10-30 10:23:25', '2020-10-30 10:23:25'),
('ed766813affeb035af017d27a79b84f0e4e01464f9c028ffdd6039d138b07c81ef15e9b077936fc3', 63, 2, NULL, '[]', 0, '2019-11-19 14:48:21', '2019-11-19 14:48:21', '2020-11-19 14:48:21'),
('edcc93e5a8967d2a7d98006f88405c526c243047d0842111ad13db91c5dea4794b786e4d1ea15bf5', 21, 2, NULL, '[]', 0, '2019-10-25 09:28:25', '2019-10-25 09:28:25', '2020-10-25 09:28:25'),
('ee14bafb000ca09dab2a008924d2494bef69da8c30fa05613da9be623278444c198aec007f0ade5d', 60, 2, NULL, '[]', 0, '2019-11-19 13:53:50', '2019-11-19 13:53:50', '2020-11-19 13:53:50'),
('ee96e8cd81c33e7623f68941bf73e90fb5b2d426de88796d4652aa02b86406e39391d8dabd0cf7e8', 65, 2, NULL, '[]', 0, '2019-11-19 14:58:25', '2019-11-19 14:58:25', '2020-11-19 14:58:25'),
('ef2f873bacf9cf5738eedcee1148bcc6faea85986f78175464c2a932c9a79df5cf9262d71032e4e0', 114, 2, NULL, '[]', 0, '2019-11-21 01:17:05', '2019-11-21 01:17:05', '2020-11-21 01:17:05'),
('efd99e87b8881830b96797d1db90b8a8711797cfc536885fec3c85dbc2c6ac8324f397d4c65d83c6', 25, 2, NULL, '[]', 0, '2019-11-12 07:03:03', '2019-11-12 07:03:03', '2020-11-12 07:03:03'),
('f03f159f6f70c003b61623ab8734dd4a141bc3de4274142cd8910da89cdd86edee32dac57cb533e3', 117, 2, NULL, '[]', 0, '2019-11-28 11:25:18', '2019-11-28 11:25:18', '2020-11-28 11:25:18'),
('f09efee43e4f044aed68d3a0bd0c36f303834976dd47da5a4a6f48aaab98d01ae719b89c9608d57b', 21, 2, NULL, '[]', 0, '2019-11-17 02:10:44', '2019-11-17 02:10:44', '2020-11-17 02:10:44'),
('f1128cd400a28d8507d6fe95c0979b31cd7587285783fd9e0c0a74805d895132bb35050f805998e3', 21, 2, NULL, '[]', 0, '2019-10-09 13:45:50', '2019-10-09 13:45:50', '2020-10-09 13:45:50'),
('f150af52eecf19ae92b3435483b72436cd2e4033ddcbaa977b164aedc66aa86798789c3fd5c50d69', 120, 2, NULL, '[]', 1, '2019-11-21 07:35:25', '2019-11-21 07:35:25', '2020-11-21 07:35:25'),
('f19c4f853d6964fb7f7b2457ad7a739007491c37df463209cc88bcca568a1ae2ef2835e89f5917aa', 9, 2, NULL, '[]', 0, '2019-10-01 09:49:10', '2019-10-01 09:49:10', '2020-10-01 09:49:10'),
('f26f1a323779386712bd0d601d82eec14ff4e8fedc66d4d1af08553ed4c5a132f2b994be33d99ae1', 102, 2, NULL, '[]', 0, '2019-11-20 13:31:05', '2019-11-20 13:31:05', '2020-11-20 13:31:05'),
('f2754cad4b12f63e22e917b92534edae11dc85c01dff2fd2ceb663b608e912cc6c6bfd577e65fb9c', 9, 2, NULL, '[]', 0, '2019-10-22 15:19:36', '2019-10-22 15:19:36', '2020-10-22 15:19:36'),
('f2c51323c4b928ac7e843ae01942e226809110a5441f6b7bc91eed8c5e7022f9490aa606f312bcb9', 21, 2, NULL, '[]', 1, '2019-11-01 13:18:13', '2019-11-01 13:18:13', '2020-11-01 13:18:13'),
('f393b2eb1139df68a0afac8977704903ad74cab30dcc700b685485b169bbcaae00609e2ff78aa1bb', 37, 2, NULL, '[]', 1, '2019-11-11 16:31:51', '2019-11-11 16:31:51', '2020-11-11 16:31:51'),
('f3b7a615dd2409e11c32e5638b6736e33ed6eafffd58026e8c529db86d91721d887fcdf3a2180f46', 12, 2, NULL, '[]', 0, '2019-10-23 07:07:03', '2019-10-23 07:07:03', '2020-10-23 07:07:03'),
('f4a82ea453e5f9decfd43b871831c49283f82404b8fec1794fa40171207b004e073cbf22651bcaf9', 12, 2, NULL, '[]', 1, '2019-10-31 05:30:36', '2019-10-31 05:30:36', '2020-10-31 05:30:36'),
('f4fed4ea26824d913a1420e65cafd108a9f49138b08a826138e499c2f0ab3d364136d02662eb20bb', 12, 2, NULL, '[]', 0, '2019-10-23 11:10:12', '2019-10-23 11:10:12', '2020-10-23 11:10:12'),
('f55418054229cfaae54bdde52b0e049df1520e2153ec4b9739ad4c950f34cbe0ed021018e9f9f2c2', 40, 2, NULL, '[]', 1, '2019-11-08 11:16:49', '2019-11-08 11:16:49', '2020-11-08 11:16:49'),
('f6afa1bfa61ac019112af2605b38ccc4be85d83e009c7029ecb1dcaebea201156c6c49c93afd19b9', 12, 2, NULL, '[]', 0, '2019-11-01 05:20:45', '2019-11-01 05:20:45', '2020-11-01 05:20:45'),
('f77662fa5fd14390bbe737f81c8cf423d59c69f5482bd2f5bc8fd7c87282289d0390003b7fdbe62a', 12, 2, NULL, '[]', 1, '2019-11-06 08:12:24', '2019-11-06 08:12:24', '2020-11-06 08:12:24'),
('f7a0659043e310af51e0a8b6d468ecb71346fd6bcbabec738f7b96cb87b1a4235fc7b8f49024518a', 19, 2, NULL, '[]', 0, '2019-11-18 21:18:02', '2019-11-18 21:18:02', '2020-11-18 21:18:02'),
('f7b3e4d910c55480f1b1ea861341c8dc040137062219049cb412aae069c83843ca8b3751e05e57df', 19, 2, NULL, '[]', 0, '2019-10-24 05:16:27', '2019-10-24 05:16:27', '2020-10-24 05:16:27'),
('f8aade1793cfef65f38fdc2f625545acdc4b07cf5ea9ba01a3069c02702273c300d1e39438290c5f', 9, 2, NULL, '[]', 1, '2019-11-11 13:57:18', '2019-11-11 13:57:18', '2020-11-11 13:57:18'),
('f93b9d8e24e3e8429947e194d88e1b47af6af38b73f5ee453703bea6944253319bcf84c468a3b6d4', 111, 2, NULL, '[]', 0, '2019-11-20 15:55:49', '2019-11-20 15:55:49', '2020-11-20 15:55:49'),
('f993c206df4c583b24deaafb3386ba9982354c283b9ce855cfaf2bea8e41c3dcffb4d1a16f5d9d0d', 12, 2, NULL, '[]', 0, '2019-11-05 06:23:53', '2019-11-05 06:23:53', '2020-11-05 06:23:53'),
('fa97893688e3fd938a414b96cb9263eb60bed81fa98916a5dc274122d5ff0a5fc45e75c266c18926', 21, 2, NULL, '[]', 0, '2019-10-10 08:40:10', '2019-10-10 08:40:10', '2020-10-10 08:40:10'),
('fb36aa46da6d7e420c7e07fb3f21ed575e57beaafea2c624194d0bdbe6cde11d05b1f8bf6b52e05f', 21, 2, NULL, '[]', 1, '2019-11-11 11:46:54', '2019-11-11 11:46:54', '2020-11-11 11:46:54'),
('fbf2fa93f94dba0703955404f43e75c34fb575c4fb5eb5df87e81566187f046ac16156d1d2423099', 21, 2, NULL, '[]', 1, '2019-11-08 11:59:45', '2019-11-08 11:59:45', '2020-11-08 11:59:45'),
('fc10b86497c4375ab7fef6f334511142aff81457385fb73bdbd96c23fbf203e084c67bb4c57eddc6', 9, 2, NULL, '[]', 1, '2019-11-15 06:04:09', '2019-11-15 06:04:09', '2020-11-15 06:04:09'),
('fcd4fe61e18642543b7a95eb4bd748ade84a72afda49d012c9d38ec9cea707c716e102ed717a8c84', 9, 2, NULL, '[]', 1, '2019-11-14 23:27:48', '2019-11-14 23:27:48', '2020-11-14 23:27:48'),
('fd10e4c1bd358ed83f1b2cfa5f28dd5b7fda28fba5f3ec4aea88befb6dcabd2784f3aa511e415a88', 117, 2, NULL, '[]', 0, '2019-11-21 05:54:21', '2019-11-21 05:54:21', '2020-11-21 05:54:21'),
('fe11aaad932c986f7c32019a876628ff370dca95cea1ca7e53139e78fdbb2a6e654ca025af797d3a', 12, 2, NULL, '[]', 1, '2019-11-15 08:54:46', '2019-11-15 08:54:46', '2020-11-15 08:54:46'),
('fe3f7e632eb596eba7702481c0b3f192577a662374e5c0ebe0e70a21e0b7f9b2b7536428147938fd', 25, 2, NULL, '[]', 0, '2019-11-13 10:13:35', '2019-11-13 10:13:35', '2020-11-13 10:13:35'),
('fe478bb6d7006d62dc174fec95996daa439b71aef5fee2df212e251c8493236a3524ea7ff8fe58e2', 29, 2, NULL, '[]', 0, '2019-10-31 09:10:56', '2019-10-31 09:10:56', '2020-10-31 09:10:56'),
('fe88c1adb63be8c466f18f801c2d51826c5c7bc2c2ef91119b5fa17b25088c331cff7d9bf8abe96c', 12, 2, NULL, '[]', 0, '2019-11-08 11:54:07', '2019-11-08 11:54:07', '2020-11-08 11:54:07');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'cCkbv8Zxl3dKeAPladazu337UiCHnPEWGqsMyWm5', 'http://localhost', 1, 0, 0, '2019-10-01 09:20:32', '2019-10-01 09:20:32'),
(2, NULL, 'Laravel Password Grant Client', 'Ug5VidAEe6J4fVtNcfCaOLX3Jmmsi7yIAsdJWjk0', 'http://localhost', 0, 1, 0, '2019-10-01 09:20:32', '2019-10-01 09:20:32');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-09-28 01:52:51', '2019-09-28 01:52:51'),
(2, 3, '2019-10-01 09:20:02', '2019-10-01 09:20:02'),
(3, 1, '2019-10-01 09:20:32', '2019-10-01 09:20:32');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('0039b33b0ed49a70763b3ef442f78ce6678709353056d338620102f0e897ba94c77e1251e96a73a3', '7c490ece9aa8fbb86ef31829d679e463dbf71476a9efc2147a5b52daf61e26c77c7d1d786cd90e2d', 0, '2020-11-21 07:44:02'),
('0052616342bb942ecbb847ba3cec9c244cd878fda2ae87f6d4518b6779f1106f0d996937e6584c35', '66d074ac248e5954fe7ab8ecaf23328bcfe0a078ef89870ea3a8c325231d296b09704faac57bc988', 0, '2020-11-21 07:31:47'),
('008954ccb5cb54833194ec395f754054e37ea0570024e92e5a19937f3882fcf93baece9b80532962', '192689ce777b50d0ddd3cc0d4b47ba12374fabf5be1cbbf115c201ecb67a8f7048f060cefd12992f', 0, '2020-11-19 12:57:43'),
('00d9d8fee02179051c25a24a4302827ee02e7c69ba135b28472cbd15229c4d6cf756f7cecdb62a85', 'ca69b62cb787da504acc84a2ddf7ccb0a8a00327371bc38c0c9df715a7528a4f38dfce73fa479de0', 0, '2020-11-13 05:23:57'),
('00e04b5ecab0771236a0ccb50792773a0fbbee31e5bacba352c813d271f7563c582d580c5d27d822', '55e828fc2984cd795c9847a73d5eea87ebc9939b4a549e952f74d5872ecdbd6db7a02ace325abe8d', 0, '2020-11-04 14:51:10'),
('012fc2c928543d352f57ccca8531a2a02beb5f77b58bfc59dd790b6a0e6819c6a701c8618bc2dc59', '35cb269dfa46dcad0b3fb66b5205e1dd8353f2e1c8461a54321785c97211823341d4b1aac336efee', 0, '2020-10-21 06:22:42'),
('0229f9374677300459d91ecb5bca672d9a19027dd7caabee93b8e149cc0621677b534926a6d80e57', '53c0e6843009e8fac0add96b0ec87c94a6e61818cb55b0fb329f61498003ce8820cbd51008f8ce21', 0, '2020-10-30 09:39:50'),
('025c2c617046a7550473d5924f89bbc70b03ca7b7053c29f9f619e13a9afc5390f69093eb7f803b3', '531300fd51455860edb9d0b09767a0ab4f030136f00b84e2582354778a0d0591d9339b0b606d0924', 0, '2020-11-18 10:31:13'),
('0370a8f828c1290abf1167bf7317458b1d98ae4ada62800c16f519f19706d6942df5a6c7a4016a8a', 'd647dd125117b6c750d209d721719f416c8bfeb8d9f41d5d0bac45a75fc313503e1f5114d3b803c8', 0, '2020-11-11 07:04:17'),
('048975ef2e6714686b1515b9af05a926f47ef6c120839c47d8419345cb5055db3ab2cec3cc14ff7c', 'dc67e3e24ed35560a04508cc095447997617374badce85032241896ef8dcd26dc47d667e89e40226', 0, '2020-10-18 09:38:54'),
('0558b4244602dad31db16b0ceabadf2c6de0018f1a8f37056578531f828ce2755a37a932d64bfa1e', 'c126d3e9511960d0c698d24d0b82f86b05e6800f3aed5dee1f4e8b6996695b931ea8d0528e76381a', 0, '2020-11-18 21:10:08'),
('06720ee6409855e5242451cd0d648da6550d32583054a2ba60501674751a09050311bd94322e8222', '6ac7c38773d3ba48f63aac07bebd30cf73098c696853e24d0f2aa18e579c8e4f3b539c5884221198', 0, '2020-10-11 09:11:52'),
('06cf8d851c4fdb85cc8818652d459b0ed175949e095e51b44e89ff3fe5c333e0890d22677a23dff0', '15ec92243eed48f71c6f72aee32e2933307698eb4be51a35288981d605e223685f59decd9449316c', 0, '2020-10-01 12:19:02'),
('06e8b46dc3a46647d39633a157458a2309e275dbe5b25c857747e983d4b9919cae8fb500d3653ed1', '54adea90ba9ca68daa86e4b9e393cd9a5218b7f4017b3e2a4d1de30e3eb107f79f3a0f4f2817cf66', 0, '2020-11-20 17:35:52'),
('0934f11a165230305f27ca6e5e3903ce4357a7faffa8b5b4c23da115154b3ddc6ef4c1aac229b1f2', '66f691fee5b5b0c680abed273fd677143b25c017d2844a5164f6f1021e9d4c408499ce2dba1efa98', 0, '2020-11-12 07:15:39'),
('09b939a83fae6936060b982e1f098da0d61cea1df0525484bcf493a3004736bb42afd3dfc1862268', '6372b4867f39f5e916449ef929f88c628da21cc9c76bcfb81b8f4e06b0a3c1886136a000b14c84b8', 0, '2020-10-25 06:45:43'),
('09c935cd723a9b72d52872d763b153d369d8ef3d02cc1da04c7c66e27a822a6183333a5b0cb7b51a', 'f1128cd400a28d8507d6fe95c0979b31cd7587285783fd9e0c0a74805d895132bb35050f805998e3', 0, '2020-10-09 13:45:50'),
('0bb6ebb281b76f8081828ff803c0fa007afff10913348861ffd1bb19c833c6d4012924f46db4cb3d', '860c56b39341613c55ba31962db3f21a2ddc183c251fd131b361b2a245f964c1897e057d223f8304', 0, '2020-11-04 12:04:55'),
('0bd69433b7061251649d55940058aa8f1e82fd54a971c21e51564642f55d1f97e7d9539c7e48e8f2', '7d6f75ea1de90d7ca9de5ba92bb31110aca9d00af346e8acdddccec773a438a00a4e2978003f9953', 0, '2020-11-04 09:00:47'),
('0bdfc9470b231744edef4d98f7da4f8cd4cb52262e21549c59c6cdd4e6fa0a973b57d8d5d694cc4d', '404101efcfac92bfad431cde5d9a0f3857c741891846bfc4300b45959b1580a00f92edae154e0fa6', 0, '2020-11-21 07:09:30'),
('0be413694b8a7b65ed84c6ac3f5c46c85c782305e672510b60feb082406c0089990e77da08811d42', 'dba848cc85c9f5452eae2d34bef00ab565d9082d411b96398a36a503a15c467ba90521445e733327', 0, '2020-11-01 06:25:22'),
('0c0ed70adcc5184a1349ba7f48639b4b1560fc179a1ee27fd73d265517fc8825be9880a9bb0f319f', '5364e4897bafa6ed7486dd412492d93df8fe3b5e1784dbaac900192454519fe2f9333a4cb77df1f2', 0, '2020-09-28 08:09:47'),
('0c61c4af1eb504ad96e11a581d491c058c532e1fd18b7c0e3321e250bf544890f85af25e9e3f6882', 'ef2f873bacf9cf5738eedcee1148bcc6faea85986f78175464c2a932c9a79df5cf9262d71032e4e0', 0, '2020-11-21 01:17:05'),
('0cc70fc9053655e03157385b7516e6ca23d49e24d231128456c8d16f097970274fe9818e67dae679', '19adfd96af66311f12a38747640ea649b687b124a8b55966a06e51adf5e2c8500a93d39ba8de00fe', 0, '2020-11-19 09:00:12'),
('0d2432d52119094a33a6d421d65fe54e969cd623c2b874ce2fc6fcde767ce1ea18c041e7bbf42a85', 'bbcf452596c8997b37282c846b9cc638c5133326e58049ea466147474693ae47be514af72169cee7', 0, '2020-11-21 07:27:17'),
('0d7d4aff74f3764b6b576d18cbb0f7ad0f7dde5c17e41ee927307a0079371fdc353b9b1d4fcf75d8', '01922fdcc811b691db656ff71007def7874e5c018e5944cf790d61a380804f3f77d11e02dfbee45f', 0, '2020-11-11 12:18:38'),
('0e1a0a75cf8b7c028c6a4c912f9e92c35159b2b02dfee52ae3dbcafc75575fc50ff4f82f26437092', 'be004b01040a80a1fd2c53a014e4984ecfe0d323909e59e656fe19c7b559fee8763e4de37d5fe28e', 0, '2020-11-19 13:53:23'),
('0e52544c2ccbc2e485184085e26ade459df244739dd584ffe8063a889c178b1ec419e5b051e8543d', '2ca64b9cbefd73827cecc7e280fffb344e6a1e7e1be685b37a30b0a335c0c77a6807a95e6778e797', 0, '2020-11-14 23:27:18'),
('0ebeac5285caa0c6bf75530af2f23526a47d60c9005c534207777379a49f81a012ab7704544eae66', 'de77e753b169a5c5284d825a76450fcbbd47079966c5774d041f705aef4c34f756ac410ea86a9628', 0, '2020-11-08 12:53:42'),
('0efe0e4bf7494ffdbafa968b9f03e76c1a90fb5bf6f26a5f069615b6799a8d7c27aaee24e930187c', 'a2d62614cc3d26fb81eb3cb605b98300c3bb34351ee5479291cf2ea0fd5cba075dc49469d439ddd7', 0, '2020-10-14 11:43:32'),
('0fc197aa9da2afa596a67328664c98b94d089f99b3ff8b25d3a01ee6dc1ea8ae6985dbb7b10f1fa7', 'c47f2b4cf59b0f8e2de85cc40eef7fefce2d6da1bae61f88a531eded3a9bb3767c49a02498c18a0e', 0, '2020-10-14 10:39:10'),
('1055b5ffe213f26e956b987d3d86142b20aeab93d5268b4fcf037b2ca1a8d8eca4fd57292dc19abc', '48161fb6f3dd7c377f709aa1ba13b92a80b422b8f14e4b3e9a7525c4439e9d33279b95aef5e48822', 0, '2020-10-24 06:15:53'),
('10686f2680498c050d85728b95f4364036c522e73c54850636431354e703338fe5874f1ada6f94f6', '84ddf71501b1387ffa3ba3b6bf300a78608e2e0e4e8e741063658e6c74919255e612cd889f011c25', 0, '2020-11-01 13:17:20'),
('10eb5488ba1e2fc53b5c3d747d340523c7b4748cb2db12c99649466def9f2b71781b0a008c5e4d4a', '02a497fa0c47b53276aac6245f9129c1a74ada8000d30652336d6a97117bed873c45363b9a869871', 0, '2020-11-08 09:36:29'),
('11be9c36d2ccf9583d88e22858794d0f35a342035afedc57102507041dabffe4e71719a13b3a8d0f', 'f150af52eecf19ae92b3435483b72436cd2e4033ddcbaa977b164aedc66aa86798789c3fd5c50d69', 0, '2020-11-21 07:35:25'),
('11e204bf48ceb424e38a917d8f849005abcf6ac781b5322017618b45076fef99730b69b8f686f842', 'db784637b859e2cefe113cffc6e49cd244d34b103e4630522205ee6fab6105cf3e9b85201ac15c11', 0, '2020-11-05 07:49:17'),
('11fa6b857303db157e467fc00fb808a2e7a0f0f524874e29ecd60df7c59e25f2a5705aa85b977b57', '94b421242b03af73d934c5e7c6d5fc913a8423ea334302c90e49f7a43812257cbf6f8b4a731f3715', 0, '2020-11-21 07:46:23'),
('12193e03d5b1fa6b395187f6eed59d3f98d81c1e1e1091021affdc774fe88cb180790f31c7bc5cd9', 'a2c9311d0b4e722109cc190692c9ab5ee537b52bfebcff676056454c09280993e6d162e8439e5d19', 0, '2020-11-19 14:37:52'),
('12a638775d7dec9325bcaa070adc97cf3889ff88ab6d0825532705969b49e4815f7173c40fd22f8b', 'ce0aedfb1926bf10fff8800a46a876860cad5c4a197a447e99ed8023322ce8927b2d14f52094b4c0', 0, '2020-10-01 16:02:09'),
('131f1275b486a7ac8ae45a9e4113f4fb446c2717556d419023fcd16b48687c7c818b50a614a82f23', 'ace1f49e73842d7186660e6effa024d6020e1e4e71004e186bf0f1339d955e1a564e7df4b4c2ca18', 0, '2020-10-23 12:49:33'),
('145b533325aa525524df1a7f02d356387420beda19f808a2ac92bcb97b4df65d297c3439f8ed9ae8', '1da2b5ff5070affb10f455ec29bb8ba94f8c0f757b724c939089865ee8cfcc2d544a2ee8b45d4b53', 0, '2020-11-19 10:32:34'),
('14ba77496b4afe9c26ea7f008903568d29d49c6614ccff410d2ec1cc50fb289d6789cc4afb06870e', 'c271671957e03a36c4a2b6d84f705db19de350fc6f621b71503a9f5f4aeffedc8746de64be748736', 0, '2020-11-11 11:51:05'),
('160442a90e5a23de6d63563265cebd10946b9fa529ee3c78dd8ab235d4c3ad2e4c74e46df39978ec', '5a1dd4bc6065e323a7327ecbed2ece3fae620ec7a657886ed9ebe5e43a6ce1940f3fcf3f53a893c2', 0, '2020-10-29 12:46:30'),
('160d2376c9c284d9c33d8d80d32c9b190aa97f65b6f4c8cccafd56c78e597f7efb6aaf579c3af9d6', '17cd91377846ce398f5b2f6f304f97ebe5dc88807c52f21ac128e57d8515ac9a7ac3f158347e3c6c', 0, '2020-11-20 03:14:19'),
('166f3e019f07e92e6d26d3ba50c69fae078babf8a71de9ed6d48c1b613b978f33edd1b79f1782823', 'eaaed082d75f4728804cf9a85c9eb086c0939b9fe7a7763b8cc430b7c2981bfc2198e46d1c72161e', 0, '2020-11-14 14:09:42'),
('16e990c291374906935c8af106f78eab3255490c4c50f7435b744509bfa16cc604e6c850ccac49d0', '7a402928d8c73e66ecc6f8a23ccb81bfddaaf819a55919433125e4e6563dbcaafce42e1debdaa818', 0, '2020-11-12 11:18:03'),
('173911e7aec3f80c07bf51e0181d6a836db61e0cceaedd8368aaf464442595f3492317d9c8e5b89a', 'f2754cad4b12f63e22e917b92534edae11dc85c01dff2fd2ceb663b608e912cc6c6bfd577e65fb9c', 0, '2020-10-22 15:19:36'),
('17c4a21c46f52bbcc614f79f0fdc8e03543a091fbd57e42360a39f2acb86fdbdb856746dfd908f0d', '172fd321913a67939d8a963e549291383e11eb7a684168e805ab0f44a39a714af6ab6998eeeeeb56', 0, '2020-11-20 15:07:21'),
('18c98b12b16d1b581900ce04a9cfe3a14657340579de7ae89d648e940640d6dc507054f9f9f95cae', 'c99f9c53953770d54fc54d5614fb03731a5645c0a62f494d2d21bb2ca6adc981f3afcb8e450f5851', 0, '2020-10-09 09:59:18'),
('190870737614d5ed034dbe1dc5fc2348ff115b9e90ad2c90433a5622670e1d6f7861973fb23412cd', 'e4b0eea7e909b269a5256be78618390ad497a07db27cb4f53862f2a732aa48d68935499b90d6b0c1', 0, '2020-10-23 09:52:21'),
('19bf2e25e55df205e991c5ab3ac2c1a540c5275a0ed7612eb88ae8cb5207eb8b96346a2b96f21c7e', 'd041fe296df907a70fefa2ceae50d3b077a7e969a358accd0d739893a379c910e1b53a8fd1ebda1d', 0, '2020-10-31 06:17:45'),
('1a0ce1aa383959e416483af0e0e5b692a204661f9dbb1ffe7d901059ee85164ed00ed34aa15b22e7', 'bf4661f48eecf5f2adbb6b1e4a09835e3e0c442e9635c33df924bd8126665d11e357624c877003af', 0, '2020-11-12 11:52:09'),
('1a59c370f0743230452fcd378f0e7538f1a696b32cbd5beefcb0f7f8eb7fb68d7ce561172023c66a', 'a466a974432ccb9fd6f5df8b5dec79f0fdadc1a1464a9e1a7a308cc7768b46fdb304b8aa36cb4958', 0, '2020-10-23 05:53:56'),
('1b77ef05d249f42065b74e2794d856cfa51f53d5b73dbf420586f78b3403b03efad44c626cbd06bd', 'f03f159f6f70c003b61623ab8734dd4a141bc3de4274142cd8910da89cdd86edee32dac57cb533e3', 0, '2020-11-28 11:25:18'),
('1ca299d481f120f8ac60fbc89bcfeccd39776d4ab7255a3e8b1cc29d2eeb3d1914eb04c83d87ad0f', '4525b37c984531eee2f81ac6980fec790b6af65fa9991dda8c3e7092d0696f27bdf3ec5862f5114d', 0, '2020-11-08 11:31:23'),
('1ce5b740ca6e905bd4bc8d7f6779a3b3f1a874b82c6ef050efb96580ca7fbee74ad945e06807b589', 'f393b2eb1139df68a0afac8977704903ad74cab30dcc700b685485b169bbcaae00609e2ff78aa1bb', 0, '2020-11-11 16:31:51'),
('1d7f8b368924fd07d64357a357815d00f1eddbedb8eb8848675f9d358db6a9e50530e7645ce9dbd8', '1c009c8eb470fb5f65137f3e085e75fe7e139f2b1baf2f4a6df62d463219a5ddef98a9c21be8d5d0', 0, '2020-10-31 12:14:29'),
('1e1a7a6416675d1caf5bcf8f48db4c1766c66650cbfe6a3801a297c835ea66076a89336a89d7a7da', 'c56efa1401e7bd654da2668b6ab62599db72d7484c83d0b706f05100185560a6e6de5d6fcf93255f', 0, '2020-11-18 21:12:41'),
('1e53a589c43fcedd35cc4cfb5e104eaec6974a4545ddb1bd689797730b7f29b5059173d6461b39c4', 'e1956bff7a54daf7d45e8309f16bd6bd8da529ac9ac8e8d606d5793babdd70db12a187b6cee431ad', 0, '2020-11-11 07:10:03'),
('1e6a22d491f6cd0e97d9ba5740c18a078b9ccb2f158aced26aa0b295e31cd04ecffda24ba0d6c35e', '600abb3f5e68a14f25f0294e2aabf850a1272a01fe98eb6c581ea8c1da7fc9836b92f97e7a3dfbcf', 0, '2020-11-06 08:09:15'),
('1eb101b713988e0c678647423613beac31dc04ef5f7050e6e8265bc1d2e2d8e00e34a220b61d0f1a', 'fa97893688e3fd938a414b96cb9263eb60bed81fa98916a5dc274122d5ff0a5fc45e75c266c18926', 0, '2020-10-10 08:40:10'),
('1f7aae891bba67f1f4161cabd9e324ec748bfa4472a8c5da279e1e6aab33ad021392604af5976383', 'adac998cda75bf84fb4fe38f489d3a6e5362fbd8a8719248336568977b44d2eb8c5c27068ac02891', 0, '2020-10-14 09:23:03'),
('1fa94ec78d5590d13d22cf5f2a878ccc804237a0a01319c6b071b14fc44da8d2d61fe87fd8071287', 'cc2fef392a5f04d6b6551008ff563372748a7f5cfc7a1938c61c757f8c373fcf464d70f58a1d6675', 0, '2020-11-05 14:44:03'),
('1fd60696530ef101117d9a8457bc945b14efb1a1865a9b5b1f4cfe2d6bd3b606107d068238b1b99e', '81c2d345468f0224d115bc1c365c7594f9418fef10be8c9087dd39737f44984aefd1c69b108dbe7e', 0, '2020-11-08 12:37:29'),
('1fdb5840d5913ec3a4da001d1525578702eaa0409fb977875b5354381f4d2e2cded439b2358e3b98', '594a9a17e5df6a71e5fa238c69d7e7864f6705458cbb48ce9eb5fa480c3f6529200fef0aaf811e4f', 0, '2020-11-11 09:21:26'),
('204eb1e13bee99aed52b55610af64fc07dc61bd85525931d7e71cbd5d8696e1d6c69b7f413abf9e5', 'cf557c17671e5084cc7f67859ddeae575f82447b3648e71c4d84a1d54b60fdcb91c93db358fb5736', 0, '2020-10-01 12:07:43'),
('20844f13f0267beffb1b8767ffbdff3abc418946fb69e212157bd43a1d0c1f86a266ddc05df70197', '09f54a120d36034155127b26f14cd181785ce6cb31e41368f3bfba7a3632b99f43e27ff897f1b32c', 0, '2020-10-18 10:30:40'),
('20ff562bb63cc85b92a694c91df17081e459d77d0d4c97dd8e6ad6b11746ce43b13906bb88e94200', 'd9d26244ea7fc8a85cfb2adb4d44780287040ba9899847648c12ae822610616008844c7bbf778c81', 0, '2020-11-20 17:39:52'),
('216cbb7396b73ffea7eec272f5a967ff5ef6d7bcd1ef414a8b9e9ed4d186f37aa9370548e2d4a269', '45668eedd9a7b84be438c2bff1c1946e1960442d33477469222e5e073f49b87905778d879f0611c7', 0, '2020-11-12 11:02:59'),
('21eafff8c0405b50f5f2774cd1efdb2de3c31ca793732ff19a2fe42c61b5ae65d5b808cf6915b171', '03b561dbb8fce2dc70aaec66d6b1ab0374ca422edd505dec22a2c6396d3d2b83aa4cf2c0559af22f', 0, '2020-11-20 09:29:13'),
('222f30c20e71f2cb922efb72a21ac927af6b815763f5de532110c4bf6d7ca3fc9bbccd865a175465', '4d1979a37060a3a508a2b00d8e55ca485cd922d8ab3371bac41f1852d6027b140e09d62821dff35e', 0, '2020-11-21 11:40:16'),
('23a17af5b4f95291e8dd0a25a0aed51b0eaac3e3d191abd6317d30b11dd5ec2f22c6ce10c727b9a9', 'b4519fb4cf087fc5421dd7ed8133f4fb5f338f8addd5b3d5f54f57a669657bcaf30deee3e357e8f1', 0, '2020-11-21 09:24:22'),
('23c7260b4b17081cd4305005ba3aface94a96cfe69ac4fa005a716d10b9aa5fd20da3b9c48864ffe', '5d853427c7ce401747de02c64de566b6587f2831a4d9803d5b3c11d98afe00f89e178041244c9513', 0, '2020-11-11 16:29:46'),
('23c7af6d585b97cdf3d67c3699676f087e5329186e2abb36a0e3ec6d88084d4268c25188429b5f27', 'fb36aa46da6d7e420c7e07fb3f21ed575e57beaafea2c624194d0bdbe6cde11d05b1f8bf6b52e05f', 0, '2020-11-11 11:46:54'),
('24198ca21dd14a6fe8c329aa54e4b32397fd8340a736c411beeba6610db1df3c18c2777640beac5d', '6de18a9fa8d1b553c00c801b3a966015eefca0cccb7eb3fcc82177607d566ce81c59b52d3522a9dc', 0, '2020-10-14 11:47:38'),
('2420502b995ed6d01156054936fbc5f280f61cfc6f4fee791906340eb491f9955d9b3a342c415311', '2873e98065e1601aa3d68029bb10dcf043aae830cc354dcd834903db063272589208af82ba575db3', 0, '2020-10-23 10:10:16'),
('248589b209c7842cb88e9d1346508bf68d3d71aed4bc3ddfb896bc888f33fbaf18053b2d51dd4f4a', 'd90eba03376f58e67f6f519fde117364fb01345a38c628eea619d7cc66b81c467388affa7da46a38', 0, '2020-11-21 06:55:55'),
('24a0cdbb455b8fb59b70f45256e321082ef86b80bff3c46f387cbf2aae3f2a4bf1ac7c1a8762e360', '1b50a1937a2130ade80acf1dd1ee7be0ec58d15bff732713859e3083de71447a4917fe606532d270', 0, '2020-11-12 09:57:52'),
('24ede3112306b057aeaaf84dc792bced13f6f2c076130913207b1c8fcc30ca0616d501c0bbfa77e5', 'a43114e522b970b0568402900da2d04e76301c95754f45093391e3777992983032f55786275e171c', 0, '2020-11-21 11:18:29'),
('261dbc3993485ada961eb62ff2aa1f4939925dfcaf82b0bd65a7db71f9f551d9bb8884bffc4863bc', '07a43c8c8f6854490b18fdc39b1d2df08412d94889f59f7879ab2738160c8fd4bf4928075725d098', 0, '2020-10-23 10:11:07'),
('26a01a8bb787ee06667d2eb8d968e68532c502dd4cc6fd661f363916a7eeedc493135a4ed8337d58', 'a1d897b22d079cecc3437c678657299eaf5642e5f7905e3a7c6b46f5a431d04c688d608de432b508', 0, '2020-10-23 08:54:27'),
('26a6d962bb72e90b33169c30fb132235887d13280875ce93f4a9f9e773deb3a0a14d1788f920eba7', '8c18fdddd9cdbaf5a85ce26efed93551c1362e0d8c55dd11d9aa6002b43cd2007e927d825172af1e', 0, '2020-11-21 11:18:34'),
('26f2bb140d644461ad54db8fdde6aca84f02a0bfd754c0ca92032bb412d5f1127622b814793e69af', '88a5e8315d4ed39ae17d1a3939c0a0c5b6826304f52ff408f1b1bcc279d3b1ccc639108473e1c129', 0, '2020-11-20 00:20:40'),
('281b77d2642f043712ff978904ff37f7ccb3eb99ba6ee87422e71341c32fe2fcfb5eded46ac16dac', '291850db846f8f544138f762d2c8777011cdcbec1c7fd8225215326c6ece416cdc413af6e28aa549', 0, '2020-10-22 15:21:02'),
('285930fb954a818af0abcac2a698432ba8ed1558be6220964ee46e69974e9d878a31672bb0e45000', '2c9adeebe7a4c05be30e75acbaec8c186696b18cc0d1b92f8d814f287eb868f8568b2bcc4d603c86', 0, '2020-10-23 10:49:27'),
('294cdced0f3288fc59b3b9c6f46e59a84184b10cf75c6d75001c8558d9295f6b2645edf135fe6180', '1a03ce978e10f25ab9ed44731bc5d1e564c295ed26c3810adf5e25d9e5a8656ada1cf21d06bbb9d8', 0, '2020-11-18 11:38:29'),
('295e1840a81a0ad92d4379e26c861dd518e1d10752e2ce1455125bf82215c58cec20d4127b840b7d', '1d83e75c1dde283bf92079c65d6edce7fc39a080ec67b83820f2ec4f914f3edc4bcf0f09fabc7bf2', 0, '2020-11-20 06:46:00'),
('2a1baf3ca48482e88bb6e3d28e3ee455ab87c6a5d999e5da932c5f62942469b938aa06efd7e2bd65', '4f98f04b9d6d2809dc7249bdf86b38603e359388ae72e4f792c84f9486286ffda6129ccfae8f2ed0', 0, '2020-11-11 11:52:17'),
('2b4d20997c5c168fe94f7d3735e7b822a98aa55b1ea536bfef543fa1c6774c1b5f565ac079ef6f4f', '7420758da41c6dd28622a3262339020b5390a32306204699ece716db55a7c0301e492afbb0f0b1f0', 0, '2020-11-12 18:29:21'),
('2b6077e8e207c03002c7e250537e7a83db06cdd785f45fe28efa555402d9f0c52ec41c08c2d46478', '0027c82ab73c66d9e32f18e550dfb3502a9d00a210f97112e1cb5d27c7fc5142eedb135d90d86897', 0, '2020-11-11 11:37:57'),
('2b7b84fbac3774f1ff4cfc9aefdc754acf8bdf0a570b705bb5c1a88b5291cc64e5929609292f601b', '27893442b6e8f6b2981a75d6e25e9c75b3b7244c532f5bb5cf3bbe2b52f4f7e8482e7c474f2b5e9e', 0, '2020-11-11 09:46:21'),
('2ba9f3069ee7cd841f5f61145d693b8137c46236f643ed7e7656d618dd898029920fadcc9e70e6a0', '233d7677214c4544ceaa055627135f50fbbf4b49259a3c69413a97b33e3850db8ab7af4dbfd8ba4e', 0, '2020-10-23 13:20:01'),
('2d3f95b40098227b97f78accdbcf66f9f3efd2c07a09581fd1c038eb8a48be4230613280657fb279', 'f09efee43e4f044aed68d3a0bd0c36f303834976dd47da5a4a6f48aaab98d01ae719b89c9608d57b', 0, '2020-11-17 02:10:44'),
('2d82473e44b09939b04f8e687831b08ff73267faf796161d4055c05563f031f824bf272928250cc2', '545abb6430f938c9f9cc5c53dcd0acc23b27ae64503eb3c8024176498f1cea24be618ec9186a1b7f', 0, '2020-10-22 16:57:41'),
('2d94b8b0cf9fea924c49b18b7ff47264558bd4bdb50898c2711e927eb1c512a1777df0489fe8bcb2', '57051510cd77af4a5506a3e8d6274b8d71951df0af8e61b56a48c07ff799106ae4bbc49d36eff52b', 0, '2020-11-04 10:03:58'),
('2da2ae4baa36579b4510ee87cd91ec5a1d728f2e69948addbfc858504aea622b3439def26485392e', 'aa80398829c55c3dffd886d32f07f16cc2e34628d7510ab7351b345fe96cafdf53859c32b54d6e46', 0, '2020-10-09 13:39:40'),
('2dccfac660c4f686330245aa10aa972159251f968005daa2a01c0ab0eedf70bd039f099679dd089e', 'aa190dfdacfc93f1e38b131a8ef9225bf183a0bf693bae501fc1c29bb252cd3ebef83d2ff6ac138d', 0, '2020-11-08 06:49:59'),
('2de1c7ddea1ec25550971959819fc021ae677fe8aa3ab045e9cb52a8187b873762fcc597692870ca', '2909c872b13e86264b5c569a5952150380613deffb0e5d542e7c16dddc887d6226cb7c0f9ca74117', 0, '2020-11-15 09:11:54'),
('2e7a75b6755f13864e793365f4f7708a0eb34b1da711d51174f8f4d679c36ef39bdad5bc63e5f5f7', '2e2e48e668ba57e472cf4ac01d0ecaa35c0c4c3337858ec588450a16fa12f8e81dd15cc1932ed7f6', 0, '2020-11-22 11:58:00'),
('2efb5557f9b6950cafddb7d0343d109dc336b5c64e71917b2d7f6502fe351ac9f1e7b5b4a7d9f769', 'dc8c34148eafe6c6695d08ce0c96b58c2be6771fea3041231d6c8d216584295024aa1298ffd2a9d8', 0, '2020-11-08 11:03:45'),
('2f10793f35b6eb132856076cdbf4e959f0dfe7b9ef743627550b4f532587a6c4f48ca0c237866408', '3a5b897489189e024b2ab8aa54864d2a5f628ef56b4d8a14490c3d6014644caa82ca60221f206fba', 0, '2020-11-18 16:38:29'),
('3097535033a81f43060bb8147010811707845cc7313f2bfc74da303b4bd3e0458ba8818365203985', '2c8bd2046d79fb4e0eadf656be06eab7bd257ca52c3c0ca9545a355149f466113245d6db5a3b2f9c', 0, '2020-11-20 11:36:13'),
('3148dacb0ed2147fae08a562862d22bcddcdbb52e11906d4215b2aab5a6836cf978a2bf3c035ac66', '744333343c5ff1892d597d9d5edaf81be1c79cf7a164fe0299a4da65a1e4a8792ce21076a2b71349', 0, '2020-11-04 10:38:18'),
('33027d5d2b7379b4c2e0e48c2a989866f4213c33b484459e275b0302c71bd8caeafacdf9e3e0721a', '6e1de9fb14fcbf8280081c9d9941cc6bb77f1d9f4a9b47e117ffbee8f0fe482367c31382a8686e50', 0, '2020-11-20 06:50:39'),
('332a1f7aa0dd0ed4361699ef2a044c517e8cc0243ed63137ec243dd01fb2afa0c830e4811e7a7e32', '50a13b51afb9c290deaf4ae7c4f75f62e4fada24d5bdd532a98f7de953a266663a219b0a86a57228', 0, '2020-09-28 07:50:59'),
('33e10e6e342bb320567280f5e3411c9952afb793b1b775830bb870bb185a8abee2813d428a074bd5', '42a335ef01460fd02de93143609c0f2d4daadd750f2bd92911633b6f9948cc68dbba578fd81b32cc', 0, '2020-11-11 09:51:18'),
('342389641aa4ca4408571b9f86db079dcbbbd4f3c3314509a1d3d1a66c30d6194f637e7655e18590', '71bda0eee798b0c358116f32b6dbc2761a946ed45087040840f0d994ffc6890e8575453fb33016ec', 0, '2020-11-08 12:50:19'),
('34486c46a21e52917d7ac96d9dbacc7ab8ff595dbe6e46b36c19dce33a9aeeb8125cf2dde1fad427', '06688fb8ae40ccca87286cb106622d2358f119bd59851928e0489a83be7afa3f9a2fe3d00883d266', 0, '2020-10-18 10:11:23'),
('34e87371add33cf4a2bc9a74ee8c57f9dcd26268454d2e9cf90682b0b3397db7ea349dbdf2b1d7ff', 'd6eba2456bd793beb26ba437c0a43463bccfb92cc13c1b3702bcdfd5db2ea1bf936ce8e9de1903cb', 0, '2020-10-30 12:32:06'),
('35cf079759bbcd61093c644c3c23f271911349a48c8e2623ed67947e8f17f021190f3fc64cbd304b', '5e990c2151caa1a68cc83f93c724bca5d7e3d11a9acc39dd0e5c2957d4ba16dbb17c41490fec1179', 0, '2020-11-15 08:59:40'),
('35eeb20dc3773a27dffabcfc6fb5412b78fc18ff5481359fb9d608255f171996d229a34143c20b24', '076c1fa9c5d1fb08053d8ab04026620460a14db3eee380cb021778150a7862b43d36656ffcf57be6', 0, '2020-11-19 12:51:21'),
('363f7818409932ff23aa58c8e9fabaadc1c176b476cee1b9091f26f37c7511f4154e9794bb5f6536', 'c2c70afcf4d72ee55a1134fe2a232c7d8c9155e6bb40f584da1444d3d94d058c6bfa0a87ff5946d1', 0, '2020-10-30 11:14:23'),
('36508c8844350021958fea67795ceb893eec2ee96de2c7c6621d290740d2fe1a9d57ae0ba92c15e4', 'b5b83db7f9643c775798ff39cd52a71e6a2dba5a4941ff4dd7f13ca88c9ddbf78e258131879569d5', 0, '2020-10-30 10:14:29'),
('378de652d8f5d76760afaf1d6a530e58c2d1e52a35c5e85f2187f6007cd58ec718785ffe2e209360', '480292a5dc28a783744b8ede3d8a7dc9472598143d629ce7dd9a54075b116f1ff4b4993bbdf69d87', 0, '2020-09-28 09:23:55'),
('37e1bafa52d877f0a134db74e116c3b1d32042adb82cbfeb1ae716ab03166e2f25f5211d728b40bb', 'e7a8bf1b2d03b03883fdfe96d8992ab1230eafaa280dbc0b35809be9e0b5b8592b631d04a8c558fd', 0, '2020-11-12 09:42:26'),
('3854e38f5fc2c935d6ea031f129fcea0bdffef11a1073c78ad201d0aaaace4f3b7b81f4fcfc814ed', 'fe3f7e632eb596eba7702481c0b3f192577a662374e5c0ebe0e70a21e0b7f9b2b7536428147938fd', 0, '2020-11-13 10:13:35'),
('38581131efccc286042a8c0646ffb1f1325d6af06191d2a92dc50834921fa95e92dd3a4aefd2a6fe', '627cee5ee3ab1829b2325e9d876da48c6a0fb10fa3e1b71047a32ef315838c8fd97b24216fa52197', 0, '2020-11-25 10:57:40'),
('38b4f8b79631afa54932e0ff4034ecea561e8f628b240085c1da4ed8bd29e83757dee25bc3ca6aaf', '349f74f8777709f1a702ed03d06fbc385fde08ec2878771d4021fa9692a8331860ea994da21d0fa4', 0, '2020-11-12 07:42:56'),
('39324107c930b15efc3a00d9c49e422dcb02aff758179b045b5556f46b9ce1ac9f3c622b08b13b40', '2e4e4dfec0b2f0cae536876b51844727b994d69c1912c6281eea7276f9c733df6af2e8d4159be544', 0, '2020-11-12 13:12:05'),
('39bae2a4f9a7372bc5f570417a1ae36de073672fd9564909bf207ad1f30b96a52acd4cee92518228', 'f7a0659043e310af51e0a8b6d468ecb71346fd6bcbabec738f7b96cb87b1a4235fc7b8f49024518a', 0, '2020-11-18 21:18:02'),
('39dab2595fe3274080bdf93d6b3d093f7e09905c10e586c248230a2c9a85890b9abaece9bcf710b9', '3dfa8e9ec95216598a8cb927a4642977d7e0697ec0c09d758620161de8f5514691dcf486d3cb160d', 0, '2020-11-16 15:55:16'),
('3a9aeed317341179a121dec0be5c534a46d2717e6041ab985a5863d37077a0ed8a8923bcbed0d1a4', 'ee96e8cd81c33e7623f68941bf73e90fb5b2d426de88796d4652aa02b86406e39391d8dabd0cf7e8', 0, '2020-11-19 14:58:25'),
('3aa2dc034231ff9cda0181eb00bce34ef1fe3b97cc78847dcb74e7bb6ce8841743a8a198c54ffeda', '703a72baf8a8ffc6a471628543e444ad9217c18145d18c7daa8ccf2a38ec055aaa7efba6d14ee444', 0, '2020-11-05 14:37:45'),
('3ab2fb75b81cfeed77d9ccc083dd0be3c13bed930dc8cdc706d0e191f8f75a253d8c7fe1e19e309f', 'd20298dd7df56926ab10bd16ffebbe9a72c145f2fbee466dd4e69f0a40e8ae82efc1fede5a9205af', 0, '2020-11-19 13:26:15'),
('3c74cf8c28af0cc79ad313389d4a0d5a7c503e27ef5a3c8d323fe829e983c6bce129c9b3f8ec9fa7', '7d7051740fe6099f1027033395a0f6fc288e2ee1b0e6274f9630c957441fbb348714a83eac18cab7', 0, '2020-10-31 12:01:31'),
('3c7dc9f1f9bf50a85bdb750c8b90d4e1df923743f066435ceb32f83be0065f9a7af87d372451ca16', '5ad2d1ca9e23de36be454327560389fb4e4869fe4eb95d08fc8d51ea807a325ddc4311b8fd758ffa', 0, '2020-11-15 07:14:18'),
('3cc617bac64a113a26abc9500cc483d1feab71006d8ccd2ce9d5fa0f1495700a1fa3d496caddbdad', '6760e5a1be8f6d948edae390ba5952abdd6262f578e42efff833e7ce89e7971d74a56eaff8cc7aff', 0, '2020-11-19 09:05:41'),
('3d22eb025a3e5dc0c847d7a1529fbf031aa8d78bb3815ea0a79bcd34122d2d534fac4e17d6495dea', '244b77fe9542c53a77456bf5c657bcf27f53edf896b7bdd47e25a39406cad0c57a4b25c280a0735b', 0, '2020-10-23 11:03:29'),
('3d9325991a826a0b8535b8817a5d47cceb35ad6a669127a04f7aedbebbab3f977f4f156b04c198d2', 'd121af3f3f5a8c3a82ffa6f1322c115e4bb195b3e8331a3f9dcf2edda6d037e9e7bb13d61149494d', 0, '2020-11-12 07:50:16'),
('3dcd012c6935804b59b1fc8e369630fcfc1b2a417b98276c670543065f22958f90f959a64cc8767b', 'b2e58e12d7250b1a91a7751a302007fbce1c50b5c4a8fca1581f082387b0a8bcf54c24c2e905ccf1', 0, '2020-10-09 13:35:40'),
('3e032a2a8998da8694a55ee111246a1c231ab2f73dae4dfa5f80c5b9deda0c502f80531a5932a72b', '37431a27d9cb29b6d27e90661485fa06feafa460d219d43635eeb55bbb93b550ea61bfdab1f83e57', 0, '2020-10-16 06:53:49'),
('3e53cd5e345b7a9fee4455ff81df443597d9da204945a5ff5f7112e0fbfcddf9ad540f7133f41391', 'f77662fa5fd14390bbe737f81c8cf423d59c69f5482bd2f5bc8fd7c87282289d0390003b7fdbe62a', 0, '2020-11-06 08:12:24'),
('3eca892c5b46122e30c70fb7f42c215d7a66725e2e57bf043052d7432ff955ba4327e1fe5bd59cd7', '1e46b81363ce7750a1ea88fa93005165b0c5bf6f107b23fc7cc191205099dc1bb080a396124f3cc3', 0, '2020-11-21 07:32:24'),
('3fbfaf927cad25eafe595777d0fa101ecfb05ac1d4489102e7dab99338803205436b968a80a3c7e3', '1fb36ee97d64f42ac324de4de1c78837fd49f2333dcd960704291cd20ce3856a42b27d450a21ea97', 0, '2020-10-01 13:13:07'),
('405dc8241fd81af8d7b809fa600e23051c13c79990f1be909b941da2e411660ce3ff3a88395af14b', '50715f31f8d49652e72da59b3256d08c96fe16cd8c7e0cfa8b63ff874964a8ba1e70b8a9b1448164', 0, '2020-11-21 06:43:34'),
('4073cb3138ecaa1d2161d03e32a8fe5eb1c1ae2c2b3300a99598ae35aa557fb70f8fa7d78b6809a9', '6a4b3082a40e957241c3fce4f3b5b6ed9ebc78808ae5f7908098c81d99670b11a4cfecd34acf7837', 0, '2020-11-13 08:02:04'),
('408085914b0ce38045d16eaaed80f77ced146e4b383295ddd2d1b376adf3c29ba14199f8b67c3e19', '0b919cec92a0be5c47f19495fa5eb33a4651860cb034face7f5250584c980938b2139e25ed150a2f', 0, '2020-11-18 21:14:34'),
('40989b5d6db8edf3fd42f9599c618a3751821a6d91faf3a65daa974fa59985bc8512e6143185e234', 'a7c249e8bda87431afc55cb9a8881b157ede65f1330b559f7e2d78ca3015b9dbe6b573354648ff09', 0, '2020-11-20 15:50:11'),
('41c43520443ae48fefc7171e5d3a700701602d9039ce8f7280d9ad8e08d0d5eb6846d1823195f129', '3046006599961ef9b23864fcec874741fb9ce7a927850c52047d33c5fd1adaa1cd83415ec0469cdb', 0, '2020-11-25 05:04:34'),
('42086107580f909920515e34394160c2d4a94adf25a7f10564ae89cbdf170e9a4054b3d0de264db6', '3fdcb133afc5c8240b1848ed4bca569bc6bd3f8bde24c5be9492f81dd24ef9cbc341448a55c07fae', 0, '2020-11-12 06:45:15'),
('420decf5cfa2118947a90cafd2a53fa930b6ce3a1d691ec99d34379c74ca6aa48791d4e178ae936a', 'db731e45b1b1ca217b282c99059a5e393b4e9c3378399a1ea28746caa918c7ee6b420a94cba9dcb7', 0, '2020-11-19 21:26:13'),
('431649c7246ba709ac73c5f64c2ae19e0285de42fa761aee9f3a5610f3fac280c16b8cc12a230925', '4c7d27f65c795f58ede92a7ecc683e71d34c2b83f53ad88cdab23b7c08a9aef56c3090b7a01b1eb4', 0, '2020-11-19 19:47:10'),
('433368d7802ab1d9fec9a1bb5476b76f069b1247bf2a81859f64e65922358327fb8476aec0b425d4', 'd705eaee07759437b6fbdb4b123f929b622ebd16bbc618209b0e5a371107df547d2b8d3ce88969be', 0, '2020-11-12 05:42:40'),
('43bc5cc951c6b6e74311a23fb69dfd37cfc52e7b24559f015187213714d7f1e9bbf837b85990628b', '200b099e2cab8597b65328f13805dd9653fdc9bba17c2541de3b0784a2b1bf7ca4314373bc90abb2', 0, '2020-10-01 11:21:30'),
('43c0f319a4285e996b03b59118a43d8a66917edc19004f1a955ae3c43f911ad1cd9f4fdcbfde048f', 'd467cb83f06efef1793ea452bcf9befed2c6eab6289a2eff4689f284b4f05f93018b6bedb595c24e', 0, '2020-11-08 11:41:30'),
('44ee032394c6d1c432ed230360a67980f8c246b367acbae6e07dab3f7a016e7403370196af8f9f71', '1b7bfb9019f33bf529d4d74a2e12232d98d2899777f0b2060a245e0b5581ea7a66d6168072452424', 0, '2020-11-12 07:45:43'),
('453ad0389065ea055b45239f21d70db5e69bf80c08d7c2adb54f39e41173edd5ebc2b07bbb99934d', 'a15041b503698030ab79e6d728d34352432acaf52b577b6ef1ce0f01b984423661412a0c32979284', 0, '2020-10-30 04:49:42'),
('453e72eb4771c6f18af682d755d0d9a5c364a520994f1eb82150096f7f6d52e6394dddc0faac423c', 'ece6d386a83502b1b96dced418a6c533af6c64aad084df76d171ec3e069cd08063e6d9638d0776cc', 0, '2020-11-20 18:14:09'),
('456c05b35cf2cee978fb07a39e8f4a0972154be0e4d43cb4a4ee0894b2ed5a0e0f4d0cd6bd235ad8', '7722a20154f4a3656861d3047f3248db142bf4e147a45a934f9a4f0b01c832fa7a1e467a3309aa86', 0, '2020-11-14 08:02:43'),
('45a2461041c77b301edaf1c7ee13959815b7b70c0727a33398beec4404e3488f038bdc8f07cb8656', '931326091e9468be11e830583cc92d4414ff75f6e4f75b85658d688dc6deaae2185e13bd037119e9', 0, '2020-11-19 10:38:39'),
('470e8b8eaa836cf2e4f3f37455cb0534d058150a6e5f5269ebe7bba9442873937ca1d16bdb8b95c5', '9b524095e9b62574271f6147b5b6f644b53d0e9ae6c79d2aa3bcc8a849ecb21455b7309819ed8071', 0, '2020-10-18 10:31:43'),
('478fa6b31066eb62635daea572600505edf142e917cd26d66f3b02719cd8ccdd6740c7bf0caca6e3', '05f2264ed5d5e052959b8d488f62f3b710183721f7d871d7c0966ad4fcc1208d8f680ead62c7b99e', 0, '2020-09-28 07:49:06'),
('47d8a8472053111311330db1981b90f2efe245fb411d0fc222908cf0a3d58c637aefd2e3bb701f00', '140af88be99b6b26a6d3c077df6cd7e8682be9146b48ce047d15bd4a0256483e566bb9392d5fa3c6', 0, '2020-10-18 09:48:31'),
('488677c710942127cfed76daad5c104ac292cd57ea2c9ba3cb4896683a5aa189fedb14a4fec14063', 'fbf2fa93f94dba0703955404f43e75c34fb575c4fb5eb5df87e81566187f046ac16156d1d2423099', 0, '2020-11-08 11:59:45'),
('48f146a3bdb189321ef9bb4b6ba2b3dbe14bd536617f525caa0c73fb0f719c399901ae02fb12092d', '4aa758793bc191360d209111c214c1fb6148ad3700bed81060ff587cdac58fed4b5c8eab282a7840', 0, '2020-10-23 07:28:33'),
('49759ffce3a03e40368e8b9a88a0e3187229363a73567361a6525c721ee4f9feacc683b73f40fd11', '45647fa3296267d8d530b9966964d22ee77772a1206b9b4bcb67359da89e287bd783712c7848f5e7', 0, '2020-11-12 11:12:58'),
('4a0db295179be7e4eba08ba639b06f6a0612cad8761f2fe1d9c5cef79fa816d771a7b6adbeb451fa', 'cd58143c56a56730e957fd7efcead3591579846b705ef75bdecfa37030ad6e907710ecf03029fe23', 0, '2020-11-01 13:29:34'),
('4a69e807fd318e9730391719b70ba6d76f3607b33bb577988b8fd8f328da68006c6295a017bd5cd2', '1c160b75b7f47134a9abaaf4568b4c8adeeebb0f2e7bbaccf526e1a5cddd4194bf69ee638c4d39ce', 0, '2020-10-01 11:47:20'),
('4ab456baf7877e3b16a5f3ebb789317ea1ac368740a95d5b2ff7ee3042b55b91a5208db2f9664d08', '2c92faca530b6f00ede1be4071a336b8ae0e16c63e90cd97ca2df5a18ef76463826c1d8e129aa39a', 0, '2020-11-08 11:12:11'),
('4b07917b8f5f8dce2055dcb6521a11fc800fd07eea0ad3199cf005a5ef27c5508820f12d4d5b59a7', '612fd4bec0322a057d0630c7038b00d96ff6cfded65fa75f625829888e2ce8de3a04110986b6f06a', 0, '2020-11-29 07:43:08'),
('4b199dd9e9b1d61de47929e96d45f45976b18b12d51de6479249c295e25f258c7438371df54d2118', 'b3779f2cab7373d81af06387a0116750373075ea667c0385fa1c7482e5d12ec2a06e3202441c4fcf', 0, '2020-11-20 13:22:30'),
('4b4b53829632ce780d30160157e321852b62155d71a2c16c3fdebd9a3517bd1bb8cac9438eec4a93', '6720f364f525a676b7b9bc71b8eba244bb86d86cfd5f38e4610e31d18ac0013ec4a803a1997501b2', 0, '2020-10-23 11:17:42'),
('4bb57408356fd85141720fc3ee44a273f71d6600ccf00f235d667296d1ef0c8f605b12bddd612753', '0d0b6b06afe7858b9c9ac7e287f8dcaf797850921a7dd91831091cc4fdec306a6d80d1e4ad312c1f', 0, '2020-10-24 10:30:06'),
('4ce3e33ee63ff449ad3748b5c228b672bd22875d8074081c5f82a1a58d47da2d52658b868c5b958f', 'f3b7a615dd2409e11c32e5638b6736e33ed6eafffd58026e8c529db86d91721d887fcdf3a2180f46', 0, '2020-10-23 07:07:03'),
('4e368f1768aa6ada3c5401160ca4cce3cfb2c830f88744a2c235a58693fa35393ef34171d6d7106a', '5f3165c4ea0360729b4ea680d617f0d49e17ba282a002e7af983908db99ec99b9af16fb1c49c8e53', 0, '2020-11-22 11:38:24'),
('4e59dcd92a43071678cd54a2a1dfe7d9058c731a1ce166d36bbe9c76edb01f1589151de5e7176a73', '53e9cab7c92792131af90bf84acfa80d6389d5fee85eccda78a9df50034a69f05c6f83d72d627ae8', 0, '2020-11-15 06:56:13'),
('4ea0ff77b291fd2ad2aa50704e1a4a2a8756f13c1e8f1e03f257735d1d5463462b5de321cbc94bac', '493ab8d131a3a8f68f717f57d94807a922dd2d46989ea1049e6a5e4a4c29f2dc44c61f76465d9289', 0, '2020-11-19 14:36:24'),
('4f0af43b8c4fc49ce23f846f393ab6361a9ff5dc7d37d8a7c9e98281b34915e73d32181cd2e20b3c', '8abc0803bda1c4cc89ec607017883ec44453913a006c78ec9c42682ee688a9d531893f629993ccbe', 0, '2020-11-19 16:00:57'),
('4f39e2ab07e73b4850a6ea6ec766a705d334e0e41025abf6d04bf11ef649c3cdc3586c1391923d55', 'ad1c3dea2b582098bd4ee56a3ffddbfc02a3660c37a36cb7051e5d7f45df8ccf73772991714b329b', 0, '2020-11-08 10:47:31'),
('5012e164a45eee8ab1c961a3281291fb96240336277975c23accf3c49a01a34ea0b88c79685ec395', 'c5d9f2e3cb29d51684bc26181453b07638bdc2a64639ff50b46e2839f8a9cb2e15d81f9e29755929', 0, '2020-10-24 18:48:16'),
('515046ab0eebc02568a5f31100f2b701c62b76506f0cd7112b6b190ceb0b7092064c32b5865c9c61', '2ae0d4b6b87de8617873940962076226b2a58de937eb54be1c8cd753b97bfd7fabff94a764f71730', 0, '2020-11-05 05:36:22'),
('51c7b2e1c8c2c7a1009f79f867645fe61f0408008ac1c94eb1d9506f564148b4c6a0a785cd0723eb', '5ad82d3b526f4429315aab857bf6772d058250acd9cb1de432dda5602996e15858bf0569f9772b29', 0, '2020-10-23 09:31:56'),
('51ca98335ab7824abaf4dadee8cc5e6a26f12af06505e9c79ca2a6a535377b2635db0e6e304af91f', '50a823f7d7ca52e316b94dc2f38b5ee1f5d3231ef6b153cf4d0ae6ad1ac17bb94c7f24ae5301e46c', 0, '2020-10-09 10:39:13'),
('52505402096220a8f694b16432c07941deb15dc508c18b57ec6f1e6ff19a465228dfc5689ce163d7', 'a7aac4c096d90f6a2a2c4778cec18a61e1ba4ab029b27ad2368b3ada98b90e06e7a858d698edf21b', 0, '2020-10-14 07:34:36'),
('53089cd3bccce2a7d4aa1d67ad2506ed8b14fcec930b1c4715b3562fd946c99649865ec8c513481b', '399e46bbcdf1fea81790931b17b81d99918945b3ec061c1c66ddb5c02301b77f97e069fcfe9e7c38', 0, '2020-11-19 13:22:56'),
('53618891433ce2c8cd98af03fe68ffb3447877550c80d292682e206a1bc5c9d6c38a8fa343877c66', '5e86be772c75020f2066c3c5f394e613eb9d632a76242119edc2ef7477618bb5774cd0bad7280611', 0, '2020-11-20 13:28:37'),
('53b4d7ccbd99249e8012266b3a46badd9f9beb91dd9dedc2f785aa446f384269c2f821d3d2165015', '38b21eadbf2c08bfae61068ef7ac90060a379c950736ba5e509f54cf64809ac42d6677f5f32ae274', 0, '2020-11-20 05:16:47'),
('53ff55cc89f1590c2826179831c39d3160d15b18574536bf73636e84edd0c65dfb654482775b73f0', '9b1dd981f3a6b32a4140b902bfff577599f1ad2f3840dbb9675ad3245ad941e8551994eeb5881cfd', 0, '2020-11-22 05:43:21'),
('54a9629e7e841dd9313d4d3326f8b27b901974fa06744666709839753b82445f3014e072474d03b3', 'a7130bc318582bcab2e60a2efcf5b8794921b35acc4f73284afc276358c9d4ce3f78196f8c33061b', 0, '2020-11-19 10:26:45'),
('555de674c58a4b3c63fdee2175b3c8bdd2105de20eb4508131498ab63321e395c0da63b42f9a1735', '86e9dfb86a06d9f5089f2b5753e6fa438c45e2e7484f3bbb666cd151eedccb6504344eaba02905f8', 0, '2020-11-21 06:10:29'),
('56034c3862ac8a8956fbe86e80826385973557478e2a6bc94ec12eb9d9759a1304e47a7346bca42a', '881823475fabc56ef72be1e837dc0141cc0cb2ab57bdbb4cd4512dbfa2b77cc34101ca141451a192', 0, '2020-11-15 07:17:21'),
('561cc1303d2ad15f297b1a89fd65c342d4c97ace02e25402e1177e1efa2a3390895f16626e55df5a', 'b4e3857ed361d3125eedca7ec820aaeaea55ed6657aec588c91b709c886abaf5b5f0e6ba60b6fb4e', 0, '2020-11-06 13:56:49'),
('569c22fbf05f9b70a55c8cd1fb0b1cfac987344e854d0804e9ffba42a0867a933b48733ab64fb2a3', 'e2bc91d3f71b1567f1302cf283f4795681eda6ed015ad2ff138cd508fdf5ba12e5a66e79220f1f83', 0, '2020-11-19 12:47:39'),
('56adfafb7c1baedfd48d46b7dd64cc20649930d0a59184dbef00e2636d02bcced2900105d9dfc901', '61304fd3648d961b73292d684d0ea75e58dcc9e187e21a84955617cdd7df518f086d2798e00e3d85', 0, '2020-10-23 05:24:24'),
('56ec402ffe091379e0d26203a5caf1b04a08126142789e3b115130ba6a920f8041b06945726f5c92', 'b4003c99d13145d53c173a6ad5a62a529c4783070271e99060951dca3b2a2015e7171f6d48519bd0', 0, '2020-10-01 10:00:46'),
('57480c92742deea7a421118b69d22a00b9ead35c6f96ac543d876aec511fdba311c5cd329ea691ca', 'bad2c457ede4880594b3f667cb0f1925707e92f07af77900ea85b52f9822550d6252f547c59937e5', 0, '2020-11-08 11:32:20'),
('5814bec28d81081204d834ecc9bcfe18de06985129a997983301d9dddc2717b8a891b1d5cf2bd77e', '45044402614c09dc6abbcabf84e9152e7a72c0447647fcb625ccd45ec9e8fccf7f09054fcf89e23a', 0, '2020-11-19 22:59:10'),
('581ac29faf67cda315be7ecb333128debb6dbd6777001f9ed8a8e4b225daf3f1308be2c2e9551939', 'ed0c9940a555021cea40b95268aaae83c87129c1c2e409db605c5ef0fb4e319379ebbb107ab8d5bd', 0, '2020-11-11 09:20:09'),
('58724fb4a65fe93e8a93d381db98bb3edbbaa9dd12138624c871d7fbea4582dcf297610289b1dab0', '923cd632cf06aba638cb1efff82e7128fb128e0b65ffb052b67560d6c1ccdbc6f12a708be2411ff4', 0, '2020-11-12 13:13:47'),
('592a481028e82a88f2407c2bf2ce68b6fb4a1407ad6614de6f03f3c178f0b3e653c157460ff9aff9', '61e0f87d700e078f9faf2fe20b528db0aa6fc811f8b98b3b850a49127dfdfd8f60983eda3b182e36', 0, '2020-11-20 15:46:11'),
('5949bc0ff5c098d966e84db5281dabb376b3da60b75b31553d6ca77b653bbdcf2ef5bd02a5f74461', '9ed80be2ec0e874d785acb1d15b8566ef8899c8d3668104b11228a1e3c60734a99b65a80a8c025b5', 0, '2020-11-29 07:38:49'),
('5957cad1acff2f7116594f83d165572c651192510ff8b434da319b6dfdf31b6ff6b86d7544bac472', 'd5e95312ddb683e262d41fa4a84703d76b6f81e95efebd13c9c755776bf43e01b376dbc00655450e', 0, '2020-11-18 09:55:56'),
('59b5f22ee55b101e6cdc243480acdb58d5258b9123306d6da2a10911ad23a5766ad23ec917c0350a', 'bad82d6428d0556953c3a200e3b03dad6015785494224dfa97146e7c46774df14882251197ce7185', 0, '2020-11-14 23:28:16'),
('59be3acfb15590f5bd48112b5bc63e751324ffa4174daca5b41ad31e1be91653941a83ac317d6821', 'd38d33cd4d4a56e89c7ca7d0eae6619c2454318cacb8c81dfd9da88f5e0f2acfeab7560eedf3a30a', 0, '2020-11-20 05:27:21'),
('59d288b11cb0d2d50e6999f830ce6b3f13decada6ff3f7b29b18ade5ba9f49b0bed257d0b565bd12', '5610c274b32f3c772e31513d2da599dd7d8eb06b31497eb3f8325cf80b77f30669a2111aaf80ee18', 0, '2020-10-22 14:59:50'),
('5a1db6cf356dc50d9269ec2732e58d9e8c151ffdd9763f3c9cea97dfc9cf2fd575992b870a54f45e', '41573217fe179c73a2f319fd443da3199b9b7654b2348a730eff029cee49bb09ae2c60b3102209c2', 0, '2020-11-20 11:37:21'),
('5be4c290daec3157a88456fffd51f482df5411ba6f80d3a292f21e1ff2fd7f63bae957ed8c09fcaa', '0369e4dbc2bdb3fda403d028e7483c6aa8446cb5e88807dc1e1a6af26254f9bd47cd3b7ef76acb23', 0, '2020-11-18 07:27:26'),
('5c23c65179d8cdd03f5ddf102509800cfcb73dccdd799a05433ff6bae5d9457a24b9a530a203a5b7', 'b4418546e6279ce78ff32f8b793c12a6cbf65bd0bf63132d6d409ca6ca183c49fdd23abb9215399d', 0, '2020-10-07 06:34:28'),
('5df81e9947f133e42c7a20dc3492940827d022b1bdd14d345628701620eecedb42294ec1ae0ab0a7', '792d8491e9dc32879784e721782a232089095965e20e95dc6d4cd32bea2143f276e95b959fc862b6', 0, '2020-11-20 06:15:50'),
('5e5589d5d8a5fb73c5029a970240837ee1cc054e0aec2789f624b20da8252afc6481b9b2ac3c250b', '9275bbc0ae0ec0858a72be10254d890eacaf9084d619c7d7d9078571bd017cf67ed8f46c874a0841', 0, '2020-11-12 12:02:38'),
('5e65aa0e42ad5cba6b22520d621ea54dbda5ce04f5e7d72159a17b164bef60b6383c1fa46bb65446', '22e2f5b3edb1120d2e79f1a5d33b2d667c7e3fb49d419295fb174bfa0a2a21b265478e0caaca0435', 0, '2020-11-22 06:17:00'),
('5e9920f99b3866d098a741b0fcb78535ac9921b6d59955411ab2cfaec2062adb21cf53cc93b6ad52', '50e7890dee59bfc02a6c3abfe4f9ef9eee8269d653b5e1cc9424c684276832aca07dec8f815393f7', 0, '2020-11-21 07:40:10'),
('5efd4b045d7e7df64ea2101e58f2c87d629c2a096b45e6897c56223ce1ef2def80eb6ad16fb81c04', '08e5ad09d65be93811f5452bb81fcc7e4860144699e86ee44367e1d8b2e874e8081a98494618534a', 0, '2020-10-31 07:54:06'),
('6020010bda5eac695774a8b94155c20741b7260f34922779f0ad63b42c0f506e135990ab9cdd33fc', 'b66060ba51e4099eb94276636e93526918b81ab624653f7cfb1aec6744acffe2206cbb0567cb3dc9', 0, '2020-11-01 13:48:52'),
('608848b3be3dd77e1554e664322c5499c42cb795632746cf2ec9fa99682eb8d7e646c23027001c3d', 'ad6cb36cc162140db9c9f816a4f43968bf9a2bbca70a6ca81c82832fb9b35a06da3f4f1adc3e6a76', 0, '2020-10-31 11:24:45'),
('617cdd6a623784b3a5509d6b516a462d1920455a3f317efba0688c561bd797a05911097b06d382cd', '534e2e12e094e60629c5b50b1e96222a1a8ac7d69bc3ad65f17f3ec36b41b571e7942212e0036480', 0, '2020-10-30 14:31:57'),
('62ae1387fa802f53c1dd3a5201d95ca963047221ec80a9f841d270668cff3596b0e273bc7857f6de', '0e370411e91e7745f43f81c92e8f32b435a909945e4d312a4581739331aa8d20b5cb2b4828e854b0', 0, '2020-11-18 13:26:53'),
('64744788688cda1e2b1ec65fedc7113f8bb6a8d679035f43c77195256f7c38bef72cf3a87acbaafb', '50c8f4c44522ad19264e0649b0fe1d597eddf44383c6b337fbc86d0b3ec12849566998bd48328380', 0, '2020-11-11 14:12:12'),
('65337e4eaf23d91cb9d9f57f3c2effe45b46bfbc4ac69739125758188729ad9056ea287b093bc710', '00a89f80487d682b2d5d78b9bfa0d782c8dc5a5dd8883b48770454c04d492ae06c3f060555debf69', 0, '2020-11-20 05:26:01'),
('66e08ae288dfd0b6e98a3162aa21a40ad1fe83947b92383879ac6ba2ea54978b9c9c6b0d34aa519a', '97ff3c53173badee0524210e6ce1982896bb06b610f23c4ac4115b2960cc0dc47aa011854995faed', 0, '2020-11-21 06:38:37'),
('6766a51d372764d039ce01ab4d8f518ac0ac5375b8c47d363c476da3ba3e345f8e7e236c02d38f88', '41d19c08a45e372e6218f505e608e6ae1a3df65a2c07ccd6d84aa500f9ece7dc0ac45b4159a84dae', 0, '2020-11-15 07:13:47'),
('679edfa552c706826f75e7e1c4b6b330dcd83c3d5d86608192adbff25d7a4689ea4d99c5fe90b9bf', '1d6dc9d0edee981e4c3aed54b65aa5961e05453a85977a9d7ccba8013dd560cfae17888b29e65122', 0, '2020-11-01 12:52:02'),
('684b6ef69c7d7428cba213069542019a8371f8f08e7650182f78edc8e1a73654dba83df465ae5b24', '0747efcab3ce3afd3eb7561fd38b85b45581b7b8f5f8b3783ab48a536d8fe0d2235d95195f772d01', 0, '2020-11-20 11:50:09'),
('68b9b8ce928d0982f840db0521ac44fdaef376bbac7e07fc34653ed414ac822aa633a87315f18ce8', '6106496d553df065c29454f770960e05b149ab1d619627909b3b009ebd084339b56bafe8cd4683d0', 0, '2020-10-24 05:36:25'),
('69477327e32b432d444cd849ff0619db0895c3a46670f07dbd74e1493e674197f7be36cc2f7ef968', '7582f3332076a5368924dc63ae317423bfae2a05c424734b94c4ec6f9adfbaeb9f552f220258d7cd', 0, '2020-11-14 13:59:57'),
('69abb3c562718e8aceaa7ae5c077c2abbc13590808bc5e5d9da0ae8cc9d635468c08e612972d6eb5', 'd85295c22637c112a2250a12cfa55b7ff7aef246aff75201dd0a2b38f9b07ab15cacf5448b742fa6', 0, '2020-11-28 11:34:03'),
('69addfe21ca54cf2159df4efe53526693bba4e7bb16daa76193bda318ae8782c8615d3663ffc9818', '0ea057589e99937f37391c6d86db55282f88b6f934588fbee84a48d493fad870af770d5a01a66a3e', 0, '2020-11-20 05:24:27'),
('6a3e26122a746bc5760876c3b8fd6e00385e03dec069e2c0eb71aa6b9961d1e8908aee787b027749', 'b96984d4c529fedd68a34ccd2d2c08cddf3e7761173a6ac13a4c23666474f7dad83f8b9a50013512', 0, '2020-11-20 06:10:01'),
('6a74cabd71c8fe59c72e0f70488ca4c8be8f239987e73b7ca15b1986e7cfeffc61670e45f6a0dd66', 'ad2cc711293c1711e25e5372aa3ac7b6278192ddae6e0de6721f049c6642f435084e6bd2b0a0f9be', 0, '2020-11-12 07:30:49'),
('6a7666325be44741b83f8a12bc3473279707db4c68711e7c49b28f787385060f6321a165395e121c', 'df1865e72eb1e45a6e4baa05b282bf261a540a8a2864a8c35edc810fe22e91b2c28cc3e273bdd41d', 0, '2020-11-19 10:15:49'),
('6b2fc190c16ed47120fd7c706ddd8c9138bef64dab47403901f54168128d18a262f196e04b12cd4e', '528012f97e852c5faae3d162abca8de7658fbf0a07ffddfd1e3d1b6f6f5366ed57325b7fefff1af4', 0, '2020-10-24 05:47:25'),
('6b4bb20a3016956f6f5252dd3bae587b72f6287bee60bb626029aebeff153a41063f064dfe49a6f7', '77ccdadcdb5ab33fdacda2d1541be213a1ee9866e45e9c5875c842434e9b5935dad518f7fac7fe6d', 0, '2020-10-14 10:09:37'),
('6bd4db7c3ee053cc7ff248989c1e170fe6b017e82322f17bb08769072e3c2ad809ee9e21a4a4247a', '63e7fda809b999d5a154bac1fb90a703439a87066fd83c96ee267653e8e0b46d08f60fb2e5f7f5aa', 0, '2020-11-19 21:27:35'),
('6be1b357d47c6470c9b83abbab253580dedd4a445493f40eb197c6ff4313dc3df189e207f1a9dabb', '5ffca2e452cd1dff3f2fa7643d353aacc164a8b4627c82680ef3d9dabe14a6b657f511c323aef8ac', 0, '2020-10-07 06:52:01'),
('6c33f086ed0789b907246083c6c30bbf904ab36c61cf794c1b82bcafda02accd542782492832fe56', '937f1a6dab3bf8713311e1f8fada01746974df951bdf6bfb34986189913bf9fd13513cba9f32a166', 0, '2020-10-10 08:35:50'),
('6c4d6939275f82aa0393b38d2338d760f1c6f96525980aa42718bcfada6477a656a306220246ab06', '9d5d864b78bb57fb52c626390bc4ffbacf4125ee40a625dcc5c9d9c06efedb9a16bee3aeb4a953aa', 0, '2020-11-06 08:11:22'),
('6ce9d8dccf7612e42e0ece94c1424548b7f880f7ca47e49584e88632c38ef3628a5753170df1dd32', 'fe478bb6d7006d62dc174fec95996daa439b71aef5fee2df212e251c8493236a3524ea7ff8fe58e2', 0, '2020-10-31 09:10:56'),
('6dc227b5080ea52c65731a4ce6239fbe108dd27c7e62a0c9418e35004d7fda0a29fb5241256be3a3', 'dddc5ecf86a82722daf3bf373c4070c15b433f0c523f31c050566a5dfe18050b91c145f4eee00952', 0, '2020-11-21 07:51:54'),
('6dce125182b0a28b5e67aa20fcb702c5c1ea64dc0a74863e2b6505b40a69d220860f8fbd56e6e1a7', 'db58fb5efcb1863476b9866df5e1a07979e53c9542b47235b05b9fa4642751d11b7d04a9f220edda', 0, '2020-11-08 11:05:38'),
('6ded9ce82e9bca90b8c0ebf4b3fdd8308f4f77afd54de4542bf800c1caf07c7caa72d7b157e423f9', '000c964fde2278928cdad76352dad40c5d0b0c228d43b54c68aab09ec9ae9b1c87f5e7a6761ba958', 0, '2020-11-19 10:20:33'),
('6f09b90098ad2ad7bc4bad15f16ef7714343400c4812271ff9161a02353db78e4be87296c1db8818', '563495f70fc0b2f9b6f794f8f7986af65ede2d278f9473f7b75be593a34ca4193555484e9e7265e3', 0, '2020-11-18 04:22:22'),
('6f3a8381c0c511a4777087c9f5a6ad2eba259dd91ebd1df33218041c04ddd0d6e1e3bc07c9e637cd', '2cfb7d6256a0d928f1a40997b673a516bf4484f317f896634bc82560b187a813afb8b0ad3bfc65b0', 0, '2020-11-21 06:33:40'),
('700573a50bf8a64b3584e375ee67c742b52b96a153ad0497cd03a451034d55bfd0c1cdb0507a6f2f', 'd56b257f26e24fd566c7e0194645da5ca532baf53516aaef1e4bbff86b916146e07d95f9a74aa2a4', 0, '2020-10-01 14:09:06'),
('70e9039ea2e4b94953de9258f3ea92186315ed5c3f84c4fe6592d011b7d3e54eb5df04cb8f0b91ad', '2affdfdac266569a5a1309ad84c4605eb215b68c9cb5ddb0bc12df1b61530ce93280ef8dfb347497', 0, '2020-11-18 12:50:27'),
('71189ab155383a250e9d7384bfeaaf7c599b6a335074ce5843298a0b12395248359eb085756910bd', '517ee6eb18660ce281c66c635ff0db30a00cee35c4a4cbe1294b3bf88ac7134bdf6f9362f06e9b2c', 0, '2020-10-01 12:24:52'),
('716a8f1a680679317bbc842a4818b334f586eb1daabf69e483a33a25d929112f8c234650dc4464e3', '6a66163cedab53aef5130697e173ab52ee8b12721c6fb0f24c3a84449b15388e4355b7d3bbece545', 0, '2020-10-01 12:47:15'),
('71b7853c47659b8dc7dd349de70ca9008fb6976d8d7bf8e16b67832e752189e2d4ef788b7858f00f', 'c21f3cb102fb47b8ef9fbe6328039766f95be6270f956b69096a8ba5892f39d9d54604bfd1f8bfb9', 0, '2020-10-15 08:49:35'),
('72ebdd40f25d94e92e5e45df8c808b65ab813749caee9d0a26c0d7896809d88b73d0d8550b666676', 'dfe625131482c1721773bbf52f9361193e0aa8688f18256d1fc182451de8af038dc786124f8e09a1', 0, '2020-11-18 05:59:52'),
('7303591623cac7fb587b203a84d943acfa200bd799c626281afec907bf83ab7768030e943cf0dd9a', 'b36d2fedbb3329c4f89549b5465cc012e359072ce00b7be1e1a58b70b2c05ed37c997e07492d6efd', 0, '2020-11-12 11:00:18'),
('732c330e18d0c02f5b57a28ee5ad4a826c6f6e8b4c6825d240a272ac2a072dd9fc9db7893b83439e', '8969115d302cb1a49dc314f924abed72b58c8ee54f9902fb3bf0e5439b9df4076731987bc371c1da', 0, '2020-11-20 15:14:48'),
('73e9f61c1944ddd5922fe0979031955cab1e6c05b32c4e592eee8f82cfc1f9a3ef35b8294bf358f4', 'd3a75dfd15d7df9a36e8069c6f00f7c43fce6f11982306633d9dff66362fe8d195977f9dec8cd6c0', 0, '2020-11-09 09:27:55'),
('7505a84f4d99d98b993c8ede07914a67a0f8a7dad9790b5678f6e685b44cc4e6cfe1c661f8d7c0f0', '8bb1f6314c296cebc329c3a647a3309ac687b59b5e3557e62be46e749f66c8ad24cb3da2810e64d1', 0, '2020-11-19 13:45:13'),
('757352bb2b94ba7ec80f8dec88f0f5eb881110fdea093ada5fc975aa79c9cbd3f6f7c67a493379b6', '6a9a8f19756f73118faea5e16fb97c59b7682cda70eefafcaf19bc62ea1bcb15a4d1c2109211260f', 0, '2020-11-12 07:34:10'),
('75cf503e6335fd2b1f9dd65a1f25488462b6f406e7299919edf0c9282a9749f2667bccb48c272753', 'c0eeef8003ecb11ef19d05d2492c8d8718b492bce155407dbe58812abb8d28f2e9d2b08abca9bcc7', 0, '2020-10-22 14:31:45'),
('75ff9d917053046b8b8f791d0c097068378a8946800cad8bfdba8b9f43c2b2052a9f11e3a65ef18a', '904fa5d15b1d84806dbf8aa06407c400851853d8b7fc997fb3686ed39916a4a470a3ee17b108c1a3', 0, '2020-11-11 09:25:57'),
('76678b41a1e722e113125ce4353780948ffd2a2e59ae9d7ba79a9e873e886ba3c70b66abb3840d99', 'ca36cf8708ac96d3a7e6e1c96a48316d7f1fdd7a076501e28c81746a5da7eb54af02269a32c0a6bc', 0, '2020-11-11 16:33:31'),
('76828fac79df0c33056a86ff49bff2107a6edd7c6bd0f651b4cb17eaf55554b590943cc4975d6b73', '9d0b98558581df027183885d604a3b6ea30932e305c39144259e60cde6207c62b16c4947b3a74570', 0, '2020-11-20 06:42:01'),
('76d30873f6b8e06fd73b517d5aa2bcea424f5ba0d36ae629ac1eb3522df96f30188effa4c5574d48', '2fc9ec78a5bcf0c48b83df6ba5dded27c3dec6ff23f6c2ac9372380f6ae1b84a24f4e85a8f288c2b', 0, '2020-09-28 09:33:19'),
('779fd3828be3c28c7970dc04d176b5795b1f2838b1232f723fefb25c5bbb1ea04515ab61ff95cb06', '63a980f07f12855757419b61fe0194f8df1537402cb6f5d9d6a88142f843a31712499402a73a477f', 0, '2020-10-16 09:27:30'),
('7815c76d226e883cba60a5b32c3da9802c467f0e3d113f6dcccbbd0bd484621f032233ed2105779a', 'c498d1d52b7a0c944a5c0b31e9172ce0d7f09a8c281d0fb604285d6dce4828ace542cfbfaa4511ba', 0, '2020-11-18 21:06:38'),
('7880832dc84999130baee036869ae3dfe7203b7cd261ff494a3efa2d641fdf5fcb300e3f95c61e88', '5e798c0b4015f71003d0738b77deb465a23965afbb31525d115c0f8f6fd66d70e37c564076256100', 0, '2020-11-05 06:12:27'),
('7892e5be77b4be00ea129885df4f8905a9129d6cce674c5437fd62b3734edc415031942878d38dcf', '95b8d2e2cff4984d9208bc575d7e80edd9aaf0d43f81eddc5b6ea36bb196b5b2f246a496cb1c393f', 0, '2020-11-20 13:47:20'),
('78baacfc2b21ecc8b6e567e30539e098438cc6612de7ff95f6f6abb97dba65d1649cc645ae5e6540', 'b9497e41ca951e22c95775350e6ab0ef49dae15e0b892a76ab2e9f5ce1d56f73ee7fbd712b376108', 0, '2020-11-21 06:08:13'),
('78dc8bfde1d66e3b76800b745ed12a1f0abbff0b92c6a679547e8aad15e1cdaa93fec2992fc58cc6', '070d4d24eeb1ada1bc3614b3880654c3148fb30b3a22ec848837285e71342405a358a17b32d5f508', 0, '2020-10-31 14:54:52'),
('7967f2114611aca7efcabaca7616e29e99ef8aca63363f2929688f5868d7f06ca2ffa1d0481c64ce', 'bac9a34bc5af873b4d0e86e5018c484f7c5b72fb3f3b2311d277af6bfef46a06518073393f8a3b72', 0, '2020-11-13 07:47:14'),
('79728ea72cd16137ab16f94adda2b32e65bfe5506391f783d6118fda00f9677a9832e9ab8086d35d', '8fc9f0f5e1617718f79bf009f88eb4b50af61e2e4a198f62e8d337de7671b146046d2b8351c1a359', 0, '2020-10-01 11:23:06'),
('79f45e18863316e2f1bda9af50825a0ea80e50f09ee4ec900c82da65d05c640218567e5b210e8481', '8a9f616b73dd0a6de0f458efe06840f9394be7b23d2aab3c99d2f1853f881d82cd6c177d9b6a0eb9', 0, '2020-11-18 17:18:15');
INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('7a7faa7a9325ae95776d2954a41c5dd850a16f513f50d2eeae5778d990f8b9a958f926851b0dd5af', '1a36e503f0166c1ce3d443bbe7f10a190c6768b0e01916efda4e37e0fd3f796fe90e42ff73b99090', 0, '2020-11-11 09:22:01'),
('7ac0ba33f9375b0f73ae1605b9c317046b2f12ba15d2203031815c140f9b80fdcfef21a41281e23d', '7560beffb6d2df0e30129bad137fffb2df5bbf17aea0d4ba2f058c4b752389dcfd48d9c8748331e7', 0, '2020-11-18 13:21:51'),
('7b4d0842f4295ed5477a4c3325d35089e6b0c5b963cccdec45cc05f0710c62d152553c33a2c5dc1c', '2c84bc0700f745c1aef296ae3249d52e9bfa4ea15ed6555c7723cd36b6868a95c45d831bfcd03442', 0, '2020-10-31 06:47:05'),
('7b8c34c5f75ca02af992292877c84443e9165f51ea1286aeb47c18af0c28b89ad11521bbb8500ae3', 'cda0dfc1a43adad1708d33600206ff3871e997edd9abfd80497ace61f69a91684be434df52cda513', 0, '2020-10-23 12:33:53'),
('7bd79e6f1f708e2c7f89c48d653f30a737b1498d9dab9c3e42accaf47321466829dce812eb70be5d', '6e91de623d3bd9ddab90e8bd9fc21259693b198d230488f6058b97a34cda52a0452cfb1300801be7', 0, '2020-11-21 07:23:13'),
('7c7921046bc40edfa15d8c4280bbc68c6a5c9c1c8f0a557259ea07217df9c7b4b96d94f2033d1652', '3c5aef4722e440e7efeeafa86ec8a07553fb589f2e76ab98e3c7d26bdde584dbf0b38df6a75c5fd2', 0, '2020-10-01 16:00:24'),
('7cb8287a74f7a252eae990bcf4e7677084df41ffa6cbb7a20d2b7c4a43473a268ac0481d9f1974b5', 'ebc0d059cb3048b9303955673d951d106214eaee8f292512cf5a08932f1a36b08ad074a456a7a843', 0, '2020-11-11 12:44:19'),
('7d9e693e847b722c9ac2c755f361551f981eedc272610ce3a4f5605db3609ffdc96c87b804da0c93', '79034f2ab50ea8faed5be0e4d837e7d08db79ef38cdd31dc965596fe6f7b340d60838fd983b3fd76', 0, '2020-10-23 18:02:02'),
('7da46ba5308ef29e2d709eab51abc07b68a3d9319797cfb070b04ba50e597803edec5dfbaeb22f1a', 'beb888152b4da2b01c3176ba3072c0d456addfea4b2f39ac261be57b1220ca6e53f5dfaed1036fde', 0, '2020-11-20 07:38:31'),
('7e8d91f637703b91e42fbaf9ae00c07c25a219104b681173c93b7ad7c3e5411fd2bca2d4e67ca62d', '65b5cd93f449fa386a8408527cfa69926254b98261f6c79c23ae9e3ac381b6443a9abbd119988e5e', 0, '2020-11-11 13:11:35'),
('7f8fc50cb8b86dba982ac446321fb5c4e9b59f8732359d514f08385e0437f22d617de0f594dcc8a4', 'ce505a2c4158b7eecb2f8fca374a405bd362faf87d2ba7f1f73e519c76498245ae4198ef5a9c4b47', 0, '2020-11-12 08:56:04'),
('8006665d1c6e18b01b0715ab191646a12094eea27bd0bbc3cc36bbef2a7ee765e497834c4ff820d8', 'f55418054229cfaae54bdde52b0e049df1520e2153ec4b9739ad4c950f34cbe0ed021018e9f9f2c2', 0, '2020-11-08 11:16:49'),
('823a41ac4536a7d9ea7357d0a7e21bda35bdbe34ce28c939dc4ee89a43ae874eaa33f9d87baa5b61', 'bbf1308b47d11d59bedb02115f4e477af2d5cdd9ff75990cd59bda83061ee51009120073df2be1b4', 0, '2020-11-01 08:15:16'),
('82cdf8e1d602ac513c31536c25eb1b9af4ce9ea97e54fc36ba4f1f6350c71705e8009b036125f0de', '0663ddecbc03a16acc1ce8304dade3681544dbcf725d4dc4fa3c7dedc0b3819352b9bf271e72d09d', 0, '2020-11-21 07:32:43'),
('832e498a255cc230abbc223bad9c793ef1c6dc55dc394149ef93b56fd3077b2c21a5a0f5dc9a1451', 'd1bc9cfc02c3b95e919459ce9e0cf1bd7dc324b011ef9958a8e75c788164d57584a6be82c702d55d', 0, '2020-11-20 13:36:34'),
('83eacb967a6a72dd267025e2a37a1512611c7bb65e29c56f4394a8ec078598f8cd212cd2bee7f66b', '604146b995f686e067861490718eae1ba4dcee8c6e698501230cda5ecc142edba3115c5e404367ce', 0, '2020-11-20 13:19:19'),
('8438f7d0cee49daa88f7418e672134b785f033378c1fd0b40ed0072433d73fcd4ed47945c7d469fa', '6273d51fa0854786cfa0c63490b7e79980ba72a3ec3583bced915b4441e28c78d0cc275d405ace86', 0, '2020-10-30 09:54:52'),
('853468bf960307cb494c14fe1cb75456446da1dec0259955597e7a75baa86bea72569770a5bad16f', '16659f2525e612e838db600b94719422734fd56768f5d76347a60d8ba8bdd079fe3b6c995409d683', 0, '2020-11-21 06:15:22'),
('85c22e1007985ff27c4dd5e3f3b2105a1dab93fc540ea62f4afce29302a1743e6e2d2d066e705792', 'cf0eb60e2ebff5268ee2e79b26eb5d4273026d6a36e135b26242164799cd1cbb3b10cb96dea35238', 0, '2020-11-15 11:21:36'),
('865b0eaf15a6602b8eb77e58143067849706ce6760804e74e61aa622a37ab71c9dbea6bd9f933aa8', 'bfd68dae47290ed64b082441f2bc99510bd0575606bcedee176dace7af4e8a39b3d9f7c8e74bfdd8', 0, '2020-11-13 07:48:38'),
('865ee42e334a95df2631e893c28098bc38c58daf385291862994aaf3219499d9f7221fdfc637c685', '2162f20ad2b9cab7ef5f39bc91395ae756b8214486226bdd72b58b721393e6162f94adf6388c3ef8', 0, '2020-11-12 11:04:52'),
('87f1c0a07b156188197b5cde1850d83486a51da779d80220367867d6379f570a48024af754967a95', '00bdc103f66694045f679a1e1b61348da63650e2531038d09bb7390c05c4ca1495b06e24e1016476', 0, '2020-11-20 17:22:22'),
('8834e53cf43eceea287b60db579a01e384059a975997d7a93c5c97e35efca77c0747fce95db0d373', '8681a4341c3a7f5525261d37127b65b75cfca089260aa1a24146045a506bae6b5a2096b4f4a2fa0c', 0, '2020-11-11 16:06:53'),
('88c029db380ddbd2835fd07185b970b2f08ba7bb87a8d74ab907e1613e558dc7ab8331baa331d5d5', 'b54f4c5a128676fac1825e5243a4e8d1abb38942e372a8a5c77145df514046c07e5ae0f1f832f410', 0, '2020-10-22 14:20:41'),
('88fc13d57bbd2ffaa119ada1c3bf95318a9627f1a6b86fabc631b7a075ca30c23f6ef310f5a0e108', 'efd99e87b8881830b96797d1db90b8a8711797cfc536885fec3c85dbc2c6ac8324f397d4c65d83c6', 0, '2020-11-12 07:03:03'),
('88ffca4cdd6628604ab02f0173687a60df4d5a4215e2a0859eec49fedf31f217e5ead802fb63dba9', '9220bb17876ca64b527746356cefcdc87dbe5469b32751a00eeea5f389d73ace0635cb2ad5cec09d', 0, '2020-09-28 08:09:14'),
('8921e71bf41c3978f89f23912573748ac3c95a336b06a0d0fc66198964929638be59c2f6e4e7c4b8', 'a2971d827499b6acb60e03502fd333e5b2100e14bc220bd7f010ff498119d72b5a111cdb30d03d74', 0, '2020-10-11 06:00:13'),
('8965ddb99966871750362b9e70d1158f2eb5dcdaf51bf55511c42b7c3bc37f5fc04953bae726d5ba', '883de0eea60eb5eaa67ce89752993025d140c6d82ac21055994f1dad536fa2f2b2657f4514de0f57', 0, '2020-11-19 19:44:46'),
('897c8c00200fa710b555e7a6764f0822f59455c296151e6c9b3f1d9a3141353a5bdfa872bbf6f7ec', 'd8b482a1ee81c856ece52fc60dadddc9a28a1c703244d51f4fd8dcce2c37ebf2ead96de65d648c5d', 0, '2020-10-16 10:02:35'),
('898ab1297a37c9bd7e63b543965d0d2c622d7c2674e882db12ee42923a3a9ce35ea247c6a3f8215c', '29ffe7ce3a27ab1d39c497b5a0fada6799596e336cc4865171332c969bc4b8ee9c39b3514a649f6d', 0, '2020-10-23 10:36:52'),
('8a003a6f4ea33028ee7fd652a6c13b72c0e0e485512bf7b922170f31cd491ecba2748924cb6649bc', '1eee6ce18cf6ca6c1e00b01035d4b52ae8f9fc4c9ab5ab24c2fcba48a26049cc0b54a0da732d74e9', 0, '2020-11-22 14:41:40'),
('8a4ae2cd6a08f52b8a5c2a25c42285cdcdd4e1decc90abb10a0d1434a8b01b2dc622dcc6b74e56bb', '7d3cadcfd71fb5f7c4798fa2c249cf5b9dba5c41f49a9df42f3105420cc884c2a4ee72500cb27147', 0, '2020-11-13 05:21:26'),
('8c037d037fe2cea4392d999e366f32948fade743fa50d739c13bd3ad9a5edf02e686fb4d8ce203fe', 'cd41736752cfd11c3d86ad55c3ef23a6040c9b40ced2498602d4688b51db39fd4d7a7e945726b920', 0, '2020-10-14 10:20:03'),
('8c2bdcb11588b6ccffd81f3876b82ebc32f0f3275d6d08490e99f8f95a3b4f768a20069aed3c7356', 'ce94faa8edd0536697b235044444a3dcf22eb97a8e528c34a7942a1d1135489ad547282482a8282b', 0, '2020-10-30 11:04:19'),
('8c429942876dca2ace9f8983bbee9f48c4281bcfbece01a26dbb66365cdf1b3d2fe22b8d595da25b', '77a29670a6323d98d4399bd2c164860d6508f8e8a826df248c502d1be395262af0ffd046eee32c1c', 0, '2020-11-15 05:52:44'),
('8ce927f18ce0ca290c9dbb5b0c3fa6af5f87e366647659e949f19ca991b08c7fdfaf68cf9d133f5a', '548dd824ca1cec087815233d1ed62e8e8eef710184a5ee9dacfbcf14b87ee926e7f2420e29b8cf61', 0, '2020-11-11 10:04:46'),
('8d73a09131c8fd13178da883ad149bc065206e9effccb9613c399656aa15d1e25d8026bcbfc203e2', 'cc2de17249b70196b27226a936ab7ed3bb2d9830b4758d2b72fd98de0acb18aaf34541d3d343fdb1', 0, '2020-11-11 16:10:53'),
('8da4ec9f5d207f59e2865ca0fcb1e4bf8a258a215e183467f1b6091b264c5edd28141f676ecabda2', '18dbc5a2957989e3680208d674630d2b09d2287be13dc6c212aae42920c1b881f8379e4883d9c3e5', 0, '2020-10-24 05:34:25'),
('8dbb68fa16cc1e018daa6fbaf7f5dbcb78e1ecd89e9173e642d88076c725f29137117b5ca1eb8a99', '5ccbb8eb590d8a0eab3789975b503fb57d2b0348c25d15544340ded623296f915f8547627a6bd8d2', 0, '2020-11-13 07:38:48'),
('8de742541b90e277a4abe9d3eea280afb5e0cf1558f94459756710c66e1f97f9e68cbfd374556cdc', 'ce0d583eaee584353c88da0667a92373c2f8fdc1e35d3931b7d124d813979e68e78644c3141157bb', 0, '2020-10-21 14:31:41'),
('8e41387a36d660d95b01089998768240f92c59575c29b90080754591e30b739e75e8a10d96f04db9', '14df868f01d370b0b3bdaa300c44792806dc18ae842fb503e96a17e0d9fbf61bb8e8b17da821f9dc', 0, '2020-11-19 10:28:19'),
('8fc537b12759c828549e1b7dd4d7dee4bfc6c496e71c0deb39c4c6799d2f94f8b340c69098a9a374', 'b14fe7defedc623b447b1490cd127a1d0c02e670abf1c3973a348dacbe60096804a4ff6c5167a0d8', 0, '2020-11-12 11:25:32'),
('908554ab9dc3aa6ed18a80cb26ebfe0e8562fe8c2968d74d2fcd4d830831e05a076962bf680ffd57', '6afe3b564f46a9055e0575fb63920eb3212424a07ac0177bbce7b03b8030b47a2531d699b94485a9', 0, '2020-10-24 18:43:29'),
('91167bb667894cae82517d6ead455c0688ffcc5deff6ac42174d17edad250ca41d09844c27ac7994', '285dfa25b43b650f91fc053c43e1654fa84633e0479f75c79a6a861e9068af7e8aae302f557db703', 0, '2020-11-21 09:33:59'),
('91297b68bdfad628819567f086a02bbb75898feb730e6fc3d407e3d2d86038004bbf2de8ffdaf4fb', '50dded796d6d5e0776584f29735fd6e88864f655f2e5c30b269c5f3cd846e90dd0d8bbf7c8fb2c8b', 0, '2020-11-12 08:58:46'),
('918835632ed6a6e72fd6441a6aced25ac3ce18fb1a02ff12ed766f03c4a3501b296abc1a10572ca0', 'f4a82ea453e5f9decfd43b871831c49283f82404b8fec1794fa40171207b004e073cbf22651bcaf9', 0, '2020-10-31 05:30:36'),
('9207749526400d42ab06c4b2819f70b95a1191c86fab1750da32efd964a74cf7f7947487578caea2', 'bae3318ba2c89269b715d3d2cec63bea67025875158774f7bf1ed0341a4e02a47c10bb85e4b16336', 0, '2020-10-31 11:06:38'),
('92114faa41a7e7a2e6d1ad3b9df4b8fa551bfede490ffeb5c32c262dc5fab2792132b858e9425486', '8ad8a4496f29d90c0298aced305c30a78065ed7fdedd51226b97a85eda773d0802b81d9dddb3ed20', 0, '2020-11-12 08:39:25'),
('922911758884507c98164e09f973aa70541ddde4de61d406b9d24cc4dd32bc7c4d40fc438a5bc52b', 'f7b3e4d910c55480f1b1ea861341c8dc040137062219049cb412aae069c83843ca8b3751e05e57df', 0, '2020-10-24 05:16:27'),
('929b0b1e3ab8c170ad7df3e247b9a95ddb4d975b20dd51d918d3338b44d08b0b429046648a7707e1', '01c4d51a0422bd9685a314a33633ec326b87003abcc6fc5dc9ff31e23b868dea60c870853bebc033', 0, '2020-10-31 11:29:34'),
('9325883db406c23a46a8f5d290a1f432b8053df1b0bd9c6d3610d16003121a4fa9db0e034f557cfc', '8bac296e6d1505b200b5773a8418f4e77536a9891c160c5106d33f8d4677825a3bce163fcd214d67', 0, '2020-11-19 13:55:22'),
('9350f6147846dcad1ef00b8144ba99499d8e24006cb6f1239dbce3b86ba554c9241409df6f2ed383', '0b8af389ac3a669aa3fcf9bb7a2a714271dd647c5f39f284dc9a0320a8845b6d88ec49b8e8eeb667', 0, '2020-11-20 07:33:28'),
('9372aa8d79cdbf3d39867f9d148d423c05fd56e0e554268dc4d3cece71d2fc756879c650144a5863', '5df4249d1fbd20988df9b3dfa48b9573ae41db7ac00fb056218c75fd7c38495e19dc72cb58c4bda7', 0, '2020-11-25 09:43:33'),
('93a1035b91c74d59a23cb8b71df358ba7a99cb41ea1e786a30d02263ac3b86e2b6c85f3c19b382d0', '5fa8e38ae5df698d4020cdf2eeecc820b284491926bfe4d23ec88305ea74b3bccddc3aed73377474', 0, '2020-11-08 12:29:20'),
('943b6bbcc380aa761b37ed4a88188e50f58816565bde9b17d2a75e0d55a67db583c24af432818cd7', '0efc972d2d349b96b030e31929f48a0c894b4bc5ec6fcd34f26094955bbe6bfbcbebb3d64325bf77', 0, '2020-11-11 13:53:52'),
('944e32e2ab6a8b199aa25a8324842d21445488f431c6725aa9a73c6643685fa993543314d9ad66c7', 'd8507d4cbfc62a8c05c97cf382864a8f68948d2e953820fbd4f83f6b6bf4dc3ca2d8a040dd6b25ca', 0, '2020-11-18 06:19:58'),
('946b55341fda0e6c1bb576414566ecb90610e5cd904ad96b6e49e06bb303c8b41ec3c283219b9ec5', 'c0a352885eb8483ed4cc0169c5d5d61c931eb8c83884d798c972251446ac08504f9dfb615c412619', 0, '2020-11-20 06:48:13'),
('94c48fe45dfe2e9b74c99e43d61bd026cf7c28b8ea71c31c1b897aaf9cc66dc59d56d31aa0d8d870', '6244703ffcfa5d12e14e2f86ba38ecadc6d5411cfd1dd34597f81c3417129384f288ec51976ab5bb', 0, '2020-10-30 12:31:28'),
('94ee8bf588ec333d50b2672958cad13651c5f06391c92eecc3f89b1a4103652de73612413afa1f3e', '474d1d2a70d48a57ae292743386934c897df96ee05540104a3b211a946efa7ef065a7576328d27f7', 0, '2020-11-19 21:11:02'),
('957614e7b9834f31d9108d426f865c3307285b5164380fd9c013e4cd10a69f582c02ec0ef7aced30', '812f3d9b62cc75dd10054e23aa70094d395014895efef752f8f88d71df7f96e90951a4bfe996cad9', 0, '2020-11-15 15:38:53'),
('96bb6f5ef3854eb526b9c8777deb2c920ac6ea67fe975fdac4d11150ac501d7f8f21522d381b5521', '213807fa8c1e47bfb067c7cacbc7e0422ff5e6e8708bc7e5cc99b57124e7dcf8760521177d237af2', 0, '2020-11-20 09:28:10'),
('96f250cc28c92810464f40b0df9412311bc153ec5467d1196393d44a35fb2cb9ba9bd39ce3386834', '81cab93df059508442452c18545a445f9766e49ac20e2f76f06ebf57104c6cfb8a9d8a12ad3ae824', 0, '2020-10-31 09:23:28'),
('974b1b8f8ae6a29077145265dea47654a59c1701169e480d4e27d02fb433721dc65df210d9c84f84', '0da21c5c3c0fa7933185c859120b232f17a827571cbf23e8036c8a75ce22c17d683baa72c22c8d95', 0, '2020-11-15 15:37:18'),
('97e0880dde0e69f93e77bd8ad9cd9f2d78bf303abff1dee6c867652536dbdb2cb62aca994f2e01a7', 'ab70bdbcf8e4397d3f04333d121e0526985ee8d5e35a2684312910afa590ef333064c4c3481365e4', 0, '2020-11-18 17:14:51'),
('98079af4b139f6449ec1beb501e4b63c12edc6266dac79448aacba01bde74be596f234a2d029130c', '399367d1e7967bf19db69b71e9997b168c5757a05f9a0c960dfc12d4ec9bcb8fbd149ae76ed6c101', 0, '2020-11-11 11:58:33'),
('99130f907f018ffa67c45bd1a1cdc86b9c82a6ad988727af4f422fbf5b4ebb939e3e34c4110bd9cf', 'ac9a4c57fd9de89890a2cab442cb561da54790398272aa900a6181d8ce285b354c2d153673fdeabc', 0, '2020-10-16 05:48:49'),
('9970a02fcde52906a84631ff514a1ae02ba29216786aa358e2dd299bf0d64c3b5e62bf41f878a067', 'e3ab708d0058698279df20d2ef3060b65673c5188b19627b533fdb57fe60e1080513d21fadab5fa7', 0, '2020-11-29 10:59:02'),
('9ab9a6a2d64c076d0e8aa35a2836a3befa8427f840fbe59c4c3b054d75b9e8c822eab8daea009920', 'deeb4cb8df6db601c32945303b26845ee43b05bf2dfd34e416102d610d72504d67b3e4444a6190f4', 0, '2020-10-23 13:19:21'),
('9af0daa7edee6736d58576635742af797fc822fc1f66b280300b5899cd0ffd718795a34bdb2fcc15', 'b5b7b572ac57ca3490086c51be6350223cac5eb6dd57b2b2f3b73060fd54d8dbefed13a41ea1cb39', 0, '2020-10-01 11:07:26'),
('9b079560b6e96e259731a427288691262c62890ee24d11506c16b806a7da34b19d7da2ca8c2ac0b2', '5fb0d95a1cb77c60a7c09f9340fe7070cf7dd6c458b3521d1d3cdc78467910890ffd97a0221c165f', 0, '2020-11-11 12:36:21'),
('9b2112cc264ce9ffab37818303bc71b1dcf7eb6220f7591aaa2c974d8d01968b4664f1b64ae65c90', 'cc1d2de08755c1378e71260b0bb2b84512e946b941f2a6fee455d92af92d779a9e49ee101a798442', 0, '2020-11-11 12:34:25'),
('9b9263e5b5a9360ff06e4ff4085926c9ea8d9cf1f86b4431b5c14257049c06c1bbdc79a96fd76c74', '57c0e67a1446862777aa62cef66990f428c0bafda718d8b0cefc364eccbaa2316d8f50a163b92d33', 0, '2020-11-15 05:56:16'),
('9bdd18da57748ed19820fd3ad7050f72d0a16f0dcd675750c11ea8db19ef8a5e9d6cafef5aa605b9', '98537734f5478ca2d8e6d3fa56e5f8b7938788f7c40b1b23d271f0a55e07ab7117b069cb47da493d', 0, '2020-11-06 08:10:57'),
('9be73f239504ce88bcae46d48dada2581ffa16fac727929797820de44ef17439b008476548456289', 'e46d61ebebc41b02522fb67daf23d44874dce200100e8b63442ceabd5c1c1199fee0d4ec081db4b7', 0, '2020-10-21 06:25:12'),
('9c4d3156825c4525980fd7532378d983d0a246a46b9eebc8cf21f6c9227a5f79cb035e1b9c4312a2', '77ff23ec6bbf20555471bc8340154143c8a9f541515a3d38b84e78d03b78cc01e663e4f53fc0e697', 0, '2020-11-19 10:30:17'),
('9c6e4196c7b68d5813eb0fa8eeb30f5863b6e6fdcc44f17067e05b620a31b9772dbfd3f1b977a7e7', '05661f183cfb24573f4bac712f38b37e95d0aa3b6bebedb751b0b0cde91a1f35c8047719a544e671', 0, '2020-10-30 11:07:32'),
('9cd95eb41f123cce6ddc804f042d066458b7d87518137316d0a006313bd99a4e48f73ab20b3ade5a', '818bc2b70204fc53480dfba2d865aa204f72f0e14f1dd89d7b6f9b0b8a706c11789ba3333c336618', 0, '2020-11-20 13:59:02'),
('9d30b06866d00a616a2e5c62a90263544b8bee6e5b0b70b408a85e13a48309515913083d5baac7ca', '2de8c88febbc01dc79d09828bcdb4503a4bb796c01b386b04b945eaf3c122b87133771a988c4a5b5', 0, '2020-10-30 10:08:08'),
('9d7d910844bce36baaaf7626db10587c7fef6253deded8470fafada0bbf6c34ad4b8d5c2b6b3da3c', '262064f61c0950f058296c74f8850907d551c90b0413e2d2dff4cedb360a0146610b6e58e28c025c', 0, '2020-11-25 05:52:14'),
('9daa82ddae21cd3c7f5c6d598e8a214e0481823ea45b7f548fc49527c3320a734bfffac7f7154d3d', '6e65a8e134d6d9cf30ca4ae6719933a74fb93e078a0c6409a8ce9a1b05ee908939936dad474f0d17', 0, '2020-11-19 15:35:06'),
('9dc17d59d2aa54edc6cb0def4c54d586570ce34ed842c4d4445bc72ae8e9e18b0078ba88a16880b0', '4fc57974436beedb61b34ba4dcbf174ce4c2521d2de5e7daf8c12573e8ff8ade832c0efb1e3be0a3', 0, '2020-11-18 12:24:46'),
('9e42d7e9097a875266f225c569043eacab44bfc77ddd57dc8849a852a53551be2d37c328a58415d6', '185447e5400790975c75eaee58b1245c3e614a1fe4bcb359d399f70bff225667e8ef70aeb6446fed', 0, '2020-11-11 12:44:15'),
('9e76f4ff727d0a347e3390715c2819fa637ab41ad0200ae6903820539bd64a95d85053485d45c911', '4a40cf2ed6640efe37f931082e1ba83c4f557aac09ebdedbd1e5d1e5c7c62b8f38cd48390962975e', 0, '2020-11-21 07:15:14'),
('9ee68046c1ef5b2799192e641cdbab0d01584ba4a1d900d083edd2446bf284c72f58fb45fbd92da0', 'f4fed4ea26824d913a1420e65cafd108a9f49138b08a826138e499c2f0ab3d364136d02662eb20bb', 0, '2020-10-23 11:10:12'),
('9ffb9cb413d0fd08f683fa54176ebefb00f5a4ba90dcf0c6476fd615e602d1deec13957230b8df98', '4f3407658dc0262c14826b0fa2f3aa01bd2f82ab30713c5ceb11bfba62507fe75895a13e206e130b', 0, '2020-10-25 06:35:47'),
('a0719cb573778dfc9fb5574c200a936f71d012a4710869b20bb5bb29ab4e672f1472d443e7d90d00', '5f332ee3f9c8cd55179631d47eb7af8e74b7d57ddaad6e95e87a6900e037b4857bedf3a613f44d6e', 0, '2020-10-31 08:04:03'),
('a098b90acddc46b4852cc6dc4a9fe5b81c2cb407ac50d5b1a1a6e0d5ee2033a49fedfd73b3400520', '74260d2f30ebe0285dba005bc12fad9ed03632dc8305cc0186c052894194a239e1e0bd3f187b4e54', 0, '2020-11-25 05:20:26'),
('a0ed50f4457024ce77a50dfc824c8968abf28b88e28c07c6bafb3f7ab236e3665f9c55a8d97d5678', 'cb1dbb37639dd3b4c43e99f0f829b98eb833ec86e514edad292aa5e33762d111305962dfa0883b5f', 0, '2020-10-31 12:05:20'),
('a1cabb3cc6f60b19f74736a23bc0c3b5bc30c14022b7d5299ab207dac2719ca54eb1a819fb966f02', '9f4d4e8f00c1db230e4483f9950160c208859076303dcaf1246b5c79a2cf68dc40cd1508cbe49cd5', 0, '2020-11-19 15:37:29'),
('a1fc498199d6249b5ca6df05fc9f71f720cc63c2dd956a7f0f804ef8b2c394bb0de22252aae8957d', '9c66619955fdf8bc6175e7611ea2a412ed40e2b2ddad3cb38e5ca80be2d578f8c4821d186d41dd35', 0, '2020-11-15 11:14:00'),
('a3028d9062a9fb4812eac1c9ab98102eecfedd5b2a9e205173c30266bd5f5b1b306d4436c6617f72', '93071ab77c62ff14bbfc509f061fee687b84dcddc6e8e0e0b0d28d1944be3e180000adbf802ebcc5', 0, '2020-11-18 11:43:06'),
('a39a25a5d090ed263a4a1ab9c2947369058a9122f8772714aa079da627a40f0082ec24e749dd6fbc', '29af3184fe7b48b02d16312d863dd7e422f4a94636f415c3d7f5b10f2e31551b912fafbf4b0707c9', 0, '2020-11-20 05:21:12'),
('a423202959414d3edecdae9802f76762e42b9d60c1ec10d0c71b1a162077a81fb14a14bbe83a10f9', '2132a5d2ead724f0e79f2311e7f99a0e6eda3dba35ebcb6317afbfb3716bb905e1c2c17f48fdb6e8', 0, '2020-11-08 12:47:18'),
('a4836777e4ab76c51534c27b05d184ace67717b2a3e09afe1f0b07d11398b8559d1a46a748504142', '1473743f68baad433e4d832570d1724bb0513f60e007ac3d8c0e38a53f3acd8699937efc06b1d8aa', 0, '2020-11-19 13:50:12'),
('a4aa3cb4997a500d8ac8609816f2d79c9ba7160b39cabf70ffb280aa67649033262ea97af60d1a1d', '3aaaa16026a2fd2913d54370579ce46192c0cf4ef14497ee210fc4c73fbfc86ff6003bb94d192b85', 0, '2020-11-21 11:24:32'),
('a4d54985f922fc430581910f8279416ae894752e278065bd71e9fa6decf86a0b1a097b644b7886f7', '29642886de1c4b42dea108b6257414788a31d5ff16e35463f100c8414e3a299721e8a36ada1d704a', 0, '2020-10-31 11:34:06'),
('a5832dd6a129ff7db6c09a5610c5ab84424a875d9d0871378529214ead9230d12753a1657de5ab75', 'df902efca282e76c38c6bf87db6092c225218185ff48a6cccc416455ccc4916b23f88e7b5dfa0134', 0, '2020-11-21 11:25:26'),
('a5d60837dd62549f6c9d53072ea61fe33347c57893db568930cac0e08f8273f1ef64ad90c56aeef2', 'ed6997a7e1c11302ceeff623da045f6fa4c5f97922163c39825f238316f2d5fc5a420758c6c4af51', 0, '2020-10-30 10:23:25'),
('a67ba89108f29670f3f79c8bff838a49b6a2e3587c08ff134a017267404bcd3381a900d8a86416b3', '2f21fcdea9fd9d8ab7a568ec41d150315f3994e74c2532978e137182a12ce8f0cd107b07d6fea450', 0, '2020-11-21 05:09:36'),
('a680264faf70686e3ebcf77057b2a39a6ca402eda116c6abfbdc69d7129a6b906aa4e9055de1fb6a', 'a12a8d8a17fb6afefca84126be1c8dde1b941ae57ce0c841cb37e5420da514e78a0a8c3af273dd07', 0, '2020-11-12 08:40:20'),
('a6cae506c0aef650201c90dcdda070fe287e9e9d9ed6f53cc444a181f01604f5e2004cf9591e7539', '91f11fbb3f59d709e52387787788b8444006535332b36fda11b11ee509ab7c326279a001987d27ca', 0, '2020-10-31 09:25:45'),
('a76538c155c187d05cfeed6f262fbad78eabd0c8f1b98b48b97839dd0c1f34bc6ad678d9441f80ae', '4602dcd9cfb0a0200c7093c7211314e381453fc65b1323acce419adf4bcc672c8901c50e660cd9e0', 0, '2020-11-20 02:51:42'),
('a788d11af89114b9349d04b80516ecfbac8c3f3303aff3d9a3658d7060e447e05caa96b2719a2301', '41b0ff47c7e639a36bff380d1c734c253a2852e2285309a3dcb7b13d64b0351571d7c9670ea058e7', 0, '2020-10-30 12:26:57'),
('a7eacaeb9ec1c27ce18421138c3602f957fbae8ea5864d6939483b280c7b37293079192f93caf3b2', 'b502dc133d97d07b77552817da08ccb43f88ded0994a1947d8a26edf54d3899a0c7927d10506f5bc', 0, '2020-11-08 11:55:11'),
('a965eb5c26201fff46fe48357c45f74b3aeb5c5a7971440a8f542d6d2b67eef752e1688fbb7829ee', '803dd415eb304bc84b123f071d008d38ab0f105a4cc1c9250c4437aa44ee16bda69d3dd04ce6062a', 0, '2020-10-14 09:53:41'),
('a97cbf9a20e75bd00df8c66dd7078eff8364d8ec7817ae3c95be4cd5a28de6ae9b7d3de52624d1a5', '6ed249fe6b85a78933fa038889e989cde1e33393dbed8ef8cb4292b78c128739e7642978577f3f8a', 0, '2020-10-01 14:07:24'),
('aa23ce394f506c6a773cae15a5b477c8b72a142b388a1a553a3baac0ad876c84172e2001586fa9d1', '5d958aba70c94e0de47997afc6ec2f2f5f4c6c3f6593b520efb13574c7e156258ccb422bb355be20', 0, '2020-11-22 06:17:51'),
('aa88be0f7b73ca883d6f711d5c65f21882a2e10bcc2c58a7fb5a7c66d9d17d8b90385c0cf1de767a', '6cb63302b2530b47529c25f993febbfc3b1770bdf0db65f3159fda61c17d8126644e0cf779d75e34', 0, '2020-10-01 09:24:46'),
('aae746abe253e35c807bbcd234e6749d212aabe063fe6be5374c0fa90c5a57e2f1151ab2b5b17212', 'de53a1f95a20d724bb2e392f95c0af707aef83f64a44816e483967db7fa39d227cb920ad759cac9e', 0, '2020-11-13 10:16:44'),
('ab6308fc81d8ce0d157682f909f6ba58c12ca3cec5ff45d1b4668a3359aa966628006d7fb1747bd9', '98eb4181b4e25dc2eeee4f7f736cae005307f1df332de6b0138a7b5e6c794424cc2684ac512562d1', 0, '2020-11-04 09:10:12'),
('abc5c5f9d68e0334f4b74e74bfbc4e540d8667f9399b47e35e58960dda3c5e176141570bebed9d91', 'c0bc59f3a9001a11146d9285d163563818e6259584c6d870362c8bc224fa801b863964a5ce2a1d80', 0, '2020-11-22 11:00:24'),
('abdfca677f784391354feb5bc30b1148f9881a296a1779d8b6e428d1c00017daf7c616906e63db36', 'f93b9d8e24e3e8429947e194d88e1b47af6af38b73f5ee453703bea6944253319bcf84c468a3b6d4', 0, '2020-11-20 15:55:49'),
('ac334457ae068e0b1bfd527e31daf030abce8fec8832da6cfb1d918c3c74a67a42b7895754a63f81', 'fc10b86497c4375ab7fef6f334511142aff81457385fb73bdbd96c23fbf203e084c67bb4c57eddc6', 0, '2020-11-15 06:04:09'),
('ad0a07c49ac9a7fbd3d89fcf2f02b5b1f1eb9dc490565cacd7b1d3af9194db7dfde34e31fae0ec13', '4640b9ab2854f43f12b6e5cffc2e38e3491191af0d100d0668f19d9a5ab18216eea6976e0590ad24', 0, '2020-11-18 09:44:06'),
('adbf7fb3f59072f1e36d7f1d8343d7ab605bf7aa9c005c4f998df6307cffa81c0a5f894a4aab9381', '0e6401f1c743f3f7655f67a0848aa8f09a32cc1251e47cb63c0aea322f06d8e3e376e1b2c756bfa6', 0, '2020-11-20 09:02:37'),
('adcff2b71b253805cff08f5e201b4898e3788dec829b92c23bdce5852bd41a31a8bcc0999b531d9b', '8eb5da30c9552f169c5c4a30a945f7f93c94c77f5bcc2911c8cbf661aaace46cdc22e437ae1da5d6', 0, '2020-11-06 05:18:35'),
('ae440453ab87ff8ace72e073d5c61ba83a6ef354ce4e69169a20aff57d5e9c4ceca0605755ee8c3f', 'cf2cbe7b6da7d5bde6c7c13e776b77d1ddc8a5f1c1d220a102eed55dfee84325a16365631952e23f', 0, '2020-10-01 09:21:56'),
('af33a62092ec5dc8422639733dec304a5c686c56a0474f97b7173f610732468b5301e88aafffa720', '20acf4bb1f5e77c58a2f807dc43473689314118a0537c7c206ba318460748eb8b3ca38cb58cee608', 0, '2020-11-19 22:09:09'),
('b0e8b5cd726a2763a8d0c7cbe46f844d1bdac67e8a5c9d1027265f12f3cf3cddf1e23278357ea05a', 'f6afa1bfa61ac019112af2605b38ccc4be85d83e009c7029ecb1dcaebea201156c6c49c93afd19b9', 0, '2020-11-01 05:20:45'),
('b343050a0897618ab6224ef249a4403acba4f7ca251db0834ff8ea2ec7c725e272c34f67627cb99b', '2a4ea38bb802bd75bd4dddff078fec897a2dcd5ef1c89f27f74f45a8d56cff6cd636926bf3918ea6', 0, '2020-11-21 06:47:36'),
('b41e695be7c41aceefac0f37fc8eaa21b4f849e6f14320fc04e78e7a4494aab2e1d642e89331b041', '270a0dc1cf081221f8352de554d9a06b56375f33676dd336c79cb88b5987864122302f45b75387ae', 0, '2020-10-01 13:11:46'),
('b43bf8a1b4d2bbdea808b03fdf019fe6e37edc8d68c3b97c1d62cf5c88c288a891b79a390124b408', 'f19c4f853d6964fb7f7b2457ad7a739007491c37df463209cc88bcca568a1ae2ef2835e89f5917aa', 0, '2020-10-01 09:49:10'),
('b587d38c2fff2d5315af635aa3da803febc8f17c3ffd6b8531c5b909f886ef3b957c09b5c1be18dc', 'ce4f7e2d9e05fd0bedde7fdf0f6c470645a5cb263d2e5c060b21bd2ed5cb3c1d7e75c32fe4b29bd0', 0, '2020-11-08 11:40:58'),
('b600f25d6bd67997f3c9e6540efa27294508849e9b956e1dbbb71db97dec3aedbe4567edd69cc499', 'b353297075a1bfac6a24cc5a954a9701af472040cb116e3271be4522046acd571d3cc8f798bc1ee8', 0, '2020-11-12 13:08:31'),
('b6f07bd281019802ef8a29f5845d0e9d23ea248f64e0d22aaeacfd8936ae36eab374e3ee03d51bca', 'a5e7f38f0cd4ac9c2e0ce6b77d8c96c4904058366e47f87212815666ac527fe03bf59fa094ce6347', 0, '2020-11-14 14:13:38'),
('b7d91c078bb8f2ca49efcb935d47d805c699d54e925d4bec68c07778875c27dac2dd57b0251d3793', '45520d50029fe329ab63cea60abf33c8a4f262bec907e146f039618a24ca11ac440fda2518b76eb5', 0, '2020-11-04 15:06:55'),
('b86aec8f55714adbc9f0b19c27e2f15704f241478271d6db95856af7340a27f0992d1b69d3938e93', '961f9c5554f5bb15751d32dc372c33fb08f5b88e338699751441c914077f0c426f65be3c7c26e9ac', 0, '2020-11-20 11:38:26'),
('b89e15fe122e601e9d6cce1034b774107c79edc7693a4d5ba079ed9d03e03ab524ed424f6588f63a', '3ab0d9e3710ba298ba45b7f68d8d2c339ada087efb31fe019a441268efbae4f256da4dd52893f5ab', 0, '2020-10-23 11:26:41'),
('b922311ce2ae3f0a8ace4d0e3ddde074b3f6b9d9ea806098c956eb5bf9df5a7323d24c2bef7abee7', 'ee14bafb000ca09dab2a008924d2494bef69da8c30fa05613da9be623278444c198aec007f0ade5d', 0, '2020-11-19 13:53:50'),
('b9b10911560a9c965bd9c307b09ad301183a3723374b684d76b43d0efb1fdbcec564998dc983ce11', 'c598279cb92d5e942b8b59244f51d49570e74ce2f46df9afc78ffa618c79576329fe68849d4bdda9', 0, '2020-10-14 10:47:49'),
('b9cf51604da66c5e73b2fca55a3f16ec22de74b93e5e8154a676c4e44bbeb01fbe80989b91065d3b', '0f29d08d3ddcbbf0f7727f8759544de32d2ab15d3e9a5b3c4471b66f5c9c480f0007543835715974', 0, '2020-10-31 11:16:49'),
('ba66d759ef2168da0779dc9068c12f191c768b6ec298b8f00a7feb6e74f432c5ba51c207f33ce70b', '4d99b28fa91235d41f806ff4bf0017e426d1fd0baf50bb40d8f50cb80ce8550d3a32076a6c415c9c', 0, '2020-11-12 11:22:36'),
('ba87063a5a875ff65d5402082263ac471a1d02add164c07b89d052ffbf6bc1948015a6f6e44db455', '70eafc8f6673e3f5c24f2273a61b9569c10d1ce881e41e6fb3b39b7f938406b7ad730c7eb59f820d', 0, '2020-10-31 11:18:44'),
('bacf11811da53454a03c1b0e3e9b23b516a768dce08a2d1c40e39533da4e857253a50b9730a06ad0', '128f11e1acffc33e2a2906ed9defb4445753670e2344befe9db3f29be89c7cebce6096db26a2d1ca', 0, '2020-11-19 18:03:14'),
('bb4e7f6bb235a4616456ea72ebaa81bd64585d39203cab2b9e9332d7bae32aced18526b743728be9', '024befbf2e161852709c588ceabcefc4adcc2a9471ed5f53b93fa109fd513c6621784398e66b4607', 0, '2020-11-20 13:26:05'),
('bb9ff35baff145d7dddec8d21db596e4e7f6f81c335b63d24775b25cb8b5d52ca70729395ad1501f', '280784acf92a7a9d882d0ddbbbb7e4fed264f7d990f22fdc6a98a4008576f499600f674197a15315', 0, '2020-10-30 14:26:16'),
('bbcd7b9be0b459f66ba8a9409669143fc80113c8b945c75cf1c0d63d13d6d7eda121d036802ee626', '256ea7410ead77f64cf2e8a85a028919823c5850cd66573de18696875cfaf3d9b39791bc3ee2c668', 0, '2020-11-19 12:39:06'),
('bc0d2e51eb68d7b9a954812064fc4335e5a56011174bb69b6765a3c97dbcdf9676315d3a7c0db160', 'd39042d97bbc4cd83fda1abcaaaa4262352d50159d09ad09a8446ff3fac35b070893198551bb2d4c', 0, '2020-11-18 09:57:39'),
('bc399d6e4fef69780f15367bc20b7cf983a2eadf24943295bafa3f2a4a97912c5a434b17dd28720b', 'c550ffc2e9dc4e05ca402653fc58ffd97fac5954034278af9fd5a80dc618b04ad520fa2bf43d9df9', 0, '2020-11-19 09:39:06'),
('bc53ba95a200f763ad7917bd7186e875c06fefbb6fd024134b03193097b03d2d630dc006b50d263b', '0a791fa6b589525508ac4af10515079944667b14ec92353fa564819f4bceb82003b4fb935568c13e', 0, '2020-11-26 19:47:48'),
('bde9fc7b3ad0f4543649992a4e9dd4e528aadabd1b80100ebaf8137c5690f304e93fb4bdc5463306', '1e1a9008d2eb7588255d514b9fbcc35967174a3bd9b5580aed75248a4b7ca1e1e79c71637890e37e', 0, '2020-10-21 12:53:07'),
('bdf33d54313b440d5a619003e207a14745928f0406bd62bddcc3b1918751d40bbd1f3b26cbf9fef4', 'c4dccc3165e863bf3538f001626d107780a3dd61b37e7c90c91ecc417b037c443f6a3596b4eb7403', 0, '2020-11-11 13:05:48'),
('be6acd348471d6319d67b458ac082efcce615121d517cbbfae43392e039359f20680f51b323a6f23', '4400833b774b809c62e96c78b18f9e422d787ce33f884701cfd529cea55ccd9e80e814cae64524b4', 0, '2020-11-20 06:34:18'),
('bedfd4e5ab3e7fd16dff22e576f790b20718d537c419ca390be573024a17feb706bd95ecfbe9d3c4', '3a2e87739eefca91554d7f92a0832c313b48c13a0bd35c1c69f7af5f6734d7be0761db6d621268bb', 0, '2020-10-23 10:35:59'),
('c05dff136a4de7169a4f0a84924f35c3a66381419d89bf3dd6e80bcd13e7044a14d385cf9eee8ada', 'bb5598fe0d2e60cf3fde0f3056ce81370c0ea16ed007c3a0b4a3458356f26a0592c836e8ef4ad584', 0, '2020-11-04 07:29:04'),
('c1626bd1cb56a9d3fc9d996fbd26af7bee232bfe6af1b896e7cfc2d944291c8ff15dca9803e29eb7', '0c179977ab6168d1ae6ab9a2211256f6c09ce7a786d23562e12b05d27cc554dba6d78a5dbd40a4bb', 0, '2020-11-14 07:49:55'),
('c17d6e0c49c3d97c13296c33cd0d8b72a832418d7c3a128303f9fd8d1ee338c87cf2a90664bf9679', '44eaf9e57d51b92dc2aa05a390c4738382969bc4f1c4085718bcc92ea4e492164dd99fb3b1b6bd99', 0, '2020-11-11 09:41:24'),
('c27dd38ce45b33fe32ca59cf0b04ac851a89f0ad49222860cd85ef0e22f4f742f5503f8b8bc38633', '79cbfd858033e074c634f2a515286ba0983afe093ca564f93b8e7e6feb6193fc6a737e719e7679a8', 0, '2020-11-12 09:20:55'),
('c28e6fdc597c42dcd7c49d2a2516971f86e5899c59a71939335df8638efb7c663ecc58e6c694fc9c', '781d93cb70f2506b08cc3847144e0c56c708f249fef8a098ed885c0932418dc0e5ada8a5b415bf09', 0, '2020-11-21 11:40:48'),
('c2de4c0e9fa87000f101fc9c044ca0ab9b1a91c5d95bfca513a36ec4c64eecea8d556c3b7630cfbb', '16bf0376020fc8f8717d5d1a64fd81b5e8363e8dd9b5af1eaed33cb5d0a9c0ff49e1d5aa7c0a3602', 0, '2020-11-21 09:23:57'),
('c309d0790fde7179fda32bf1d5e8f897e0c093dae65ba599e4b90cfda2ef856d578f8a1a7cc95ebf', 'd1fdb10aa0f95e116a1e9b1c6c87b4522e469b5a787c15b557e0681e180de4c0eeb6b25fb3828463', 0, '2020-11-12 07:08:04'),
('c352a8e2ad85d0c8ba583a13e9b5e2dbb98a5722432fbebdd5dc7945879e4a4e410c95e6bdf77079', '9eddf2b063b511f30fa1b59e62186e439eca313217df2016a5685e69ba416f80c48536f97ca6686f', 0, '2020-10-23 07:57:48'),
('c4a5d11835569a3ff4b4907c38cbf5d0899930ed1e8cbd03db1f9624aa62474a10d18c205048aa50', '1d6f2ede983559ed7a60e602bdfbc2a04bf15cf6ec7104b432ab4a41ff6b1cbf839388557ea2eada', 0, '2020-11-05 06:09:44'),
('c50c625a731347f78c71eaf41c6ce7eddad8a0f1c8a19d67373caa049b065f78355d431062e753e8', 'bc9a62e07f6fa5c5752920591d8db3ac18af87b5c741e1c95ffe5cb225396957d781deec63dd7f97', 0, '2020-10-31 11:56:52'),
('c5202aa3293f85eb90a46579bec3e266ef395e4d78eea4ec546234662b7d5f5e5ca9f5dee1e21696', '90786e5dfb3555f9c58f563ba6679ed18fdca5da3fdd3788be073dab8297d6296d57a4dd173274f0', 0, '2020-11-21 11:41:46'),
('c58a1792249fb6d5b9109209111d6771517b15867f92e671912622f28d3c3d2466bba88c84036908', '7fa4178adbdf4d0b34a76133aa6d8fc90821c1d57dc87d259893c40f4f8cf1e07aa1faa4ae28e64b', 0, '2020-11-19 14:44:52'),
('c60ff12686517e18bf2afc1b9d824197c5725e7e8d24e53291d6b1363e39afd16f0a65e1c22006ff', '74cf3a13abd27597fc4445e3dcbe604b11164f76957016177696581f3d82e1dd620c1385346dd554', 0, '2020-10-07 06:10:13'),
('c615eca67b901ab5cf0539ae67e2b1c7843d27071242803dbca41f65c859ebb52189252ac2737ce5', 'fd10e4c1bd358ed83f1b2cfa5f28dd5b7fda28fba5f3ec4aea88befb6dcabd2784f3aa511e415a88', 0, '2020-11-21 05:54:21'),
('c6c79f6359007f09cecd3e181e8cf0a18fc417e22534623178ef16586fb54386c1b2168520a7c998', '504211ccb49678cc9a0b82920548e1b4bada38ff325d0cc762d87309633b09c08f7839df2f0087f0', 0, '2020-11-04 07:54:04'),
('c77c330f8ba2f6acfeba7e763e767ec36f992c86f9dc319cb3bc9cb967bbda3c486ed633497cd81d', '45b002577a3663f8838de96e8e762fb31eff315ca64f9d014ef11414680c7ffcfc757dba14dc86d2', 0, '2020-11-14 14:28:49'),
('c804a7938f0945417a52f0bf6f87da9bf9885c6ea00738b5536f7e887fc9cd3c267ac805e06b9da3', 'ac77833ee36d3c5dddf148d282bafb2bec2395b64693123db88c24960234006b886d15b38cea9955', 0, '2020-11-04 08:36:21'),
('c8a4f24449405a04d46ef7b51b479d4ccb4f2ed483c19fb17237c4e81fed410195a65989d8820e16', 'afe35342d2232e1af57136999f08537bf04f218cb50d79a0fc65306bd3e9d0ad978305b9398546f0', 0, '2020-10-30 05:18:16'),
('c8bf011cebcd356358a65ab9d54e158c1774a0ceca2c157ecaf3e9fbeee375ca98e0c60b5133ccf7', '40dfed2ecb9f2e55f6646c9601772d3a255f7fd9e149ec2bfd9296b43053ec3237da6873240ce1a5', 0, '2020-11-20 14:49:21'),
('c908b2786ea39941d61c2c956e2f1a9806d191e3c5a4b33b3467665d32e6b965945cb3eb12e64cf3', '7694ade7ab5dfa9f159631e0d4c6506cf6ff73fb9118bc07083926895f29579c124ad069e47e0dd1', 0, '2020-11-12 07:40:10'),
('c96f93f6fc87cf78ab2b187762300cbd7ac48dcdf9f0ec63a4ac5b505467fddaa04c8d316f7b18b9', 'cb0c9551d3d996ea2648fe7fc9271d21d3370456543e426a0a9c76871e83c922a1b9e9825d11840f', 0, '2020-10-31 11:05:21'),
('c98df8018865ec2b90664b4ee4eff7eb19390706b717419a45c4524eb7068bf67c1437822a980168', '19821186866c64c89b52d99bf002e8f96ab40663be111e9e68c10d43267bb1631cf5591ea1ee1931', 0, '2020-10-31 05:16:46'),
('c9ad1db35d75bcf8deab5cfcb3d1d989a895393b23ea14f4173e381bf97bf875b683175dc4b4f796', 'bc9ba6433fd61232a907bff13e0fa110f761cf32f9b6dda1e3df0c47dbc17adadc2ae4b2a21a7f22', 0, '2020-11-20 06:20:08'),
('c9cc660bcdc4431f2edd6e17424a2d396c19ab81e8fb84aca5e81b2f1add4fb62af07ffa7c7295d3', '563d893622b6caf8895535c5c7f40ea6d5f7d4f01936c0fc4d03c853d80cc6b0b71e8987efa217b8', 0, '2020-11-15 05:52:00'),
('ca961966bb9c90e5ef6093827de0bd369592a97f4e7b66f6038d52df7af5142a2f901e8113b54486', 'bc0372c9db4631d586d4fb56c76b55a0908048e5e91458a4b8c5e40a9e450381483328e3b9628c2e', 0, '2020-10-22 13:54:48'),
('cbca8d53504b8b05212cee8ddf4ab569a77b79470732c79cfd2c9e44b4a941941d0baa0878dcc502', 'a618fe79980344a9dc4f4764bcb8bfd9d1c4549041c17d4351ca4022215fe6db527dfe075862e29d', 0, '2020-11-20 11:32:09'),
('cbfc314007a6f9361e3a6351f1b1713c59d4b75f952920465b2747c3f10c1b9f1024b430492f3126', 'aa841f59198c9aba866703142b3c74685f308e94a8e1c3e328ed8c1a0cd18e8fd890be8167a618f9', 0, '2020-11-01 07:13:11'),
('cc4293a59af7edc135a6ccf5537ded0e175d815b9d67049e094e2d8ae32d06964b778f90ffe7aa85', 'e14ecca9754ea23a201f904c6590e5f5c0991e1b3d08a65e3d388705cb6f408967191b8697c6d5ad', 0, '2020-11-11 09:25:25'),
('cc8139b0e5f0f93f10920cc01eeff06499647bd14ecc4a42ef42318349195233ee74dc4c60158f6c', '35cbf0130268137feb068f7270d5768a5dbb9062caf0accd1b4de9c1c5085a8e04ee24f975193666', 0, '2020-10-23 09:10:04'),
('ccbd133e427754b18a848532c295a9741a8fac708a994d436b95f8c876dac7608134c679973d9931', 'e1820ee30434b3ae8d360aa9f9028873c112df69986b0e9804ee54f1a8e53bc021c748a3c028f0d9', 0, '2020-11-15 05:59:05'),
('cd5448da7f1023f5480ed1b38db989703a109ddc2cdb3c1b0b59a905240d2bdaba40b9dc3103d4a2', 'aeb165f1989f18adbbc80d72fc91f3fe61e031534ae9421ab0cc4d1be5c53bffa5c7db5906e658d0', 0, '2020-11-12 07:54:38'),
('cdad2c6560d41a93c1daa7f477c7fd3a4d744a44dee5cbe1c9b472aa18d5f56cbaf9e4d223f60df1', 'b0a134b1122259b3c6a6eb26a772643cb781f60ba9c195c572fb274a94504039aea73f581913e4fd', 0, '2020-11-21 11:29:19'),
('cf24b5a1e806621befb6c584e36f39d5fa3da9eb123a52de619548be36e2667d7050a094a4f30503', 'd98c1c8907d46dce5b80a5c18a723e0c3e22faba5d7ba9bab714a5bf6c80b4309ddc6fac45a2b6c1', 0, '2020-11-11 11:49:49'),
('cf72a0b74cf296f25a2475b7df9ae468245c1a06e29f0b27c7cc6bc6348c0add39c10dd9817d6522', '88cc22331846a5edac2d880fc2c5dff440f29538b33b83462e5a8278eaaec1b5544cd9781f838a14', 0, '2020-10-23 07:32:19'),
('cf8eaebe12e16045ce70a49cd7a4354173aff35a1079c390a2869cea94b33a149292ba3f5fffa7fb', '075bc8a37e8a0a4630031b27c6d05833f00611d0e29be24b2a6e6e7554e9b8dd57436c4f4fb74bc7', 0, '2020-11-18 14:55:13'),
('cff23df38980c1743f4e0c0adb79aca410dd82d630316c59d2d89d3f12df7bf9cd138209c3ee9ba9', 'eb566f8a9312794e015b24e33a5fea92fead814b0d787f01be79d548d390e8a82b89b109dcbcedb8', 0, '2020-11-14 07:41:42'),
('d049b753a4ec942eeecd908f1a6757526583f873b114825efa4129a11542e59b88108ac974a1d453', '994daeb81fa119486be0ee2a67fbbfb067676aa57f2b3fcfea6353eac425bd99bf95fd03b444105d', 0, '2020-10-09 09:53:19'),
('d0942f0976d25810bbc3e963b9b78085a9b4bf19738a18abdd496aee077b9e5bd23bb71c8b37bffa', '648735e3bcdff3b8526f53a54937d8e2ca816c039d04de0330062fdaf9a1a7e6132f3bcae20cbd03', 0, '2020-11-04 14:54:07'),
('d0c0ddb36c28d8487b09822ef45323da3dfdd0cb50e013e3705d512dc5e08799cf563bfefde2d3ab', 'be89e54453d821940758db08c000a3916af5d73f39ba518195b868c617e73c758d2be9959da8c9bb', 0, '2020-11-19 13:52:21'),
('d0c5ae38d1ebacbe8e7971b5996cd1beec444b3bf18d408c1c74849a4a497295946ec80c7e6cccd3', '0cef7d138b8192541edd4f7751d2285a2754d1128757eb689b04bf9f21b507b36acfb38c7adf4635', 0, '2020-11-11 07:01:36'),
('d246d53b31d4c01ee42262426632d1600881e187160eeb1e84c4ca421c0ea4f4abb68bee97205e4e', 'a00f3824917d8ec3aa58ef3f9fad471aabedfaa32cfbe9024af69452bf009536b9a8905d1293693e', 0, '2020-11-21 05:27:44'),
('d295094fbc0c4430190635447a2aa4ccfd08856df64cf477d1efb91b81c6685b20f37ff42587c3b1', 'a09047c96ad821663e61740b99008efa647c01e285ace75ba703514fedf6efefa4689b4c506c3804', 0, '2020-11-18 11:38:47'),
('d3c2367666353fd856d95d4f1daba4b217800c85a89c3ef752384a2ba11ce90baf3c84b2096bbac3', '9b7b4c8bbbc6e4a6e1c4a7f0e3977c44ea226c7e04ffa9e5e2cd89b2683964179dda4232b54e675b', 0, '2020-10-01 10:00:04'),
('d4020c8c49e983e5c9c2896b5df8384fb8bd833d9d334026e8a95b0817f9d54cd51935e4382bec90', '7ac8be3c6b7f0b6e3e49eed3a8544672c191f358aa47eb0908cb2ac22bdb14f0f0c57ed04c0c5c4a', 0, '2020-11-08 07:17:45'),
('d432c6b001764f0423365e05a9750c6da60660eba4f57da616d136f48e6606fac88081dedbe5917d', '0098412dfe751afb6d7fc13af522a258c0687f78a755c67263dd323a63f54ed5df0f7321e2199c7c', 0, '2020-10-30 13:14:39'),
('d45c6c6429e365259cef3a0d06d1f6542a322cf9268b05a6cddcd563d01976fbf0351484bd0b43b1', 'c4c0a4c346aa0379fc02d9d37cb1539975b9429f632f6c9108f6cfadd45403ab7f7e3523b43c840b', 0, '2020-10-01 11:12:02'),
('d5022ee3c2858acdd41fb70ab13ab23014431f073d77e0801342d8f784087af8df9bb3fba6cf9eb6', '2278a1653619d85c9bc40e18a57d4d3c05e904902fede543e6b74a11dfbeb0e4e149d7c59d257d04', 0, '2020-11-20 05:41:41'),
('d505f56a1a7e6c28e83b0557ab893f4b60b91eebbcf0fa5d3234ba1e5cbd0524d00003dcb5d49110', 'a0138ca288b0f95c06b550e2a1db31a28861beb0039c5b93b02ffa32fd8312ec3217db1ae75f8f24', 0, '2020-11-21 07:33:10'),
('d591b9dc58bbeecfd758645445da88e9bb5edec43683f85ec21618576dee07178d3db847f044b490', 'df6d9c12fefd249586bc94aedde58c66dc3a6bebce0e20d5be4749d6cbb3e610fad8c69041cb7018', 0, '2020-10-15 09:56:54'),
('d59944b6a03c765538e166e8b2627ed55a77aca3b18795669b8fe56ef01c19259606c51b5dc98205', 'b1165b0daf5536000eca68a3cf4efc5cd6cf63a4ae40bbbbb9f84205e65e05740648f62f89ca43bd', 0, '2020-10-25 06:18:36'),
('d5e786adb8ab1c45c23cb213697101555b26ffd7b702090b095cba7367dc59fc7c872560acec88b1', '9e2e6f42d43eb0782b8a8e97b3abdac14a8d4e3b888385bf48f4edd43861984b84340e1d611dc582', 0, '2020-11-29 10:18:50'),
('d603c90a5c935df4b197ddab0501d0b1538af1a417b3d93c6e386de7200496e3ccd15bc16c666e46', '9578ada7312c4f9c9362897d9faf664de5b46f80e6ddab28b246c1105458f44e406dbde1dd407005', 0, '2020-11-12 05:45:11'),
('d603db25d96ddbe30b65c14fa4ccefbdbb1edacce142f6fdbc490d7050567b793c062c0aaabd406f', '7427b8d84705abfe14a0e53c0cd7c68576465fa956bea12b74923e5fcaaf519dae25fcb24ae02dd2', 0, '2020-10-24 05:24:20'),
('d6f09beeeca091c6eb6559bd957d2395a02b9c74a17fe4549c27c1b0637a159ee44c059054f488b4', 'ab465201c48b3ba5f18c11c3b9d417ee9680ef025f906f3342fef53819e795e0ba31978675fc1000', 0, '2020-11-19 10:34:28'),
('d7590ad73447da92b6936f8db336884837d7f484270ecbd44a669fb062c313d22e7671b98f9dd56b', 'ed766813affeb035af017d27a79b84f0e4e01464f9c028ffdd6039d138b07c81ef15e9b077936fc3', 0, '2020-11-19 14:48:21'),
('d7eba85f41ba287fbe92e77e1462c3aa7e1648a4c98af45cd004dd1c98ec56f29bdc56e1c1a2a913', '1650826eb8379bcee20a85a6024647825181c7caf22275c618cd2da1a7c24d4b0e609aee1314035c', 0, '2020-10-14 11:09:10'),
('d7fcda6d36a6e0e4c729fc3f920d877775264be55fda5229af2a0af7d8394b682eba4c01677dcfc5', 'c1097d74e3cc62f6d4d56465b0de646325f24dc71413af32b4537b348518e0a5a98f3834e73e4c77', 0, '2020-11-13 05:51:32'),
('d84505a6931bb24ee95b7f048059fc4390e84ea51dc87cb2183bc364cd7d669fcdb3221dd6e75eee', '20815bd4561a085f2da17e8ea85cdf692e01cfcea3baa29e21636891bd8cb97c1f5ffa145ae73630', 0, '2020-11-11 15:19:18'),
('d850bb1805f42b3716a9e13746743bef2f6827bd5e659e75b705cc5d70d3e36d260ff420168f1a90', '9b4edbd54c33765408ad44012f50b042e2db973ec744216223c8968be8f1427fa78d7ae8df387780', 0, '2020-11-19 13:49:34'),
('d8940bc6d87b87e8fb78f506c6df2cc52d77aa18233d49382ae812743d3f0081a0eddd07c7541bd8', '7165f0d7ac44ac36d0c7a80fc732c3dbdc6e2a86309f1335cb474d84491f7b0052e15353151dd7ae', 0, '2020-11-20 13:31:41'),
('d896506efced0d1fe0baba340666cf75cbebc054116f9919bdccac0e326fbe5d9134aaec676b07ce', 'd6d0fdd0594bc43f65d7f71250e1dfc176bc8087762d785228892a7b4e3234d69cca0d3958f2265c', 0, '2020-11-18 09:08:30'),
('d912d790de9243edda1fc052eaad87c4712c32d7e6372c4b92ebeedb019f75905acf9451961c8287', '6a0d7343938c434906d17a2db7d704b6b93effa4fc8a4e650c0572103ec5f4b6eb66e140793c6c40', 0, '2020-11-13 09:26:12'),
('d9795fa7d2e0d4245e10129ed8a914eab967de251acdac6915f83dffb496b86cf21c7867656a9448', '5a47a203abaa81595dd1208292f28d765b8f5437e398ba421748ed72ac31cf211a104aecdacc662b', 0, '2020-11-15 08:53:26'),
('d9f773563977b981a9fdd91edfde815563e951dc4f461319267e03fa62260fd9013ea4f30154e0ca', '5cc397260534c1f21d14676e3c61cd6fbe2ec1ac24c293c1da60044f856d487e1785e4541874a1dc', 0, '2020-11-20 13:40:27'),
('da079d533f520ab9fe1af692fd697ad6c9f47353300d188a2e8b9bd94b3e714814ffabe48ab30af6', 'd120a50a6352b878a7834439eaa5fe66b5bb906bd209e3683e76a7604b5a6130742c79a8e729c8b6', 0, '2020-10-14 11:06:04'),
('da2dd4767d839945ceb02be4379caf4f7c5398691a0e2b30df38638532a3e21766da0567a3caa540', 'b41bf95780d3fb7169da74bd60c36ad7dba50191b0b0deb17c0b344c80b660f97963639f0c8725c3', 0, '2020-11-11 15:09:00'),
('da6dc944f78e29e0e2da32feaf3e602069de1e02b07021ad1a85728d4ed02050e3cc3706966020e3', '53a8a3fa0998c05eb4f952668d8fdb6326e9f72b8da1e1dbf76b90a714d2e8eb149624e909feda12', 0, '2020-11-09 11:33:41'),
('da7f24f7abded166b8a1d3f2097eaf882818d7dca52b48e69ccbe2a8e4fe9d68d54298e36164c948', '7870befb1c815b22f572e1ee532508d00369a508162b59c60ff5a5cd5ea68b6254c86ff8492ac96e', 0, '2020-11-13 07:41:00'),
('da8ee76f97045ef940e3259f6ceb15626fe866fa9d0aa4639a591c6875ab8702317ce79469c2fe6d', '73d5ca97a35aebce0dc4086d119d86e4ff555974e41712ed35d864fdba6bac92ae13e2a7f949a1f4', 0, '2020-11-21 06:23:09'),
('daee0af1018ac5fae7748a96049fcfb039537e064d4112f1e4fe8be8381b6511dc278399e73f3da0', 'd0e9d1599d6dae25d03806e2db1ec3ffbc813baa371ab03680b9ed7a5775f9619b1daf069b60932e', 0, '2020-11-20 02:11:08'),
('dbb92b6270e46df18848d5f705e7ed603a8f7dbbbc26b2e5ce590721e0d2f60dbaf9a7278145ce61', 'b871837a8a2f36ebff6b41ea831d212935186a3b0338f26940224ec0738263f825d2f0474507c227', 0, '2020-11-19 08:00:39'),
('dc825a81401d0ac93bfa248a6372a7cb5bb89300498b7fe96ad45f119c12e8332eec98950b87dbbd', '709537e3c8ec663ba073f15e470652cbd3f990ca48b740f6dec0422310de5347b8320107227c2b56', 0, '2020-11-12 09:36:23'),
('dcb5de73785b4fd4ae87d23f7332b993ef17a6ae7f3b231680a32537bf83a6113cf1987186fa6e5d', '3a588e92f7f446d1be7f0e912dcd48e4769a19ed5b2afd75795fb983c605d782d50a3e53c4756ae0', 0, '2020-10-24 18:53:40'),
('de04849cc925816c2001fbe68b5c38d66c2656406cc92a06e9ed37511b1bf22bf49543bdf195d8a5', '0c108ef4a03c2d8184422ae5aa792aea74d4f23a657b80a346e74a894293504f78ac6fe01ef903d2', 0, '2020-10-22 12:14:59'),
('de8f2f5ec0a5cf52699b3add2e598bf1c06bc5e57a513b77e7a3774231a1c1ed1de63542d2918631', 'f8aade1793cfef65f38fdc2f625545acdc4b07cf5ea9ba01a3069c02702273c300d1e39438290c5f', 0, '2020-11-11 13:57:18'),
('de90c875afe35840da9827d81d747dce3f0c1569f935bff44832985257014876475353d528f51bb2', '32af57508675632aacf1cf3252af5566bb7c8858046b794ab67b7fc2fe1e9c492acf2c84648b2c82', 0, '2020-11-11 14:05:58'),
('df3b269cf1cf383ea0cedf241ad44ccd0518ee254e40478887498dda0ea2a1f58e927475f5d275c1', '3af32459e305f42e5ce7c4641471ee4583c2912b7f32c7bb206d80ec3c66d9c92a365d6544796ac9', 0, '2020-11-22 15:30:00'),
('df63826a442bf7bad867de4a7f9169488d0361c78d1723becff47d799c829ebed5e7155e3fac0e15', 'a3761e724e09170cbbbd20f8916c14a2cb78e306ceee9906b9f1693e67c2356f3a8a191cf0b0088c', 0, '2020-11-21 06:41:52'),
('dfa207c6810ec2aaf53acc41c71325227b7e830e75cee94feb59b6459a0ed7d657935c2756fdded2', 'c200236709099ae45314d15dd0f30eee0a04dd4f435d71836509c55cecc605411d0acb2e39797d41', 0, '2020-11-29 07:10:33'),
('dffd9146a5c55e8fce013ac3311113867b8c69f9159804d34c6151a825dc7988044f9b0fdda5a284', '6215526f8093e1bf2928d5f12d192769da11f7d8505d9233ba70fd3bbbdce4445513cb7299e530b0', 0, '2020-11-21 06:52:33'),
('e00bfbfdfb10fb66d8726f9396eb642ab86178de3eaab02ff4db7a09c12fbfd1c825a7b89aac1ebb', '53be022ca6075ceac8692ee62b359e48f0c913283e9db94ed95770788d398485efe90d62f4bd6983', 0, '2020-11-06 08:13:16'),
('e03cc4a209f99a15c2a9e81a6c7e1a8b4e58d65f999cc40ffacd843d133318b2f5e22b8d96158731', 'b60a7447e725d0e8d8520945bc9102014c8a072255283240956541e1fa91b72b0ac4fc5c8d70f7a7', 0, '2020-11-21 07:25:32'),
('e09b5daeeca3d36cf9a2e9630133afa709c4f451fe7403a3cfa8152e361c90b206cf792dd2f9cdd6', '22c62ac305430fddb0705d42cf3143b6b9ee627ab6b33dd894c163cc54f28edbf52d3a66bba69548', 0, '2020-11-18 12:16:51'),
('e0d1244d4c923a0af897452787139d4f1923786ede8accb731981aafc0cc373c33478a68ced78e94', 'a85397816c44195f7ed8a5c1ce8cfce7d50422c9fa65f3af2b2a7c2cd161938988223053edfaa97f', 0, '2020-10-25 08:26:42'),
('e1d781ea462f522d1b9e41508afbe833638d7324c1879bc00713bba0fb8712c3e6742ed7dc572706', '08598e3d9073f44884ac8814ac236a2853b6cfbd6c11073e3f687b7f77df429a23e51cd743792e75', 0, '2020-11-19 14:47:56'),
('e1e16516a064784a8e43d5363d93ff1c6a89621a62eba0befcd761dd53d9998e3b997aebc4afdc2b', '88953b48cac3fd3ada41870d2c0ad0bdf9c103377f299847a47c2928bbc0397c87126c99ebdce9fa', 0, '2020-10-22 13:58:28'),
('e2418ee4418a8b156c8301680c3dcd07b5a7a3ec10ac46e6772bef44de69c1414e1106cae17a9218', '1de32c8eb74208b86661ea0a72a11005314d361918e81e0338ea4602f7f2221424030d60f2598d05', 0, '2020-11-18 11:37:04'),
('e338cbf67f9b82362fa561dc99a4325215b2e2ec47ce2a8add70662fa86b19450753bae7322b6967', 'bab7f08c8a2bfa13de4be2266c374df87941ec5b3a23bceee66d5fd3ca8b419d5769c426cfe101fd', 0, '2020-11-19 13:32:01'),
('e395ab1a1f6f9f1e2821a536596526e8966a021b53b18a58e7a6e4efa42ace348d6b197167566e4b', '7d5a6aea0735d86702bd9ef8885b6230e96b74aa077489edb910bc4fbef0da9960b3c28426f5efe6', 0, '2020-10-21 05:26:44'),
('e4512810943187af87eb48605349698c450e3dda130f794538af6b522b304b0279a72bafdcae95d3', '5d5d92cc4d17c9000078b0a5544e226e0f27929c5705a8530eee114f02e8610a6631b0e7a7553994', 0, '2020-10-24 07:51:34'),
('e48aaa5bbfed1cdea9387158c5a1359c5b0b5093cc3c0692ce3eba749fda7e0b0e34aa258303aae7', '30f57d8c1814bbabf60d50b34d7759d707cef9dda8ba1f5679d0a8d91ff0e1dc029b8d4e5bdd38af', 0, '2020-11-19 14:51:55'),
('e4d2822e5765ebd5028e00d0ba8dda1a4e9cb83c600def25052f37de01eea1adc460e8c0ff2ade6a', 'f26f1a323779386712bd0d601d82eec14ff4e8fedc66d4d1af08553ed4c5a132f2b994be33d99ae1', 0, '2020-11-20 13:31:05'),
('e4ed63554742eb9f6cb0cfee025476a4ea55ffc8a1bf14ced4083427e84bfddec15c52c49da2ccad', '688bb3968f43eeab8ce185d3bb55730fefc55bfd5d6b3d9ebc30ea707758b7d4aa17ac813e0c4a1e', 0, '2020-11-20 10:49:24'),
('e560eaf1f22f02b31ef1784cce7a2149ed28d263a5af87f0fa44f77f6c55d09e021bf0a590b00dcf', '5567392422ed5ccba352a532c12dc69bc2740c45b514fc49c4a1fd6f9791ad9ddd5c7876fe37366d', 0, '2020-11-19 14:27:14'),
('e62b7ea9e7f04dd301f79c57f46e9eb15b7098d7cd430f09ad62a97b3b135873812103e21b13abe4', '6d8477d40aecbbf2d73790f327b2ed1580ac4a71963aab762267b659d6fcaa20fc5cc3f53133c541', 0, '2020-10-23 10:07:30'),
('e75f1518b64d56684b1ba1d91556dec6d4ea8261f7e54eeeaaef7db48d836599d73230c3cb9faa55', '32e94d50e2bcd5ebbe641cfa09cf39355b349d35a6e48c700474b00b38ace2cbfce92bab988f36c7', 0, '2020-11-12 13:08:57'),
('e7693f5248f5ca214eb4b2da1cb79cc25a496854329c5719f85b21fcabe4de4d6681a2404b30f7e0', '130059987892b3971716369b947748d407ed3b332e7c671dab9c68654e872cd15a51d06835056915', 0, '2020-11-21 07:41:32'),
('e8119ab802e39cfa84874cc9f8393a34e8bd553ee15d014f12c963adedf708df2ae3e70cb541ccb8', '6f2e7c178ac70fe7730812c2c484d678341839c56993818d83951217f31364d7e7ae7fd37a3c1a1f', 0, '2020-11-20 11:03:30'),
('e85eed9f1cf8bff48bdf1687b337fc5e4676588a6d5a7ff707369eea24e7335e56027daee79efcc8', '1add499b61f502d72c2f72ecae2cb4177269b6b10cf0a85c4c522c9e2d2db8daa1ce8647b0a9fac1', 0, '2020-11-12 11:08:54'),
('e87aae331da252406bd61fb5fe452a099237ae71521dec4ca4c91cc55bc629a9c1dbbd3016bfe0e9', '33731a0a5c0b2bf46a55791ffb4ac3850845d459f8c1dc1c4bb3cf9371aea5f5b2b487c2cc60c1d7', 0, '2020-10-01 12:53:12'),
('e8c736359cf87a3b0098fdfce2062b0e0689951a01adb468312dc68fbc3c6f9b28298a457694ab7e', 'a54cfdb41585866b3018c43fc3f58559c537a36737ec796280b361f7ca222b38d9762f45fb7ceb95', 0, '2020-10-24 10:46:14'),
('e90725f68a660ef45b6cf775ba09439dd20833409eb977494fd87b1151468cc93d56d0b90c99cef3', '8bfab1725caa0acf723d5e12ff8b90373af16c9b10b8c0839ff8b66c6620d16c089d5aa06b77b6a6', 0, '2020-10-01 13:19:57'),
('e91051921a25bdd0642fac60505805a90005225f2fb7957f4ac21726ad9d3285942107cf42e044b5', '967e96ded0497ea420afc73cf7094151b8ab211fd6a34f7856ffe3e81fb27da331a0b7a56bff8ace', 0, '2020-10-09 13:32:34'),
('e93a4b8ac733371b1ead9c64161ba609c0e23bee1bff35f5b5a8fe1812af4bf958274ee48b024984', 'fe88c1adb63be8c466f18f801c2d51826c5c7bc2c2ef91119b5fa17b25088c331cff7d9bf8abe96c', 0, '2020-11-08 11:54:07'),
('e9626b281de0c15e2c23e70efc83960387750d982336cfc741ed28c51d98a819b324b449a5b62bd2', 'df113b8ed1bc84e718a7f6397a507fa7beac3ce97cee144434313006205d5402cd074bd02c9bcbe7', 0, '2020-11-19 17:27:19'),
('e9830b777154bfa3d9d86deaa29c98850e4ae88c10d39aff14f5389c15fe3d7c4a70d75451abe918', 'f2c51323c4b928ac7e843ae01942e226809110a5441f6b7bc91eed8c5e7022f9490aa606f312bcb9', 0, '2020-11-01 13:18:13'),
('ea144380efd128313071eed7149f4bb8da339f2cc25430d5bc0c3bf6537c5f98f56b2556f5625c83', '54129f142e0f7a40617b74fc122f1f3e164c94febd3d34498830b9dbf966a9705121e1af0162351f', 0, '2020-11-12 07:42:17'),
('ea8d0f7a0a9616d0e0a668a455fb0563c7fbf726aeeb2db3df6c42da804e96e53d05ea656f4120ee', '6ec0cdbf5607f6a2bb471bb480faa926e9f29cd32d78d2ebb1d01f8b3edd5eabf60cc7dc537826b4', 0, '2020-10-01 11:01:21'),
('ead63c8de85f319bbcfab448b66e99ce4dfd6666d84a546b538d9d65d7604cd550d308cb89ca181a', '6399033959ef8cdba90002eba356f4e5e05e36552710c8ebf864e3cf85e572247ea5c5d4332d3fb2', 0, '2020-11-14 14:08:30'),
('eb02cbd6298c5b9cf85c4333b3d86873f6e06a51b96f29e2c02ff63508e9b11cc1631809f4ed4ec4', '59830a819cd332300413eee972a3dbc7e7df44b087eadfea3f72e380560b419efb4fba37760f5d55', 0, '2020-11-21 06:27:08'),
('ec581906f8f130536e737e47b6018d5736a8564a6ea751643397c1faa5f30dc0d77d2fd286a7bb82', 'f993c206df4c583b24deaafb3386ba9982354c283b9ce855cfaf2bea8e41c3dcffb4d1a16f5d9d0d', 0, '2020-11-05 06:23:53'),
('ece03dd5f11d9ce6eeeeaa71e6bc2c56cbcdfbf4fed39e72898b00889f695d61dd2734fdce685c2b', '638830a4a889cdece62aaefb63cbafc929c9ec8e9f7be683275cdb540940af67e6cde5eae93ebde1', 0, '2020-11-18 12:42:10'),
('eddb39c942f162b9cebff7a0727a45c9f6de8fac359f77c6a50b1e328acef61cf13c1c220441f602', '02eac8e94a5c189c25368d1f68aea39cfd275bd1fe1cec5a4a251c036d67e0c8e4437b94e5954fd8', 0, '2020-11-14 07:42:11');
INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('ee4aa286d76863eeb75e44ccf6e7127b1d5da61e57dc05c65247b1c2c51e451ae42ede252fb705b8', 'e4edda0edd9b241c068ce46f37afc258d368fe2a4ccf1b4f18a379870d4f02f7cba25fc028e39b61', 0, '2020-11-20 06:38:18'),
('ef19d1eaee1a47186aa8216a686e282378632c7287e26302f3478827e87082d13532350a1eadb09c', '257738101e9696f3b9fdd4e2dc291195474d090f7e1a4158612e9cbbc4fd404f74519ec4614bdf5f', 0, '2020-11-01 21:00:45'),
('f04a6395b0fe63776c28e1072fdb8b4c94240a3f3e458580a8bbe2827bb9e5d34e57e63dd1a332a3', '3ef27a8e1a2abcbaf6c6e7871a29553c4aaa84ed3c52fa4ee35cf20a1ca3ce69c00476f763c41bb2', 0, '2020-10-01 11:21:01'),
('f26bbaf834de6c017585fd1e2af3758240791a3a08a30479363ec5126e09d5ba7bde95ab815ef8a3', 'b3912b978f362f01dab9a89d023d9fd5901306a352f4296bd4fbfdfdcac2299e15cea411e4cd337c', 0, '2020-10-11 09:22:45'),
('f2f0920a8ad1da04fdb870ede1c717a7ddb07b34593e75c5b9e72f2f854b054ed72e74c7673cb228', '8c6d721e8462368fdc52a6d7fe6b3aef69c6241ed275c0f1e8631bb7fe2065b60ea8fef45f59c1ca', 0, '2020-10-01 14:08:08'),
('f43cb914ba46ca0ea8e5910be6afcab1d7c3806cc5bcdc4e820126bad18bf23d736ec1f0fa190d78', '34925c8c053c1b4a903c67cfb9fec56b176a9e1456cc4afe3f28d1fd579343defd47c192865cba72', 0, '2020-10-11 08:52:45'),
('f4f48efc5d7f5fdd1ce8e5420f99ad2bf641a4a22fac44d9013cd6f1d4d46c3be9b6e0c6aada7129', '75d0ef9ea903c188e75184604fceea661d386cf66c31a9d81c8aba08d5fab5537817b6ccee2d534f', 0, '2020-11-21 09:30:12'),
('f507f73f3b3de91d79c212f06672b5a64af79e30dbb732fea06af186e9b0f5caf48f4d4f52f5c2fe', 'b3a8366d4f29987f302cd07be36eb69b7bb455f756faeafdcded109a4ab9f913794fc670cd3e10ed', 0, '2020-10-23 11:10:29'),
('f55c07465e27b60b1f6441319384dde19875c6c6d48335baf92db81110746b4e0337b4727de2b97c', '2857c43bedf99027e5bde2a3b352134bfc1782be3125160a1e3337a33010de7b1fea84aee60f8d20', 0, '2020-11-20 12:56:13'),
('f57b216f414aa8290edb51b99e776f624c3b6bbeffe0e004456e25aa2b6c9dbf4a33277088c83320', '6d601d9679f4e802e854c3f224aa141234c4c21fbf372d14c1d3fa45db1ff2907d094e7742e2e77f', 0, '2020-10-30 09:55:34'),
('f5c9dbb2d143cc9a9ad9d45047a607039dc8539da2d25463716a51fec53da5ca1cb2aa9b2045f8a8', '5ea56e190c623a2b3d67b73a72c236ff347ced66152bc331f5ee71e8e3d59e5eadf27074b6928fef', 0, '2020-11-18 16:08:18'),
('f5f2a9a766984e2742d856b039eb97576b101f68101a40e9a52d169324a62a10a452e521ce2273b3', 'fe11aaad932c986f7c32019a876628ff370dca95cea1ca7e53139e78fdbb2a6e654ca025af797d3a', 0, '2020-11-15 08:54:46'),
('f6462d6743eb843722fa632b43684e436e50afe2ab483294b302e8cbfdc1d2d9e82a0c37aa92abfb', 'ea862316488ec14847f62e7b74d087765e1eadc2d3c85a5a6d29dfaff304daf3b7d63d548a22f2e9', 0, '2020-11-18 06:18:01'),
('f78963f55ef8fb6f0f5cafd7a5407c0dbdb380aca082f61541043cde8392536cdbe941c33dc996b5', 'bd3d0b30a26577945a8c8ce47b4afe4688374da57fc568673545aef45afe1d72adbbbd1bd7fc4f28', 0, '2020-11-12 09:19:44'),
('f7a82eb997fa11ff4be972324f6a9989986af32c178fdd121198e814025a692674975be4331eba54', '93ec63122e1956da5d0efbe025f225f59c35ffffa118220811d1a3c89046400a884419ec88098892', 0, '2020-11-29 07:48:50'),
('f7ee68277e290c14d9a6cbffdadc9a22fdbbe1f9cc6137fa08699e691b732df9250e8759007ff6dd', '82acfb1563d838047e68f1ba0dc2cbbec04888bcd0026ab8a9e1f287777d003850339e59228b05d1', 0, '2020-11-11 07:41:24'),
('f872109f785257c888be72904c2ac0eaaa3539b044837fa907e605853d544cfe2589083b1c8ee54d', 'cafa351e7a6b01cd7d37c9d5d174b30eea7bc9e90c1bc1cda9ce0ce46afe62da223d8b895aa4ca85', 0, '2020-11-18 13:13:22'),
('f8980b5b24ded2b8ee261607953534833cb24139f785d26d5b748a7c072c4fca9c4199277685fb0a', 'b027dfdff683e4843e162e1cba3b88e367aaeb6c385666a3bbc70f22c69d16c96d2447235c93394e', 0, '2020-11-19 09:02:03'),
('f8e68139a25a18c00920ea613c039ca09ee540a1234645a40f648bc754957501a43ba09ea93ea843', 'edcc93e5a8967d2a7d98006f88405c526c243047d0842111ad13db91c5dea4794b786e4d1ea15bf5', 0, '2020-10-25 09:28:25'),
('f9496504512c10643c3ed322a86d4fdb5ca72900ae20368b60f7276096bccbeb948048e986199189', 'a9bdb8f66f09b2ba639e9f694859ed29e3ae556a432c4f1fe9ccca34aeddafb7bf78cdd18314a839', 0, '2020-11-08 06:47:40'),
('fa440687911f5549c98267163b0043236165a688f4b1986b177560e061bd3ded52e929abb4f800a2', 'c5e2175c1f4d5ba3f9e9893f3023558754edd14af865030b328256a2a9df719d900444daf6374082', 0, '2020-11-12 06:59:07'),
('faefb013efc2cb71cc19f552edb34777ddd5bd4f1dca298ee0a108c60045248689720a6f9e0a361e', 'c5e6673d7079d92b7d5d978f47d9c20af6b5e8ee0ea2c917e2037904787378b0e20ae80c3e9d998e', 0, '2020-11-05 07:42:49'),
('fb53f3f06574654e0eab8a869ba64b54796dffdeeb50acc89aabdc36b01b3a5b402f1e02da509ed8', '2c0e8cb5940f37f8639a42da0578471817595b9bf5cd42c91c2dea51b50278e803a3c9de358cf1a9', 0, '2020-11-19 04:47:34'),
('fbb22ed2ab2309a4b87fd1e39edd94161762e99b55c38ad4210cd18844b60fffb5e135bb7a84ab96', 'bef6a3de9b0c33023d184b7b3d207b5f0183bbb7698a2711f9dfb511139f1c4fdab49744e3b2a2bd', 0, '2020-10-01 13:57:36'),
('fbf4343f9c731fd89ca8de38b18d7a4e1fc688674eb12425311100786d4276b369570b6cef88d533', 'fcd4fe61e18642543b7a95eb4bd748ade84a72afda49d012c9d38ec9cea707c716e102ed717a8c84', 0, '2020-11-14 23:27:48'),
('fc3e2352e9e308031e569af585bf07b00c174c6be083b926774a291ca69d816452492f475413ef89', '7d8e94714681170ada5449a2e560c1071a8d40627705efd2a106d469ff66cde6908d36884eb6db82', 0, '2020-11-18 10:34:19'),
('fcee1ddf8aa1d1f3da49732a052fc1bfabe259619c400cf19a4c9c9832d951617faffab54111909e', '95c9de4d407b941c8bd56a18dde6a196e6f086f5ab648927d6ae439779da5bc26bf61119178744aa', 0, '2020-10-31 14:55:57'),
('fd7078e58d6f7c7412d24ae0c8f16f9bd61a31544925da88abfa670944f1f36c33b1eca024f30f86', 'd3df80c95d78e8700af135df30588579f41effac8af7b0f9709db85f96fc06c5346c65c26c39f0ba', 0, '2020-11-05 15:45:16'),
('fd70ff1115c23e50dab8aa767bee82c63c8e841c754541fbed0fba0ff7236b6a79dd117a598f0945', 'ebf2a4bd51a624f3cccf007ffa269357c1398f0df99add8c597bd0cfb42cbe09bd071ab1eb82903e', 0, '2020-10-22 13:22:50'),
('fdd0df85274c2d5cd78dc7e5bf0a692ea91113049579afee32ec5af778760d5b31196805ccc50710', '8f4e94fd8e9c5212144b50424048abfc7589989df221fe7740b37cec340f7b8f8350b3d8363b1c5f', 0, '2020-09-28 08:01:06'),
('fdd724a6995d99ca467336dc9e0ff5837346876a9a80e62f9dfd8da06b863adbf24dbc9619655cd6', 'd3e7f5f768eaf9abbfdd3667cd4875fdd1a6385b7be3488fd82dc0522226c835c4d58528eb1d34eb', 0, '2020-11-18 05:39:05'),
('fe0fc67beeb97512e68608dc05595e8e1d8c0db5cbe3fe1a0b20f1c6d9841d3d20adcc104300a81e', '55a27c0b464114bc31d951786c2d15cc4358ad4416c180d5391c3f0992d760bf6211a9245ed4c87b', 0, '2020-11-20 17:08:36'),
('fe6bf38e622a62919d42d630c25c351a62ada0359ee975b3d63eae723cf0a732980a5aee9fb0c2bc', 'da4291865687742b6d34ae1f6e0b3f302199de9d939c94fa9469466408c19ddeb09d2593fd08b5f3', 0, '2020-10-23 11:38:35'),
('fe7c07d76dfc93507f9464e1278b9e59dc16606a78f6508729d7508719cbb7a9330cf0dec2e099f3', '3877a711f121164a410ef20eb3f1757cb3dd50e612af266c05363906f540de3afc1ee8b74157c6a8', 0, '2020-11-18 06:22:34'),
('ff753d1963f2285edb14ff65039941ada63557eeda763f58c39d91b8ac635572051e40ccae4510e3', '1c9c54a3852560e0298d814ed3a4d156b9c1fb59966d3a86e5566a179a8e2858cbbcc205531ba913', 0, '2020-11-13 07:53:47'),
('ff9f93761d1385e03419a2d52bfa27d5061308eb8bfaa9e3866993b2f4a560e824406d3a003940c5', '0c41b2ff3d63fe73fa0a8fdf34085babeb38c391b3116a1b3e0a828915b36c1dc597736e6b25f284', 0, '2020-11-29 10:05:58'),
('ffd16c80ae806289107db73b13d298bd061390ab890b91111344d0fbbc917f73e6d1b83b84be7796', '68c1cfeca65639feeaafa3d4913b9cce8e99680b1a68074ae809b8b03799b27a829cf7423ebf31a1', 0, '2020-11-19 09:03:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_verifications`
--

CREATE TABLE `password_verifications` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `verify_code` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `password_verifications`
--

INSERT INTO `password_verifications` (`id`, `email`, `verify_code`, `created_at`, `updated_at`) VALUES
(3, 'ankit.mittal@mobulous.com', 'S16RJ', '2019-09-28 06:09:39', '2019-09-28 06:10:15');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `post_content` text,
  `post_image` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `event_id`, `user_id`, `post_content`, `post_image`, `created_at`, `updated_at`) VALUES
(1, 1, 9, 'MMC is an event gathering some people who have technical skills', '', '2019-11-15 06:57:23', '2019-11-15 06:57:23'),
(2, 6, 9, 'Wow, Event for story writers. Gathering best of story writers', '', '2019-11-15 06:58:26', '2019-11-15 06:58:26'),
(3, 8, 9, 'Event with business people as per industry standard.', '', '2019-11-15 07:09:19', '2019-11-15 07:09:19'),
(4, 10, 66, 'Exciting to visit Guyana and attend Gipex 2019 representing Governor Control Systems.. thank you all for the opportunity to meet !', '', '2019-11-19 15:38:28', '2019-11-19 15:38:28'),
(5, 10, 20, 'Registrations Open. To avoid qeue tomorrow it is suggested to collect passes today', 'postimage/1574193660_1574193561822.jpg', '2019-11-19 20:01:00', '2019-11-19 20:01:00'),
(6, 10, 20, 'Since we are at full capacity, it is hence requested to collect your passes and be seated by 9:00 AM.', '', '2019-11-20 00:54:14', '2019-11-20 00:54:14');

-- --------------------------------------------------------

--
-- Table structure for table `questionnares`
--

CREATE TABLE `questionnares` (
  `id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `question` varchar(250) DEFAULT NULL,
  `optiona` varchar(200) DEFAULT NULL,
  `optionb` varchar(200) DEFAULT NULL,
  `optionc` varchar(200) DEFAULT NULL,
  `optiond` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questionnares`
--

INSERT INTO `questionnares` (`id`, `event_id`, `question`, `optiona`, `optionb`, `optionc`, `optiond`, `created_at`, `updated_at`) VALUES
(1, 1, 'Overall level of satisfaction with our event.', 'Very Satisfied', 'Satisfied', 'Neutral', 'Not Satisfied', '2019-10-10 09:29:01', '2019-10-30 10:21:25'),
(2, 1, 'How relevant were the session to your interest?', 'Highly Relevant', 'Somewhat Relevant', 'Not Relevant', NULL, '2019-10-10 09:29:01', '2019-10-30 10:21:28'),
(6, 2, 'What is your Pet Name', 'Golu', 'Bholu', 'Sonu', 'Monu', '2019-11-04 13:57:48', '2019-11-04 13:57:48'),
(7, 2, 'What is your System\'s Password?', 'ilovemyfamily', 'nhibtaunga', 'dobaramtpuchhna', 'asdf1234', '2019-11-04 13:58:41', '2019-11-04 13:58:41'),
(8, 3, 'Overall level of satisfaction with our event.', 'Very satisfied', 'Satisfied', 'Neutral', 'Not satisfied', '2019-11-05 07:31:06', '2019-11-05 07:31:06'),
(9, 4, 'how do you rate this event ?', 'good', 'could be better', 'FAIR', 'BEST', '2019-11-06 05:43:48', '2019-11-06 05:43:48'),
(10, 5, 'how do you rate this event ?', 'good', 'could be better', 'FAIR', 'BEST', '2019-11-06 05:55:26', '2019-11-06 05:55:26'),
(12, 6, 'How are you today?', 'Excellent', 'Fine', 'Good', 'Not good', '2019-11-08 07:37:25', '2019-11-08 07:37:25'),
(13, 6, 'Are you Hungry?', 'Damn Hungry', 'Yes', 'Little', 'No', '2019-11-08 07:37:54', '2019-11-08 07:37:54'),
(14, 6, 'How much you rate our event?', '5 Star - Very good show', '3 Star - More Than Average', '2 Star - Average', '1 Star - You Not good.', '2019-11-08 07:39:25', '2019-11-08 07:39:25'),
(15, 1, 'Hi', 'a', 'b', 'c', 'd', '2019-11-13 05:36:39', '2019-11-13 05:36:39'),
(23, 10, 'Overall level of satisfaction with our event.', 'Very Satisfied', 'Satisfied', 'Neutral', 'Not Satisfied', '2019-11-18 18:39:57', '2019-11-18 18:39:57'),
(24, 10, 'How would you like to participate in Gipex 2020?', 'Sponsor', 'Exhibitor', 'Delegate', 'I won\'t attend', '2019-11-18 18:40:22', '2019-11-18 18:40:22'),
(25, 10, 'How relevant were the sessions to your interest?', 'Highly relevant', 'Somewhat relevant', 'Not relevant', NULL, '2019-11-18 18:41:16', '2019-11-18 18:41:16'),
(26, 10, 'Were the discussions valuable?', 'Yes', 'No', NULL, NULL, '2019-11-18 18:41:33', '2019-11-18 18:41:33'),
(27, 10, 'Were the keynote addresses generally of interest to you?', 'Yes', 'No', NULL, NULL, '2019-11-18 18:41:57', '2019-11-18 18:41:57'),
(28, 2, 'ghijiuioopiuyuu ghuy bui ghijiuioopiuyuu ghuy bui ghijiuioopiuyuu ghuy bui ghijiuioopiuyuu ghuy bui ghijiuioopiuyuu ghuy bui ghijiuioopiuyuu ghuy bui ghijiuioopiuyuu ghuy bui ghijiuioopiuyuu ghuy bui ?', 'ghijiuioopiuyuu ghuy bui ghijiuioopiuyuu ghuy bui', 'ghijiuioopiuyuu ghuy bui ghijiuioopiuyuu ghuy bui', 'ghijiuioopiuyuu ghuy bui ghijiuioopiuyuu ghuy bui', 'ghijiuioopiuyuu ghuy bui ghijiuioopiuyuu ghuy bui', '2019-11-19 05:36:07', '2019-11-19 05:36:07');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `reason` varchar(250) DEFAULT NULL,
  `description` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `detailsid` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `event_date` varchar(100) DEFAULT NULL,
  `event_from` varchar(100) DEFAULT NULL,
  `event_to` varchar(100) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `speaker` varchar(200) DEFAULT NULL,
  `description` mediumtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `event_id`, `userid`, `detailsid`, `name`, `event_date`, `event_from`, `event_to`, `location`, `speaker`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 7, 'MMC 2019', '2019-12-05 06:30', '2 PM', '4 PM', 'Lorem Ipsum', NULL, 'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum', '2019-10-30 09:08:27', '2019-10-30 09:08:27'),
(2, 1, 1, 7, 'This is pase', '2019-10-29 17:40', '8 o\' clock', '9 o\' clock', 'Noida sec 420', NULL, 'This event was biggest in the color background. This event was biggest in the color background. This event was biggest in the color background. This event was biggest in the color background. This event was biggest in the color background. This event was biggest in the color background. This event was biggest in the color background.', '2019-10-30 12:15:02', '2019-10-30 12:15:02'),
(3, 1, 1, 7, 'This is Upcomming', '2019-10-31 18:50', '6\' o clock', '7:30', 'Noida sec 675', NULL, 'This is the upcomkming', '2019-10-30 12:15:54', '2019-10-30 12:15:54'),
(4, 2, 7, 15, 'Pakistan Vs Srilanka', '11/04/2019', '4 : 00 PM', '6 : 00 PM', 'Indira Gandhi Stadium', NULL, 'Full Tune dJ songs', '2019-11-04 13:45:28', '2019-11-04 13:45:28'),
(5, 2, 7, 15, 'Afganistan Vs Nepal', '11/01/2019', '2:00 PM', '4:00 PM', 'Indira Gandhi Stadium', NULL, 'Gaming', '2019-11-04 13:46:33', '2019-11-04 13:46:33'),
(6, 2, 7, 15, 'India Vs South Africa', '11/27/2019', '12:00 PM', '2:00 PM', 'Indira Gandhi Stadium', NULL, 'These are the Description of the Stadium generated matches. These matches are not in the Schedule but we are going through this.', '2019-11-04 13:47:56', '2019-11-04 13:47:56'),
(9, 3, 6, 23, 'demo event', '11/05/2019', '1:00pm', '2:00pm', 'noida', NULL, 'demo demo demo', '2019-11-05 06:47:18', '2019-11-05 06:47:18'),
(10, 3, 6, 23, 'demo one', '11/06/2019', '3:00pm', '4:00pm', 'noida', NULL, 'demo demo demo', '2019-11-05 06:48:02', '2019-11-05 06:48:02'),
(11, 4, 1, 25, 'test mmc', '05/14/2019', '8:00 M', '10:00 PM', 'MARRIOT HOTEL NEW DELHI', NULL, 'hjfjfdjkfdjksfdajkfdjkasfdjklasfdjlkasfdklfjkfasjldhafdshjlfdkssfd', '2019-11-06 05:40:43', '2019-11-06 05:40:43'),
(12, 6, 1, 34, 'QUIZ round.', '11/16/2019', '11:00 Am', '12:00 Am', 'Block - c , Cannuaght Place', NULL, 'Description', '2019-11-08 06:23:56', '2019-11-08 06:23:56'),
(13, 6, 1, 34, 'Seminar', '11/20/2019', '2:00 Pm', '4:00 PM', 'Block-D, Connaught Place', 'Speaker Unknown_1', 'DescriptionDescriptionDescription Description Description Description Description Description Description', '2019-11-08 06:40:04', '2019-11-08 06:40:04'),
(14, 6, 1, 35, 'Dotta_member_Seminar', '11/22/2019', '1:00 PM', '2:00 PM', 'Block - S, Connaught Place', 'Speaker Unknown_2', 'Descriptionb Testc Descriptionb Testc Descriptionb Testc Descriptionb Testc Descriptionb Testc Descriptionb Testc Descriptionb Testc Descriptionb Testc', '2019-11-08 06:41:02', '2019-11-08 06:41:02'),
(15, 6, 1, 36, 'Seminars for Dotta Members', '11/20/2019', '11:00 AM', '12:00 AM', 'Block - c , Cannuaght Place', 'Dotta Ruppasokova Scwaggenergar', 'fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis', '2019-11-08 07:11:39', '2019-11-08 07:11:39'),
(16, 6, 1, 36, 'Seminar Past', '11/02/2019', '2:00 PM', '4:00 PM', 'Block - S, Connaught Place', 'Top Rated Speaker', 'Dexcription', '2019-11-08 07:13:00', '2019-11-08 07:13:00'),
(17, 6, 1, 36, 'Event Seminar_Today', '11/08/2019', '1:00 PM', '5:00 PM', 'Block-D, Connaught Place', 'Speaker Unknown_2', 'fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis fuf Du Plessis', '2019-11-08 07:20:48', '2019-11-08 07:20:48'),
(18, 7, 1, 47, 'Seminar', '11/15/2019', '1:00 PM', '5:00 PM', 'H-147/146', NULL, 'Mahasahat', '2019-11-08 09:21:24', '2019-11-08 09:21:24'),
(19, 7, 1, 48, 'Tug of War', '11/20/2019', '8:00 AM', '10:00 AM', 'H-147/146', 'Poniha Bhattaraya', 'Titu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu Moon', '2019-11-08 09:23:30', '2019-11-08 09:23:30'),
(20, 8, 1, 52, 'Event Schedule', '05/28/2019', '11:00 Am', '23:00 PM', '21 street', 'Speaker Unknown_1', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', '2019-11-08 10:40:49', '2019-11-08 10:40:49'),
(28, 4, 1, 25, NULL, '11/18/2019', '4:00pm', '5:00pm', NULL, NULL, 'LKJHGFDSe4r5tyuiujhgfewhjhgfe fg htr ewehjfds fgfeegf', '2019-11-18 06:11:41', '2019-11-18 06:11:41'),
(53, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 08:00', '2019-11-20 09:00', NULL, NULL, 'REGISTRATION, COFFEE BREAK AND NETWORKING', '2019-11-18 13:32:48', '2019-11-18 13:32:48'),
(54, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 09:00', '2019-11-20 09:03', NULL, NULL, 'Health, Safety & Housekeeping Concerns.', '2019-11-18 13:34:02', '2019-11-18 13:34:02'),
(55, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 09:03', '2019-11-20 09:06', NULL, NULL, 'Guyana National Anthem Performed by Circle of Love Choir.', '2019-11-18 13:34:42', '2019-11-18 13:34:42'),
(56, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 09:06', '2019-11-20 09:16', NULL, NULL, 'Welcome Remarks by Dr. Mark Bynoe, Director, Department of Energy.', '2019-11-18 13:35:23', '2019-11-18 13:35:23'),
(57, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 09:16', '2019-11-20 09:26', NULL, NULL, 'Remarks by GoInvest.', '2019-11-18 13:36:02', '2019-11-18 13:36:02'),
(58, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 09:26', '2019-11-20 09:31', NULL, NULL, 'Remarks by, Shariq Abdul Hai, CEO, Valiant Business Media', '2019-11-18 13:36:42', '2019-11-18 13:36:42'),
(66, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 11:15', '2019-11-20 11:30', NULL, NULL, 'Spotlight 2 - Mr. Greg Hill, President and Chief Operating Officer, Hess - Significance of Guyana as an Emerging Global Oil Resource .', '2019-11-18 13:43:52', '2019-11-18 13:43:52'),
(67, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 11:30', '2019-11-20 12:15', NULL, NULL, 'Theme One (1): Exploring the Equatorial Atlantic Margin - Chasing the Conjugate Mirror.\r\nSession  1  -  Panel  Discussion  examining  the  complexities  of  the  Equatorial  Atlantic  conjugate  margins,  the  acquisition,  monetization  and  management  of  seismic data and how oil companies rely heavily on seismic images of the subsurface beneath the ocean floor to determine where to drill.\r\n\r\nIntroduction by: Mr. Newell Dennison, Acting Commissioner, Guyana Geology and Mines Commission (GGMC).\r\n\r\nPresented by: Dr. William A. Heins, Professional Geologist and Management Consultant, Getech', '2019-11-18 13:45:36', '2019-11-18 13:45:36'),
(68, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 12:15', '2019-11-20 13:15', NULL, NULL, 'LUNCH AND NETWORKING', '2019-11-18 13:46:56', '2019-11-18 13:46:56'),
(69, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 13:15', '2019-11-20 13:45', NULL, NULL, 'Theme Two (2): Digital Transformation: Tools and processes to improve country and people performance  \r\nSession 2, the Digital Shift, examines the transformation to new business paradigms that leverage digital technology to challenge previously held assumptions about the Energy Industry. Q&A to follow.\r\n\r\nIntroduction by: Mr. Newell Dennison, Acting Commissioner, Guyana Geology and Mines Commission (GGMC).\r\n\r\nPresented by: Mr. Dale Blue, Senior Manager – Global Digital Services, Halliburton.', '2019-11-18 13:48:21', '2019-11-18 13:48:21'),
(77, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 19:00', '2019-11-20 21:00', NULL, NULL, 'Launch of GCCI’s Business Directory \r\n\r\nVenue: Aura Lounge, 8th Floor, Pegasus', '2019-11-18 14:00:12', '2019-11-18 14:00:12'),
(78, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 08:00', '2019-11-21 09:00', NULL, NULL, 'REGISTRATION,COFFEE BREAK AND NETWORKING', '2019-11-18 14:02:55', '2019-11-18 14:02:55'),
(79, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 09:00', '2019-11-21 09:05', NULL, NULL, 'Guyana National Anthem performed by Youth Choir', '2019-11-18 14:03:27', '2019-11-18 14:03:27'),
(81, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 10:00', '2019-11-21 10:15', NULL, NULL, 'Spotlight 5: Mr. Justin Nedd, Chief Executive Officer, GTT+:  “Why Guyana is the Investment Opportunity of Choice”', '2019-11-18 14:05:39', '2019-11-18 14:05:39'),
(82, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 10:15', '2019-11-21 10:25', NULL, NULL, 'Spotlight 6: Dr. Carlos Tooge, Vice-President for BU Oil & Mining Services Clariant Latin America: “Managing Risk of a Production Chemical Management Program”', '2019-11-18 14:06:29', '2019-11-18 14:06:29'),
(83, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 10:25', '2019-11-21 10:40', NULL, NULL, 'TEA/COFFEE BREAK AND NETWORKING', '2019-11-18 14:07:10', '2019-11-18 14:07:10'),
(84, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 10:40', '2019-11-21 10:50', NULL, NULL, 'Spotlight 7:  Ms. Ayanna Watson, Commercial Manager, SOL Guyana', '2019-11-18 14:07:49', '2019-11-18 14:07:49'),
(86, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 12:00', '2019-11-21 13:00', NULL, NULL, 'LUNCH AND NETWORKING', '2019-11-18 14:10:36', '2019-11-18 14:10:36'),
(87, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 13:00', '2019-11-21 13:10', NULL, NULL, 'Spotlight 8: Mrs. Kerri Gravesande-Bart, Strategic Recruitment Solutions Inc. .', '2019-11-18 14:11:11', '2019-11-18 14:11:11'),
(88, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 13:10', '2019-11-21 13:20', NULL, NULL, 'Spotlight 9:  Mr. Darryl Eisler, Guyana Country Lead, Baker Hughes - A GE Company “Low Carbon Overview”.', '2019-11-18 14:11:46', '2019-11-18 14:11:46'),
(91, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 14:20', '2019-11-21 14:35', NULL, NULL, 'TEA/COFFEE BREAK AND NETWORKING', '2019-11-18 14:15:11', '2019-11-18 14:15:11'),
(92, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 14:35', '2019-11-21 15:05', NULL, NULL, 'Workshop Two (2): Risks, Assessments & Insurance Presentation:\r\n\r\nPresenter:\r\n\r\n1. Mr. Willem Bloem, Principal, Bloem Consultants LLC\r\n\r\nThis is an in-depth look into the insurance complexities of Risk Identification and Evaluation, Drilling Risks and Control of Wells, Upstream and Downstream Liabilities, Offshore Construction. Upstream Operational Insurance, Offshore Business Interruption, Energy Policy Wordings and Claims, FPSO Insurance, Risk Management, and Downstream Operational Insurance.', '2019-11-18 14:16:34', '2019-11-18 14:16:34'),
(94, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 15:15', '2019-11-21 15:25', NULL, NULL, 'Spotlight 12:  The ISIKA Shore Base - Mr. Jean-Louis Chassagne, Vice President, InterOil', '2019-11-18 14:17:47', '2019-11-18 14:17:47'),
(99, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 08:00', '2019-11-22 09:00', NULL, NULL, 'REGISTRATION,COFFEE BREAK AND NETWORKING', '2019-11-18 15:16:51', '2019-11-18 15:16:51'),
(100, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 09:00', '2019-11-22 09:05', NULL, NULL, 'Guyana National Anthem - Roy on Sax and Mikey Smith on Steel Pan .', '2019-11-18 15:17:36', '2019-11-18 15:17:36'),
(106, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 09:05', '2019-11-21 10:00', NULL, NULL, 'Workshop One (1): Risks, Assessments & Insurance Presentation:\r\n\r\nPresenters:\r\n1. Mr. Denzel Mensah, Marsh JLT Specialty \r\n2. Ms. Tracy Gibson, Director of the Insurance Supervision Department, Bank of Guyana\r\n3. Dr. Joanthan Wylde, Global Head of Innovation, Clariant Oil & Mining Services, Clariant\r\n\r\nWorkshop  1    is  an  in-depth  look  into  the  insurance  complexities  of  Risk  Identification  and  Evaluation,  Drilling  Risks  and  Control  of  Wells,  Upstream  and  Downstream  Liabilities,  Offshore  Construction,  Upstream  Operational  Insurance  Offshore  Business  Interruption,  Energy  Policy  Wordings  and  Claims,  FPSO  Insurance, Risk Management, and Downstream Operational Insurance.', '2019-11-18 15:36:37', '2019-11-18 15:36:37'),
(115, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 10:50', '2019-11-21 12:00', NULL, NULL, 'Theme Six (6): Strategies to Ensure Successful Transition – During the “Big Boom”. \r\nSession 6 – Panel Discussion of senior level policy-makers representing established oil and gas provinces sharing the challenges and successes of their transition from non-producer to producer country, the early capture of the petroleum sector value-chain, managing a finite resource while preparing for the future in the present, and their economic diversification strategies for sustainable and inclusive growth.\r\n\r\nModerator: Professor Gary Dirks, Senior Director Golden Futures Laboratory Wringley Chair, Director LightWorks, Arizona State University.\r\n\r\n1. Mr. Rudolf T. Ellias, Managing Director, Staatsolie - Suriname\r\n\r\n2. Adowa Afriyie Wiafe, Manager (Legal), Ghana National Petroleum Corporation (GNPC) - Ghana\r\n\r\n3. Samuel Addoteye, Human Resource Manager. Ghana National Petroleum Corporation (GNPC) - Ghana\r\n\r\n4. Ambassador Dr. Niel Parsan, Senior Consultant., Parsons Cross', '2019-11-18 16:41:57', '2019-11-18 16:41:57'),
(122, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 10:05', '2019-11-22 10:25', NULL, NULL, 'Spotlight 13: Go- Invest Presentation', '2019-11-18 17:41:35', '2019-11-18 17:41:35'),
(123, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 10:25', '2019-11-22 10:35', NULL, NULL, 'Spotlight 14: Michiel Heuven, Regional Head of Operations, North America & Caribbean SBM Offshore', '2019-11-18 17:42:39', '2019-11-18 17:42:39'),
(125, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 10:55', '2019-11-22 11:00', NULL, NULL, 'TEA/COFFEE BREAK AND NETWORKING', '2019-11-18 17:45:42', '2019-11-18 17:45:42'),
(127, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 11:30', '2019-11-22 12:00', NULL, NULL, 'Workshop Four (4) Opportunities Within The Oil & Gas Sector For Local Business Development (CLBD) | Presenter: Mr. Patrick Henry, Director, Centre for Local Business Development', '2019-11-18 17:49:13', '2019-11-18 17:49:13'),
(128, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 12:00', '2019-11-22 13:00', NULL, NULL, 'LUNCH AND NETWORKING', '2019-11-18 17:49:52', '2019-11-18 17:49:52'),
(137, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 19:00', '2019-11-21 21:00', NULL, NULL, 'Welcome Reception & Business Mixer hosted by Go-Invest \r\n\r\nVenue: MovieTowne Guyana', '2019-11-18 19:07:53', '2019-11-18 19:07:53'),
(140, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 09:31', '2019-11-20 09:36', NULL, NULL, 'Mr. Nicolas Boyer, President, Georgetown Chamber of Commerce (GCCI).', '2019-11-19 08:08:18', '2019-11-19 08:08:18'),
(142, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 09:36', '2019-11-20 09:41', NULL, NULL, 'Captain Gerry Gouveia, Chairman Private Sector Commission.', '2019-11-19 08:13:37', '2019-11-19 08:13:37'),
(150, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 13:20', '2019-11-21 13:30', NULL, NULL, 'Spotlight 10: Ms. Catherine Duggan, Stakeholder Engagement Manager, Guyana, Tullow Oil - “Tullow in Guyana” .', '2019-11-19 08:52:04', '2019-11-19 08:52:04'),
(151, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 15:05', '2019-11-21 15:15', NULL, NULL, 'Spotlight 11:  JHI International & Associates', '2019-11-19 08:53:46', '2019-11-19 08:53:46'),
(152, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 15:25', '2019-11-21 16:15', NULL, NULL, 'Theme Eight (8): Strategies to Ensure Economic Diversity – More than the “Big Boom”.\r\nSession 8 - Panel Discussion of senior financial analyst discussing the negative consequences for an emerging producer to develop too heavy a dependence on oil revenue and examining successful diversification methods to stimulate the creation of an agile and durable economic portfolio.\r\n\r\nModerator: Mr. Mark Beare, Senior Associate, Natural Resources and Energy Portfolio, Oxford Policy Management\r\n\r\n1. Mr. Wendell Ramoutar, Partner, PwC Trinidad and Tobago \r\n\r\n2. Mr.  Pedro Haas, Partner, Hartree Partners\r\n\r\n3. Mr. Schreiner Parker, VP Latin America/Caribbean, Rystad Energy\r\n\r\n4. Dr. Norman  Munroe, Visiting University of Florida Fulbright Scholar to The University of Guyana Faculty of Engineering  and Technology, Oil and Gas Programme', '2019-11-19 08:56:04', '2019-11-19 08:56:04'),
(155, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 11:00', '2019-11-22 11:30', NULL, NULL, 'Workshop Three (3) Securing & Maintaining Successful Partnerships in Oil & Gas.  \r\nPresenter:  Mr. Peter Stewart, Partner, Clyde & Co', '2019-11-19 09:04:39', '2019-11-19 09:04:39'),
(156, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 13:00', '2019-11-22 13:10', NULL, NULL, 'Spotlight 16: : Repsol', '2019-11-19 09:05:54', '2019-11-19 09:05:54'),
(157, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 13:10', '2019-11-22 13:20', NULL, NULL, 'Spotlight 17: CWSG Inc.', '2019-11-19 09:06:38', '2019-11-19 09:06:38'),
(161, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 14:30', '2019-11-22 14:40', NULL, NULL, 'Spotlight 18: Dr. Dennis A. Pieters, Director, Mid Atlantic Oil and Gas', '2019-11-19 12:53:15', '2019-11-19 12:53:15'),
(162, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 14:40', '2019-11-22 14:55', NULL, NULL, 'TEA/COFFEE BREAK AND NETWORKING', '2019-11-19 12:54:02', '2019-11-19 12:54:02'),
(163, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 14:55', '2019-11-22 15:40', NULL, NULL, 'Theme Thirteen (13): Highlighting Young Guyanese Professionals- The Next Generation.\r\n\r\nModerator: Dr. Kiven Pierre, University of Guyana \r\n\r\n1. Mr. Ajmer Samaroo, Facilities Engineer, ExxonMobil\r\n\r\n2. Ms. Marissa Foster-Gordon, Petroleum Geologist, Department of Energy (DOE)\r\n\r\n3. Dr. Nikita La Cruz, Research Associate,Department of Earth and Environmental Sciences, University of Michigan \r\n\r\n4. Mr. Richard Rambarran, Executive Director, Georgetown Chamber of Commerce \r\n\r\n5. Shaunette Rayside, HR Manager, Saipem Guyana', '2019-11-19 12:55:49', '2019-11-19 12:55:49'),
(164, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 09:42', '2019-11-20 09:57', NULL, NULL, 'Keynote Address by Mr. Hunter Farris, Senior Vice President, Upstream Oil and Gas Deep Water, ExxonMobil', '2019-11-20 09:39:26', '2019-11-20 09:39:26'),
(165, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 09:58', '2019-11-20 10:15', NULL, NULL, 'Opening Address by H.E. Brigadier David Granger, President, Cooperative Republic of Guyana', '2019-11-20 09:40:26', '2019-11-20 09:40:26'),
(166, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 10:15', '2019-11-20 10:20', NULL, NULL, 'Closing Performance - A moment like this sung by Shenice with Mark Hall on the piano', '2019-11-20 09:40:56', '2019-11-20 09:40:56'),
(167, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 10:20', '2019-11-20 10:50', NULL, NULL, 'RIBBON CUTTING BY DR. MARK BYNOE FOLLOWED BY THE OFFICIAL TOUR OF THE 2019 GIPEX EXPO / COFFEE BREAK', '2019-11-20 09:41:55', '2019-11-20 09:41:55'),
(168, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 11:00', '2019-11-20 11:12', NULL, NULL, 'Spotlight 1 - Guyana’s History of Exploration, Prof Suresh Narine, Chair CGX /On Energy Inc.', '2019-11-20 09:43:52', '2019-11-20 09:43:52'),
(171, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 14:30', '2019-11-20 14:45', NULL, NULL, 'TEA/COFFEE BREAK AND NETWORKING', '2019-11-20 09:48:18', '2019-11-20 09:48:18'),
(174, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 15:30', '2019-11-20 15:40', NULL, NULL, 'Go-Invest Magazine Launch', '2019-11-20 09:54:51', '2019-11-20 09:54:51'),
(175, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 15:40', '2019-11-20 15:55', NULL, NULL, 'Spotlight 3: Mr. Jean-Michel Lavergne, Senior Vice President of the Americas, TOTAL EREN', '2019-11-20 09:55:17', '2019-11-20 09:55:17'),
(176, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 15:55', '2019-11-20 16:05', NULL, NULL, 'Spotlight 4: Working Together Mr. Giorgio Martelli, President and Chief Executive Officer, Saipem', '2019-11-20 09:55:43', '2019-11-20 09:55:43'),
(177, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 16:05', '2019-11-20 16:55', NULL, NULL, 'Theme Five (5): Protecting the Guyana-Suriname Basin: Guyana’s Blue/ Green Commitment.\r\nSession 5 - Panel Discussion examining the diverse ecosystems of the Guyana-Suriname basin and the judicious nature of enforceable protection plans to ensure  minimal  environmental  disruption,  the  benefits  of    peer-to-peer  and  host  government-to-industry  information  share,  regional  and  international  cooperation initiatives and the adoption and application of Conventions and Protocols to strengthen preparedness and appropriate response to incidents of oil, hazardous substance and chemical pollution during the three phases of petroleum operations: 1) exploration;2) development and production;and 3) decommissioning and rehabilitation. \r\n\r\nModerator: Dr. Gyanpriya Maharaj, Director, Center for the Study of Biological Diversity (CSBD), Faculty of Natural Sciences, University of Guyana \r\n\r\n1. Dr. Vincent Adams, Director, Environmental Protection Agency (EPA)\r\n\r\n2. Mr. John Flores, MARAD\r\n\r\n3. Lt. Col Kester Craig, Director General, Civil Defense Commission (CDC) \r\n\r\n4. Mr. Nya Gbaintor, Principal Director Administration & Legal Services, Liberian Maritime Authority\r\n\r\n5. Prof. Judith Rosales, Director University of Guyana,  PhD Programme in Biodiversity\r\n\r\n6. Dr. David Singh, President WWF Guianas.', '2019-11-20 09:57:35', '2019-11-20 09:57:35'),
(178, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 13:30', '2019-11-21 14:20', NULL, NULL, 'Theme Seven (7) – Strategies to Ensure Economic Strength – Sustaining the “Big Boom”.\r\nSession  7  –  Panel  Discussion  of  regional  heads  of  global  development  banks  discuss  structured  development  policies  for  the  accountable  management  of  petroleum  resources,  fiscal  policies  for  macroeconomic  stability,  growth  and  income  distribution,  fair  and  efficient  tax  system,  the  benefits  of  an  efficiently  managed sovereign wealth fund and the critical role of governance institutions.\r\n\r\nModerated: Mr. Mark Beare, Senior Associate, Natural Resources and Energy Portfolio, Oxford Policy Management \r\n\r\n1. Ms. Susana Moreira, Oil and Gas Specialist, World Bank\r\n\r\n2. Mr. Sydney Armstrong, Head of Department of Economics, Faculty of Social Sciences, University of Guyana \r\n\r\n3. Mr. Richard Rambarran, Executive Director, Georgetown Chamber of Commerce\r\n\r\n4. Professor Gary Dirks, Senior Director Golden Futures Laboratory Wringley Chair, Director LightWorks, Arizona State University and Retired VP BP Asia', '2019-11-20 10:00:57', '2019-11-20 10:00:57'),
(179, 10, 1, 101, NULL, '11/21/2019', '2019-11-21 16:15', '2019-11-21 16:55', NULL, NULL, 'Theme Nine (9): Strategies to Support Guyana’s Green Development Initiatives.\r\nSession  9  –  Panel  Discussion  from  current  and  former  senior-level  policy  makers  and  innovators  on  strategies  to  sustainable  protect  ecosystems  and  biodiversity  in  the  management  of  natural  resources  and  stimulate  the  greening  of  economically  diverse  investments  in  energy  efficient  technologies,  renewable energy, public transport, sustainable agriculture, and environment friendly tourism.\r\n\r\nModerator: Ms. Janell Christian, Director, Office of Climate Change Department, Ministry of the Presidency- she has not confirmed participation\r\n\r\n1. Ms. Ndibi Schwiers, Director, Department of Environment (DoE)\r\n\r\n2. Mr. Olivier Wattez, Managing Director of Guyana, TOTAL E&P Guyana\r\n\r\n3. Dr. Gyanpriya Maharaj, Director, Centre for the Study of Biological Diversity (CSBD), Faculty of Natural Sciences, University of Guyana \r\n\r\n4. Dr. Devon Gardner, Head Energy Desk, CARICOM', '2019-11-20 10:03:51', '2019-11-20 10:03:51'),
(180, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 09:05', '2019-11-22 09:20', NULL, NULL, 'Theme Ten (10): Growing Together - The Evolution of Local Content - The Ghanaian Example \r\n\r\nPresenter: Mr. Kwame Jantuah, CEO, Africa Energy Consortium     \r\n\r\nSession 10 - Presentation on local content and developing an effective oil and gas policy and governance. Q&A', '2019-11-20 10:06:09', '2019-11-20 10:06:09'),
(181, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 09:20', '2019-11-22 10:05', NULL, NULL, 'Theme Eleven (11): Working Together.\r\nSession 11 - Presentations by senior level executives representing exploration companies currently operating in Guyana chronicling their past, present and future national integration, capacity building and employment strategies.\r\n\r\n1. Mr. Rod Henson, President, ExxonMobil Guyana\r\n\r\n2. Mr. Olivier Wattez, President, Total E&P Guyana\r\n\r\n3. Mr. Ryan Ramjit, Exploration Country Manager, Repso\r\n\r\n4. Mr. Lancelot Khan, President, Guyana Drones Operators Association\r\n\r\n5. Mr. Dane Gobin, Chief Executive Officer, Iowkarama', '2019-11-20 10:08:22', '2019-11-20 10:08:22'),
(182, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 10:35', '2019-11-22 10:45', NULL, NULL, 'Spotlight 15: TechnipFMC', '2019-11-20 10:09:41', '2019-11-20 10:09:41'),
(183, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 13:20', '2019-11-22 14:45', NULL, NULL, 'Theme Twelve (12): Expanding Education for the Oil and Gas Industry.\r\nSession 12 – A series dedicated to developing national capacity and participation through building capacity in Universities, creating accredited vocational training facilities and introducing STEM programs throughout the educational system', '2019-11-20 10:13:03', '2019-11-20 10:13:03'),
(184, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 13:20', '2019-11-22 13:55', NULL, NULL, 'Universities in transition\r\n\r\nModerator: : Prof. Paloma Mohamed, Chair, Transition Management Committee and Energy Focal Point (UG Response), University of Guyana \r\n\r\n1. Dr. Sally Radford, Editor, Energy Caribbean Observer\r\n\r\n2. Dr. Raffie Hosien, Head of Department, Department of Chemical Engineering, University of the West Indies\r\n\r\n3. Prof. Andrew Jupiter, Coordinator of the Petroleum Studies, Department of Petroleum Engineering, University of the West Indies Professor \r\n\r\n4. Gary Dirks, Senior Director Golden Futures Laboratory Wringley Chair, Director LightWorks, Arizona State University and Retired VP BP Asia.\r\n\r\n5. Dr. Norman Munroe, Visiting University of Florida Fulbright Scholar to The University of Guyana Faculty of Engineering and Technology, Oil and Gas Programme', '2019-11-20 10:14:46', '2019-11-20 10:14:46'),
(185, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 13:55', '2019-11-22 14:30', NULL, NULL, 'Technical Vocation\r\n\r\n1. Ms. Betty Namubiru, Manager National Content, Petroleum Authority of Uganda\r\n\r\n2. Mr.Christopher Lynch, Director, GGMC  - Steam/ Data\r\n\r\n3. Floyd Scott, Director, CTVET', '2019-11-20 10:16:25', '2019-11-20 10:16:25'),
(186, 10, 1, 101, NULL, '11/22/2019', '2019-11-22 15:50', '2019-11-22 16:10', NULL, NULL, 'Spotlight 18: Dr. Dennis A. Pieters, Director, Mid Atlantic Oil and Gas', '2019-11-20 10:18:11', '2019-11-20 10:18:11'),
(187, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 14:45', '2019-11-20 15:30', NULL, NULL, 'Theme Four (4): Drilling the Equatorial  Atlantic Margin: Near-Term High-Impact Drilling Programs.\r\nSession 4 - Spotlight Presentations by senior-level executives representing oil companies familiar with exploration in the Equatorial Atlantic Margin with each examining the last 12 years of “chasing the conjugate”, if the Liza (Guyana 2015) discovery validates the “twin basin” theory, the new economics of deep-water exploration and unlocking the “secrets” of the South American / West African margins with near-term high-impact drilling programs.\r\n\r\n1. Mr. Olivier Wattez, Managing Director of Guyana, TOTAL E&P Guyana\r\n\r\n2. Ms. Maria Guedez, Guyana and Suriname Exploration Manager, ExxonMobil', '2019-11-20 13:42:36', '2019-11-20 13:42:36'),
(188, 10, 1, 101, NULL, '11/20/2019', '2019-11-20 13:45', '2019-11-20 14:30', NULL, NULL, 'Theme Three (3): Examining the Equatorial Atlantic Margin: Geology & Governance\r\nSession 3 - Panel Discussion of senior government officials from Countries on both sides of the Equatorial Atlantic Margin on their development strategies and a comparative discussion of the challenges experienced by potential, emerging and mature producers during the phases of exploration, discovery and production.\r\n\r\nModerator: Mr. Mark Beare, Senior Associate, Natural Resources and Energy Portfolio, Oxford Policy Management.\r\n\r\n1. Dr. Mark Bynoe, Director, Ministry of the Presidency, Department of Energy\r\n\r\n2. Mr. Michael Aryeetey, Ghana National Petroleum Corporation \r\n\r\n3. Hon. Timothy Kabba, Director General, Petroleum Directorate – Sierra Leone \r\n\r\n4. Atty. Siafuah-Mai Gray, Chief Executive Officer, National Oil Company of Liberia – Liberia', '2019-11-20 14:02:48', '2019-11-20 14:02:48'),
(189, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 08:00', '2019-12-03 09:00', NULL, NULL, 'Registration, Coffee Break and Networking', '2019-11-29 09:28:35', '2019-11-29 09:28:35'),
(190, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 09:00', '2019-12-03 09:05', NULL, NULL, 'Health, Safety and Housekeeping Concerns', '2019-11-29 09:29:20', '2019-11-29 09:29:20'),
(191, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 09:10', '2019-12-03 09:20', NULL, NULL, 'Greeting Remarks from Organizing Agency - Valiant Business Media', '2019-11-29 09:30:17', '2019-11-29 09:30:17'),
(192, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 09:25', '2019-12-03 09:45', NULL, NULL, 'Remarks by Supporting Partners:  Sean Finlay (GI) & Nicola Nixon, Vice President IMQS', '2019-11-29 09:30:52', '2019-11-29 09:30:52'),
(193, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 09:50', '2019-12-03 10:05', NULL, NULL, 'Address by Lead Sponsor (AURUM)', '2019-11-29 09:31:30', '2019-11-29 09:31:30'),
(194, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 10:10', '2019-12-03 10:25', NULL, NULL, 'Opening address -  Dr John Guven Minerals (ICRAG)', '2019-11-29 09:32:06', '2019-11-29 09:32:06'),
(195, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 10:30', '2019-12-03 11:00', NULL, NULL, 'Coffee Break and Networking', '2019-11-29 09:32:54', '2019-11-29 09:32:54'),
(196, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 11:00', '2019-12-03 11:15', NULL, NULL, 'Keynote 1: An overview of European mining industry: Past, Present and Future - Eoin McGrath, Head of Minerals, GSI', '2019-11-29 09:33:45', '2019-11-29 09:33:45'),
(197, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 11:20', '2019-12-03 11:35', NULL, NULL, 'Keynote 2: Mineral Policy - Petri Peltonen (MEAE Finland)', '2019-11-29 09:34:27', '2019-11-29 09:34:27'),
(198, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 11:40', '2019-11-11 11:55', NULL, NULL, 'Keynote 3: New Initiatives to bolster European mining industry. AURUM Speaker', '2019-11-29 09:35:07', '2019-11-29 09:35:07'),
(200, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 12:00', '2019-12-03 13:00', NULL, NULL, 'Panel 1: Association Panel Session 1: Europe as a mining investment destination - Policies and Progress. - Ministries and Associations (DGEG Portugal, GMG Group, MEAE Finland, EFG) Andrew Gaynor (M)', '2019-11-29 09:37:07', '2019-11-29 09:37:07'),
(201, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 13:00', '2019-12-03 14:00', NULL, NULL, 'Lunch and Networking', '2019-11-29 09:37:49', '2019-11-29 09:37:49'),
(202, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 14:00', '2019-12-03 14:15', NULL, NULL, 'Keynote 4:The 3D EXPERIENCE mine – a platform to accelerate innovation and collaboration - Rob Allen (Dassault Systèmes)', '2019-11-29 09:38:29', '2019-11-29 09:38:29'),
(203, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 14:20', '2019-12-03 14:35', NULL, NULL, 'Keynote 5: See Your Earth More Clearly - Reece van Buren (HiSeis)', '2019-11-29 09:39:06', '2019-11-29 09:39:06'),
(204, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 14:40', '2019-12-03 14:55', NULL, NULL, 'Keynote 6 Topic TBD - Kate McCormick (CDE Global)', '2019-11-29 09:40:34', '2019-11-29 09:40:34'),
(205, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 15:00', '2019-12-03 15:15', NULL, NULL, 'Keynote 7: Cutting-Edge Technology for Industrial Lubricants in Mining Applications - George Diloyan (Nanotech Industrial Solutions )', '2019-11-29 09:41:09', '2019-11-29 09:41:09'),
(206, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 15:20', '2019-12-03 15:30', NULL, NULL, 'Keynote 8:The reinvention of explosives-Jonathan Cohen (Autostem B.V)', '2019-11-29 09:42:16', '2019-11-29 09:42:16'),
(207, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 15:35', '2019-12-03 15:50', NULL, NULL, 'Keynote 9: Zero-Trip Innovations – The Zero-Trip Wedge - Michael Dineen (Zero Trip Innovations)', '2019-11-29 09:42:56', '2019-11-29 09:42:56'),
(208, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 15:55', '2019-12-03 16:10', NULL, NULL, 'Keynote 10: Pedro Bernardo - Orica', '2019-11-29 09:43:30', '2019-11-29 09:43:30'),
(209, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 16:10', '2019-12-03 16:30', NULL, NULL, 'Coffee Break and Networking', '2019-11-29 09:44:09', '2019-11-29 09:44:09'),
(210, 11, 1, 113, NULL, '12/03/2019', '2019-12-03 16:30', '2019-12-03 17:10', NULL, NULL, 'GI Session', '2019-11-29 09:44:52', '2019-11-29 09:44:52'),
(211, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 08:00', '2019-12-04 09:00', NULL, NULL, 'Registration, Coffee Break and Networking', '2019-11-29 09:45:36', '2019-11-29 09:45:36'),
(212, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 09:00', '2019-12-04 09:15', NULL, NULL, 'Keynote 1: Automation in Mining Sector - Brian Carroll (Sandvik)', '2019-11-29 09:46:07', '2019-11-29 09:46:07'),
(213, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 09:20', '2019-12-04 09:35', NULL, NULL, 'Keynote 2: ONE-STEP INSTALLATION OF ROCK BOLTS - Michael Hosp (Minova)', '2019-11-29 09:46:44', '2019-11-29 09:46:44'),
(214, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 09:40', '2019-12-04 09:55', NULL, NULL, 'Keynote 3: Robominers – Resilient Bio inspired modular Robotic Miners - Marko Komac (EFG)', '2019-11-29 09:47:24', '2019-11-29 09:47:24'),
(215, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 10:00', '2019-12-04 10:15', NULL, NULL, 'Keynote 4: Liv Caroll (Accenture)', '2019-11-29 09:47:56', '2019-11-29 09:47:56'),
(216, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 10:20', '2019-12-04 10:35', NULL, NULL, 'Keynote 5: Simulation made for industry 4.0 -Jan Jindra (ESS)', '2019-11-29 09:48:27', '2019-11-29 09:48:27'),
(217, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 10:40', '2019-12-04 10:55', NULL, NULL, 'Keynote 6:   Delineating mineralisation using Predrilling Virtual Boreholes - Gordon Stove  (Adrok Ltd.)', '2019-11-29 09:48:59', '2019-11-29 09:48:59'),
(218, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 10:55', '2019-12-04 11:25', NULL, NULL, 'Coffee Break and Networking', '2019-11-29 09:49:38', '2019-11-29 09:49:38'),
(219, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 11:25', '2019-12-04 11:40', NULL, NULL, 'Keynote 7: Ireland, The gateway to innovation in mining - Gunnar Nyström (Boliden - Tara Mines Ireland)', '2019-11-29 09:51:13', '2019-11-29 09:51:13'),
(220, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 11:45', '2019-12-04 12:00', NULL, NULL, 'Keynote 8: Portugal a mining country - Challenges and opportunities for sustainable mining - Paula Dinis (DGEG Portugal)', '2019-11-29 09:52:27', '2019-11-29 09:52:27'),
(221, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 12:05', '2019-12-04 12:20', NULL, NULL, 'Keynote 9: Why the world needs mining? - Jane Isaacs (ABMEC)', '2019-11-29 09:53:15', '2019-11-29 09:53:15'),
(222, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 12:25', '2019-12-04 12:40', NULL, NULL, 'Keynote 10: Mining industry in Finland 2020 and ahead, innovations and digitalization - Harry Sandström (Mining Finland)', '2019-11-29 09:53:46', '2019-11-29 09:53:46'),
(223, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 12:45', '2019-12-04 13:30', NULL, NULL, 'Panel 1: CEO panel - Sustainability Challenges in the Mining Sector and Strategies to mitigate them (Somincor, Nordic Mining, Thomson Resources, Nordic Gold)', '2019-11-29 09:54:15', '2019-11-29 09:54:15'),
(224, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 13:30', '2019-12-03 14:30', NULL, NULL, 'Lunch and Networking', '2019-11-29 09:54:56', '2019-11-29 09:54:56'),
(225, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 14:30', '2019-12-04 14:55', NULL, NULL, 'Keynote 11: European Mining Renaissance - Christopher Ecclestone (Hallgarten & Company)', '2019-11-29 09:55:43', '2019-11-29 09:55:43'),
(226, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 15:00', '2019-12-04 15:15', NULL, NULL, 'Keynote 12: Methods in Improving Directional Drilling with Directional Core Barrels. - Nils Ivar Iversen  (Aziwell)', '2019-11-29 09:56:12', '2019-11-29 09:56:12'),
(227, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 15:20', '2019-12-04 15:35', NULL, NULL, 'Keynote 13: Increasing productivity at Neves-Corvo Mine - Kenneth Norris (Somincor)', '2019-11-29 09:56:48', '2019-11-29 09:56:48'),
(228, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 15:40', '2019-12-04 15:55', NULL, NULL, 'Keynote 14:  High tensile steel to increase safety and efficiency - Rico Brandle (GeoBrugg)', '2019-11-29 09:57:15', '2019-11-29 09:57:15'),
(229, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 15:55', '2019-12-04 16:15', NULL, NULL, 'Coffee Break and Networking', '2019-11-29 09:57:50', '2019-11-29 09:57:50'),
(230, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 16:15', '2019-12-04 16:30', NULL, NULL, 'Keynote 15: Path to Production - Brian Wesson (Nordic Gold)', '2019-11-29 09:58:21', '2019-11-29 09:58:21'),
(231, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 16:35', '2019-12-04 16:50', NULL, NULL, 'Keynote 16: Resource Industry: 2020 Vision - Heather Ednie\r\n(Global Mining Guidelines Group)', '2019-11-29 09:58:58', '2019-11-29 09:58:58'),
(232, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 16:55', '2019-12-04 17:10', NULL, NULL, 'Keynote 17: Professional Support for a Mining Renaissance - Robin Dean  (IOM3)', '2019-11-29 09:59:27', '2019-11-29 09:59:27'),
(233, 11, 1, 113, NULL, '12/04/2019', '2019-12-04 17:15', '2019-12-04 17:30', NULL, NULL, 'Keynote 18: Scandinavia offers new supply of industrial minerals for Europe. Example from Norway - Ivar Fossum (Nordic Mining)', '2019-11-29 09:59:57', '2019-11-29 09:59:57');

-- --------------------------------------------------------

--
-- Table structure for table `speakers`
--

CREATE TABLE `speakers` (
  `id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `detailsid` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `cname` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `position` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `description` text,
  `toprated` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `speakers`
--

INSERT INTO `speakers` (`id`, `event_id`, `userid`, `detailsid`, `name`, `cname`, `image`, `position`, `email`, `phone`, `description`, `toprated`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 10, 'Hon. Mohamed Abdel Vetah', 'Minister  Ministry of Petroleum, Energy and Mines', 'speakerlogo/1572428101_Mohamed-Abdel-Vetah.png', 'Minister', 'Vetah@gmail.com', '9876543212', 'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum', 0, '2019-10-30 09:35:01', '2019-10-30 09:35:01'),
(2, 2, 7, 18, 'Vinayak Bhatt', 'HCL Technologies', 'speakerlogo/1572875645_mmc2019logo.png', 'Senior PHP Developer', 'email@yopmail.com', '356554646', 'Fraction if the Detail in the element in the category if the Speakers, it is the best i n the market.', 1, '2019-11-04 13:54:05', '2019-11-04 13:54:05'),
(3, 6, 1, 43, 'Speaker Unknown_1', 'Company Unknown_1', 'speakerlogo/1573194418_istockphoto-825083570-612x612.jpg', 'Delta Member_1', 'delta@yopmail.com', '965899655', 'Delta Members are very funny to hear. Please copy and paste it in . Delta Members are very funny to hear. Please copy and paste it in . Delta Members are very funny to hear. Please copy and paste it in . Delta Members are very funny to hear. Please copy and paste it in . Delta Members are very funny to hear. Please copy and paste it in . Delta Members are very funny to hear. Please copy and paste it in . Delta Members are very funny to hear. Please copy and paste it in .', 0, '2019-11-08 06:26:58', '2019-11-08 06:26:58'),
(4, 6, 1, 43, 'Speaker Unknown_2', 'Company Unknown_2', 'speakerlogo/1573194478_man2.jpeg', 'Delta Member_2', 'dol@yopmail.com', '805541584158', 'Delta Members are very funny to hear. Please copy and paste it in . Delta Members are very funny to hear. Please copy and paste it in . Delta Members are very funny to hear. Please copy and paste it in . Delta Members are very funny to hear. Please copy and paste it in . Delta Members are very funny to hear. Please copy and paste it in .', 0, '2019-11-08 06:27:58', '2019-11-08 06:27:58'),
(5, 6, 1, 43, 'Dotta Ruppasokova Scwaggenergar', 'Company Unknown_4', 'speakerlogo/1573194892_man3.jpeg', 'Senior Mandate Minister', 'sadsadas0d@yopmail.com', '3243243243', 'What the descriptionWhat the descriptionWhat the descriptionWhat the descriptionWhat the descriptionWhat the description', 0, '2019-11-08 06:34:52', '2019-11-08 06:34:52'),
(6, 6, 1, 43, 'Top Rated Speaker', 'Speaker Company 5', 'speakerlogo/1573194960_man4.jpeg', 'Dotta Membe _44', 'do@yopmail.com', '398846464', 'What the descriptionWhat the descriptionWhat the descriptionWhat the descriptionWhat the descriptionWhat the descriptionWhat the descriptionWhat the descriptionWhat the description', 1, '2019-11-08 06:36:00', '2019-11-08 06:36:00'),
(7, 6, 1, 44, 'One Speaker _11', 'Company Unknown_6', 'speakerlogo/1573195034_man5.jpeg', 'Position_Unknown6', 'position.Position@yopmail.com', '9631525421454', 'What the descriptionWhat the descriptionWhat the descriptionWhat the descriptionWhat the descriptionWhat the descriptionWhat the descriptionWhat the descriptionWhat the descriptionWhat the descriptionWhat the descriptionWhat the description', 1, '2019-11-08 06:37:14', '2019-11-08 06:37:14'),
(8, 7, 1, 50, 'Poniha Bhattaraya', 'El Classico', 'speakerlogo/1573204957_logo1.png', 'Position_Unknown6', 'email@mainleast.com', '70255514644', 'Titu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu MoonTitu Moon', 0, '2019-11-08 09:22:37', '2019-11-08 09:22:37'),
(9, 8, 1, 55, 'Speaker Unknown_1', 'Company Unknown_1', 'speakerlogo/1573209599_man2.jpeg', '1', 'email@mainleast.com', '9631525421454', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:39:59', '2019-11-08 10:39:59'),
(14, 10, 1, 64, 'ggggggg', 'kkkkkkkkkkkkkkkkkkkkkkkkkk', 'speakerlogo/1573922698_45.PNG', 'jfjgjfjjgjgjg', 'info@guyanaoilexpo.com', '12122121212121', 'hjhjdhjsfdhshdhusfdhufsuifshhfi', 1, '2019-11-16 16:44:58', '2019-11-16 16:44:58'),
(44, 10, 1, 95, 'Mark Bynoe', 'Department of Energy, Ministry of the Presidency - Guyana', 'speakerlogo/1573946496_mark.jpg', 'Director', '*****@*****.com', '**********', 'The Department of Energy was created on August 1, 2018 to effectively manage the hydrocarbon resources of the Cooperative Republic of Guyana. The May 2015 and subsequent discoveries of oil and associated gases, have resulted in Guyana being regarded amongst the top reserve holders worldwide. This has the potential for improved standard of living for all Guyanese and transformational and impactful development to occur.', 0, '2019-11-16 23:21:36', '2019-11-16 23:21:36'),
(45, 10, 1, 95, 'Hunter Farris', 'ExxonMobil', 'speakerlogo/1573946653_hunter1.jpg', 'Senior Vice President, Upstream Oil and Gas Deep Water', '*****@*****.com', '**********', 'ExxonMobil is the world\'s largest publicly traded international oil and gas company. It holds an industry-leading inventory of global oil and gas resources. It is the world\'s largest refiner and marketer of petroleum products, and its chemical company ranks among the world\'s largest. ExxonMobil applies science and innovation to find better, safer and cleaner ways to deliver the energy the world needs. ExxonMobil\'s upstream business encompasses high-quality exploration opportunities across all resource types and geographies, an industry-leading resource base, a portfolio of world-class projects, and a diverse set of producing assets. It has an active exploration or production presence in 36 countries. ExxonMobil is proud to play a leading role in providing the energy the world needs to support economic growth, technological advancement and the well-being of communities around the globe.', 0, '2019-11-16 23:24:13', '2019-11-16 23:24:13'),
(46, 10, 1, 95, 'Richard Rambarran', 'Georgetown Chamber of Commerce', 'speakerlogo/1573946883_rec.jpg', 'Executive Director', '*****@*****.com', '**********', 'The Georgetown Chamber of Commerce and Industry was established under the name “The Chamberof Commerce of the City of Georgetown” in the year 1889. The formation of the chamber begun at a meeting with Royal Agricultural Commercial Society and a Commercial Committee held on the 20 the December 1888. Under the initiative of Mr. J. Errest Tinner a sphere of useful discussions were held and it was decided to merge these two bodies. On the 17th June, 1889 at a meeting of Merchants and others it was decided to form a Chamber of Commerce for the City, which should be recognized by law. The Memorandum and Articles of Association were drafted. These were submitted at a General Meeting held on the 8 th July, 1889 and the President, Vice-Presidents and Council were elected.', 0, '2019-11-16 23:28:03', '2019-11-16 23:28:03'),
(47, 10, 1, 95, 'Rod Henson', 'ExxonMobil Guyana', 'speakerlogo/1573946969_rod1.jpg', 'President', '*****@*****.com', '**********', 'ExxonMobil is the world\'s largest publicly traded international oil and gas company. It holds an industry-leading inventory of global oil and gas resources. It is the world\'s largest refiner and marketer of petroleum products, and its chemical company ranks among the world\'s largest. ExxonMobil applies science and innovation to find better, safer and cleaner ways to deliver the energy the world needs. ExxonMobil\'s upstream business encompasses high-quality exploration opportunities across all resource types and geographies, an industry-leading resource base, a portfolio of world-class projects, and a diverse set of producing assets. It has an active exploration or production presence in 36 countries. ExxonMobil is proud to play a leading role in providing the energy the world needs to support economic growth, technological advancement and the well-being of communities around the globe.', 0, '2019-11-16 23:29:29', '2019-11-16 23:29:29'),
(48, 10, 1, 95, 'Vincent Adams', 'Environmental Protection Agency (EPA)', 'speakerlogo/1573947094_v1.jpg', 'Director', '*****@*****.com', '**********', 'The Environmental Protection Agency (EPA) was legally established by the Environmental Protection Act in 1996. It has the responsibility to take the necessary measures to manage, conserve, protect and improve environment. This entails that the Agency takes actions to prevent and control pollution; assess the impact of economic development on the environment; and ensure the sustainable use of Guyana’s natural resources.\r\n\r\nThe EPA is under the umbrella of the Department of Environment, Ministry of Presidency.', 0, '2019-11-16 23:31:34', '2019-11-16 23:31:34'),
(50, 10, 1, 95, 'Ndibi Schwiers', 'Department of Environment - Guyana', 'speakerlogo/1573947272_nb.png', 'Director', '*****@*****.com', '**********', 'The Department of Environment (DoE), under the aegis of the Ministry of the Presidency, is responsible for leading the transformation of Guyana into a green state and ensuring environmental management and compliance across the country. The DoE coordinates all sustainable transformation actions and initiatives related to the natural and built environment, development of comprehensive environmental policies and legislation in keeping with the green transition and finally ensuring effective environmental safeguards to preserve public health and securing a better life for all Guyanese.', 0, '2019-11-16 23:34:32', '2019-11-16 23:34:32'),
(51, 10, 1, 95, 'Greg Hill', 'Hess', 'speakerlogo/1573947351_gh.jpg', 'President and Chief Operating Officer', '*****@*****.com', '**********', 'Hess Corporation is a leading global independent energy company engaged in the exploration and production of crude oil and natural gas globally. Hess operates across four continents with operations in the U.S. Europe, Asia and South America. Hess has industry-leading positions in the U.S. as an operator in the leading shale play of the Bakken and is also as one of the largest producers in the deepwater Gulf of Mexico. In addition Hess is a key producer in Malaysia, Thailand, Denmark and Libya. The company is engaged in the exploration, development and production of oil and gas resources offshore Guyana where we are participating in one of the industry’s largest oil discoveries in the past decade, with the first phase of a planned multi-phase development of the Stabroek Block in Guyana underway. To find out more about how Hess is working to become the world’s most trusted energy partner.', 0, '2019-11-16 23:35:51', '2019-11-16 23:35:51'),
(52, 10, 1, 95, 'Newell Dennison', 'Guyana Geology and Mines Commission (GGMC)', 'speakerlogo/1573947486_nd.jpg', 'Acting Commissioner', '*****@*****.com', '**********', 'The Guyana Geology and Mines Commission (GGMC) was created in 1979 from the Department of Geological Surveys and Mines which itself was the successor to the Geological Survey of British Guiana. Currently GGMC is divided into the following technical divisions:\r\n\r\n    Geological Services\r\n    Mines\r\n    Environment\r\n    Petroleum\r\n    Land Management\r\n\r\nThe technical divisions are assisted by the Administration and Finance Divisions to carry out their respective mandates.The Guyana Geology and Mines Commission\'s Role is:\r\n\r\n    To act as a development change agent in the diversification of the economic base of Guyana through its activities in the mineral sector\r\n    To create the opportunities for rapid economic development which an expanding mineral sector is ideally suited to provide\r\n    To act as a national repository for all information relating to geology and mineral resources which will facilitate an understanding of the resource base of the country\r\n    To provide to the general public the basic prospection information and advisory services, on the available economic mineral prospects\r\n    To provide advice to the government on appropriate mineral policy matters so that Guyana\'s mineral resources can be rationally developed and utilized\r\n    To regulate on behalf of the government all activities in the mineral sector\r\n\r\nThe functions of the commission are :\r\n\r\n    Promotion of mineral development\r\n    Provision of technical assistance and advice in mining, mineral processing, mineral utilisation and marketing of mineral resources\r\n    Mineral exploration\r\n    Research in exploration, mining, and utilisation of minerals and mineral products\r\n    Enforcement of the conditions of Mining Licences, Mining Permits, Mining Concessions, Prospecting Licences (for Large Scale Operations), Prospecting Permits (for Medium and Small Scale operations) and Quarry Licences\r\n    Collection of Rentals, fees, charges, levies etc. payable under the Mining Act\r\n    Hall Marking', 0, '2019-11-16 23:38:06', '2019-11-16 23:38:06'),
(54, 10, 1, 95, 'Patrick Henry', 'DAI Global Sustainable Business Group', 'speakerlogo/1573947741_ph.png', 'Project Director', '*****@*****.com', '**********', 'The Centre for Local Business Development (the Centre) is Guyana’s leading source for oil and gas business and industry information, policy advocacy, and professional networking. Since 2017, the Centre has been meeting the growing demands of Guyanese and international businesses by offering training, mentoring for growth and procurement linkages. The Centre’s Supplier Registration Portal (SRP) is the premier platform for oil and gas suppliers as they seek to contract, partner, and purchase goods and services from Guyanese businesses. With new offerings like the Health, Safety, Security and the Environment (HSSE) programme and its online Computer-based Training (CBT), the Centre continues to expand its services to advance local business. With decades of transformative growth ahead, the Centre is at the forefront of Guyanese and international business.', 0, '2019-11-16 23:42:21', '2019-11-16 23:42:21'),
(55, 10, 1, 95, 'Timothy Kabba', 'Petroleum Directorate – Sierra Leone', 'speakerlogo/1573947824_tk.jpg', 'Director General', '*****@*****.com', '**********', 'N/A', 0, '2019-11-16 23:43:44', '2019-11-16 23:43:44'),
(56, 10, 1, 95, 'Peter Cameron', 'N/A', 'speakerlogo/1573947905_dummy.jpg', 'Professor', '*****@*****.com', '**********', 'N/A', 0, '2019-11-16 23:45:05', '2019-11-16 23:45:05'),
(57, 10, 1, 95, 'Nya Gbaintor', 'Liberian Maritime Authority', 'speakerlogo/1573948036_ng.jpg', 'Principal Director Administration & Legal Services', '*****@*****.com', '**********', 'The Liberian Maritime Program was established in 1948 with strong support from the United States of America.  In 1949, Liberia became a founding member of the International Maritime Organization (IMO) and has over the years played a critical role in promulgating maritime safety, security and environmental protection. The Bureau of Maritime Affairs (BMA) administered the program under the supervision of the Ministry of Finance and later the Ministry of Transport. With the passage of Liberia Maritime Authority Act of 2010, the BMA then transitioned into the Liberian Maritime Authority (LiMA); a public corporation with greater responsibility of managing all commercial activities within the maritime domain of Liberia.  The rational for this transition was to diversify the Authorityâ€™s activities from its long-standing focus of being a nation with a leading maritime shipping and corporate registry program, to a nation that strategically focuses on building and, or supporting enterprises across the domestic maritime domain for greater economic and social benefits to the Nation and its People.', 0, '2019-11-16 23:47:16', '2019-11-16 23:47:16'),
(58, 10, 1, 95, 'Guy Cowan', 'Marsh JLT Specialty', 'speakerlogo/1573948124_dummy.jpg', 'Senior Vice President', '*****@*****.com', '**********', 'N/A', 0, '2019-11-16 23:48:44', '2019-11-16 23:48:44'),
(59, 10, 1, 95, 'Tracy Gibson', 'Bank of Guyana', 'speakerlogo/1573948233_dummy.jpg', 'Director of the Insurance Supervision Department', '*****@*****.com', '**********', 'N/A', 0, '2019-11-16 23:50:33', '2019-11-16 23:50:33'),
(60, 10, 1, 95, 'Willem Bloem', 'Bloem Consultants LLC', 'speakerlogo/1573948300_dummy.jpg', 'Principal', '*****@*****.com', '**********', 'N/A', 0, '2019-11-16 23:51:40', '2019-11-16 23:51:40'),
(61, 10, 1, 95, 'Carlos Tooge', 'Clariant', 'speakerlogo/1573948413_ct.jpg', 'Vice-President  BU Oil & Mining Services Clariant Latin America', '*****@*****.com', '**********', 'Clariant Oil Services is the fastest growing international supplier of specialty oilfield production chemicals and services to the oil and gas industry. Our product offerings have an impressive reach extending from Offshore and Deep Water, Conventional and Unconventional Oil & Gas, Paraffin Control Technologies to VERITRAX™, our Intelligent Chemical Management System. Our global presence provides a unique position from which to address and resolve customer challenges anywhere in the world. We use chemistry, systems and experts to understand our customers\' needs, then create high-value, innovative solutions that help our customers win. We strive to develop and deliver the most economical solutions with an unwavering commitment to high standards for health, safety and the environment and work to implement your custom chemical program with the utmost respect to the workforce and community.', 0, '2019-11-16 23:53:33', '2019-11-16 23:53:33'),
(62, 10, 1, 95, 'Mark Beare', 'Oxford Policy Management', 'speakerlogo/1573948498_dummy.jpg', 'Senior Associate  Natural Resources and Energy Portfolio', '*****@*****.com', '**********', 'N/A', 0, '2019-11-16 23:54:58', '2019-11-16 23:54:58'),
(63, 10, 1, 95, 'Pedro Haas', 'Hartree Partners', 'speakerlogo/1573948637_ph1.jpg', 'Partner', '*****@*****.com', '+1 212 536 8915', 'Hartree Partners, LP® is a global merchant commodities firm specializing in energy and its associated industries. We focus on identifying value in the production, refinement, transportation and consumption of tradable commodities and anticipating opportunities in the supply chain where they may be under or over-valued.\r\n\r\nFounded originally as Hess Energy Trading Company LLC (HETCO) in 1997, Hartree has a global reach spanning 10 offices and approximately 85 traders and originators. The company’s rigorous research, analytical approach, and entrepreneurial culture have contributed to its strong track record and growth over that time.\r\n\r\nHartree Partners LP is owned by the company’s Managing Partners, senior staff, and Oaktree Capital.', 0, '2019-11-16 23:57:17', '2019-11-16 23:57:17'),
(64, 10, 1, 95, 'Schreiner Parker', 'Rystad Energy', 'speakerlogo/1573948778_sp.jpg', 'VP Latin America/Caribbean', '*****@*****.com', '**********', 'We are an independent energy research and business intelligence company providing data, tools, analytics and consultancy services to clients exposed to the energy industry across the globe. Our high-end products on energy fundamentals, oil and gas markets, oilfield services and renewables strengthen the insight and expertise of our clients, thereby assuring quality decisions. Since 2004, we have developed confidence and trust in our data, analytics and support. Our highly qualified and experienced people help our clients in business development and portfolio strategy, market and trend analysis, benchmarking and competitor assessment. Known for our up-to-date, complete, consistent and comprehensive product and service delivery, we engage with energy owners and suppliers, financial professionals, E&P and oilfield service companies, and governments, focusing on global and regional oil, gas, power and renewable energy markets.', 0, '2019-11-16 23:59:38', '2019-11-16 23:59:38'),
(65, 10, 1, 95, 'Bill Heins', 'Getech', 'speakerlogo/1573948916_bh.jpg', 'Professional Geologist and Management Consultant', '*****@*****.com', '**********', 'We supply the expertise, support and knowledge that companies and governments need to better discover, develop and manage Natural Resources.\r\nOur customers work across a wide range of industries including petroleum, mining, nuclear and water. Our data rich products, GIS solutions and trusted advisory services help our customers to achieve their business goals of cost control, operational excellence, regulatory compliance and environmental responsibility.', 0, '2019-11-17 00:01:56', '2019-11-17 00:01:56'),
(66, 10, 1, 95, 'Suresh Narine', 'CGX Energy Inc.', 'speakerlogo/1573949024_sn.jpg', 'Executive Chairman', '*****@*****.com', '**********', 'Positioned for Guyana Atlantic Basin Success Positioned for Guyana Atlantic Basin Success\r\n\r\nCGX is a Canadian oil and gas exploration company that holds three licenses in the Guyana-Suriname Basin, a frontier Basin in South America with a proven hydrocarbon system and highly prospective deep water plays that can be drilled in shallow water. In 2000, the United States Geological Survey (USGS)  identified the Guyana-Suriname Basin as having the second highest resource potential among unexplored oil basins in the world and currently estimates mean recoverable oil reserves of over 13.6 billion bbls and gas reserves of 32 trillion cubic ft. The Company is pursuing the Equatorial Atlantic Margin Play, analogous to West Africa and Brazil. CGX is surrounded by major oil companies including Anadarko, Apache, Exxon, Inpex, Kosmos, Murphy, Repsol, Shell, Statoil, Total, and Tullow. With numerous prospects and leads and an independent P50 resource estimate of 2.9 billion boe of potential resources, CGX is positioned for success in this frontier Basin.', 0, '2019-11-17 00:03:44', '2019-11-17 00:03:44'),
(67, 10, 1, 95, 'Sydney Armstrong', 'University of Guyana', 'speakerlogo/1573949303_dummy.jpg', 'Head of Department of Economics, Faculty of Social Science', '*****@*****.com', '**********', 'N/A', 0, '2019-11-17 00:08:23', '2019-11-17 00:08:23'),
(68, 10, 1, 95, 'Peter Stewart', 'Clyde & Co', 'speakerlogo/1573949384_ps.jpg', 'Partner', '*****@*****.com', '**********', 'N/A', 0, '2019-11-17 00:09:44', '2019-11-17 00:09:44'),
(69, 10, 1, 95, 'David Hess', 'Clyde & Co', 'speakerlogo/1573949428_dummy.jpg', 'Partner', '*****@*****.com', '**********', 'N/A', 0, '2019-11-17 00:10:28', '2019-11-17 00:10:28'),
(70, 10, 1, 95, 'Sally Redford', 'Energy Caribbean Observer', 'speakerlogo/1573949520_dummy.jpg', 'Editor', '*****@*****.com', '**********', 'N/A', 0, '2019-11-17 00:12:00', '2019-11-17 00:12:00'),
(71, 10, 1, 95, 'Kiven Pierre', 'University of Guyana', 'speakerlogo/1573949563_kp.png', 'Professor', '*****@*****.com', '**********', 'N/A', 0, '2019-11-17 00:12:43', '2019-11-17 00:12:43'),
(72, 10, 1, 95, 'Martin Rocher', 'TOTAL EREN', 'speakerlogo/1573949663_mr.jpg', 'Vice-President-Business Development Latin America', '*****@*****.com', '**********', 'Founded in 2012, Total Eren is the subsidiary dedicated to renewable energy of EREN Groupe, the first player dedicated to natural resource efficiency.\r\nTotal Eren is an Independent Power Producer (IPP), which develops, finances, invests in, builds and operates renewable energy power plants (solar, wind, hydro) worldwide over the long-term. Since December 2017, Total S.A., a key player in the energy sector, has been a shareholder of Total Eren. On 5 April 2019, Total Eren acquired NovEnergia Group, thereby strengthening its presence in southern Europe in particular.', 0, '2019-11-17 00:14:23', '2019-11-17 00:14:23'),
(73, 10, 1, 95, 'Maria Guedez', 'ExxonMobil', 'speakerlogo/1573949737_mg.jpg', 'Guyana and Suriname Exploration Manager', '*****@*****.com', '**********', 'ExxonMobil is the world\'s largest publicly traded international oil and gas company. It holds an industry-leading inventory of global oil and gas resources. It is the world\'s largest refiner and marketer of petroleum products, and its chemical company ranks among the world\'s largest. ExxonMobil applies science and innovation to find better, safer and cleaner ways to deliver the energy the world needs. ExxonMobil\'s upstream business encompasses high-quality exploration opportunities across all resource types and geographies, an industry-leading resource base, a portfolio of world-class projects, and a diverse set of producing assets. It has an active exploration or production presence in 36 countries. ExxonMobil is proud to play a leading role in providing the energy the world needs to support economic growth, technological advancement and the well-being of communities around the globe.', 0, '2019-11-17 00:15:37', '2019-11-17 00:15:37'),
(74, 10, 1, 95, 'Ajmer Samaroo', 'ExxonMobil', 'speakerlogo/1573949780_as.jpg', 'Facilities Engineer', '*****@*****.com', '**********', 'ExxonMobil is the world\'s largest publicly traded international oil and gas company. It holds an industry-leading inventory of global oil and gas resources. It is the world\'s largest refiner and marketer of petroleum products, and its chemical company ranks among the world\'s largest. ExxonMobil applies science and innovation to find better, safer and cleaner ways to deliver the energy the world needs. ExxonMobil\'s upstream business encompasses high-quality exploration opportunities across all resource types and geographies, an industry-leading resource base, a portfolio of world-class projects, and a diverse set of producing assets. It has an active exploration or production presence in 36 countries. ExxonMobil is proud to play a leading role in providing the energy the world needs to support economic growth, technological advancement and the well-being of communities around the globe.', 0, '2019-11-17 00:16:20', '2019-11-17 00:16:20'),
(75, 10, 1, 95, 'Michael Aryeetey', 'Ghana National Petroleum Corporation', 'speakerlogo/1573949870_dummy.jpg', 'Ag. Exploration & Appraisal Manager', '*****@*****.com', '**********', 'The Ghana National Petroleum Corporation (GNPC) is Ghana’s National Oil Company (NOC), established in 1983 by PNDC Law 64, to support the government\'s objective of providing adequate and reliable supply of petroleum products and reducing the country\'s dependence on crude oil imports, through the development of the country’s own petroleum resources.', 0, '2019-11-17 00:17:50', '2019-11-17 00:17:50'),
(76, 10, 1, 95, 'Olivier Wattez', 'Total', 'speakerlogo/1573949959_ow.jpg', 'Guyana Branch Manager & Country Chair', '*****@*****.com', '**********', 'Total’s ambition is to become the responsible energy major. To become the Responsible Energy Major means providing energy that is affordable, reliable and clean. Energy is a vital, constantly changing resource that has accompanied major shifts in society throughout time. And energy must continue to adapt if it is to play a key role in addressing the complex challenges facing the world today. We want to contribute to these changes because energy is Total’s history: its past, its present and its future.', 0, '2019-11-17 00:19:19', '2019-11-17 00:19:19'),
(77, 10, 1, 95, 'Dr Jonathan Wylde', 'Clariant Oil & Mining Services', 'speakerlogo/1573950080_dr.jpg', 'Global Head of Innovation', '*****@*****.com', '**********', 'Clariant Oil Services is the fastest growing international supplier of specialty oilfield production chemicals and services to the oil and gas industry. Our product offerings have an impressive reach extending from Offshore and Deep Water, Conventional and Unconventional Oil & Gas, Paraffin Control Technologies to VERITRAX™, our Intelligent Chemical Management System. Our global presence provides a unique position from which to address and resolve customer challenges anywhere in the world. We use chemistry, systems and experts to understand our customers\' needs, then create high-value, innovative solutions that help our customers win. We strive to develop and deliver the most economical solutions with an unwavering commitment to high standards for health, safety and the environment and work to implement your custom chemical program with the utmost respect to the workforce and community.', 0, '2019-11-17 00:21:20', '2019-11-17 00:21:20'),
(78, 10, 1, 95, 'Sheila Khama', 'Natural Resource Development', 'speakerlogo/1573950141_sk.jpg', 'Consultant', '*****@*****.com', '**********', 'N/A', 0, '2019-11-17 00:22:21', '2019-11-17 00:22:21'),
(79, 10, 1, 95, 'Jean-Michel Lavergne', 'TOTAL EREN', 'speakerlogo/1573950467_dummy.jpg', 'Senior Vice President of the Americas', '*****@*****.com', '**********', 'Total’s ambition is to become the responsible energy major. To become the Responsible Energy Major means providing energy that is affordable, reliable and clean. Energy is a vital, constantly changing resource that has accompanied major shifts in society throughout time. And energy must continue to adapt if it is to play a key role in addressing the complex challenges facing the world today. We want to contribute to these changes because energy is Total’s history: its past, its present and its future.', 0, '2019-11-17 00:27:47', '2019-11-17 00:27:47'),
(80, 10, 1, 95, 'Justin Nedd', 'GTT+', 'speakerlogo/1573950575_dummy.jpg', 'Chief Executive Officer', '*****@*****.com', '**********', 'N/A', 0, '2019-11-17 00:29:35', '2019-11-17 00:29:35'),
(81, 10, 1, 95, 'Dennis A. Pieters', 'Mid Atlantic Oil and Gas', 'speakerlogo/1573950629_dummy.jpg', 'Director', '*****@*****.com', '**********', 'Mid Atlantic Oil & Gas Inc. (MOGI) is a Guyana Petroleum Exploration Company which was incorporated and registered in early 2013. On March 4, 2015, MOGI was awarded the CANJE Petroleum Prospecting License (PPL) along with JHI Associates Inc., a privately held Canadian Company. In February 2016, Exxon Mobil farmed into the CANJE PPL and was appointed Operator and in late 2018 TOTAL joined the consortia. Our partnership is committed to an Exploration Program that is sustainable, local content oriented, with the highest levels of HSE (health, safety, environment) and adherence to the laws, regulations etc. governing such licenses in Guyana.', 0, '2019-11-17 00:30:29', '2019-11-17 00:30:29'),
(82, 10, 1, 95, 'Ayanna Watson', 'SOL Guyana Inc.', 'speakerlogo/1573950709_aw.jpg', 'Commercial Manager', '*****@*****.com', '**********', 'The world was first introduced to the Sol brand in 2005, when the company was born and the Sol logo was unveiled. Sol (the Spanish word for sun) embodies the spirit of the Caribbean region. Our name and our sunburst logo are direct representations of our team’s energy and commitment to being a central element of all aspects of life in the Caribbean. The Sol visual brand reflects the vibrancy of the Caribbean; our prominent orange and blue colour palette were designed to personify the warm hues of the Caribbean landscape.', 0, '2019-11-17 00:31:49', '2019-11-17 00:31:49'),
(83, 10, 1, 95, 'Rudolf T. Elias', 'Staatsolie Maatschappij Suriname N.V.', 'speakerlogo/1574023270_dummy.jpg', 'Chief Executive Officer', '*****@*****.com', '**********', 'We are Staatsolie Maatschappij Suriname N.V., a vertically integrated state-owned company founded in 1980. We explore, drill, produce, refine, market, sell and transport petroleum and products refined from it. We also generate electricity. With the production and supply of energy, we meet the growing energy needs of our society.', 0, '2019-11-17 20:41:10', '2019-11-17 20:41:10'),
(84, 10, 1, 95, 'Siafuah-Mai Gray', 'National Oil Company of Liberia – Liberia', 'speakerlogo/1574023420_dummy.jpg', 'Chief Executive Officer', '*****@*****.com', '**********', 'N/A', 0, '2019-11-17 20:43:40', '2019-11-17 20:43:40'),
(85, 10, 1, 95, 'Prof. Gary Dirks', 'Arizona State University', 'speakerlogo/1574023494_dummy.jpg', 'Senior Director Golden Futures Laboratory Wringley Chair, Director LightWorks', '*****@*****.com', '**********', 'Arizona State University is a top ranked research university in the greater Phoenix metropolitan area.\r\nRecognized by U.S. News & World Report as the country’s most innovative school, Arizona State University is where students and faculty work with NASA to develop, advance and lead innovations in space exploration.\r\nASU’s nationally ranked programs inspire the top-qualified graduates and have positioned the university as a “top-tier” recruiting and hiring institution by more than 50 of the country’s top corporations, according to professional recruiters and rankings services around the world. ASU graduates more than 20,000 thinkers, innovators and master learners every year. Take a deeper look at how ASU is building the next generation of leaders.', 0, '2019-11-17 20:44:54', '2019-11-17 20:44:54'),
(86, 10, 1, 95, 'Adowa Afriyie Wiafe', 'Ghana National Petroleum Corporation', 'speakerlogo/1574023615_dummy.jpg', 'Legal Manager', '*****@*****.com', '**********', 'The Ghana National Petroleum Corporation (GNPC) is Ghana’s National Oil Company (NOC), established in 1983 by PNDC Law 64, to support the government\'s objective of providing adequate and reliable supply of petroleum products and reducing the country\'s dependence on crude oil imports, through the development of the country’s own petroleum resources.\r\n\r\nThe Petroleum [Exploration and Production] Law, 1984, PNDC Law 84, was subsequently enacted to provide the regulatory framework for the exploitation of the country’s hydrocarbon resources. PNDC Law 84, establishes the contractual relationship among the state, GNPC and prospective investors in upstream petroleum operations.   This law also grants GNPC the right of entry into any open acreage to undertake exploration activities.', 0, '2019-11-17 20:46:55', '2019-11-17 20:46:55'),
(87, 10, 1, 95, 'Samuel Addoteye', 'Ghana National Petroleum Corporation', 'speakerlogo/1574023681_dummy.jpg', 'Human Resource Manager', '*****@*****.com', '**********', 'The Ghana National Petroleum Corporation (GNPC) is Ghana’s National Oil Company (NOC), established in 1983 by PNDC Law 64, to support the government\'s objective of providing adequate and reliable supply of petroleum products and reducing the country\'s dependence on crude oil imports, through the development of the country’s own petroleum resources.\r\n\r\nThe Petroleum [Exploration and Production] Law, 1984, PNDC Law 84, was subsequently enacted to provide the regulatory framework for the exploitation of the country’s hydrocarbon resources. PNDC Law 84, establishes the contractual relationship among the state, GNPC and prospective investors in upstream petroleum operations.   This law also grants GNPC the right of entry into any open acreage to undertake exploration activities.', 0, '2019-11-17 20:48:01', '2019-11-17 20:48:01'),
(88, 10, 1, 95, 'Neil Parsons', 'Parsons Consultancy', 'speakerlogo/1574023771_dummy.jpg', 'Senior Consultatnt', '*****@*****.com', '**********', 'N/A', 0, '2019-11-17 20:49:31', '2019-11-17 20:49:31'),
(89, 10, 1, 95, 'Susana Moreira', 'World Bank', 'speakerlogo/1574024375_dummy.jpg', 'Oil and Gas Specialist', '*****@*****.com', '**********', 'The World Bank is an international financial institution that provides loans and grants to the governments of poorer countries for the purpose of pursuing capital projects. It comprises two institutions: the International Bank for Reconstruction and Development (IBRD), and the International Development Association (IDA). The World Bank is a component of the World Bank Group.\r\n\r\nThe World Bank\'s most recent stated goal is the reduction of poverty. As of November 2018, the largest recipients of World Bank loans were India ($859 million in 2018) and China ($370 million in 2018), through loans from IBRD', 0, '2019-11-17 20:59:35', '2019-11-17 20:59:35'),
(90, 10, 1, 95, 'Wendell Ramoutar', 'PwC Trinidad', 'speakerlogo/1574024479_dummy.jpg', 'Partner', '*****@*****.com', '**********', 'PwC has been in operation for over 80 years in Trinidad and Tobago and is the largest professional services firm in the country. We bring a global perspective along with in-depth knowledge of local, regional and international issues to help you optimize your business solutions.\r\n\r\nWith 12 partners, 199 professional staff and 46 support staff, we have a long history of delivering value-added professional services to our clients. Our audit and assurance, tax and consulting services are based on resolving complex issues and identifying opportunities.  In Trinidad and Tobago, PwC has expertise in 5 key industries or sectors - energy, banking, and capital markets, insurance, public sector and consumer and industrial products.\r\n\r\nOur reputation lies in building lasting relationships with our clients and delivering value in all we do. Let us apply our world-class capabilities to your business goals.', 0, '2019-11-17 21:01:19', '2019-11-17 21:01:19'),
(91, 10, 1, 95, 'Gyanpriya Maharaj', 'University of Guyana', 'speakerlogo/1574024645_dummy.jpg', 'Director, Center for the Study of Biological Diversity (CSBD), Faculty of Natural Sciences', '*****@*****.com', '**********', 'The University of Guyana, in Georgetown, Guyana, is Guyana\'s sole national higher education institution. It was established in April 1963 with the following Mission: \"To discover, generate, disseminate, and apply knowledge of the highest standard for the service of the community, the nation, and of all mankind within an atmosphere of academic freedom that allows for free and critical enquiry.\"', 0, '2019-11-17 21:04:05', '2019-11-17 21:04:05'),
(92, 10, 1, 95, 'Ryan Ramjit', 'Repsol', 'speakerlogo/1574024819_dummy.jpg', 'Exploration Country Manager', '*****@*****.com', '**********', 'Repsol S.A. is an energy company based in Madrid, Spain. It carries out upstream and downstream activities throughout the entire world. It has more than 24,000 employees worldwide. It is vertically integrated and operates in all areas of the oil and gas industry, including exploration and production, refining, distribution and marketing, petrochemicals, power generation and trading.', 0, '2019-11-17 21:06:59', '2019-11-17 21:06:59'),
(94, 10, 1, 95, 'Oystein Forsvoll', 'Stavanger Offshore Technical School (SOTS) Course Centre', 'speakerlogo/1574025017_dummy.jpg', 'Director', '*****@*****.com', '**********', 'N/A', 0, '2019-11-17 21:10:17', '2019-11-17 21:10:17'),
(95, 10, 1, 95, 'Stella Guedes do Nascimento Aguirre', 'Stavanger Offshore Technical School (SOTS) Course Centre', 'speakerlogo/1574025056_dummy.jpg', 'Social Counsellor', '*****@*****.com', '**********', 'N/A', 0, '2019-11-17 21:10:56', '2019-11-17 21:10:56'),
(96, 10, 1, 95, 'Marissa Foster-Gordon', 'Ministry of the Presidency, Department of Energy', 'speakerlogo/1574025145_dummy.jpg', 'Petroleum Geologist', '*****@*****.com', '**********', 'The Department of Energy was created on August 1, 2018 to effectively manage the hydrocarbon resources of the Cooperative Republic of Guyana. The May 2015 and subsequent discoveries of oil and associated gases, have resulted in Guyana being regarded amongst the top reserve holders worldwide. This has the potential for improved standard of living for all Guyanese and transformational and impactful development to occur.\r\n\r\nAs the Department responds to the rapid rate of developments within the sector, it invites suitably qualified individuals to apply to fill vacancies in the following areas by submitting: (i) a cover letter written as a Statement of Interest, and (ii) a curriculum vitae\r\n• Economics\r\n• Econometrics\r\n• Law\r\n• Procurement\r\n• Monitoring and Evaluation\r\n• Administration\r\n• Environmental Safeguards', 0, '2019-11-17 21:12:25', '2019-11-17 21:12:25'),
(97, 10, 1, 95, 'Nikita La Cruz', 'Department of Earth and Environmental Sciences, University of Michigan', 'speakerlogo/1574025248_dummy.jpg', 'Research Associate', '*****@*****.com', '**********', 'The University of Michigan (UM, U-M, U of M, or UMich), often simply referred to as Michigan, is a public research university in Ann Arbor, Michigan. The university is Michigan\'s oldest; it was founded in 1817 in Detroit, as the Catholepistemiad, or University of Michigania, 20 years before the territory became a state. The school was moved to Ann Arbor in 1837 onto 40 acres (16 ha) of what is now known as Central Campus. Since its establishment in Ann Arbor, the flagship university campus has expanded to include more than 584 major buildings with a combined area of more than 34 million gross square feet (780 acres; 3.2 km2) spread out over a Central Campus and North Campus, two regional campuses in Flint and Dearborn, and a Center in Detroit. The university is a founding member of the Association of American Universities.', 0, '2019-11-17 21:14:08', '2019-11-17 21:14:08'),
(100, 10, 1, 95, 'Raffie Hosein', 'The University of the West Indies', 'speakerlogo/1574262468_asd.jpg', 'Head, Department of Chemical Engineering', '*****@*****.com', '**********', 'Established in 1948, UWI is the largest and longest standing higher education provider in the English-speaking Caribbean .\r\n In its more than 60 years of existence, UWI has evolved from a fledgling college in the Caribbean island of Jamaica with 33 students to a full-fledged University with over 45,000 students, approximately 9000 graduates annually and more than 120,000 alumni. This impressive network of UWI Alumni includes one Nobel Laureate, dozens of Rhodes Scholars and more than 18 current and former Caribbean Prime Ministers and Heads of State.', 0, '2019-11-20 15:07:48', '2019-11-20 15:07:48'),
(101, 10, 1, 95, 'Giorgio Martelli', 'Saipem America', 'speakerlogo/1574262581_qwe.jpg', 'President & CEO', '*****@*****.com', '**********', 'Globality, multiculturalism, skill and sustainability have been the distinctive features of our work for more than 60 years.\r\nWe are present in over 70 countries and are backed up by 60 years of history. We have unique expertise in the management of complex projects in harsh environments, remote areas and deep water, in particular for the Oil&Gas industry. We make the most of our own people’s experience, and the high technological value of our assets, in order to provide tailor-made solutions, focusing a great deal of attention on sustainability, the health and safety of people and operations, and innovation.', 0, '2019-11-20 15:09:41', '2019-11-20 15:09:41'),
(102, 10, 1, 95, 'Andrew Jupiter', 'The University of the West Indies (UWI)', 'speakerlogo/1574262703_azx.jpg', 'Coordinator of the Petroleum Studies Unit', '*****@*****.com', '**********', 'Established in 1948, UWI is the largest and longest standing higher education provider in the English-speaking Caribbean .\r\n In its more than 60 years of existence, UWI has evolved from a fledgling college in the Caribbean island of Jamaica with 33 students to a full-fledged University with over 45,000 students, approximately 9000 graduates annually and more than 120,000 alumni. This impressive network of UWI Alumni includes one Nobel Laureate, dozens of Rhodes Scholars and more than 18 current and former Caribbean Prime Ministers and Heads of State.', 0, '2019-11-20 15:11:43', '2019-11-20 15:11:43'),
(104, 11, 1, 111, 'Petri Peltonen', 'Ministry of Economic Affairs and Employment, Finland', 'speakerlogo/1574860699_petri.jpg', 'Under-Secretary of State at the Ministry of Economic Affairs and Employment (MEAE)', '*****@*****.com', '**********', 'Petri Peltonen is Under-Secretary of State at the Ministry of Economic Affairs and Employment (MEAE). In this capacity he oversees the policy development and implementation in the sectors of economic growth, enterprise and innovation as well as foreign direct investments. Prior to taking his current position in 2016, Petri Peltonen was Director General of the Enterprise and Innovation Department at MEAE and the Technology Department at the Ministry of Trade and Industry. From 1996 to 2006, he worked at the Finnish Funding Agency for Innovation (Tekes), most recently as Executive Director and member of the management team. From 1987 until 1996, he held several technology development positions at the European Space Agency (ESA) in the Netherlands and in electronics industry in Finland. Within his duties, Petri Peltonen takes part in several national and European functions such as the boards of the Business Finland organisation and the Nordic Investment Bank. He is also acts as the European Commission’s SME Envoy for Finland. Petri Peltonen (b. 1962) has M.Sc. and Lic.Tech. degrees from Tampere University of Technology. He is an invited member of the Finnish Academy of Technical Sciences.', 0, '2019-11-27 13:18:19', '2019-11-27 13:18:19'),
(106, 11, 1, 111, 'Michael Hosp', 'Minova', 'speakerlogo/1574860969_michael hosp.png', 'Technology Manager - Steel Products Europe', '*****@*****.com', '**********', 'Master’s degree in mining and tunneling from the Mining University in Leoben.\r\n\r\n(1998 – 2003): Rio Tinto Talc Mining in Austria.\r\n\r\n(2003-2011): Atlas Copco Rock Reinforcement: Regional Manager and Product Manager, from 2005 Technical Manager.\r\n\r\n(2011 to present): Minova: Technology Manager Steel Products Europe', 0, '2019-11-27 13:22:49', '2019-11-27 13:22:49'),
(107, 11, 1, 111, 'Nils Ivar Iversen', 'Aziwell AS', 'speakerlogo/1574866144_nils.jpg', 'Mechanical Engineer and Senior Project Engineer', '*****@*****.com', '**********', 'Nils Ivar Iversen is a Senior Project Engineer for Aziwell AS, an engineering company that delivers services in Directional Drilling and Bore hole survey. He has worked as project leader in Directional Drilling projects in North and South America, and is involved in the Aziwell R&D department, which aims to continually innovate and improve the field of Directional Drilling. His presentation will address topics such as how directional drilling provides means to save both time, money and environment in mineral exploration and geotechnical projects, and in specific how the emerging industry of Directional Core Barrel Technology revolutionize directional drilling by providing far better production rates as well as considerably less environmental impact than existing technology, without the need for bringing extra equipment such as pumps or rods to site.\r\nNils Ivar Iversen has an Engineering degree from Norwegian University of Science and Technology (NTNU).', 0, '2019-11-27 14:49:04', '2019-11-27 14:49:04'),
(108, 11, 1, 111, 'Kenneth Norris', 'Somincor S.A (Lundin Mining)', 'speakerlogo/1574866454_kenneth.jpg', 'Managing Director', '*****@*****.com', '**********', 'Nationality\r\nCanadian\r\n\r\nEducation\r\nHarvard Business School – Leadership Course Certificate (2015)\r\nHeriot Watt University – Edinburgh Business School MBA (2012)\r\nUniversity of British Columbia B.ApSc. - Mineral Process Engineering (1986)\r\n\r\nWork Experience\r\nHighly qualified senior mining executive with more than 33 years of experience. Since January 2018 is the Managing Director for Lundin Mining’s Neves Corvo Mine located in southern Portugal. Previously I was the Vice-President and General Manager for five (5) years at Kinross Gold’s Chirano Gold Mine located in Ghana. I have been at a senior manager position for more than 20 years working at both gold mines and base metal mines located in Africa, South America and Europe, including four years at the Las Cruces Mine located near Seville, Spain and two years during the construction of the Aguas Teñidas Mines, both mines which are located and currently operating in the Iberian Pyrite Belt of Spain. I started my career in British Columbia, Canada working for Placer Dome for 10 years principally in Canada starting as a process plant metallurgist. A result oriented and effective leader with a proven track record of managing change for improving safety, financial and operational performance.\r\n\r\nMarried with three sons, one of which is now a mining engineer who now is working in Ecuador at the Fruta del Norte gold project. Currently I live in Portugal at the minesite and on weekends often I am in Seville with my wife at our home for more than 18 years.', 0, '2019-11-27 14:54:14', '2019-11-27 14:54:14'),
(109, 11, 1, 111, 'Heather Ednie', 'Global Mining Guidelines Group (GMG Group)', 'speakerlogo/1574866537_heather.png', 'Managing Director', '*****@*****.com', '**********', 'Heather Ednie is an influential mining professional who has more than 20 years’ experience in the sector, developing and spearheading vital business solutions through communications and association management. Since 2012, she has been the Managing Director of Global Mining Guidelines Group (GMG), a network of representatives from mining companies, OEMs, OTMs, research organizations and consultants around the world who recognize that innovation does not happen in silos. GMG creates multi-stakeholder working groups to systematically remove the impediments to building safe, sustainable and innovative mines of the future. Prior to joining GMG she lead a young communications consulting firm, Modica Communications, focused on serving the Canadian mining sector with the aim of advancement of a safe, responsible and respected industry. From 2004 to 2009, as Director, Media and Communications for the Canadian Institute of Mining, Metallurgy and Petroleum (CIM), Heather successfully reinvented the CIM communications and editorial department as the relevant source of information for the industry. Heather served as Vice-Chair of Women in Mining Canada and is a founding member of the Women in Mining Montreal Branch organizing committee. Heather has received the CIM Distinguished Lecturer Award and was recognized in WIM UK’s 100 Global Inspirational Women in Mining.', 0, '2019-11-27 14:55:37', '2019-11-27 14:55:37');
INSERT INTO `speakers` (`id`, `event_id`, `userid`, `detailsid`, `name`, `cname`, `image`, `position`, `email`, `phone`, `description`, `toprated`, `created_at`, `updated_at`) VALUES
(110, 11, 1, 111, 'Jane Isaacs', 'ABMEC - Association of British Mining Equipment Companies', 'speakerlogo/1574866650_jane.jpg', 'Director General', '*****@*****.com', '**********', 'Jane was educated in Yorkshire and Lincolnshire in business studies and marketing. She worked for local newspapers and Yellow Pages before a career break to enjoy her children. Following further education in Computer Literacy & Information Technology (CLAIT) she worked in fast moving consumer goods (FMCG) logistics and publishing. Jane’s introduction to the mining industry came about by writing for mining publications before joining ABMEC in 2013. Since her leadership, the ABMEC has grown in membership, quadrupled the size of its Annual Conference, and increased the number of missions, exhibitions and other events. ABMEC is the only Association representing British mining expertise. Its members range from large OEMs to component suppliers, service providers and educational establishments. The organisation is keen to collaborate with other membership groups to improve the quality of services on offer to all parties.', 0, '2019-11-27 14:57:30', '2019-11-27 14:57:30'),
(111, 11, 1, 111, 'Rico Brändle', 'GeoBrugg', 'speakerlogo/1574866724_rico.png', 'Director Mining Europe', '*****@*****.com', '**********', 'N/A', 0, '2019-11-27 14:58:44', '2019-11-27 14:58:44'),
(112, 11, 1, 111, 'Brian Carroll', 'Sandvik Mining and Rock Technology', 'speakerlogo/1574866809_brian.jpg', 'Parts, Service & Warranty Lead and Sales Manager Ireland', '*****@*****.com', '**********', 'Brian Carroll – Parts, Service & Warranty Lead and Sales Manager Ireland – Working with Sandvik in Ireland and Europe for past 20 years beginning my career as a service technician to today where I am looking after operations in Sandvik Ireland (Underground Mining Division).', 0, '2019-11-27 15:00:09', '2019-11-27 15:00:09'),
(113, 11, 1, 111, 'Marko Komac', 'European Federation of Geologists', 'speakerlogo/1574866916_marko.jpg', 'President', '*****@*****.com', '**********', 'Assoc. Prof. Marko Komac, Ph.D., EurGeol #1294, currently an independent consultant, the President of the European Federation of Geologists (EFG), an external researcher at the Faculty for civil engineering at the University of Ljubljana, an Associate Professor for GIS at University of Nova Gorica and a Member of the IntRaw Observatory Board as the Treasurer. From 2006 to 2014 Marko was the director of the Geological Survey of Slovenia where he also worked as a part-time researcher. From November 2016 to May 2019 Marko was the member of the Board of EFG, where he served as the External Relations Officer. From 2012 to 2016 he was a Vice-President of the IUGS, and in years 2011 and 2012 he was the President of the EuroGeoSurveys. He has more than 21 year experiences in the field of landslide analyses, geographical information systems (GIS), application of remote sensing in geology, spatial analyses and modelling, geostatistics, mass-movements analyses, management of organisations and teams, and international networking. Currently he’s involved in several EU-funded geological projects. He\'s an author or co-author of over 500 bibliographic units mainly from the above listed research areas and several times IronMan 70.3 finisher.', 0, '2019-11-27 15:01:56', '2019-11-27 15:01:56'),
(114, 11, 1, 111, 'Reece van Buren', 'HiSeis', 'speakerlogo/1574866998_reeceeeee.png', 'Marketing Director', '*****@*****.com', '**********', 'Experienced Professional Natural Scientist with over 15 years in the geoscience exploration, technology and services industry.', 0, '2019-11-27 15:03:18', '2019-11-27 15:03:18'),
(115, 11, 1, 111, 'Ivar S. Fossum', 'Nordic Mining ASA', 'speakerlogo/1574867063_ivar.jpg', 'CEO', '*****@*****.com', '**********', 'Ivar S. Fossum holds a Master of Science in Mechanical Engineering from the University of Science and Technology (NTNU) in Trondheim, Norway. He has previously held various managerial and commercial positions within the petroleum and fertilizer industries in the Norsk Hydro Group and in FMC Technologies, including as General Manager of Norsk Hydro East Africa Ltd. and as Chief Executive Officer of Loke AS. Fossum is a Norwegian citizen and resides in Asker, Norway.', 0, '2019-11-27 15:04:23', '2019-11-27 15:04:23'),
(116, 11, 1, 111, 'Harry Sandström', 'Mining Finland', 'speakerlogo/1574867132_harry.jpg', 'CEO', '*****@*****.com', '**********', 'Harry Sandstrom has a long experience in planning, setting up, marketing and managing industrial services. He has long and diversified project management experience in multi national and cultural environment including scientific cooperation projects and institutional strengthening projects in developing countries. Currently he is the CEO of MINING FINLAND association, aiming to support internationalization and export of Finnish mining technology and service companies. After being in CEO and director level in multi site and functional organizations, he has collected good know how also of business administration including, financial and HRD/HRM functions.', 0, '2019-11-27 15:05:32', '2019-11-27 15:05:32'),
(117, 11, 1, 111, 'Paula Dinis', 'DGEG Portugal', 'speakerlogo/1574867231_paula.jpg', 'Policy Officer - Directorate General for Energy and Geology', '*****@*****.com', '**********', 'N/A', 0, '2019-11-27 15:07:11', '2019-11-27 15:07:11'),
(118, 11, 1, 111, 'Rob Allen', 'Dassault Systèmes', 'speakerlogo/1574867295_rob.jpg', 'Senior Sales Manager', '*****@*****.com', '**********', 'N/A', 0, '2019-11-27 15:08:15', '2019-11-27 15:08:15'),
(119, 11, 1, 111, 'Brian Wesson', 'Nordic Gold Inc.', 'speakerlogo/1574867369_wesson.jpg', 'President & CEO', '*****@*****.com', '**********', 'Mr. Brian Wesson has extensive experience spanning a career of over 30 years in the management, operation design and construction of natural resource operations globally. He qualified as an engineer in South Africa, gained an MBA in Australia, studied Economics at the University of South Africa and is a fellow of the Australasian Institute of Mining and Metallurgy and a fellow of the Australian Institute of Company Directors. Brian founded the Wesson Group of Companies, of which Lionsbridge and Westech form part, with a view to utilising the Groups’ experience in the ownership, management and development of natural resource companies and the intellectual property developed to unlock shareholder value. Mr Wesson brings unique value in being highly experienced in both the corporate and technical aspects of managing a company; he understands natural resource companies from underground to the ‘board room’.', 0, '2019-11-27 15:09:29', '2019-11-27 15:09:29'),
(120, 11, 1, 111, 'Gordon Stove', 'Adrok Ltd', 'speakerlogo/1574867602_gordon.png', 'Co-Founder and Managing Director', '*****@*****.com', '07939051829', 'Co-founder & Managing Director, Adrok Ltd. ( www.adrokgroup.com)   \r\n\r\nGordon wants to be remembered in life as being a great dad, husband and friend and hopefully do a few things that work out well for everyone along the way and have lots of laughs. \r\n\r\nAfter 20 years in business (but still looking in his twenties!), Gordon has figured-out how to create, protect and generate high value from intellectual property; whilst running a proprietary, global technology services company - Adrok. \r\n\r\nHis inspiration has come from setting up and working with his Dad, Dr Colin Stove.\r\n\r\nGordon thrives on being around like-minded people, he has a real thirst for knowledge and enjoys listening to people’s stories.  One of the most important things for Gordon is having a laugh, he believes too many people take life too seriously, he thinks you should enjoy and appreciate every day on the planet and use humour to brighten up other people’s day.\r\n\r\nGoal:\r\nTo develop and implement innovative technologies to help find hydrocarbons and minerals vital to the world’s health and welfare.\r\n\r\nResponsibilities:\r\nStrategic Technology Planning and Innovation in Upstream Oil & Gas, as well as Mining.\r\nCoaching and managing staff\r\nFinancial management and wealth creation\r\nOverseeing key operations in our provision of Geophysical Survey Services\r\nIntellectual Property creation and protection\r\nTechnology and Product development\r\nHealth & Safety – ensuring that our staff returns home safely every day.\r\n\r\nBackground:\r\nGordon has over 20 years’ experience in developing and applying geoscience technologies. He is co-founder and shareholder of Adrok and since Adrok’s inception Gordon has managed technology developments and the company’s global services business. Gordon’s a Member of the Energy Institute, PESGB, EAGE, SEG, AAPG, the Scottish Oil Club, Institute of Directors, the Caledonian Club and YPO. Gordon supports young entrepreneurs as a Business Mentor for Business Mentoring Scotland, as well as for the Prince’s Trust Youth Business Scotland. Gordon holds a BSc (Hons) in Geography from the University of Edinburgh, is a PRINCE2 Registered Practitioner, completed the School of CEOs and is a qualified Life Coach. \r\n\r\n LINKEDIN Profile:  https://www.linkedin.com/in/gstove/ \r\n\r\nSkype: gstove1', 0, '2019-11-27 15:13:22', '2019-11-27 15:13:22'),
(121, 11, 1, 111, 'Gunnar Nyström', 'Boliden', 'speakerlogo/1574867722_gunnar.jpg', 'General Manager - Boliden Taramines', '*****@*****.com', '**********', 'Gunnar Nystrom is a mining professional with 25 years within the hard rock mining sector. Since 2019, he has been the General Manager of the Boliden Tara Mines on Ireland. Gunnar has held several different positions within the mining industry in Sweden, mostly at different mining sites for the Boliden mining company but also Lundin Mining. He has a Master Degree in Mining from the University of Lulea in Sweden.', 0, '2019-11-27 15:15:22', '2019-11-27 15:15:22'),
(122, 11, 1, 111, 'Dr. George Diloyan', 'Nanotech Industrial Solutions', 'speakerlogo/1574867788_dr.jpg', 'CEO', '*****@*****.com', '**********', 'Dr. George Diloyan obtained a Ph.D. in Mechanical Engineering from Temple University, USA, with a focus on nanotechnology, electrochemistry, and material science. He also holds MS degrees in Computer Science and Thermodynamics. Dr. Diloyan has extensive experience with applications of nanomaterials to solve complex industrial problems, with nanoparticle processing mechanisms, and has a successful track record of bringing new innovative products to market. From 2002 to 2008, Mr. Diloyan was working for an international company in Sweden, dealing in energy-related technology projects, due diligence, and commercialization.', 0, '2019-11-27 15:16:28', '2019-11-27 15:16:28'),
(123, 11, 1, 111, 'Eoin Rothery', 'Thomson Resources', 'speakerlogo/1574867856_eoin.png', 'CEO', '*****@*****.com', '**********', 'Eoin Rothery studied geology at Trinity College, Dublin earning a Masters in 1985. Emigrating to Australia in 1989 he worked at the Broken Hill and Macarthur River zinc-lead-silver mines before moving to Western Australia and leading the team that drilled out the first million-ounce resource at Jundee Gold Mine. Eoin later directed Consolidated Minerals’ successful manganese exploration at Woodie Woodie, before re-opening the Surda copper mine in India which broke a 50-year production record in its first full year of production. Eoin has been CEO of Thomson Resources Ltd since 2009: whose current focus is gold and tin exploration projects in New South Wales.', 0, '2019-11-27 15:17:36', '2019-11-27 15:17:36'),
(124, 11, 1, 111, 'Jonathan Cohen', 'AutoStem Europe B.V.', 'speakerlogo/1574867929_jonathan.jpg', 'Director', '*****@*****.com', '**********', 'Jonathan has a Masters degree in Chemical Engineering, and has coordinated AutoStem\'s R&D activities for the last 9 years, overseeing the introduction of the world\'s first high energy, non-detonating, self-stemming rock breaking technology in the AutoStem range of products, including AutoStem Generation 1 in 2013, Generation 2 in 2015 and Generation 3 in 2018.', 0, '2019-11-27 15:18:49', '2019-11-27 15:18:49'),
(125, 11, 1, 111, 'Michael Dineen', 'Zero Trip Innovations', 'speakerlogo/1574867992_dineen.jpg', 'Innovation Manager at Zero-Trip Innovations', '*****@*****.com', '**********', 'Michael Dineen the inventor of the Zero-Trip Wedge. Michael has over 30 years drilling experience and is an expert in directional drilling techniques. Frustrated for years by the repetitive nature of setting directional wedges for directional drilling operations, Michael began to toy with the idea that there was a more efficient method of running in directional wedges. He envisaged a directional wedge that could be run into the borehole in front of the core barrel, with the dropping mechanism pulled out after setting on the wireline, eliminating the need for multiple tripping of rods.', 0, '2019-11-27 15:19:52', '2019-11-27 15:19:52'),
(126, 11, 1, 111, 'Jan Jindra', 'ESS Engineering Software Steyr GmbH', 'speakerlogo/1574868080_jan.png', 'Global Business Development', '*****@*****.com', '**********', 'University trained in Social Sciences at Charles University in Prague - specialization in German-speaking territories and Latin America\r\n\r\n    Courses in the Czech Rep., Germany, Spain and Chile\r\n\r\nAt ESS responsible for Global Business Development', 0, '2019-11-27 15:21:20', '2019-11-27 15:21:20'),
(127, 11, 1, 111, 'Christopher Ecclestone', 'Hallgarten & Company', 'speakerlogo/1574868160_christopher.png', 'Mining Strategist', '*****@*****.com', '**********', 'Experience in Mining and Emerging Market equity analysis and investment banking over the last 18 years. Hedge fund manager and strategist.\r\n\r\nSpecialties: Mining. Strategist for an offshore hedge fund. Equity analyst.', 0, '2019-11-27 15:22:40', '2019-11-27 15:22:40'),
(128, 11, 1, 111, 'Kate McCormick', 'CDE Global', 'speakerlogo/1574868230_kate.png', 'Mining Sector Executive', '*****@*****.com', '**********', 'N/A', 0, '2019-11-27 15:23:50', '2019-11-27 15:23:50'),
(129, 11, 1, 111, 'Liv Carroll', 'Accenture', 'speakerlogo/1574868311_liv.png', 'Senior Principal - Applied Intelligence - Mining Transformation at Accenture', '*****@*****.com', '**********', 'N/A', 0, '2019-11-27 15:25:11', '2019-11-27 15:25:11'),
(130, 11, 1, 111, 'Pedro Bernardo', 'Orica Mining Services Portugal, SA', 'speakerlogo/1574921845_bernardo.png', 'Territory Manager - Portugal', '*****@*****.com', '**********', '(MAIN) EDUCATION\r\n\r\nPhD in Mining Engineering (Technical University of Lisbon, 2004). Thesis: ENVIRONMENTAL IMPACTS FROM THE USE OF EXPLOSIVES IN ROCK EXCAVATION, WITH EMPHASIS ON VIBRATION\r\nMaster of Science in Mining Engineering (Technical University of Lisbon, 1995). Thesis: DESIGN AND SIMULATION OF MINE VENTILATION NETWORKS\r\nGraduated (5 years degree) in Mining Engineering (Technical University of Lisbon, 1993)\r\n\r\n(SOME) PROFESSIONAL QUALIFICATIONS\r\n\r\nGeotechnical Specialist, by the Portuguese Engineering Association, since 2007\r\nSenior Engineer, Member of the Portuguese Engineering Association (Mining Engineering College), since 2005\r\nBlasting certification (nº 10817, to use civil Explosives, issued by the Portuguese National Police, since 2000)\r\n\r\n(BRIEF) EMPLOYMENT HISTORY\r\n\r\nManager (Sales and Blasting Departments, since 2006) of an explosive manufacturing company (SEC, SA; in the AUSTIN POWDER Group, bought by ORICA MINING SERVICES in June 2011)\r\nRepresenting ORICA Europe in Blasting Practice Group of FEEM (Federation of European Explosives Manufacturers), since September 2016\r\nTechnical Services Lead in ORICA MINING SERVICES for the region: SOUTH WEST EUROPE, since November 2013\r\nInvited Professor in Polytechnic University of Porto (Geotechnial Eng. Department – Instituto Superior de Engenharia do Porto), between 2014 and 2019\r\nInvited Professor in Technical University of Lisbon (Mining Engineering Department – Instituto Superior Técnico), since 1992\r\n\r\nPROFESSIONAL MEMBERSHIPS\r\n\r\n AP3E, Associação Portuguesa de Estudos e Engenharia de Explosivos (presently in the Board of Directors, since 2007)\r\nPortuguese Engineering Association (Mining Engineering college), since 1993 (in the National Board from 2004 to 2010)\r\nSPG, Sociedade Portuguesa de Geotecnia (in the Board of Directors from 2004 to 2008)\r\nISRM, International Society for Rock Mechanics (General Secretary of 11th ISRM Congress in Lisbon, 2007)\r\nITA, International Tunneling Association', 0, '2019-11-28 06:17:25', '2019-11-28 06:17:25');

-- --------------------------------------------------------

--
-- Table structure for table `sponsers`
--

CREATE TABLE `sponsers` (
  `id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `detailsid` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `level` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `description` text,
  `toprated` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponsers`
--

INSERT INTO `sponsers` (`id`, `event_id`, `userid`, `detailsid`, `name`, `level`, `image`, `website`, `email`, `phone`, `description`, `toprated`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 9, 'OCP', 'Silver', 'sponserlogo/1572427851_Ocp.jpg', 'www.ocpgroup.ma', 'info@ocpgroup.com', '9876543212', 'OCP GROUP is a global leader in the phosphate industry and its derivatives. With nearly a century of history and expertise', 0, '2019-10-30 09:30:51', '2019-10-30 09:30:51'),
(2, 2, 7, 17, 'Wipro Solutions tech', 'Platinum', 'sponserlogo/1572875480_privacy.png', 'www.free.com', 'free@yopmail.com', '235235345435', 'Very long processed development', 0, '2019-11-04 13:51:20', '2019-11-04 13:51:20'),
(3, 2, 7, 17, 'HCL technologies', 'Diamond', 'sponserlogo/1572875565_ffffff.png', 'www.hcl.com', 'q@yopmail.com', '625826394829', 'Description of the Company is not known by anyone. But this uis the est company to be a Sponsors. It is a top Rated company', 1, '2019-11-04 13:52:45', '2019-11-04 13:52:45'),
(4, 4, 1, 27, 'jahan test', '1', 'sponserlogo/1573018923_images1.jpg', 'www.test.com', 'test@tets.com', '9596775491', 'akjsbcAKJSBc.jsbdcjhSBDCJH', 0, '2019-11-06 05:42:03', '2019-11-06 05:42:03'),
(5, 6, 1, 41, 'Money provider Company _1', '1', 'sponserlogo/1573195500_logo3.png', 'www.goppm.com', 'gopp@yopmail.com', '4656546549846', 'Descriptosdf fsdhksd fdsf sdfsfd', 0, '2019-11-08 06:45:00', '2019-11-08 06:45:00'),
(6, 6, 1, 41, 'They ,need t Provide the Mo', '2', 'sponserlogo/1573195540_logo1.png', 'www.facebook.com', 'face@yopmail.com', '8859546464', 'Description of thr ebskjnksdfbksd  Description of thr ebskjnksdfbksd  Description of thr ebskjnksdfbksd  Description of thr ebskjnksdfbksd  Description of thr ebskjnksdfbksd', 1, '2019-11-08 06:45:40', '2019-11-08 06:45:40'),
(7, 8, 1, 54, 'Sponsors_Company_1', '1', 'sponserlogo/1573208886_logo1.png', 'www.goppm.com', 'rtyrte@ty.ret', '5464654646', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:28:06', '2019-11-08 10:28:06'),
(8, 8, 1, 54, 'Sponsors_Company_2', '2', 'sponserlogo/1573208906_logo3.jpg', 'www.galgotia.com', 'reterz@fdhfg.gfyh', '9631525421454', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:28:26', '2019-11-08 10:28:26'),
(9, 8, 1, 54, 'Sponsors_Company_3', '3', 'sponserlogo/1573208924_logo2.jpeg', 'www.facebook.com', 'email@mainleast.com', '9655564655', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:28:44', '2019-11-08 10:28:44'),
(10, 8, 1, 54, 'Sponsors_Company_4', '4', 'sponserlogo/1573208945_logo4.png', 'www.goppm.com', 'email@gmail.com', '9631525421454', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:29:05', '2019-11-08 10:29:05'),
(11, 8, 1, 54, 'Sponsors_Company_5', '5', 'sponserlogo/1573208964_logo4.png', 'www.fegrh.fvui', 'email@gmail.com', '879885764', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:29:24', '2019-11-08 10:29:24'),
(12, 8, 1, 54, 'Sponsors_Company_6', '6', 'sponserlogo/1573208986_logo3.jpg', 'www.goppm.com', 'rtyrte@ty.ret', '5464654646', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:29:46', '2019-11-08 10:29:46'),
(13, 8, 1, 54, 'Sponsors_Company_7', '7', 'sponserlogo/1573209013_logo4.png', 'www.facebook.com', 'reterz@fdhfg.gfyh', '879885764', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:30:13', '2019-11-08 10:30:13'),
(14, 8, 1, 54, 'Sponsors_Company_8', '8', 'sponserlogo/1573209041_logo3.png', 'www.fegrh.fvui', 'email@mainleast.com', '879885764', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:30:41', '2019-11-08 10:30:41'),
(15, 8, 1, 54, 'Sponsors_Company_9', '9', 'sponserlogo/1573209062_logo3.jpg', 'https://facebook.com', 'email@mainleast.com', '5464654646', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:31:02', '2019-11-08 10:31:02'),
(16, 8, 1, 54, 'Sponsors_Company_10', '10', 'sponserlogo/1573209084_logo4.png', 'https://facebook.com', 'rtyrte@ty.ret', '9631525421454', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 1, '2019-11-08 10:31:24', '2019-11-08 10:31:24'),
(17, 8, 1, 54, 'Exhibitor_COmpany_11', '11', 'sponserlogo/1573209099_logo4.png', 'www.goppm.com', 'email@gmail.com', '9631525421454', 'This Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:31:39', '2019-11-08 10:31:39'),
(18, 8, 1, 54, 'Sponsors_Company_12', '12', 'sponserlogo/1573209119_logo2.jpeg', 'www.facebook.com', 'etadmin@gmail.com', '7014552245', 'This Event is the Main Worth', 0, '2019-11-08 10:31:59', '2019-11-08 10:31:59'),
(19, 8, 1, 54, 'Sponsors_Company_13', '13', 'sponserlogo/1573209134_man2.jpeg', 'www.galgotia.com', 'email@gmail.com', '879885764', 'This Event is the Main Worth', 0, '2019-11-08 10:32:14', '2019-11-08 10:32:14'),
(20, 8, 1, 54, 'Sponsors_Company_14', '14', 'sponserlogo/1573209148_logo4.png', 'www.galgotia.com', 'fegf@gfhgf.ewrew', '5464654646', 'This Event is the Main Worth', 0, '2019-11-08 10:32:28', '2019-11-08 10:32:28'),
(21, 8, 1, 54, 'Sponsors_Company_15', '15', 'sponserlogo/1573209164_logo3.png', 'www.goppm.com', 'email@mainleast.com', '879885764', 'This Event is the Main Worth', 0, '2019-11-08 10:32:44', '2019-11-08 10:32:44'),
(22, 8, 1, 54, 'Sponsors_Company_16', '16', 'sponserlogo/1573209181_logo3.png', 'www.facebook.com', 'email@mainleast.com', '9631525421454', 'This Event is the Main Worth', 0, '2019-11-08 10:33:01', '2019-11-08 10:33:01'),
(23, 8, 1, 54, 'Sponsors_Company_17', '17', 'sponserlogo/1573209200_logo1.png', 'www.facebook.com', 'reterz@fdhfg.gfyh', '9631525421454', 'This Event is the Main Worth', 0, '2019-11-08 10:33:20', '2019-11-08 10:33:20'),
(24, 8, 1, 54, 'Sponsors_Company_18', '18', 'sponserlogo/1573209217_man2.jpeg', 'www.galgotia.com', 'reterz@fdhfg.gfyh', '5464654646', 'This Event is the Main Worth', 0, '2019-11-08 10:33:37', '2019-11-08 10:33:37'),
(25, 8, 1, 54, 'Sponsors_Company_19', '19', 'sponserlogo/1573209233_logo3.jpg', 'https://facebook.com', 'reterz@fdhfg.gfyh', '9631525421454', 'This Event is the Main Worth', 0, '2019-11-08 10:33:53', '2019-11-08 10:33:53'),
(26, 8, 1, 54, 'Sponsors_Company_20', '20', 'sponserlogo/1573209262_man3.jpeg', 'www.fegrh.fvui', 'email@gmail.com', '879885764', 'This Event is the Main Worth', 0, '2019-11-08 10:34:22', '2019-11-08 10:34:22'),
(27, 8, 1, 54, 'Sponsors_Company_21', '21', 'sponserlogo/1573209286_man2.jpeg', 'https://facebook.com', 'email@mainleast.com', '5464654646', 'This Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:34:46', '2019-11-08 10:34:46'),
(28, 8, 1, 54, 'Sponsors_Company_22', '22', 'sponserlogo/1573209304_logo2.jpeg', 'www.fegrh.fvui', 'reterz@fdhfg.gfyh', '9631525421454', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:35:04', '2019-11-08 10:35:04'),
(29, 8, 1, 54, 'Sponsors_Company_23', '23', 'sponserlogo/1573209321_logo3.png', 'www.galgotia.com', 'rtyrte@ty.ret', '9631525421454', 'This Event is the Main Worth', 0, '2019-11-08 10:35:21', '2019-11-08 10:35:21'),
(30, 8, 1, 54, 'Sponsors_Company_24', '24', 'sponserlogo/1573209335_logo3.png', 'www.goppm.com', 'email@gmail.com', '9631525421454', 'This Event is the Main Worth', 0, '2019-11-08 10:35:35', '2019-11-08 10:35:35'),
(31, 8, 1, 54, 'Sponsors_Company_25', '25', 'sponserlogo/1573209352_man2.jpeg', 'www.facebook.com', 'reterz@fdhfg.gfyh', '879885764', 'This Event is the Main Worth', 0, '2019-11-08 10:35:52', '2019-11-08 10:35:52'),
(32, 8, 1, 54, 'Sponsors_Company_26', '26', 'sponserlogo/1573209369_logo2.jpeg', 'https://facebook.com', 'email@gmail.com', '5464654646', 'This Event is the Main Worth', 0, '2019-11-08 10:36:09', '2019-11-08 10:36:09'),
(33, 8, 1, 54, 'Sponsors_Company_27', '27', 'sponserlogo/1573209388_man2.jpeg', 'www.fegrh.fvui', 'rtyrte@ty.ret', '879885764', 'This Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:36:28', '2019-11-08 10:36:28'),
(34, 8, 1, 54, 'Sponsors_Company_28', '28', 'sponserlogo/1573209403_logo3.jpg', 'www.fegrh.fvui', 'rtyrte@ty.ret', '879885764', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:36:43', '2019-11-08 10:36:43'),
(35, 8, 1, 54, 'Sponsors_Company_29', '29', 'sponserlogo/1573209423_logo3.jpg', 'www.galgotia.com', 'email@mainleast.com', '879885764', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:37:03', '2019-11-08 10:37:03'),
(36, 8, 1, 54, 'Sponsors_Company_30', '30', 'sponserlogo/1573209443_man2.jpeg', 'www.galgotia.com', 'email@mainleast.com', '9631525421454', 'This Event is the Main Worth', 1, '2019-11-08 10:37:23', '2019-11-08 10:37:23'),
(37, 8, 1, 54, 'Sponsors_Company_31', '31', 'sponserlogo/1573209459_logo3.png', 'www.facebook.com', 'email@gmail.com', '5464654646', 'This Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:37:39', '2019-11-08 10:37:39'),
(38, 8, 1, 54, 'Sponsors_Company_32', '32', 'sponserlogo/1573209474_man2.jpeg', 'https://facebook.com', 'rtyrte@ty.ret', '5464654646', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:37:54', '2019-11-08 10:37:54'),
(39, 8, 1, 54, 'Sponsors_Company_33', '33', 'sponserlogo/1573209489_man2.jpeg', 'www.fegrh.fvui', 'rtyrte@ty.ret', '9655564655', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:38:09', '2019-11-08 10:38:09'),
(40, 8, 1, 54, 'Sponsors_Company_34', '34', 'sponserlogo/1573209509_logo4.png', 'www.goppm.com', 'reterz@fdhfg.gfyh', '879885764', 'This Event is the Main Worth', 0, '2019-11-08 10:38:29', '2019-11-08 10:38:29'),
(41, 8, 1, 54, 'Sponsors_Company_35', '35', 'sponserlogo/1573209529_logo3.jpg', 'www.goppm.com', 'rtyrte@ty.ret', '879885764', 'This Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:38:49', '2019-11-08 10:38:49'),
(42, 8, 1, 54, 'Sponsors_Company_36', '36', 'sponserlogo/1573209544_logo3.png', 'https://facebook.com', 'email@mainleast.com', '9631525421454', 'This Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:39:04', '2019-11-08 10:39:04'),
(43, 8, 1, 54, 'Sponsors_Company_37', '37', 'sponserlogo/1573209568_logo2.jpeg', 'https://facebook.com', 'etadmin@gmail.com', '7014552245', 'This Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main WorthThis Event is the Main Worth', 0, '2019-11-08 10:39:28', '2019-11-08 10:39:28'),
(44, 9, 1, 57, 'fgdfg', '4', 'sponserlogo/1573217626_ffffff.png', 'df.tyh.tyur', 'sdf@fdh.yjt', '35645646456', 'fddh sjhs hs rhtr  tr ytr', 0, '2019-11-08 12:53:46', '2019-11-08 12:53:46'),
(50, 10, 1, 62, 'njfvfnjvnvbfnjmv', 'one', 'sponserlogo/1573925647_ex6.jpg', 'www.test.com', 'info@guyanaoilexpo.com', '122122', 'fdlkdmlkmlksdmkldsmklsfdmlksfmlkd', 0, '2019-11-16 17:34:07', '2019-11-16 17:34:07'),
(56, 10, 1, 70, 'Valiant Business Media', 'Organizer', 'sponserlogo/1573929081_logo.png', 'www.valiantbmedia.com', 'info@valiantbmedia.com', '+44 (0)208 242 6566', 'Valiant Business Media is a global business information and corporate marketing group. Since our inception in 2009, we have successfully worked with the number of corporations from medium-sized firms to multi-billion dollar organisations, uniquely positioned to deliver cutting-edge solutions to our clients.\r\n\r\nBased out of London, we have a global presence with a representative office in Birmingham and branch offices across the Middle East, Asia, Africa and the Americas.', 0, '2019-11-16 18:31:21', '2019-11-16 18:31:21'),
(59, 10, 1, 70, 'Department of Energy, Ministry of the Presidency', 'Organizer', 'sponserlogo/1573929495_hostedby.jpg', 'www.motp.gov.gy', '*****@*****.com', '**********', 'The Department of Energy was created on August 1, 2018 to effectively manage the hydrocarbon resources of the Cooperative Republic of Guyana. The May 2015 and subsequent discoveries of oil and associated gases, have resulted in Guyana being regarded amongst the top reserve holders worldwide. This has the potential for improved standard of living for all Guyanese and transformational and impactful development to occur.\r\n\r\nAs the Department responds to the rapid rate of developments within the sector, it invites suitably qualified individuals to apply to fill vacancies in the following areas by submitting: (i) a cover letter written as a Statement of Interest, and (ii) a curriculum vitae\r\n• Economics\r\n• Econometrics\r\n• Law\r\n• Procurement\r\n• Monitoring and Evaluation\r\n• Administration\r\n• Environmental Safeguards', 0, '2019-11-16 18:38:15', '2019-11-16 18:38:15'),
(60, 10, 1, 70, 'Guyana Office for Investment', 'Host', 'sponserlogo/1573929974_o2.png', 'www.goinvest.gov.gy', 'ceosec@goinvest.gov.gy', '+592-225-0658, 227-0653', 'To contribute to Guyana’s economic development by promoting and facilitating local and foreign private-sector investment and exports in accordance with the country’s approved investment and export strategies.\r\n\r\nThe Guyana Office for Investment (GO-Invest) was established under the Public Corporations Act (1994) in 1994 as a semi-autonomous body and comes under the direct purview of the Ministry of Business. The CEO answers to a Board of Directors which is composed on representatives of both the private and public sectors.\r\n\r\nGO-Invest is divided into two divisions, one responsible for Investment Facilitation and Promotion and the other for Export Promotion. With these divisions, GO-Invest offers a full complement of services to local and foreign investors and exporters', 0, '2019-11-16 18:46:14', '2019-11-16 18:46:14'),
(65, 10, 1, 94, 'Industrial Technology', 'Media', 'sponserlogo/1573945403_m1.jpg', 'www.industrialtechmag.com', '*****@*****.com', '+39 02 87176540', 'Industrial Technology is an international magazine focusing on machines, plants, components, automation, plastic, industry 4.0, steel. It contains news, information. case histories and product profiles about OeMs, technology suppliers and EPCs. Available both printed and digital, its main focuses are plants, automation, controls, instrumentation, pumps, compressors, blowers, boilers, heat exchangers, hvac, electrical, process units, filters, tanks, valves, softwares. Through the most important fairs and exhibitions, and the collaboration with the main companies of the industrial sector, Industrial Technology Magazine reaches every year thousands of readers all over the world.', 0, '2019-11-16 23:03:23', '2019-11-16 23:03:23'),
(66, 10, 1, 94, 'Worldoils', 'Media', 'sponserlogo/1573945485_m2.jpg', 'www.worldoils.com', '*****@*****.com', '**********', 'Worldoils is a company that combines the power of marketing as well as the in-house expertise for the Oil, Gas. Offshore and the Maritime industries. Worldoils\' web portal www.worldoils.com has become a truly central platform for visitors who need information regarding oil and gas products and services, research, training, conferences, news and events as well as a popular advertising base for providers of Oil, Gas. Marine and Offshore services. Worldoils has also launched the jobs system and a marketplace. In the recent months, Worldoils has strengthened its position as a fast-developing central place for buying and selling of land rigs, offshore rigs, barge rigs and other oilfield and subsea equipment.', 0, '2019-11-16 23:04:45', '2019-11-16 23:04:45'),
(67, 10, 1, 94, 'PetrolPlaza', 'Media', 'sponserlogo/1573945570_m3.jpg', 'www.petrolplaza.com', '*****@*****.com', '+49 172 72 33442', 'With a growing community, PetrolPlaza continues to be the No.1 online information source for decision makers in the petrol retailing, fuel logistics and car wash markets. After going online in 1998, PetrolPlaza has built an excellent reputation amongst industry professionals across the world. PetrolPlaza offers daily updated market news and a newsletter service free of charge. Here you find information on the latest technologies, trends and products, as well as case studies and video reports and can access extensive equipment supplier and fuel retailer listings. Over 17,000 subscribers receive our free of charge weekly newsletters.', 0, '2019-11-16 23:06:10', '2019-11-16 23:06:10'),
(68, 10, 1, 94, 'AfricaBusiness', 'Media', 'sponserlogo/1573945675_m4.jpg', 'www.africabusiness.com', '*****@*****.com', '**********', 'AfricaBusiness.com is an online digital business magazine covering: news, ICT, green energy, forex , online hotel reservations, money transfers, events and interviews. AfricaBusiness.com has been voted among the most influential blogs in Africa, thus giving it authority as a credible and reliable channel to reach to the African audience and market. For more information on Africa news.', 0, '2019-11-16 23:07:55', '2019-11-16 23:07:55'),
(69, 10, 1, 94, 'Sagacity', 'Media', 'sponserlogo/1573945793_m5.jpg', 'www.sagacity.gy', 'sagacity.bis@gmail.com', '592-611-5312/225-6740', 'Sagacity is the leading public relations firm in Guyana with services in marketing and advertising and event management. Through its affiliate – CinNex – the company also offers video production and photography services.', 0, '2019-11-16 23:09:53', '2019-11-16 23:09:53'),
(70, 10, 1, 94, 'OilNOW', 'Media', 'sponserlogo/1573945859_m6.png', 'www.oilnow.gy', 'editor@oilnow.gy', '(592) 611-5312 / 601-0111', 'OilNOW is an online-based Information and Resource Centre based in Georgetown, Guyana. It serves to complement the work of stakeholders in the country\'s emerging oil and gas sector. OilNOW is an apolitical, non-partisan, privately owned agency that strives to ensure the highest degree of accuracy and balance in the dissemination of information in the public domain.', 0, '2019-11-16 23:10:59', '2019-11-16 23:10:59'),
(71, 10, 1, 69, 'Department of Energy, Ministry of the Presidency', 'Host', 'sponserlogo/1574081883_doe.png', 'www.motp.gov.gy', '*****@*****.com', '**********', 'The Department of Energy was created on August 1, 2018 to effectively manage the hydrocarbon resources of the Cooperative Republic of Guyana. The May 2015 and subsequent discoveries of oil and associated gases, have resulted in Guyana being regarded amongst the top reserve holders worldwide. This has the potential for improved standard of living for all Guyanese and transformational and impactful development to occur.\r\n\r\nAs the Department responds to the rapid rate of developments within the sector, it invites suitably qualified individuals to apply to fill vacancies in the following areas by submitting: (i) a cover letter written as a Statement of Interest, and (ii) a curriculum vitae\r\n• Economics\r\n• Econometrics\r\n• Law\r\n• Procurement\r\n• Monitoring and Evaluation\r\n• Administration\r\n• Environmental Safeguards\r\n\r\n1. Eligibility Requirements\r\n\r\nTo be considered for employment, applicants must meet the following requirements:\r\n\r\n? Possess a Bachelor’s degree or higher relevant to the field of study.\r\n? Have excellent academic performance as demonstrated by university or institution records.\r\n? Be proficient in English language.\r\n? Additional consideration would be given to past professional experience in the field for which a candidate in applying.\r\n\r\n2. The DoE offers a competitive remuneration package\r\nPersons desirous of having their applications considered, can submit same on or before March 30th, 2019 to.', 0, '2019-11-18 12:58:03', '2019-11-18 12:58:03'),
(74, 11, 1, 110, 'Europe & Middle East Outlook', 'Media', 'sponserlogo/1574857034_eme.png', 'www.emeoutlookmag.com', 'tom.wadlow@outlookpublishing.com', '+44 (0) 1603 959 657', 'EME Outlook is a digital and print product aimed at boardroom and hands-on decision-makers across a wide range of industries in the region.\r\n\r\nWith content compiled by our experienced production team ensuring delivery to the highest standards, we look to promote the latest in engaging news, industry trends and success stories from the length and breadth of both Europe and the Middle East.\r\n\r\nThe magazine is owned and published by Outlook Publishing, who also produce global business titles, Asia Outlook and Africa Outlook, providing a superb platform for advertisers and media partners to showcase your services and place you in the hands of the continent’s most influential people.', 0, '2019-11-27 12:17:14', '2019-11-27 12:17:14'),
(75, 11, 1, 110, 'Coal Zoom', 'Media', 'sponserlogo/1574857162_coal.jpg', 'www.coalzoom.com', 'billreidcoal@gmail.com', '304-920-2588', 'Coal Zoom, powered by Coal News, is coal\'s foremost on-line publication for coal operators, managers, mining personnel, equipment manufacturers, mining consultants, government employees in coal related positions, students and faculties of mining colleges, mining association and institutes, and to others providing products and services to the coal mining industry.\r\n\r\nCoal Zoom *Promotes greater safety and health in coal mining and processing; *Presents reports on coal conferences, meetings, trade shows, and expositions; Coal Zoom is your resource for quick and insightful information about the coal industry.', 0, '2019-11-27 12:19:22', '2019-11-27 12:19:22'),
(76, 11, 1, 110, 'GeoConnexion', 'Media', 'sponserlogo/1574857305_geoconnexion.png', 'www.geoconnexion.com', 'maiward@geoconnexion.com', '+44 1223 279151', 'GeoConnexion International and GeoConnexion UK bring you the latest news and stories plus reports from geotechnology industries in UK, Europe, the Middle East, Africa, North America and Asia.\r\n\r\nCoverage of topics such as 3D Visualisation, Remote Sensing, LiDAR, Cloud, Mobile Mapping, Navigation with emphases on healthcare, public safety, retail, the environment, utilities, surveying, LBS, transport/ logistics, telecommunications and more.', 0, '2019-11-27 12:21:45', '2019-11-27 12:21:45'),
(77, 11, 1, 110, 'Zimbabwe Mining News', 'Media', 'sponserlogo/1574857402_zimbabwe.jpg', 'www.zimbabweminingnews.com', 'editor@zimbabweminingnews.com', '+263771196983', 'Prime Media Network is the official publisher of the Zimbabwe Mining and Energy Review Quarterly Magazine and ZimbabweMiningNews.Com. The print and digital media channels complement each other to offer an insight into the mining, energy and related industries in Zimbabwe and the African continent.\r\n\r\nThe Zimbabwe Mining & Energy Review Magazine comes out every quarter to give readers a summary of the mining industry developments. Assuming a fresh focus on the country and its natural wealth, this magazine offers an independent approach to news reporting in and about Zimbabwe.\r\n\r\nZimbabweMiningNews.Com is a premier online news platform offering reliable and well researched mining industry news, developments and reports. Our platform is also the official digital media channel for the Zimbabwe Mining and Energy Review Quarterly Journal Magazine. The digital platform shares its content and media to a targeted Corporate Audience via Social Media Networks such as LinkedIn, Twitter and Facebook. The editorial focus is to provide daily updates to local and international investors, policy makers, buyers and marketers within the industry.', 0, '2019-11-27 12:23:22', '2019-11-27 12:23:22'),
(78, 11, 1, 110, 'Jobs4Mining', 'Media', 'sponserlogo/1574857498_jobs4.jpg', 'www.jobs4mining.com', 'info@jobs4mining.com', '+44 1256 336632', 'Jobs4mining is an online jobs board dedicated to the mineral mining and quarrying industries worldwide. We connect employers and candidates across all disciplines, levels and aim to provide a secure and user friendly platform that incorporates the mutual interests for all within their specialist sectors :\r\n\r\nMining Companies\r\nMining Consultancies\r\nRecruitment Specialists\r\nMining Professionals\r\nExploration Professionals', 0, '2019-11-27 12:24:58', '2019-11-27 12:24:58'),
(79, 11, 1, 110, 'Mining Turkey', 'Media', 'sponserlogo/1574857594_turkey.jpg', 'www.mining-turkey.com', 'info@mining-turkey.com', '+90 (312) 482 18 60', 'Mining Turkey has been the first and only English mining magazine published in Turkey since 2011. End of 2017, we have converted our magazine to news portal “Mining-Turkey.com”. The vision of our website is promoting Turkish mining industry to international mining companies and foreign-invested companies and their executives who present in Turkey. We undertake introducing Turkish mining industry to whole world. Adopting the motto “Discover the Turkish Mining Industry”, Mining-Turkey.com serves to the mining professionals all around the world as the one and only true mining news source for those seeking to follow the Turkish Mining Industry.', 0, '2019-11-27 12:26:34', '2019-11-27 12:26:34'),
(80, 11, 1, 110, 'Madencilik Türkiye', 'Media', 'sponserlogo/1574859163_Maden.jpg', 'www.madencilikturkiye.com', 'info@madencilikturkiye.com', '+90 (312) 482 18 60', 'The first and only mining magazine of Turkey, Madencilik Türkiye Magazine, which was founded and have been managed by an engineer team with several working experience, was published on September 2009 firstly. Madencilik Türkiye Magazine takes attention for its rich sectoral news content. Besides, mining stages, starting from exploration of a mine, whole phase of production and rehabilitation of the mine after its production are included to the magazine’s content. Moreover, our magazine includes advertisements and company introductions about all departments which serve to mining explorers and producers. Reviews written by sector professionals and magazine directors, contribute development of sector. Having widespread distribution network and audience, Madencilik Türkiye creates an unmatched platform for company advertisements. Madencilik Türkiye Magazine is the best alternative for the companies which want to introduce their operations to the Turkish mining industry.', 0, '2019-11-27 12:52:43', '2019-11-27 12:52:43'),
(81, 11, 1, 110, 'Resource World', 'Media', 'sponserlogo/1574859325_resource.jpg', 'www.resourceworld.com', 'info@resourceworld.com', '(604) 484 3800', 'Resource World Magazine reports on the business of mining, oil & gas, green technologies and the events that affect these sectors. The magazine provides a platform to profile companies on a non-advertorial basis and has no paid editorial. Resource World provides readers with an objective perspective, relevant, timely information, is committed to its policies of journalistic integrity and remains a reliable source for information about the resource sector.', 0, '2019-11-27 12:55:25', '2019-11-27 12:55:25'),
(82, 11, 1, 110, 'Mining Review Africa', 'Media', 'sponserlogo/1574859428_minrew.jpg', 'www.miningreview.com', 'Laura.Cornish@clarionevents.com', '+27 10 009 6846', 'Mining Review Africa is the leading monthly magazine and digital platform in the African mining industry. Every month, MRA reaches an audience of over 50 000 influential mining authorities and key decision makers through a variety of channels, including an interactive website, videos and print distribution at all major mining conferences in Africa and across the globe. MRA serves as a knowledge, news and information sharing platform which drives upliftment and sustainable development across the mining sector in Africa through articles on project developments and the technology and financial models that drive them. Through close interaction with major, mid-tier and junior mining houses; suppliers of capital; technology and plants, MRA is recognized as the industry’s thought leadership, innovation and strategic business content leader.', 0, '2019-11-27 12:57:08', '2019-11-27 12:57:08'),
(83, 11, 1, 110, 'Mining Zimbabwe', 'Media', 'sponserlogo/1574859591_minzim.png', 'www.miningzimbabwe.com', 'sales@be.co.zw', '0242 665 313', 'Mining Zimbabwe is the Premier source of Zimbabwe Mining News. Our core focus is on the ever evolving face of the Zimbabwe mining Industry , Trends, New Technologies being developed and used improve this crucial sector and well as new opportunities and investments arising from it. Our sole purpose is growing and empowering the mining industry and highlighting all its challenges as well as putting forth expert solution.', 0, '2019-11-27 12:59:51', '2019-11-27 12:59:51'),
(84, 11, 1, 110, 'Plant & Equipment', 'Media', 'sponserlogo/1574859688_plant.jpg', 'www.plantandequipment.com', '*****@*****.com', '**********', 'Plant & Equipment is a leading magazine and online classifieds’ website catered for the heavy equipment and truck industry. Our strategy focuses on solutions to help our sellers build a better network that allows profitable growth. With over 19 years of experience, our dedicated specialists at Plant & Equipment, pride themselves on quality customer service and a long history of relationships we have attained over the years; some of which are the most reputable names in the region.\r\nPlantandequipment.com is a truly simple, yet innovative classified website, coupled with our regional magazines to deliver a marketing solution that combines both online and print.\r\nPlant & Equipment publishes and prints 3 magazines: Plant & Equipment Middle East, Plant & Equipment Africa, and Trucks & Transport Middle East.\r\nOur growing Team at Plant & Equipment is committed to establishing lifetime relationships and opportunities for both the seller and the buyer. The outcome is a printing and online construction marketplace well-suited for the Middle East, Africa, Asia, Europe, and North America.', 0, '2019-11-27 13:01:28', '2019-11-27 13:01:28'),
(85, 11, 1, 110, 'Redimin Mining Magazine', 'Media', 'sponserlogo/1574860008_Redi.jpg', 'www.redimin.cl', '*****@*****.com', '**********', 'Digital Magazine Minera was founded in 2011 in Chile by Editor and Founder Cristian Recabarren Ortiz. Since then, the main mission of Revista Digital Minera has been to communicate the latest advances in Mining in Chile at one place on the Internet. Currently Digital Magazine Minera is a worldwide recognized magazine under the name of \"REDIMIN Mining Magazine\" and now based in London, England. The Magazine is dedicated to communicate the main advances in mining technology by actively participating in the largest mining congresses Worldwide.\r\n\r\nOur roadmap is to communicate the main technological advances, opinions and raise awareness about the guidelines that govern the growth of the productive chain of the Mining Industry in Chile. Our digital policy is our main legacy and reasons for making sustainable mining journalism spreading our information through our social networks and digital subscriptions to our readers.', 0, '2019-11-27 13:06:48', '2019-11-27 13:06:48'),
(86, 11, 1, 110, 'African Mining Brief', 'Media', 'sponserlogo/1574860116_afmin.jpg', 'www.africanminingbrief.com', 'info@collegepublishers.co.za', '+27 11 781 4253/88', 'African Mining Brief is a bi-monthly magazine for the mining industry incorporating mining, quarrying, oil exploration and drilling; and other large-scale extractive operations aimed at the discovery, extraction, storage, transport, sale and use of Africa’s mineral wealth and resources. AMB boasts of a targeted, well-researched and insightful editorial content that offers readers and advertisers a unique interactive forum to exchange information; identify, pursue, and close business deals; in addition to gaining favourable insight into the entire mining industry and public sector.', 0, '2019-11-27 13:08:36', '2019-11-27 13:08:36'),
(87, 11, 1, 110, 'Mining Industry Professionals Network Forum', 'Media', 'sponserlogo/1574860232_minindus.jpg', 'www.miningindustryprofessionals.net', '*****@*****.com', '**********', 'The Mining Industry Professionals Network Forum is the new and fastest growing Mining related community in the world and the industry\'s only spam-free, independent curated Forum for intelligent conversations with mining thought leaders. Professionals in and serving the mining industry worldwide are invited to join the Mining Industry Professionals Network.', 0, '2019-11-27 13:10:32', '2019-11-27 13:10:32'),
(93, 11, 1, 105, 'Valiant Business Media', 'Organizer', 'sponserlogo/1575019653_1000.jpg', 'www.valiantbusinessmedia.com', 'info@valiantbmedia.com', '+44 (0)208 242 6566', 'Our focus is on providing business services in different formats such as Training, Conferences, Government Sponsored Forums, Consulting, Global Summits, Exhibitions, Congresses, Publications and Market Research. We work with great enthusiasm to be the best at what we do, designing and delivering effective business solutions for accelerating growth and sustainability.\r\n\r\nValiant Business Media offers creative delivery of events and meetings for a variety of industries including oil & energy, mining & metals, healthcare, pharmaceutical, technology and more. We are a leading business information company providing total meeting solutions and creative production management.', 0, '2019-11-29 09:27:33', '2019-11-29 09:27:33');

-- --------------------------------------------------------

--
-- Table structure for table `staticpages`
--

CREATE TABLE `staticpages` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `content` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staticpages`
--

INSERT INTO `staticpages` (`id`, `page_id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 'About us', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\" \"http://www.w3.org/TR/REC-html40/loose.dtd\">\n<html><body><p>ETapp helps to build relationships at conferences and events through private event communities. Download the mobile event app and find our events in order to: Network with fellow attendees in a private event&hellip;</p></body></html>\n', '2019-10-20 10:46:09', '2019-11-13 10:00:06'),
(2, 2, 'Terms and condition', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\" \"http://www.w3.org/TR/REC-html40/loose.dtd\">\n<html><body><p><strong>Terms and Conditions</strong></p>\r\n\r\n<p>Services are provided to you subject to the below-mentioned Terms and conditions, to make you aware of your legal rights and responsibilities with respect to your access to and use of our website, and may be updated from time to time without any prior notice. By using Eventackle\'s Services, you agree to comply with and be bound by these Terms, as applicable to you.</p>\r\n\r\n<p>When these Terms use the term \"Organizer,\" we mean event creators using the Services to create events displayed for consumers using our Services (a) to consume information about or attend Events (\"Consumers\"), or (b) for any other reason. Organizers, Consumers and third parties using our Services are all referred to in these Terms as \"Users,\" \"you\" or \"your.\"<br><br>\r\nWhen these Terms use the term \"Eventackle,\" \"we,\" \"us,\" or \"our,\" that refers to Eventackle, and each of its officers, directors, agents, partners, and employees.&nbsp;</p>\r\n\r\n<p>\"Content\" will include (but is not limited to) reviews, images, photos, audio, video, location data, nearby places, and all other forms of information or data. \"Your content\" or \"User Content\" means content that you upload, share or transmit to, through or in connection with the Services, such as likes, ratings, reviews, images, photos, messages, profile information, and any other materials that you publicly display or displayed in your account profile.</p>\r\n\r\n<p>These Terms are effective for all existing and future Eventackle users. Please read these Terms carefully.</p>\r\n\r\n<p>1.Description of the services</p>\r\n\r\n<p>Eventackle offers online services to event organizers and attendees. Eventackle allows an arrangement between its users to make business connections and offers&nbsp; services through its website to find business events happening across the world and also create, promote events and manage events.</p>\r\n\r\n<p>Eventackle is not the creator, organizer or owner of the events listed on the Services. Rather, Eventackle provides its Services, which allow Organizers to manage ticketing ,registration and promote their events.</p>\r\n\r\n<p>2. Eligibility for Membership</p>\r\n\r\n<p>User must be 18 or above to register as a member of Eventackle or use the Eventackle website. Membership is void where prohibited. Your membership registration for, or use of, the Service shall be deemed to be your agreement to abide by this Agreement including any materials available on the Eventackle website.</p>\r\n\r\n<p>3.Membership</p>\r\n\r\n<p>Signing up for&nbsp; Eventackle membership is free. Eventackle reserves the right to charge for the&nbsp; Service or any portion thereof, modify the pricing of, add to, or terminate the Service or any portion thereof without prior notice. You agree to provide Eventackle with complete and accurate contact information. If the contact information you have provided is false or fraudulent, Eventackle reserves the right to discontinue your access to the Service as well as potential legal liability depending on the circumstances.</p>\r\n\r\n<p>4.License Grant</p>\r\n\r\n<p>Eventackle grants you a personal, limited, non-exclusive, non-transferable, non-sublicensable (except to sub-Users registered via the Services), revocable right to use our Services solely to (a) browse the Services and search for, view, register for or purchase tickets or registrations to an event listed on the Services; and/or (b) create event registration, organizer profile and other webpages to promote, market, manage, track, and collect sales proceeds for an event. You shall not use the Services for any unlawful intent or in any manner inconsistent with these Terms. You may use information made available through the Services solely for your personal, non-commercial use. You agree not to use, copy, display, distribute, modify, broadcast, translate, reproduce, reformat, incorporate into advertisements and other works, sell, promote, create derivative works, or in any way exploit or allow others to exploit any of eventackle Content in whole or in part except as expressly authorized by us. Except as otherwise expressly granted to you in writing, we do not grant you any other express or implied right or license to the Services, Eventackle Content.&nbsp;</p>\r\n\r\n<p>5. Intellectual Property Rights</p>\r\n\r\n<p>Eventackle solely owns all rights, title and interest, including all related Intellectual Property Rights, in and to the Eventackle Technology, the Content and the Service and any reviews recommendations or other information provided by you or any other party relating to the Service. This Agreement does not entitle you to any rights of ownership in or related to the Service, the Eventackle Technology or the Intellectual Property Rights owned by Eventackle. The Eventackle name, eventackle.com, eventackle.com logo, and the product names associated with the Service are trademarks of Eventackle and no right or license is granted to use them. Any infringement of IP Rights by you contained in this Section may result in the immediate termination of your right to use the Services.</p>\r\n\r\n<p>6.Data Security</p>\r\n\r\n<p>Eventackle has put in place security measures to prevent your personal data from being accidentally lost, used, altered, disclosed, or accessed without authorisation. The security of your Data may be maintained through the use of data encryption, data security protocols, passwords and other methods which Eventackle may employ, or which Eventackle may suggest or require that you employ. We also allow access to your personal data only to those employees and partners who have a business need to know such data. They will only process your personal data on our instructions and they must keep it confidential. We have procedures in place to deal with any suspected personal data breach and will notify you and any applicable regulator of a breach if we are legally required to.</p>\r\n\r\n<p>7.Data storage and retention</p>\r\n\r\n<p>You agree that Eventackle has no responsibility or liability either for the deletion or failure to store any data transmitted by you or anyone else to the Services or the operation, or failure, or weakness, of any data encryption, data security protocols, passwords or other security methods employed by Eventackle. You shall have sole responsibility for the accuracy, quality, integrity, legality, reliability, appropriateness, and intellectual property ownership or right to use of all Customer Data, and Eventackle shall not be responsible or liable for the deletion, correction, destruction, damage, loss or failure to store any Customer Data. Upon termination, your right to access or use Customer Data immediately ceases, and Eventackle shall have no obligation to maintain or forward any Customer Data.</p>\r\n\r\n<p>We will only retain your personal data for as long as necessary to fulfil the purposes we collected it for, including for the purposes of satisfying any legal, accounting, or reporting requirements. When deciding what the correct time is to keep the data for we look at its amount, nature and sensitivity, potential risk of harm from unauthorised use or disclosure, the processing purposes, if these can be achieved by other means and legal requirements. For tax purposes the law requires us to keep basic information about our customers (including Contact, Identity, Financial and Transaction Data) for six years after they stop being customers. In some circumstances we may anonymise your personal data for research or statistical purposes in which case we may use this information indefinitely without&nbsp; notice to you.</p>\r\n\r\n<p>8. Fees and Refunds.&nbsp;</p>\r\n\r\n<p>Fees for Creating an account, listing an event and accessing the Services are currently free. However, we charge fees when you sell or buy paid tickets or registrations. These fees may vary based on individual agreements between Eventackle and certain Organizers. Organizers ultimately determine whether these fees will be passed along to Consumers and shown as \"Fees\" on the applicable event page or absorbed into the ticket or registration price and paid by the Organizer out of ticket and registration gross proceeds. The fees charged to Consumers may include certain other charges, including without limitation, facility fees, royalties, taxes, processing fees and fulfillment fees. Therefore, the fees paid by Consumers for an event are not necessarily the same as those charged by Eventackle to the applicable Organizer or the standard fees described on the Services to Organizers. In addition, certain fees are meant, on average, to defray certain costs incurred by Eventackle, but may in some cases include an element of profit and in some cases include an element of loss. Eventackle does not control (and thus cannot disclose) fees levied by your bank and/or credit card company, including fees for purchasing tickets and registrations in foreign currencies or from foreign persons. Be sure to check with your bank or credit card company prior to engaging in a transaction to understand all applicable fees, credit card surcharges and currency conversion rates.&nbsp;</p>\r\n\r\n<p>9.Third party services</p>\r\n\r\n<p>Any terms, conditions, warranties or representations associated with Event manager, Partner, Sponsor, Advertiser services(collectively, \"Third Parties\"), is solely between you and the applicable Third Party. Eventackle shall have no liability, obligation or responsibility for any purchase or promotion between you and any such Third Party.</p>\r\n\r\n<p>Eventackle or its&nbsp; Users may provide, links to other Internet websites or resources as it has no control over such websites and resources, you acknowledge and agree that Eventackle is not responsible for the availability of such websites or resources, and does not endorse and is not responsible or liable for any Content, advertising, products, services or other materials on or available from such websites or resources, or any damages or losses related thereto, even if such websites or resources are connected with Eventackle&nbsp; partners or third party service providers.&nbsp;</p>\r\n\r\n<p>Third party content, including content posted by our users, does not reflect our views or that of our partners and we assume no responsibility or liability for any of Your Content or any third party content.&nbsp;</p>\r\n\r\n<p>10. Termination of services</p>\r\n\r\n<p>You can agree that Eventackle may, with or without cause, immediately terminate your membership and access to the services without prior notice. You agree and acknowledge that Eventackle has no obligation to retain the your Data, and may delete such Data, after termination.</p>\r\n\r\n<p>Any breach of unauthorized use of the Eventackle Service will be deemed a material breach of this Agreement. Eventackle, in its sole discretion, may terminate your password, account or use of the Service if you breach or otherwise fail to comply with this Agreement.</p>\r\n\r\n<p>If you use the Services in violation of this agreement, we may, in our sole discretion, retain all data collected from your use of the Services. Further, you agree that we shall not be liable to you or any third party for the discontinuation or termination of your access to the Services.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>11.Disclaimer of Warranties</p>\r\n\r\n<p>NOTHING IN THIS AGREEMENT SHALL BE CONSTRUED AS A REPRESENTATION MADE, WARRANTY, OR GUARANTEE AS TO THE RELIABILITY, TIMELINESS, QUALITY, SUITABILITY, TRUTH, AVAILABILITY, ACCURACY OR COMPLETENESS OF THE SERVICE OR ANY CONTENT. EVENTACKLE&nbsp; AND ITS LICENSORS DO NOT REPRESENT OR WARRANT THAT (A) THE USE OF THE SERVICE WILL BE SECURE, TIMELY, UNINTERRUPTED OR ERROR-FREE OR OPERATE IN COMBINATION WITH ANY OTHER HARDWARE, SOFTWARE, SYSTEM OR DATA, (B) THE SERVICE WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS, (C) ANY STORED DATA WILL BE ACCURATE OR RELIABLE, (D) THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION, OR OTHER MATERIAL PURCHASED OR OBTAINED BY YOU THROUGH THE SERVICE WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS, (E) ERRORS OR DEFECTS WILL BE CORRECTED, OR (F) THE SERVICE OR THE SERVER(S) THAT MAKE THE SERVICE AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. THE SERVICE AND ALL CONTENT IS PROVIDED TO YOU STRICTLY ON AN \"AS IS\" BASIS. ALL CONDITIONS, REPRESENTATIONS AND WARRANTIES, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT OF THIRD PARTY RIGHTS, ARE HEREBY DISCLAIMED TO THE MAXIMUM EXTENT</p>\r\n\r\n<p>EVENTACKLE SERVICES MAY BE SUBJECT TO LIMITATIONS, DELAYS, AND OTHER PROBLEMS INHERENT IN THE USE OF THE INTERNET AND ELECTRONIC COMMUNICATIONS. EVENTACKLE IS NOT RESPONSIBLE FOR ANY DELAYS, DELIVERY FAILURES, OR OTHER DAMAGE RESULTING FROM SUCH PROBLEMS.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>12. Limitation of Liability</p>\r\n\r\n<p>IN NO CIRCUMSTANCES SHALL EITHER PARTY\'S AGGREGATE LIABILITY EXCEED THE AMOUNTS ACTUALLY PAID BY AND/OR DUE FROM YOU IN THE TWELVE (12) MONTH PERIOD IMMEDIATELY PRECEDING THE EVENT GIVING RISE TO SUCH CLAIM. IN NO EVENT SHALL EITHER PARTY AND/OR ITS LICENSORS BE LIABLE TO ANYONE FOR ANY INDIRECT, PUNITIVE, SPECIAL, EXEMPLARY, INCIDENTAL, CONSEQUENTIAL OR OTHER DAMAGES OF ANY TYPE OR KIND (INCLUDING LOSS OF DATA, REVENUE, PROFITS, USE OR OTHER ECONOMIC ADVANTAGE) ARISING OUT OF, OR IN ANY WAY CONNECTED WITH THIS SERVICE, INCLUDING BUT NOT LIMITED TO THE USE OR INABILITY TO USE THE SERVICE, OR FOR ANY CONTENT OBTAINED FROM OR THROUGH THE SERVICE, ANY INTERRUPTION, INACCURACY, ERROR OR OMISSION, REGARDLESS OF CAUSE IN THE CONTENT, EVEN IF THE PARTY FROM WHICH DAMAGES ARE BEING SOUGHT OR SUCH PARTY\'S LICENSORS HAVE BEEN PREVIOUSLY ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>13. Indemnity</p>\r\n\r\n<p>You agree to indemnify, defend, and hold harmless our parent, subsidiaries, affiliates, officers, directors, agents, and employees from any claim or demand, including reasonable attorneys\' fees, made by any third party due to or arising out of your breach of this Agreement or the documents it incorporates by reference, or your violation of any law or the rights of a third party.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>14. Legal Compliance</p>\r\n\r\n<p>Users shall comply with all applicable local and international laws, statutes, ordinances and regulations regarding your use of our service.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>15. Notices</p>\r\n\r\n<p>A party who intends to seek arbitration must first send to the other a written Notice of Dispute (\"Notice\"). The Notice to Eventackle must be addressed to the following address (\"Notice Address\") and must be sent by certified mail: Eventackle, Clarence Centre, 6 St. George Circus,London, United Kingdom. Notice shall be deemed given 24 hours after email is sent, unless the sending party is notified that the email address is invalid. Alternatively, we may give you notice by certified mail, postage prepaid and return receipt requested, to the address provided to Eventackle during the registration process. In such case, notice shall be deemed given 3 days after the date of mailing.</p>\r\n\r\n<p><br>\r\n&nbsp;</p>\r\n\r\n<p>16. Modifications</p>\r\n\r\n<p>Eventackle reserves the right to modify the terms and conditions of this Agreement or its policies relating to the Service at any time, effective upon posting of an updated version of this Agreement on the Service. You are responsible for regularly reviewing this Agreement. Continued use of the Service after any such changes shall constitute your consent to such changes.</p>\r\n\r\n<p><br>\r\n&nbsp;</p></body></html>\n', '2019-10-20 10:46:09', '2019-11-14 10:01:37'),
(3, 3, 'Privacy Policy', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\" \"http://www.w3.org/TR/REC-html40/loose.dtd\">\n<html><body><p>We respect your privacy and understand the need for appropriate protection and management of your personal data. We have created this privacy policy to give you an understanding how we collect , process and share your personal data as you use our Services, including Eventackle website or any mobile Apps operated, managed and marketed by our Clients. Any capitalized terms not defined in this Policy will have the meaning set forth in our <a href=\"https://attendify.com/terms_of_service/\">Terms of Service</a>. Eventackle is the data controller and we are responsible for your personal data (referred to as \"we\", \"us\" or \"our\" in this privacy notice).</p>\r\n\r\n<p>Our Policy Policy can be summarized as follows:</p>\r\n\r\n<ul><li>\r\n	<p>We may collect some information about you as mentioned in this policy, but you will be provided with some options about how much information you can share about yourself.</p>\r\n	</li>\r\n	<li>\r\n	<p>We do not deliberately receive, use or disclose any kind of personal information from children under 13 years of age.</p>\r\n	</li>\r\n	<li>\r\n	<p>We do not sell your personal information to third-party but may share the information with our clients in connection with providing the Services. However, we require all third parties to whom we transfer your data to respect the security of your personal data and to treat it in accordance with the law. We only allow such third parties to process your personal data for specified purposes and in accordance with our instructions.</p>\r\n	</li>\r\n	<li>\r\n	<p>We may collect, use and share aggregated anonymous information about our users.</p>\r\n	</li>\r\n	<li>\r\n	<p>In certain legal situations, we may be compelled to disclose your personal information.</p>\r\n	</li>\r\n</ul><p>&nbsp;</p>\r\n\r\n<p><strong>This Policy applies to all users of the Services.</strong></p>\r\n\r\n<ul><li>\r\n	<p>This policy applies to your use of Services, whether you are a Client or an End User of Services, including in connection with one or more events. Please read this policy that describes how we collect, use and share the information provided by you or collected on Eventackle site or application where this privacy policy is posted. By accessing and interacting with the Services, you consent to this Policy.</p>\r\n	</li>\r\n	<li>\r\n	<p>If you are an End User, you acknowledge that we will share your information, including your personally identifiable information with our Client(s) as specified in this Policy for the purpose of providing the Services, including in connection with one or more Event(s) that you may attend.</p>\r\n	</li>\r\n</ul><p>&nbsp;</p>\r\n\r\n<p><strong>We collect information from and about you</strong></p>\r\n\r\n<p><strong>Information you provide while using our Services.</strong> When you register for an Eventackle account, you are required to provide us with certain information about yourself i.e, name and email address. You may also voluntarily choose to provide us with other information while using our Services without limitation, by completing your profile and registering for and interacting with others in connection with one or more Events. You may be required to share your personal information including your address, phone number and company name (if you are a Client). If you correspond via email, we may retain the content of your email messages, email address and our responses. We may also retain any messages you send through this Service.</p>\r\n\r\n<p><strong>Information we collect from you passively. </strong>We automatically retain certain information about you while using our Services, including, without limitation, through cookies on the Eventackle website and in-app tracking when you use our Apps. We may collect your device information including your location, internet usage and will keep a track of the interaction with our Services. This may include information about the way you use Services, the parts of our Services you use, any event sessions you may attend, and third-party apps or websites you visit when you leave our Services.</p>\r\n\r\n<p><strong>Information we collect from third parties.</strong> In case you are an end-user, we may receive your information from the clients or if given permission to access the social media accounts, we may collect the required information from those platforms. We may also work with third parties who may collect information about you while using our app. For example, our vendors might use scripts or other tools to track your activities on our Services. They may do this to make sure our Services function properly.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>We use information about you as disclosed and described here</strong></p>\r\n\r\n<p>We may utilise the information to manage and provide essential functionality of services. For instance, we may use your personal information that is your contact information with reference to any events or event sessions you desire to be a part of.</p>\r\n\r\n<p>We may use the information to enhance the quality of our products and services for better client experience. We may use your information to make our website, apps and products work better.</p>\r\n\r\n<p>We may use the information to reply to your requests or queries. For example, we might use your information, such as email address to respond to your customer questions or feedback.</p>\r\n\r\n<p>We may utilise the information for security purposes. We may use the information to protect our company, client, website or/and apps.</p>\r\n\r\n<p>We may use the information for promotional purposes. For example, we might provide you with information about the latest features, updates, new products or special offers from time to time. If you prefer not to receive promotional messages from us, please read the <strong>choices section</strong> below.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>We may share information with third parties</strong></p>\r\n\r\n<p>We may share your information with the third parties to provide better functionalities of our services. These are the third parties who perform services on our behalf, for instance, if an end-user chooses to cross-publish a post, we may share the information with the external social media services. We may also share your information with our vendors who provide us with services like payment processing, hosting, customer support, email marketing and other services. If you are an End User, we will also share your information (including your personal and contact information and information about your engagement on the Services such as any Event sessions you attend) with the Client associated with your account in order to provide the functionality of the Services.The client may contact you for their own marketing or promotional purposes in relation to the events you have registered for (including through push notifications on your device). In many cases, we may also track the attendee&rsquo;s behaviour information including how you interact with the Services in relation to events organised by our clients. While this information may not personally identify you on its own, this information may be combined with other personally identifiable information, either by us or the Client.</p>\r\n\r\n<p>We may share information to comply with the law. In case of any legal situations, we will also share your information in order to comply with the law or to protect ourselves from any fraudulence. For example, responding to a court order, subpoena, or a request from a government agency or investigatory body.</p>\r\n\r\n<p>We may otherwise share information with your permission.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>You have certain choices about sharing and marketing practices</strong></p>\r\n\r\n<p>You can opt-out of receiving our promotional emails. In order to stop receiving our promotional emails, follow the instructions in any promotional message you will receive from us. Even if you opt-out of getting marketing messages, we will still send you transactional messages. These include responses to your queries.</p>\r\n\r\n<p><strong>You can manage cookies and other browser-based tracking tools.</strong> For instance, your browser may provide you with an option to control cookies or other browser-based tracking tools. Please note that if you block cookies, certain features on the Eventackle website might not function well.</p>\r\n\r\n<p><strong>Do Not Track Policy.</strong> Some browsers have &ldquo;do not track&rdquo; characteristics that allow you to tell a website not to keep a track of you. However,these features are not completely uniform and we do not currently respond to those signals.</p>\r\n\r\n<p><br><strong>You can manage tools on your mobile devices.</strong> For example, you can turn off the GPS locator or push notifications on your phone. Please note that turning off certain tools on your mobile device may affect the functionality of certain features of the Services.</p>\r\n\r\n<p><strong>Our Services are not intended for children under 13 years of age </strong><br>\r\nOur Services are intended for use by adults only. We do not deliberately collect personally identifiable information from children under 13 of age without seeking permission from their parent or guardian. If you are a parent or legal guardian and think your child is under 13 has given us information, you can email us at info@eventackle.com.You can also write to us at the address listed at the end of this Policy.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>How we store your information</strong></p>\r\n\r\n<p>We keep a track of your personal information as long as we provide the functionality of the Services, as otherwise required by law. For accessing or modification of your personal information, you may contact us at info@eventackle.com, or you may write to us at the address listed at the end of this Policy. However, we may also keep your information as otherwise required by law. For the continuation of effective services, we may save your non-personally identifiable data permanently and may anonymize and save that anonymized information permanently. Additionally, we use third-party services and do not control their practices related to storage and retention of your information.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>How we protect your information</strong></p>\r\n\r\n<p>For security purposes, we make reasonable efforts to safeguard your information and strive to prevent the loss, misuse, and modification of the information that we obtain from you. However, the Internet and mobile apps are not 100% secure. We cannot guarantee that your use of our Services will be entirely safe. In case of any loss, misuse or alteration of the data provided by users, we are not responsible to our users or to any third party except to the extent required by the Privacy Shield Principles or applicable law. We encourage you to use caution when using apps and the Internet. This includes not sharing your passwords.</p>\r\n\r\n<p><strong>For third party services where we don&rsquo;t have control over data being monitored or utilised</strong><br>\r\nOur Services may have links to sites and third-party services which include plugins which we do not control. This Policy does not apply to the privacy policies of those websites or third parties. When you interact with those websites or third parties, they may collect information from you. Read the privacy policy of other websites carefully. We are not responsible for, and disclaim any and all liability for, these third-party practices.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Contact us if you have questions</strong></p>\r\n\r\n<p><strong>In case of any queries or want to update your information, reach us at info@eventackle.com.</strong></p>\r\n\r\n<ul><li>\r\n	<p><strong>Email: info@eventackle.com.</strong></p>\r\n	</li>\r\n	<li>\r\n	<p><strong>Phone: +44 (0)208 242 6566</strong></p>\r\n	</li>\r\n</ul><p><strong>We may update this Policy</strong><br>\r\nThis Policy may be updated from time to time, but the changes will not apply retroactively. If you continue using our Services after updates become effective, you will be bound by the updated Policy.<br>\r\nHowever, you will be notified of any notable changes to our Policy as required by law. We will also post an updated copy on our website (<a href=\"http://www.eventackle.com/\">www.eventackle.com</a>) and app. Please check our website and app regularly for any updates.</p></body></html>\n', '2019-10-20 10:46:09', '2019-11-14 09:30:58');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profileImage` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_email_verify` tinyint(1) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `notify_status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `profileImage`, `position`, `company_name`, `website`, `location`, `latitude`, `longitude`, `device_type`, `device_token`, `phone`, `is_email_verify`, `status`, `notify_status`, `created_at`, `updated_at`) VALUES
(9, 'Tarun', 'tarun.kumar@mobulous.com', NULL, '$2y$10$nyDjSfUebMRG5j1l/qdtqOGQl64oUR5U/7oLTeFx1gsqhic2.Ivby', NULL, '', 'iOS developer', 'mobulous', 'https://www.mobulous.com', 'Unnamed Road, Sector 32, Noida, Uttar Pradesh 201303, India', '28.629214', '77.391662', 'iOS', '977363028FFDB72D56DF7515286EB99739CD49FDE56C941A1A4E2835F47226AC', '8800190916', 1, 1, 0, '2019-10-01 09:49:10', '2019-11-20 11:03:30'),
(12, 'Gaurav Kumar', 'grv@yopmail.com', NULL, '$2y$10$qX6H.US1rPfftj440.h35e7ShlQtpR5ocioKXPSataTjM7FpClgtK', NULL, '', 'Jr. Project Manager', 'Mobulous technologies', 'www.sangeetabisht.com', 'N2, 60117 Coyolles, France', '28.629214', '77.391662', 'iOS', '58C548DEB515829E6EA9F24B36B3EC6277C34E29377641EDAB4656510D4C84CB', '98036523888', 1, 1, 1, '2019-10-01 11:21:01', '2019-11-25 05:20:25'),
(15, 'ravi', 'ravi.kant@mobulous.com', NULL, '$2y$10$w4TQXIbi1dI0grvXR/7p2u.3wrGcP3JHnm1JSCsWCwwt4VXYo06ya', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'android', 'jksdhkjshkfjhskjfdjksfkjdshfkj', NULL, 1, 1, 1, '2019-10-01 12:24:52', '2019-10-01 13:03:18'),
(16, 'Nandan jha', 'nandu@yopmail.com', NULL, '$2y$10$I2caPoKB3bIQfBs5I/ClBekXL0BwSwFMnKzNmM/XAImKCQNh2Elga', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-10-01 13:19:57', '2019-10-01 13:19:57'),
(17, 'abhi', 'abhi1@yopmail.com', NULL, '$2y$10$JcJyQAs6CUA19ogVMcvhkur./.8qkqHshT1N82kJqdALXf4qx0kR6', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-10-01 13:57:36', '2019-10-01 13:58:43'),
(18, 'Raj Kumar', 'raj@yopmail.com', NULL, '$2y$10$PL2uXNiaky4wKpa.KNvvzus1AA8.1SFnNrYdKwNsyG0P/7Y/..d7q', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'android', 'jksdhkjshkfjhskjfdjksfkjdshfkj', NULL, 1, 1, 1, '2019-10-01 14:07:24', '2019-10-01 14:08:26'),
(19, 'Mir Suhaib', 'meer.nayn@gmail.com', NULL, '$2y$10$MUudB6tfRTU0nIkDV1CF6.WjiyZ7fId/AKf3kLXjASv7oW2K5kPDu', NULL, '', 'Admin Etapp', 'Valiant Business Media', 'www.valiantbmedia.com', '125-A, Ajmal Bagh, Noor Nagar Extension, Jamia Nagar, Okhla, New Delhi, Delhi 110025, India', '28.084', '77.987', 'android', 'eyAuEHZSC-o:APA91bExzhwtzjJ71zVc327m8zJFPSFl6QKewCpJ1rb5WgmiD4VyBB5Kt_djszM-WFeOEA1RqCYtMlI9IsxzXMg7rL4DpE0b9ikbxfmgBKkx_TYA97KCOZlZVKJFZLsceFMv-tS-WCxQ', '+918882567899', 1, 1, 1, '2019-10-01 16:00:24', '2019-11-25 10:57:40'),
(20, 'jameel', 'jameelfarooqkhan@gmail.com', NULL, '$2y$10$j.t.opppNaTYHg6ccpesXOMJHJhS6NGK0KNOVqakBScXpU72Q8stO', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'android', 'dF8dSeHaddM:APA91bFmk0fqeCaQRv-JQCltcL0L5GtzBJ_EywxcLji_h6r9Klf3LlgRfRt9wSIsC5tJfLs_dpoppnJfIDlg5v6ULIIK-zdwRIup7EwCpyKMqQe4bjPJ8Vt36qw_aQ0uqH-Fsxfgsu-f', NULL, 1, 1, 1, '2019-10-07 06:34:28', '2019-11-19 10:38:39'),
(21, 'Nadim Ahmad', 'nadim.ahmad@mobulous.com', NULL, '$2y$10$yEYsUIqjZ0MAGr4T3uiNv.CEufoG.rbO2.FOdBHc7sP8fpRWnLGbO', NULL, 'userimage/1572859785_1572859781018.jpg', 'Android Developer', 'Mobulous tech', 'www.nadimahmad.com', 'H147, H Block, Sector 63, Noida, Uttar Pradesh 201301, India', '28.084', '77.987', 'android', 'fiCkguytThQ:APA91bGLI40RjWdjO-SZAh62Jfbe35G1xYxT-kIGD2XSYH_7g1AJXn0l1ETNCPQsJ88cmlfN2Qc3gWVcU1nY7TsA3VXF9Z5EhS6MDDxWNMy0YfyDa6_HKTlRtj0C30RJ8AFX3yfShOUj', '97273736388', 1, 1, 1, '2019-10-09 09:53:19', '2019-11-21 07:41:32'),
(22, 'Prince', 'prince.sinha@mobulous.com', NULL, '$2y$10$xjAJ5LKg6uSsqQf/P340B.JvZcD19KKgTCb9QcGgGY.KbELXgnuJK', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', 'b1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-10-16 06:53:49', '2019-10-16 06:53:49'),
(23, 'sonika sharma', 'sonika.sharma@mobulous.com', NULL, '$2y$10$Eknor72WGU5kbAcPy0rGRevQG0t05y7hZo66T681GDn8ye3ikzHka', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', 'b1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-10-23 09:52:21', '2019-10-23 09:52:21'),
(24, 'prince', 'priyanka.mathur@mobulous.com', NULL, '$2y$10$fgSeES8mO674K5CQ4Xfztu1na2SVVpqMGi2y/J6LDRAa3ikWI4nUe', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', 'b1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-10-23 10:10:16', '2019-10-23 10:10:16'),
(25, 'Ankit Mittal', 'ankit.mittal@mobulous.com', NULL, '$2y$10$nyDjSfUebMRG5j1l/qdtqOGQl64oUR5U/7oLTeFx1gsqhic2.Ivby', NULL, 'userimage/1573721310_ProfileImage.jpeg', 'developer', 'mobulous', 'mobulous.com', '113, Pandit Pant Marg Area, Sansad Marg Area, New Delhi, Delhi 110001, India', '28.629214', '77.391662', 'iOS', '', '9852632532', 1, 1, 1, '2019-10-24 18:43:29', '2019-11-15 07:16:49'),
(26, 'Raj Gaurav', 'araj@yopmail.com', NULL, '$2y$10$rPKe5mVUf58wBqBm6t2dm.Wfv.vrNtrNOPppakXBf/N9CVpYOivCm', NULL, 'userimage/1571993219_ProfileImage.jpeg', '', '', '', '', '28.629214', '77.391662', 'iOS', '56DA6D1DE13AE3C23F792346A842688D99C5B986C6678BE7BFE7F5A0E2410CEC', '', 1, 1, 1, '2019-10-25 06:45:42', '2019-10-25 08:46:59'),
(27, 'Anas', 'anas@yopmail.com', NULL, '$2y$10$HpB8FkTvnHjm.Fl3veLOv.i3BTXQEflqb37WZjgQNGhQV2YqeN3we', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'android', 'cI042HVCpv0:APA91bGP3eXYZ1Vs4QqCAYE9Z1i-bhQdgKqu-SgTp_Q0FykhEn54qXkpOOGh4lerjqJgpV7RVKqDIDtCcGpOJZJ-rwoqT9wzx0Iapm7njYB0RVT5C8eBSYt70Nv9le279muzVNKcim1U', NULL, 1, 1, 1, '2019-10-30 12:26:57', '2019-11-06 08:08:43'),
(28, 'jahan', 'jahanzaibsami1@gmail.com', NULL, '$2y$10$dJFT5WI/KxQr3n9gsUYZHOy/ilKQJ3o76OMVHWm9l.GTPxU/x0om2', NULL, '', '', '', '', '', '28.084', '77.987', 'android', 'cDfKfw3p2lw:APA91bEXjl8q9h8wprN8-khrky98SN2nlgkl-WEgIY5Rh7aXFaq4Ck2cOyEQH3CxByGRABfJt_zmdRFZhioDMWd_Sz9-GKKqYVzffwp1PwEaxMbwJvQPYXtbmkoL4azXTd5xRWSVSuaN', '', 1, 1, 1, '2019-10-31 05:16:46', '2019-11-13 05:51:32'),
(29, 'Ankit Mittal', 'ankit05.mittal@gmail.com', NULL, '$2y$10$S/y66.yg8AOlA8wgnTCZM.jvbRhzR2BI9kwBBJaen9Pg.ShHHs4di', NULL, NULL, NULL, NULL, NULL, NULL, '121.100.11.22', '43.100.11.90', 'android', 'TEVDFVSEFG2545SDFSDF32SDFSDSSD', NULL, 1, 1, 1, '2019-10-31 09:10:56', '2019-10-31 09:11:14'),
(30, 'Anup', 'anup@yopmail.com', NULL, '$2y$10$Vi8moELxkI1kfLk4Iz6dVuS7gi1lJHxMIrNXjrPK5Y9Df0CzWe3GO', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'android', 'fDevWFtNiAo:APA91bGlp8e1sy-J1eOZOqdM5jGzK_Vd9-nPPWrzgif2sCY24MCd2RxnBh3U_HbnYWg8ZWbrVG-rVtGYqSearSySApgOsRw4pI5W5ypCdRG5TQmEJ1ansng2S7lm0Dptvt4lLU4akYHU', NULL, 1, 1, 1, '2019-10-31 11:05:21', '2019-10-31 11:07:29'),
(31, 'Kodak mehta', 'kopal@yopmail.com', NULL, '$2y$10$gLpRDlNkSFxbhFqiEOeybOS7zef1jrIpTAYkQPkX4HYSgu4fWdHF6', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'android', 'fDevWFtNiAo:APA91bGlp8e1sy-J1eOZOqdM5jGzK_Vd9-nPPWrzgif2sCY24MCd2RxnBh3U_HbnYWg8ZWbrVG-rVtGYqSearSySApgOsRw4pI5W5ypCdRG5TQmEJ1ansng2S7lm0Dptvt4lLU4akYHU', NULL, 1, 1, 1, '2019-10-31 11:16:49', '2019-10-31 12:01:31'),
(32, 'Ambalika', 'amba@yopmail.com', NULL, '$2y$10$Lt1LV04sFzzSn3ilSlexMuXu2k8seAKuacun0mYd0REXtlsn2n31y', NULL, '', '', '', '', 'D-4, D Block, Sector 59, Noida, Uttar Pradesh 201307, India', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '9876765432', 1, 1, 1, '2019-10-31 11:18:44', '2019-10-31 11:24:19'),
(33, 'harshit', 'har@yopmail.com', NULL, '$2y$10$tjRRnvL6GXB3Iq.SXDT0Eeb3sY61JYCn6pDN5YzN6wfJRbCohKhDO', NULL, 'userimage/1572864977_1572864945318.jpg', 'android. devs.', 'biobio', 'n.com', 'GH/9, Maharaja Agarsain Marg, Sector 4, Sector 6, Vasundhara, Ghaziabad, Uttar Pradesh 201012, India', '28.084', '77.987', 'android', '', '7098989099', 1, 1, 1, '2019-11-04 10:38:18', '2019-11-12 13:13:31'),
(35, 'Nadeem', 'nadeembhat24@gmail.com', NULL, '$2y$10$Oe4I4KknjTXK8h538dDZV..cYFsinDma0Qmbmn3UeWI7Jck50SowO', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'android', 'dcJJtV0v3EE:APA91bGiddGvW9ACcmKymCbtWxwUh_1DGq_GRL3kanKaVRo7XKEm6FN7plS2HcdfSQ5N2MQvMsuTLLSPqt7HvrCFjixz3Rde7E5aRe5bvAk1okvbSiedr6Kug_UIoo1z1BRb_SPzA7QU', NULL, 1, 1, 0, '2019-11-05 06:09:43', '2019-11-05 15:21:22'),
(36, 'Prashant Singh', 'prashant@yopmail.com', NULL, '$2y$10$JHmuoud3wjSt2Q0uwW5ieunVJ8/MBCRpmJfE0LXcBKxkQaNUF.Vfi', NULL, 'userimage/1573205967_1573205890657.jpg', 'Quality Analyst', 'Tobacco causes Cancer', 'www.storywriter.com', 'Eastern Peripheral Expy, Uttar Pradesh 250101, India', '28.629214', '77.391662', 'iOS', '9D2E6F5DCC7E2630724B7B28C2FB09E20E2DC331FD1EAB2C763A6750A1775AC1', '8087973666', 1, 1, 1, '2019-11-08 09:36:29', '2019-11-11 16:33:31'),
(37, 'Nikhil singh', 'nikhil@yopmail.com', NULL, '$2y$10$BZo5PHK99Ka5ewVlJgbAzOD7icX8WaX9iZi.BUnJpHbzhVq/Pkx6W', NULL, '', '', '', '', '', '28.629214', '77.391662', 'iOS', '7416EFB6BAFD30F17FF6EC1044E1FCFCE2463F79FC601CDBA68D6B3251B41E1D', '', 1, 1, 1, '2019-11-08 11:03:44', '2019-11-13 08:02:03'),
(38, 'Devendra User', 'dev@yopmail.com', NULL, '$2y$10$7Qel1gfMmV.bhjaUal6reub6CkoAFkOC1p85f4aE9wP7WCLeeJwOy', NULL, 'userimage/1573211255_1573211252959.jpg', '', '', '', '', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '', 1, 1, 1, '2019-11-08 11:05:38', '2019-11-08 11:07:35'),
(39, 'Ramshankar Kumar', 'ram@yopmail.com', NULL, '$2y$10$RMI403Nt8NhO8bs/mYuYFuTmPhsCeG45Gs59p/b0bqPHUXiermof.', NULL, 'userimage/1573211675_1573211671974.jpg', '', '', '', '', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '', 1, 1, 1, '2019-11-08 11:12:11', '2019-11-08 11:14:35'),
(40, 'Deepak Kumar', 'deepak@yopmail.com', NULL, '$2y$10$DXxsc8jCeaMkpppr7zcj1uWdac.2ig3/fMQML61U8NMVYn/zfvDRa', NULL, 'userimage/1573212576_1573212572941.jpg', '', '', '', '', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '', 1, 1, 1, '2019-11-08 11:16:49', '2019-11-08 11:29:36'),
(41, 'Abhishek Kumar', 'abhi@yopmail.com', NULL, '$2y$10$SjzPZnEPx9y946v3g8yT9OenfGd0wep4I/LgKbrkAzew75Z5Z4wTe', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'android', '', NULL, 1, 1, 1, '2019-11-08 11:31:23', '2019-11-21 07:47:58'),
(42, 'Bindusar', 'bin@yopmail.com', NULL, '$2y$10$7xC4VOdEUsHT5azZtyHE6OtO5pq7vXPFzmXwC9DJdUwEs/MUEnQUW', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'android', 'fNAozPqHagc:APA91bGv5zxiPkA63P2xM85O4I_g4D700g3RoDQi2Lo-6x6btCmzls8dRd875V3PEYDzPCacbVas34n9GLHXanCOMUtVZdXByLamUcEXj0K7ux3SBzwsuz6DQOmu3Ujj2OiU8HKG7Dsn', NULL, 1, 1, 1, '2019-11-08 11:40:58', '2019-11-08 11:41:29'),
(43, 'Degree of Arts', 'art@yopmail.com', NULL, '$2y$10$iwfucf4ZY73cUNVK.6Fznuu0vLHwB5KfQDA5sX/THF1kCqSObP4fa', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', '28C760C8F857A3F3F2E90146A539DAE77D3EEB2F5F015F136CB1C813C692DAC8', NULL, 1, 1, 1, '2019-11-08 12:50:19', '2019-11-08 12:50:19'),
(44, 'Abu Ababwa', 'bradbach76@icloud.com', NULL, '$2y$10$1xbYpWBKx8r38F9vnfW/lOszZZBNkEGPftBR9loYE3jC2iYSp6HLO', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', '', NULL, 1, 1, 1, '2019-11-14 23:27:18', '2019-11-14 23:27:40'),
(45, 'Mariam', 'mariam.kamran@valiantbmedia.com', NULL, '$2y$10$9KqDxgOBQjJv0rC1m87DAOREG2b71C60Fy4ipZXC0Fwq9E11BKnGa', NULL, NULL, 'Accounts Manager', 'Valiant Business Media', 'www.valiantbmedia.com', '703, Sector MU, Greater Noida, Uttar Pradesh 201310, India', '28.629214', '77.391662', 'iOS', 'BB6B24B409C328929DBD3729986ADD1111059A90702FFA0FE258C285EC797DB5', '9971060898', 1, 1, 1, '2019-11-18 04:22:21', '2019-11-18 04:24:52'),
(46, 'NewUser', 'abc@gmail.com', NULL, '$2y$10$/o48qhB0TykhehZFNDyDauDPbcRTZ3PvKXwIh0mdiOMiz8iGjPoby', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-18 11:37:04', '2019-11-21 05:52:03'),
(47, 'Willem Bloem', 'bloemw@hotmail.com', NULL, '$2y$10$wkoCSkQQkH25qxoGJkfXTe4fiW7en7yuKbB/3tjGEqxOakFT1DC0y', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', '8E9F67F5E885EEAC2B3AEB44B794DCBDC40C965D719E6AA458F26721D39AE175', NULL, 1, 1, 1, '2019-11-18 14:55:13', '2019-11-18 14:55:13'),
(48, 'Suhail Bhat', 'suhail.bhat@valiantbmedia.com', NULL, '$2y$10$nDvVm9EqpS3GFOirrVLXpeY6PGgzCTXEfjzJuKuTSTPlazqunIXnG', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-18 16:08:18', '2019-11-18 16:08:18'),
(49, 'John', 'valiant.b.m48@gmail.com', NULL, '$2y$10$P56.6xysm4WCdJHsoJtYA.1lQzgrHB5pILTpqhhXGsNG2cAOr9sU6', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', 'BD0BC6594469003D919071CE8993189BD25F059665FD727E97510C1EC708B50E', NULL, 1, 1, 1, '2019-11-18 17:18:14', '2019-11-18 17:18:14'),
(50, 'name', 'fat@gmail.com', NULL, '$2y$10$L1VVAtUzjh9t/DgulOuABuYYgLHuTbskmXFBhAUhASFlXZzxEegPS', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-19 10:15:49', '2019-11-19 10:15:49'),
(51, 'Hilton Wong', 'hwong@atni.com', NULL, '$2y$10$j9ippBNLn1xqK0jWebpCZucvrUKEzxv4aCL0Kt4OFKf/QDUVAbGSa', NULL, NULL, 'Senior Manager', 'GTT', 'gtt.co.gy', '25 Saffon St, Georgetown, Guyana', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '5926244800', 1, 1, 1, '2019-11-19 12:39:06', '2019-11-20 03:01:08'),
(52, 'Cletus Kennedy Bertin', 'cbertin@carilec.org', NULL, '$2y$10$WVPQlA1fB7Nw/UcOYg6lOejHuNn7yum04L8qv8GexOHDYxF7Nyq3q', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-19 12:47:39', '2019-11-19 12:47:39'),
(53, 'Dawn Hackett', 'dawn.hackett@woodplc.com', NULL, '$2y$10$o4NaknthDQJo.ePfn3W3ZeDcDKbLH/DDj40alV2s2rtFqIZKbWoTu', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-19 12:51:21', '2019-11-19 12:51:21'),
(54, 'Priya maharaj glasgow', 'prsh.maharaj@gmail.com', NULL, '$2y$10$mEDhXcKSlFdDyAO1Aa9Z2O/X7PoxBPisM/JLPBnciIaA0.QoOv6v6', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-19 12:57:43', '2019-11-19 12:57:43'),
(55, 'Noel Arenas', 'narenas@classiccontrols.com', NULL, '$2y$10$J1PHtb51pUWhox2zkMChA.KiMIjVPo.2NI0yeSPCCq9MtRIdSFTf6', NULL, NULL, 'Sales Engineer', 'Classic Controls Inc.', 'www.classiccontrols.com', '1 Water St, Georgetown, Guyana', '28.629214', '77.391662', 'iOS', '805DA8FE498119D8C3CDD4C4D0C3EC5EAED67F40090A08BF79C71018D77E2AFE', '+17875316635', 1, 1, 1, '2019-11-19 13:22:56', '2019-11-19 21:26:12'),
(56, 'ghjklop', 'sdsd@ddsd.dds', NULL, '$2y$10$RGx3fryfkoKj/4IhLeC.tu8/okl0tq96EVVFcLxFXkPNsgsfT2ZsK', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-19 13:26:15', '2019-11-19 13:26:15'),
(57, 'ghjklop', 'sdsdyuo@ghg.hgh', NULL, '$2y$10$X.UvEPHCs6DEHPPYCyp9n.28ZyxVWg6rZdf/6T9igmjQZLVe3rJuC', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-19 13:32:01', '2019-11-19 13:32:01'),
(58, 'John Quelch', 'johnquelch@gmail.com', NULL, '$2y$10$p3JXAIv.POhaQN8y3lTGi.3m5xta5h/YtR84WviDo/M1bJqRl6opG', NULL, NULL, 'Manager, Strategic Planning and Administration', 'Century Tamara Energy Services', 'www.ctes-inc.com', '86 Fifth St, Guyana', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '6082330', 1, 1, 1, '2019-11-19 13:45:13', '2019-11-20 06:50:26'),
(59, 'Shivon Idal', 'sidal@atni.com', NULL, '$2y$10$rLxrhN1.YdWXfu1SJW58ZuGAnI/2TqmFuDTpo86ZG3YqbtpTKUkI6', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'android', 'fhDdR53sMnk:APA91bGn-snbMq_8cA2jTu8HWTqEffNJi3HMTMJ_GypJhrceXL_xQT9bylsY72b5M_AlAOt9OjWeJDKU2zAoXVMksbFYHzAbs7hSYbYe9B5r_rTpZINo8QlBqeXFwjJbRCdCWx6ghn6u', NULL, 1, 1, 1, '2019-11-19 13:53:23', '2019-11-19 13:55:22'),
(60, 'haseeb rafeek', 'haseeb.rafeek@dofsubsea.com', NULL, '$2y$10$BhznrloZubs92/Myc1g3huCDMStQ.4qXYVVqQbqtRQdoIuEExdUDS', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-19 13:53:49', '2019-11-19 13:53:49'),
(61, 'Rajesh Mohan', 'rmohan@classiccontrols.com', NULL, '$2y$10$M16cXhAcuer8kiAF2aFCKeP98QQl0kVb8WtJwbi2XrBmEm7xRDJrG', NULL, NULL, 'Sales Representatives', 'Classic Controls Inc', 'www.classiccontrols.com', '1 Water St, Georgetown, Guyana', '28.629214', '77.391662', 'iOS', '9E83B16C1CEE82EF6C190EC2E59F7C965BB12224FC358767C6E9635A44046824', '8687555755', 1, 1, 1, '2019-11-19 14:27:14', '2019-11-20 11:32:09'),
(62, 'Esther', 'esther_ug@yahoo.ca', NULL, '$2y$10$3L/Bk0rGHL6dylRkk5AeKOGOaoxC3bmT/tj3D8un0Pjau1mMNdstS', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', '37E90AC58276025893CBB9CA54954FE9CAB3AA3670C9F4CECB1246722962BCA9', NULL, 1, 1, 1, '2019-11-19 14:44:52', '2019-11-19 14:44:52'),
(63, 'Shawn Warren', 'swarren@classiccontrols.com', NULL, '$2y$10$FqhvZELy9ACaYQVqkDEaoevv1UW2u55bDr6vu9gpFn/ti8zJMTToW', NULL, NULL, 'President', 'Classic Controls, Inc.', 'www.classiccontrols.com', '5085 S Lakeland Dr, Lakeland, FL 33813, USA', '28.629214', '77.391662', 'iOS', '35F590D9C296A2D0F779D3DE51AF033F1C027C90B5E9B07F9F8FED1BA7DAA43E', '+18636401418', 1, 1, 1, '2019-11-19 14:48:21', '2019-11-19 22:09:34'),
(64, 'Celia Garcia-James', 'celia.james@halliburton.com', NULL, '$2y$10$yA7sizyNTGWFCczf0zaTn.iQIUVoJbkgbFWoH6xuCfF3ttP0IP0f.', NULL, NULL, 'Technical Solutions Consultant', 'Halliburton', 'www.halliburton.com', 'Unnamed Road, Trinidad and Tobago', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '8683897753', 1, 1, 1, '2019-11-19 14:51:55', '2019-11-20 13:46:09'),
(65, 'mark bhopaul', 'mbhopaul@demship.com', NULL, '$2y$10$08fjM3VO6J8PSPSA0mO8ee2MtujBCZTyxy1TLOCXg8zo.t1XAcncK', NULL, 'userimage/1574177174_1574176717539.jpg', 'Commercial Representative', 'Demerara Shipping Co Ltd', 'www.demship.com', 'Guyana', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '608 2354', 1, 1, 1, '2019-11-19 14:58:25', '2019-11-19 15:26:14'),
(66, 'Luis Wilhelm', 'luis.wilhelm@govconsys.com', NULL, '$2y$10$yWTkxmt9rzpjdFELh38LXuBMH0aAn3V5f5tIHL7FUAV7mPosI0DhS', NULL, NULL, 'Account Manager', 'Governor Control Systems', 'www.govconsys.com', 'Fort Lauderdale, FL, USA', '28.629214', '77.391662', 'iOS', '389D730479470BA797241F6CD5A451CF1B45F62A03536A5CFC139508B9B6E1E2', '+19544627404', 1, 1, 1, '2019-11-19 15:35:06', '2019-11-19 15:41:39'),
(67, 'Christina Bahadoor-Hosein', 'cbhosein@eaastaff.com', NULL, '$2y$10$03rhjT2C8qyDrUzJolag7.Ydby7CmTB3WFtKeiWxxZ/VOy6GikB.i', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-19 15:37:28', '2019-11-19 15:37:28'),
(68, 'Miguel Fernandes', 'mcolinfernandes@solenis.com', NULL, '$2y$10$rfe6TFMzyFrmPgI5Szx7HOXodbMcJ1Xh3funpXl2helgKeZP8zrnK', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-19 16:00:57', '2019-11-19 16:00:57'),
(69, 'Danielle Dronet', 'danielle@machinetech.biz', NULL, '$2y$10$4as5mk3ko16xBl6rv9RQ3OxY6I0grLUFAVnXjvFOgwoarhGXlv9pG', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', '34AFB31A75DE7194671F9D53D6038AD368AC426397D1211D6FE1088D442B9820', NULL, 1, 1, 1, '2019-11-19 17:27:19', '2019-11-19 17:27:19'),
(70, 'Shinelle Padmore', 'shinellepadmore@gmail.com', NULL, '$2y$10$eV1iyt7EUlpysdSIK3DYTuL1N6LDhC2P.HJX4DfWaxwJbTDdzIBXi', NULL, 'userimage/1574186936_1574186923867.jpg', 'General Manager', 'Caribbean Resourcing Solutions', 'www.crsrecruitment.co.tt', '16 Scott-Bushe St, Port of Spain, Trinidad and Tobago', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '18687533272', 1, 1, 1, '2019-11-19 18:03:14', '2019-11-19 18:08:56'),
(71, 'Jason Kelleher', 'kelleher.jason@gmail.com', NULL, '$2y$10$ewrCKGY9wm6cRUgTxrVrwOsIxbFtkTh6R2YPvSdfU94dCRI7cWrsC', NULL, 'userimage/1574193597_1574193593229.jpg', 'Director, Program Development', 'FHI 360', 'www.fhi360.org', '1825 Connecticut Ave NW, Washington, DC 20009, USA', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '+1-202-884-8459', 1, 1, 1, '2019-11-19 19:44:46', '2019-11-19 19:59:57'),
(72, 'Ricardo Michel', 'RMChl99@gmail.com', NULL, '$2y$10$RjhR9NmUJZKU66y.RMgj/.8xD8xfiE5BOFgjcBwe7BapRgKKNR3C.', NULL, NULL, 'Managing Director', 'FHI Partners', 'FHI-Partners.org', '1825 Connecticut Ave NW, Washington, DC 20009, USA', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '(202) 884-8166', 1, 1, 1, '2019-11-19 19:47:09', '2019-11-19 19:58:04'),
(73, 'Patries Ramkaran', 'patries@ramkarancontracting.com', NULL, '$2y$10$eEM6Ui0.vFkeosWG26IvWeMz.mw2m3Ct8eK/uMNCo2tmZIFjLzpOe', NULL, NULL, 'Managing Director', 'Ramkaran Contracting Services Ltd', 'www.ramkarancontracting.com', 'Chandranagar St, Georgetown, Guyana', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '5926773294', 1, 1, 1, '2019-11-19 21:11:02', '2019-11-20 10:42:25'),
(74, 'Jamie Stewart', 'jstewart@smitlamnalco.com', NULL, '$2y$10$amiRIoAl2rkYcL1vDk6/6e5tMFNpOyjVoBe0R3NnMHEN0uuYdLiE6', NULL, NULL, 'Group Tender Manager', 'Smit Lamnalco', 'www.smitlamnalco.com', 'Waalhaven Oostzijde 85, 3087 BM Rotterdam, Netherlands', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '+31646936135', 1, 1, 1, '2019-11-19 21:27:35', '2019-11-19 21:30:27'),
(75, 'isaac Gbenjo', 'tigbenjo@yahoo.com', NULL, '$2y$10$awdmjQlBe7X4Vz2h3av4MOl.G5m/nnxogqbCIhRmvRY9FjdfkF1CC', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-19 22:09:09', '2019-11-19 22:09:09'),
(76, 'Rory Harding', 'rory@angloguyana.com', NULL, '$2y$10$h02ODRwkHFbtOPhXNHwyI.qXX0KYRElLbTZJkfrgzRvcKaWAZDMDC', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-20 00:20:40', '2019-11-20 00:20:40'),
(77, 'Richard Seeg', 'richard.seeg@zeamarine.com', NULL, '$2y$10$BR35yDdO8gC100DRr2DkWe2VabC2TcLlGwQoFTwdfd/uU9KaTrR0G', NULL, NULL, 'Regional Director - Americas', 'ZEAMARINE', 'ZEAMARINE.COM', '365 Canal St, New Orleans, LA 70130, USA', '28.629214', '77.391662', 'iOS', 'A9DD00F36256314F2F7FD02CF5E452F54057B9DC9E64E70685A8B204B42E52C4', '+15043431854', 1, 1, 1, '2019-11-20 02:11:08', '2019-11-20 02:23:52'),
(78, 'Anthony Croley', 'anthony.croley@zeamarine.com', NULL, '$2y$10$Jinxdgz.qdP7FHNa.Do.r.LfCPRBaRGOkrxb7umKnXnAYmFD24vPq', NULL, NULL, 'Worldwide Chartering', 'ZEAMARINE', 'ZEAMARINE.com', '1 Water St, Georgetown, Guyana', '28.629214', '77.391662', 'iOS', '1380BBE0A97757FF6371EE45B31C7C60F8668466D8EA97D6A97A4BF0D3152B75', '+18323302208', 1, 1, 1, '2019-11-20 02:51:42', '2019-11-20 02:56:16'),
(79, 'asdf', 'asdf@gmail.com', NULL, '$2y$10$/dwGS4bD2kCX/yHUi6BqjO6khM7Ff58WJ41Py9AYK0kcascmdod7G', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 05:16:47', '2019-11-20 05:16:47'),
(80, 'try', 'trte@ggf.gfgf', NULL, '$2y$10$fBrx5zOuaFGzCTJZhk0/seSY/XM7Dw/BC6UYOHF7pl32IBzlIfazy', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 05:21:12', '2019-11-20 05:21:12'),
(81, 'gfdgfd', 'gfdgfdg@ggd.hfghfg', NULL, '$2y$10$VSMDNJ0DeR75VILtMa9K6erDQr0dESw85G80vzxygfHmOmjsRgbdu', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 05:24:27', '2019-11-20 05:24:27'),
(82, 'ghfhg', 'gfhfh@gfg.fgf', NULL, '$2y$10$hIaWPf0PvnFD/0gWQQSiEecBkN9Pgxepex4a6DtBsg0xA2E49aqbG', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 05:26:01', '2019-11-20 05:26:01'),
(83, 'fhdf', 'fgdgd@dfgh.ggg', NULL, '$2y$10$tFgWIlR21Cw5yTJYNl01.uEQLgH5sctIhHyKur/9hGMkK/jjprKMy', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 05:27:21', '2019-11-20 05:27:21'),
(84, 'draft', 'dfg@fd.gfg', NULL, '$2y$10$JuzH8/iKS1QdJWm5f07d3OA6.bL6q7VTVwjUSno.D1iFxlZFEuxG6', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 05:41:41', '2019-11-20 05:41:41'),
(85, 'gggggggg', 'gggggg@kk.jj', NULL, '$2y$10$.bi7llZgNOLewW6rchZTge/CJ7RwaxJZvq/G4yXYQwJ1LnlLKmoPa', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 06:10:01', '2019-11-20 06:10:01'),
(86, 'ghijklmnop', 'abcdefg@gmail.com', NULL, '$2y$10$rTUVBf6WyHLyjVEKZlINzORtC3pc5OD5tbq2iLQL9dgpTq1bclAim', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 06:15:50', '2019-11-20 06:15:50'),
(87, 'fathers', 'gdg@fdf.fdfdf', NULL, '$2y$10$jeh6BqRKO.HzPBAi.vgYguCF4h7wLvFPi/vqecqf9fzPNuWUD5zXq', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 06:20:07', '2019-11-20 06:20:07'),
(88, 'ttttttttttttt', 't@g.km', NULL, '$2y$10$LwkKEL.Fvu4bs.0cXUichuh8kHFt3NuaZWX8etK.QMvuY2NHNT0.K', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 06:34:18', '2019-11-20 06:34:18'),
(89, 'hjdfhjf', 'sghasgajh@dfd.ffd', NULL, '$2y$10$z5XbJlHFO2//mD8T04crHOZ/IOftqA/RnuiJOZ4a.4yOPAuWRlzoC', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 06:38:18', '2019-11-20 06:38:18'),
(90, 'hdddddd', 'hdddddd@gf.hgfhgfhfgh', NULL, '$2y$10$HFxTwyNds4wEODiNNjlU4.8UCU4I.2.1jEBBCxc8c5mfAcMoL.l2G', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 06:42:01', '2019-11-20 06:42:01'),
(91, 'fdffd', 'wdfrg@hgfh', NULL, '$2y$10$js7aGYXypb5UYttCFxi1WO.BFFn/HQYWo1vkJJTUhNxpOBdmeGbja', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 06:46:00', '2019-11-20 06:46:00'),
(92, 'fgfgfgf', 'fgfgfg@gfgfg.dfd', NULL, '$2y$10$GzY4NdXJwjAL825pOz896eUwv3TEsBfEFjkxDexccrkbpAhSepub2', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 06:48:13', '2019-11-20 06:48:13'),
(93, 'Keron Noble', 'keron.noble@saipem.com', NULL, '$2y$10$04jXnjk4mJebbK9y4kuknOlEYSG2/evRGylu1YOCW06R3pYO6QbzW', NULL, NULL, 'Security/ Logistics Coordinator', 'Saipem Guyana', 'www.saipem.com', 'Camp &, New Market St, Georgetown, Guyana', '28.084', '77.987', 'android', 'eL5E5S5oW1o:APA91bG94BWUVVkRiEYBO5PHKhqHapyCmdH7TFs-7beBGtpzQWQ9NopxUzgQOuTnM0VDwI278qJofYr6OST7AbbQ4HE9KGmye4BxsI3NDKKkLjStlTA-tMO9rWCONbAjlzFCN4lbnFKI', '+18327747652', 1, 1, 1, '2019-11-20 07:33:27', '2019-11-20 07:38:31'),
(94, 'G. Luigi Rota', 'luigi.rota@panampcr.com', NULL, '$2y$10$Zt02bMvk70hFVE0mAo3SqOi4.YCjjiS.HL08nn73en/JAaqK3LCL6', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-20 09:28:10', '2019-11-20 09:28:10'),
(95, 'fathers', 'farheen@gmail.com', NULL, '$2y$10$/.byBckmBtvOVFkC9RA5Yer/PsSVuGB69v45hbrYGSXSRWYIaA8Wm', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 11:36:13', '2019-11-20 11:36:13'),
(96, 'Fran CRUZ', 'francisca.cruz@zeamarine.com', NULL, '$2y$10$KryZ6/7tQ6XyzBMv7uKHLuF7xHck1F2MOH8ljD85oBVoCJvCSwtTq', NULL, 'userimage/1574251154_ProfileImage.jpeg', 'Line Manager Americas', 'ZEAMARINE', 'ZEAMARINE.COM', '365 Canal St, New Orleans, LA 70130, USA', '28.629214', '77.391662', 'iOS', '246074F729461DEB7B3E9C5A3DEF42995FC5E6EB9E3F3E50BB48CA281F59AE20', '+15043392372', 1, 1, 1, '2019-11-20 11:50:09', '2019-11-20 11:59:14'),
(97, 'Marcos Rihan', 'marcos.rihan@gmail.com', NULL, '$2y$10$b/g41XMpyzpA/Z66eYrzzeyQPLjvIeBzjYTj8oKoAcwmX.sFC7pUO', NULL, NULL, 'Sales Manager', 'NEUMAN & ESSER', 'www.neuman-esser.com', 'R. Gabriela de Melo, 401 - Olhos D\'Água, Belo Horizonte - MG, 30390-080, Brasil', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '+5531997633665', 1, 1, 1, '2019-11-20 12:56:12', '2019-11-20 13:02:51'),
(98, 'fsfdsf', 'qwerty@gmail.com', NULL, '$2y$10$4YQ7mUlqhE6937NBcYEOP.NS6YZAYRH1456NHEyz.xAsEnkVTXDl6', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 13:19:19', '2019-11-20 13:19:19'),
(99, 'rrrrrrrrr', 'qwertyu@gmail.com', NULL, '$2y$10$tIICFWe0KAtx.olG05Wb7.JrhNcodqyrptnSG9fBvtYH3VD0EcPJu', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 13:22:30', '2019-11-20 13:22:30'),
(100, 'fdfdfsdf', 'poiuyt@hjkl.com', NULL, '$2y$10$h74ort6D4KQEQ.GM3WZR8.FXSIKtBF8qGHYCWevF1ZsuZj6sawT2i', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 13:26:05', '2019-11-20 13:26:05'),
(101, 'fdfdf', 'qwer@gmail.com', NULL, '$2y$10$A9oLTLe4RQhbgZDpUSUS2ep1jkLQ8fEdZvCeVTGFWchMR55FsbgZe', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 13:28:37', '2019-11-20 13:28:37'),
(102, 'fdfdsf', 'qwert@gg.kk', NULL, '$2y$10$gAt/SB2J86QoZTSC4CwNde5/RoY.sW./SdShLeTb.k8te/S6PYhVa', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-20 13:31:05', '2019-11-20 13:31:05'),
(103, 'Devon Mahabir', 'devonmahabir@gmail.com', NULL, '$2y$10$/DDXAhTtB/KZjz3RRH4od.hAWCblgNhiLVyJJmFLMqMbYwjj7Og0e', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-20 13:31:41', '2019-11-20 13:31:41'),
(104, 'Damir Tomicic', 'damir.tomicic@airswift.com', NULL, '$2y$10$rIM37aeMiXa7N6SxxQ36.uS8WbeyZuK7KuBL1pcAfQF04gdVH1aI6', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', 'A3CA6E8AE038791173B9C6C2815D0D0FFAF20C76C43D6822E46ED4CA2BE6451C', NULL, 1, 1, 1, '2019-11-20 13:40:27', '2019-11-20 13:40:27'),
(105, 'Oscar Bernal', 'obernal@summumcorp.com', NULL, '$2y$10$OJ1s6fqvj9n7eBpdJY4W8O3tQmZk8RgVga6U4hDVBjYMdtH6i4SY.', NULL, NULL, 'Business Development Manager', 'Summum', 'www.summumcorp.com', 'Block Alpha, Battery Road, Kingston, Georgetown, Guyana', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '+573153427397', 1, 1, 1, '2019-11-20 13:47:20', '2019-11-20 14:50:14'),
(106, 'Rawattie Mohandeo', 'rawattie.mohandeo@gbtibank.com', NULL, '$2y$10$vWD9KILtFUIA1AjMO92vjuIjI0Nj9cGRlvUtxP0UEKjyZ5tQ/bCTS', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-20 14:49:21', '2019-11-20 14:49:21'),
(107, 'Jack Swift', 'mrjackswift@hotmail.com', NULL, '$2y$10$9NzQRmWHDCX466vaXSpBi.3RWAEqZNItZ8O5IxbRQPoEfS6XyDI4K', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-20 15:07:21', '2019-11-20 15:07:21'),
(108, 'Adnan Hassan', 'adnanbhat52@gmail.com', NULL, '$2y$10$B2W683EojDzMBzwUy8eTyOurRBl7Bdd/.vxJRyTK6WPG4SYSja8Ie', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-20 15:14:48', '2019-11-20 15:14:48'),
(109, 'Arpit Anand', 'ashishgaurav13.kngd@gmail.com', NULL, '$2y$10$VQ0Pz6y.X6Nxr5aKWrymLuMiQTXieXG9hk0VvFwIg1GM/SLrEkCwi', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', '', NULL, 1, 1, 1, '2019-11-20 15:46:11', '2019-11-22 07:24:17'),
(110, 'Macdonald Wento', 'macdonaldwento@yahoo.com', NULL, '$2y$10$.zFkxnossE7L7eP7zNZWOuesEJaNdWGuAh1w6Pljd4UrXVQQXrbtW', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-20 15:50:11', '2019-11-20 15:50:11'),
(111, 'Faheem Wani', 'faheem@valiantbmedia.com', NULL, '$2y$10$QGGBajYMdRrissDkPGclFetBlcxr1dr/0rx/BtesL6GczqQUjctOy', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-20 15:55:49', '2019-11-20 15:55:49'),
(112, 'Asmita Kissoon', 'asmita.kissoon@relocationguyana.com', NULL, '$2y$10$N.f3Hi8dd13okBEYt2XA4edz5sNbjyAw78SR3fj8vLyfE3ns7fS4y', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', 'EE2DCCC9E8CA5076ADB3C3AB9BCC795ED61F274A918C8AEE5C5A02705D47FDFE', NULL, 1, 1, 1, '2019-11-20 17:08:36', '2019-11-20 17:08:36'),
(113, 'Jade Baboolal', 'jade.baboolal@oegoffshore.com', NULL, '$2y$10$ZlblOlh2ONsHpc/4Hcw4a.pwmiGBzm5F5GeHNghOxgpXD19MT.6tC', NULL, NULL, 'Commercial Manager', 'OEG Offshore Limited', 'www.oegoffshore.com', 'Salt Mine Trace, Trinidad and Tobago', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '1 868 305 5621', 1, 1, 1, '2019-11-20 17:22:22', '2019-11-20 17:24:58'),
(114, 'Kelly Chin', 'kellymchin1@gmail.com', NULL, '$2y$10$FwVM.o1kY4F.oRFY0Sk84ur9rMR41R74pVoSQSMVCSGCCNQ35aXK2', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'android', 'eZxBY2X7HN8:APA91bErJPFr-E6AP0Ccd2ltSW1gjCDBW8QF_CH7p9T-umzIteuSO-QjP6--zCvoWnMHDBP-nDLBem1XtsoWRzrMArymxQYGiDjHAPIYQ-3lOj3LiA5SILZXpGrNdQ3pXeLDKwOpGClU', NULL, 1, 1, 1, '2019-11-20 17:35:52', '2019-11-21 01:17:05'),
(115, 'Mark Brand', 'mark.brand@omega.no', NULL, '$2y$10$cVPPpy7M6Ml3hrtls2EGpOMWdnro3b6iGmKoL2bJ1UXikjlpvZ1TO', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', '260E96DF9C1A31349DB9863243CAA9B1047A9367C0D8AD1AFBA12A3CD28C2781', NULL, 1, 1, 1, '2019-11-20 17:39:52', '2019-11-20 17:39:52'),
(116, 'Sameer Baral', 'sameer.baral@stratumreservoir.com', NULL, '$2y$10$dRY61f3dCid1TFIBxWk2pu5JmFw.2Q4zHRUJMupcDFSlnzUKcjLiy', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', 'FC15415E0CEF5825CD476753A9881D18E08F64197A3656E3F03830D912CC2D08', NULL, 1, 1, 1, '2019-11-20 18:14:09', '2019-11-20 18:14:09'),
(117, 'fdsghfdghs', 'farheengauhar@mobulous.com', NULL, '$2y$10$Z7kI/BDiek7yAzhsC7DlcOIohBaKsIy7uqhl9ZRBdSn6RXC4JhQLq', NULL, NULL, 'oil', 'Reliance Pvt. Ltd', 'reliance.com', 'Noida', '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', '9977220044', 1, 1, 1, '2019-11-21 05:09:36', '2019-11-29 10:18:50'),
(118, 'atendra', 'atendra@mobulous.com', NULL, '$2y$10$0jBV7mXA04h6cRLr5if8fuPHaGGof2YjOOboUaVoHb10FMnS4DUd2', NULL, NULL, NULL, NULL, NULL, NULL, '28.084', '77.987', 'ios', 'Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326', NULL, 1, 1, 1, '2019-11-21 05:27:44', '2019-11-21 05:27:44'),
(119, 'Priyanka', 'gaurav.kumar@mobulous.com', NULL, '$2y$10$ApDub6EdSHxfsxQXvwL7V.5PEySGwrWbDqC8QX5P8QfgjulcaxM2C', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', '', NULL, 1, 1, 1, '2019-11-21 07:32:24', '2019-11-21 07:32:30'),
(120, 'pronce', 'abhishek.pathak@mobulous.com', NULL, '$2y$10$Pv8YdVQ50PNWv6WN3I2fReLWTH4oY3tw45bbJF0io52XFRfXqbn2q', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', '', NULL, 1, 1, 1, '2019-11-21 07:35:25', '2019-11-21 07:36:00'),
(121, 'Sonu Kumar', 'sonu@yopmail.com', NULL, '$2y$10$UhIBuKs0eLlN5o.Fws.AseHslj7tv6wliL505ohWsHDVN/69WnHw.', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-21 07:46:23', '2019-11-21 07:46:23'),
(122, 'Paramhans Singh', 'paramhans.singh@mobulous.com', NULL, '$2y$10$SLiTDpkOYK2iqqb/KU2lPeopaJrWBb1ODTvZRQns5cZQZgnBi8F5K', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-21 07:51:54', '2019-11-21 07:51:54'),
(123, 'kom', 'komalbothra26@gmail.com', NULL, '$2y$10$Aff9SLvjS87XNNwLJFCIaOOpGNuQ0AiLiYGKSykzY8G49..gGkEzy', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-22 05:43:21', '2019-11-22 05:43:21'),
(124, 'Alena Trotman', 'alena.trotman@valiantguyana.com', NULL, '$2y$10$hov5fTHfwScI43Dd52jVZOrHa2nFecstq21TNEza/WDGlzvW2nw/m', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', 'CE126CDFFFE3CF4127514F647FD85425308F76E8BC252F7CE2DA3E4741FA4C53', NULL, 1, 1, 1, '2019-11-22 15:29:59', '2019-11-22 15:29:59'),
(125, 'Anil Sharma', 'info@mobulous.com', NULL, '$2y$10$NHNzXbILlqu4kTbWjNTTYeLS4nTDhnhlbNjDscG2qlDvmRm/nq7QC', NULL, NULL, NULL, NULL, NULL, NULL, '28.629214', '77.391662', 'iOS', '826F6787E3D3E1E411C0DB16BAB2961D0B4252AB48213E8985CC1B64F097D920', NULL, 1, 1, 1, '2019-11-26 19:47:48', '2019-11-26 19:47:48'),
(126, 'Sami Ullah', 'Sami@valiantbmedia.com', NULL, '$2y$10$G/nuFhaiNz.7LF3XKTwao.U.2jll6vJnA2gmtkxbBkvKzbMOn2nq2', NULL, NULL, NULL, NULL, NULL, NULL, '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', NULL, 1, 1, 1, '2019-11-29 07:48:50', '2019-11-29 07:48:50'),
(127, 'Naseer Hussain', 'naseer.hussain@valiantbmedia.com', NULL, '$2y$10$LgK1sk0LonN/sGySaxLypeI0bXuKosaAzsPA2/eaRbbF6RwmH6kx.', NULL, NULL, 'Project Manager', 'Valiant Business Media', 'www.europeanminingconvention.com', '1956, Rajpath Area, Malka Ganj, New Delhi, Delhi 110011, India', '28.0444', '77.035', 'android', 'sdjhsjdhsdjcjksdcjscjsda', '+91 7838552238', 1, 1, 1, '2019-11-29 10:05:58', '2019-11-29 10:13:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_questionnares`
--

CREATE TABLE `user_questionnares` (
  `id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `question_id` int(11) NOT NULL,
  `selected` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_questionnares`
--

INSERT INTO `user_questionnares` (`id`, `event_id`, `user_id`, `question_id`, `selected`, `created_at`, `updated_at`) VALUES
(1, 1, 19, 1, '2', '2019-11-13 05:38:32', '2019-11-13 05:38:32'),
(2, 1, 19, 2, '1', '2019-11-13 05:38:32', '2019-11-13 05:38:32'),
(3, 1, 19, 15, '4', '2019-11-13 05:38:32', '2019-11-13 05:38:32'),
(4, 1, 21, 15, '2', '2019-11-13 06:31:20', '2019-11-13 06:31:20'),
(5, 1, 21, 1, '0', '2019-11-13 06:31:20', '2019-11-13 06:31:20'),
(6, 1, 21, 15, '2', '2019-11-13 06:36:51', '2019-11-13 06:36:51'),
(7, 1, 21, 1, '2', '2019-11-13 06:36:51', '2019-11-13 06:36:51'),
(8, 1, 21, 2, '0', '2019-11-13 06:36:51', '2019-11-13 06:36:51'),
(9, 2, 9, 6, '3', '2019-11-19 09:17:45', '2019-11-19 09:17:45'),
(10, 2, 9, 7, '1', '2019-11-19 09:17:45', '2019-11-19 09:17:45'),
(11, 2, 9, 28, '1', '2019-11-19 09:17:45', '2019-11-19 09:17:45'),
(12, 10, 66, 23, '1', '2019-11-20 22:15:38', '2019-11-20 22:15:38'),
(13, 10, 66, 24, '2', '2019-11-20 22:15:38', '2019-11-20 22:15:38'),
(14, 10, 66, 25, '1', '2019-11-20 22:15:38', '2019-11-20 22:15:38'),
(15, 10, 66, 26, '1', '2019-11-20 22:15:38', '2019-11-20 22:15:38'),
(16, 10, 66, 27, '1', '2019-11-20 22:15:38', '2019-11-20 22:15:38'),
(17, 10, 41, 25, '0', '2019-11-21 07:47:01', '2019-11-21 07:47:01'),
(18, 10, 41, 24, '0', '2019-11-21 07:47:01', '2019-11-21 07:47:01'),
(19, 10, 41, 23, '0', '2019-11-21 07:47:01', '2019-11-21 07:47:01'),
(20, 10, 41, 26, '0', '2019-11-21 07:47:01', '2019-11-21 07:47:01'),
(21, 10, 41, 27, '2', '2019-11-21 07:47:01', '2019-11-21 07:47:01'),
(22, 10, 41, 23, '1', '2019-11-24 13:55:13', '2019-11-24 13:55:13'),
(23, 10, 41, 24, '1', '2019-11-24 13:55:13', '2019-11-24 13:55:13'),
(24, 10, 41, 25, '2', '2019-11-24 13:55:13', '2019-11-24 13:55:13'),
(25, 10, 41, 26, '0', '2019-11-24 13:55:13', '2019-11-24 13:55:13'),
(26, 10, 41, 27, '0', '2019-11-24 13:55:13', '2019-11-24 13:55:13'),
(27, 10, 61, 23, '3', '2019-11-24 20:21:59', '2019-11-24 20:21:59'),
(28, 10, 61, 24, '2', '2019-11-24 20:21:59', '2019-11-24 20:21:59'),
(29, 10, 61, 25, '2', '2019-11-24 20:21:59', '2019-11-24 20:21:59'),
(30, 10, 61, 26, '1', '2019-11-24 20:21:59', '2019-11-24 20:21:59'),
(31, 10, 61, 27, '2', '2019-11-24 20:21:59', '2019-11-24 20:21:59'),
(32, 12, 21, 29, '1', '2019-11-25 09:06:41', '2019-11-25 09:06:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookmarks`
--
ALTER TABLE `bookmarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_room`
--
ALTER TABLE `chat_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_verifications`
--
ALTER TABLE `email_verifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_details`
--
ALTER TABLE `event_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exhibitors`
--
ALTER TABLE `exhibitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `floor_plans`
--
ALTER TABLE `floor_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `join_events`
--
ALTER TABLE `join_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `password_verifications`
--
ALTER TABLE `password_verifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questionnares`
--
ALTER TABLE `questionnares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `speakers`
--
ALTER TABLE `speakers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsers`
--
ALTER TABLE `sponsers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staticpages`
--
ALTER TABLE `staticpages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_questionnares`
--
ALTER TABLE `user_questionnares`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `bookmarks`
--
ALTER TABLE `bookmarks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=729;

--
-- AUTO_INCREMENT for table `chat_room`
--
ALTER TABLE `chat_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `email_verifications`
--
ALTER TABLE `email_verifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `event_details`
--
ALTER TABLE `event_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `exhibitors`
--
ALTER TABLE `exhibitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;

--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `floor_plans`
--
ALTER TABLE `floor_plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `join_events`
--
ALTER TABLE `join_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `password_verifications`
--
ALTER TABLE `password_verifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `questionnares`
--
ALTER TABLE `questionnares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234;

--
-- AUTO_INCREMENT for table `speakers`
--
ALTER TABLE `speakers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `sponsers`
--
ALTER TABLE `sponsers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `staticpages`
--
ALTER TABLE `staticpages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `user_questionnares`
--
ALTER TABLE `user_questionnares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
